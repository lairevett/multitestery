# MultiTestery

Perfume store written in React (w/ Node Server-Side-Rendering) and Django.

## Preview

#### Homepage

![homepage preview](previews/homepage.png)

#### Sign in

![sign in preview](previews/sign_in.png)

#### Perfume details

![perfume details preview](previews/perfume_details.png)

#### Perfume details (delete action)

![perfume details delete action preview](previews/perfume_details_admin_action_delete.png)

#### Cart

![cart preview](previews/cart.png)

#### Checkout

![checkout preview](previews/checkout.png)

#### Order

![order preview](previews/order.png)

#### Order in admin panel

![order in admin panel preview](previews/admin_panel_order.png)
