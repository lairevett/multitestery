# MultiTestery

## Introduction

-   **Name:** MultiTestery
-   **Website:** ~~[www.multitestery.pl](https://www.multitestery.pl/)~~
-   **Description:** Frontend for [the perfume store API](https://www.bitbucket.org/lairevett/multitestery/src/master/api/) written in React.
-   **Additional info:**

    -   [Repository with config files](https://www.bitbucket.org/lairevett/multitestery/src/master/cfg/).

## Requirements

-   Vagrant box w/ FreeBSD 12.1
-   NGINX
-   Axios
-   Bootstrap + JQuery
-   React + React Router
-   Redux
-   [...](https://www.bitbucket.org/lairevett/multitestery/src/master/webapp/package.json)
