module.exports = api => {
    api.cache(true);

    return {
        compact: true,
        presets: [
            [
                '@babel/env',
                {
                    modules: 'auto',
                    targets: {
                        node: 'current',
                    },
                },
            ],
            '@babel/react',
        ],
        plugins: [
            '@babel/proposal-object-rest-spread',
            '@babel/proposal-class-properties',
            '@babel/proposal-optional-chaining',
            '@babel/syntax-dynamic-import',
            '@babel/proposal-nullish-coalescing-operator',
            'macros',
            [
                'css-modules-transform',
                {
                    camelCase: true,
                    extensions: ['.css'],
                },
            ],
            'dynamic-import-node',
            [
                'module-resolver',
                {
                    root: ['./src/'],
                },
            ],
        ],
    };
};
