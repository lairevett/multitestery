import React from 'react';
import {Switch, Route} from 'react-router-dom';
import {convertTitleToUrlFriendlyString, makeTitle} from '../../src/perfumes/utils';

export default (products, pages) => {
    const additionalRoutes = [];

    const productsPages = Math.ceil(products.length / 25);
    for (let i = 0; i < productsPages; i++) {
        additionalRoutes.push(<Route key={i} exact path={`/perfumes/?page=${i + 1}`} />);
    }

    const productsMalePages = Math.ceil(products.filter(product => product.category === 0).length / 25);
    for (let i = 0; i < productsMalePages; i++) {
        additionalRoutes.push(<Route key={i} exact path={`/perfumes/male/?page=${i + 1}`} />);
    }

    const productsFemalePages = Math.ceil(products.filter(product => product.category === 1).length / 25);
    for (let i = 0; i < productsFemalePages; i++) {
        additionalRoutes.push(<Route key={i} exact path={`/perfumes/female/?page=${i + 1}`} />);
    }

    const productsUnisexPages = Math.ceil(products.filter(product => product.category === 2).length / 25);
    for (let i = 0; i < productsUnisexPages; i++) {
        additionalRoutes.push(<Route key={i} exact path={`/perfumes/unisex/?page=${i + 1}`} />);
    }

    const productsOnSalePages = Math.ceil(products.filter(product => product.percent_off > 0).length / 25);
    for (let i = 0; i < productsOnSalePages; i++) {
        additionalRoutes.push(<Route key={i} exact path={`/perfumes/on-sale/?page=${i + 1}`} />);
    }

    return (
        <Switch>
            <Route exact path="/sign-up" />
            <Route exact path="/sign-in" />
            <Route exact path="/perfumes" />
            <Route exact path="/perfumes/male" />
            <Route exact path="/perfumes/female" />
            <Route exact path="/perfumes/unisex" />
            <Route exact path="/perfumes/on-sale" />
            {additionalRoutes}
            {products.map(({id, brand, model, amount}) => (
                <Route
                    key={`_${id}`}
                    exact
                    path={`/perfumes/details/${id}/${convertTitleToUrlFriendlyString(makeTitle(brand, model, amount))}`}
                />
            ))}
            {pages.map(({id, slug}) => (
                <Route key={`_${id}`} exact path={`/${slug}`} />
            ))}
        </Switch>
    );
};
