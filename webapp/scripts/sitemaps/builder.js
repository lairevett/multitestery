require('../../babel_loader');

const axios = require('axios');
const SiteMap = require('react-router-sitemap').default;
const Routes = require('./routes').default;
const {API_URL} = require('../../src/app/utils/api/constants');

const generateSiteMap = async (hostname, filename) => {
    const productsResponse = await axios.get(`${API_URL}/v1/products/all/`);
    const pagesResponse = await axios.get(`${API_URL}/v1/pages/`);

    return new SiteMap(Routes(productsResponse.data.results, pagesResponse.data.results))
        .build(hostname)
        .save(`./public/${filename}`);
};

generateSiteMap(API_URL.replace('api.', ''), 'sitemap.xml')
    .then(() => console.log('SiteMap file generated successfully.'))
    .catch(error => console.error(error));
