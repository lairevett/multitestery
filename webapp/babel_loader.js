require('@babel/register')({
    extensions: ['.js', '.jsx'],
});

require('@babel/polyfill');

const objectFromEntriesPolyfill = require('object.fromentries');
const allSettledPolyfill = require('promise.allsettled');

if (typeof Object.fromEntries === 'undefined') Object.fromEntries = objectFromEntriesPolyfill;
if (typeof Promise.allSettled === 'undefined') Promise.allSettled = allSettledPolyfill;
