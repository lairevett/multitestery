import {ACTION, INITIAL_STATE} from './constants';

const perfumeReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ACTION.RETRIEVE_FEATURED_LIST:
        case ACTION.RETRIEVE_MALE_LIST:
        case ACTION.RETRIEVE_FEMALE_LIST:
        case ACTION.RETRIEVE_UNISEX_LIST:
        case ACTION.RETRIEVE_SEARCHED_LIST:
        case ACTION.RETRIEVE_ALL_LIST:
        case ACTION.RETRIEVE_ON_SALE_LIST:
            return {
                ...state,
                list: action.payload,
            };
        case ACTION.CLEAR_LIST:
            return {
                ...state,
                list: INITIAL_STATE.list,
            };
        case ACTION.RETRIEVE_DETAILS:
            return {
                ...state,
                details: action.payload,
            };
        case ACTION.CLEAR_DETAILS:
            return {
                ...state,
                details: INITIAL_STATE.details,
            };
        case ACTION.DESTROY:
            return {
                ...state,
                // Let views know that destroy action from perfume details page was emitted.
                destroySwitcher: !state.destroySwitcher,
            };
        default:
            return state;
    }
};

export default perfumeReducer;
