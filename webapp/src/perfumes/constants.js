import {DEFAULT_LIST_INITIAL_STATE, DEFAULT_STATUS_INITIAL_STATE} from '../app/utils/api/constants';

// API endpoints.
export const ENDPOINT = {
    LIST_FEATURED: '/v1/products/?featured=1',
    LIST_MALE: page => `/v1/products/?category=0&page=${page}`,
    LIST_FEMALE: page => `/v1/products/?category=1&page=${page}`,
    LIST_UNISEX: page => `/v1/products/?category=2&page=${page}`,
    LIST_SEARCHED: (keyword, page) => `/v1/products/?search=${keyword}&page=${page}`,
    LIST_ALL: page => `/v1/products/?page=${page}`,
    LIST_ON_SALE: page => `/v1/products/?on_sale=1&page=${page}`,
    DETAILS: productId => `/v1/products/${productId}/`,
};

// Redux action types.
export const ACTION = {
    RETRIEVE_FEATURED_LIST: '@perfume/RETRIEVE_FEATURED_LIST',
    RETRIEVE_MALE_LIST: '@perfume/RETRIEVE_MALE_LIST',
    RETRIEVE_FEMALE_LIST: '@perfume/RETRIEVE_FEMALE_LIST',
    RETRIEVE_UNISEX_LIST: '@perfume/RETRIEVE_UNISEX_LIST',
    RETRIEVE_SEARCHED_LIST: '@perfume/RETRIEVE_SEARCHED_LIST',
    RETRIEVE_ALL_LIST: '@perfume/RETRIEVE_ALL_LIST',
    RETRIEVE_ON_SALE_LIST: '@perfume/RETRIEVE_ON_SALE_LIST',
    CLEAR_LIST: '@perfume/CLEAR_LIST',
    RETRIEVE_DETAILS: '@perfume/RETRIEVE_DETAILS',
    CLEAR_DETAILS: '@perfume/CLEAR_DETAILS',
    DESTROY: '@perfume/DESTROY', // Required for perfumes details page.
};

// Redux form names.
export const FORM = {
    SEARCH: 'FORM_SEARCH',
    ADD_PRODUCT_TO_CART: 'FORM_ADD_PRODUCT_TO_CART',
};

// Reducer initial states.
export const LIST_INITIAL_STATE = {
    ...DEFAULT_LIST_INITIAL_STATE,
    ...DEFAULT_STATUS_INITIAL_STATE,
};

export const DETAILS_INITIAL_STATE = {
    id: 0,
    photo_small: '',
    photo_medium: '',
    photo_big: '',
    brand: '',
    model: '',
    category: 0,
    unit_price: 0,
    amount: 0,
    quantity: 0,
    description: '',
    top_notes: '',
    heart_notes: '',
    base_notes: '',
    percent_off: 0,
    is_featured: false,
    is_deleted: false,
    ...DEFAULT_STATUS_INITIAL_STATE,
};

export const INITIAL_STATE = {
    list: LIST_INITIAL_STATE,
    details: DETAILS_INITIAL_STATE,
    // This works as an event emitter when user uses destroy perfume action from details page.
    destroySwitcher: false,
};

// Prop types.
export const LIST_PROP_TYPES = PropertyTypes => PropertyTypes.arrayOf(DETAILS_PROP_TYPES(PropertyTypes));

export const DETAILS_PROP_TYPES = PropertyTypes =>
    PropertyTypes.exact({
        id: PropertyTypes.number.isRequired,
        photo_small: PropertyTypes.string.isRequired,
        photo_medium: PropertyTypes.string.isRequired,
        photo_big: PropertyTypes.string.isRequired,
        brand: PropertyTypes.string.isRequired,
        model: PropertyTypes.string.isRequired,
        category: PropertyTypes.number.isRequired,
        unit_price: PropertyTypes.number.isRequired,
        amount: PropertyTypes.number.isRequired,
        quantity: PropertyTypes.number.isRequired,
        description: PropertyTypes.string.isRequired,
        top_notes: PropertyTypes.string.isRequired,
        heart_notes: PropertyTypes.string.isRequired,
        base_notes: PropertyTypes.string.isRequired,
        percent_off: PropertyTypes.number.isRequired,
        is_featured: PropertyTypes.bool.isRequired,
        is_deleted: PropertyTypes.bool.isRequired,
        __meta__: PropertyTypes.exact({status: PropertyTypes.string.isRequired}),
    });

// Template modes.
export const MODE = {
    LIST_MALE: 'MODE_LIST_MALE',
    LIST_FEMALE: 'MODE_LIST_FEMALE',
    LIST_UNISEX: 'MODE_LIST_UNISEX',
    LIST_SEARCHED: 'MODE_LIST_SEARCHED',
    LIST_ALL: 'MODE_LIST_ALL',
    LIST_ON_SALE: 'MODE_LIST_ON_SALE',
    DETAILS: 'MODE_DETAILS',
};

// Modal ids.
export const MODAL_DESTROY_PERFUME = 'destroy-perfume';
