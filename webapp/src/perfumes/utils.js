import {useEffect} from 'react';
import {useHistory} from 'react-router';
import {useSelector} from 'react-redux';
import {
    clearDetails,
    clearList,
    retrieveAllList,
    retrieveDetails,
    retrieveFeaturedList,
    retrieveFemaleList,
    retrieveMaleList,
    retrieveOnSaleList,
    retrieveSearchedList,
    retrieveUnisexList,
} from './actions';

import {MODE} from './constants';
import {makeDispatchEvents} from '../app/utils/dispatch_events';
import {STATUS} from '../app/utils/api/constants';

export const getPrice = (unitPrice, percentOff) => {
    const price = percentOff ? Math.ceil(unitPrice - unitPrice * (percentOff / 100)) : unitPrice;
    return price / 100;
};

export const makeTitle = (brand, model, amount) => `${brand} ${model} ${amount}ml`;

export const convertTitleToUrlFriendlyString = title =>
    encodeURIComponent(
        title
            .toLowerCase()
            .replace(/ /g, '-')
            .replace('&', 'and')
    );

export const callbackGetDispatchEvents = urlProperties => (urlParameters, queryParameters) => {
    const productId = +urlParameters.productId;
    const page = +queryParameters.page || 1;
    const keyword = queryParameters.keyword ?? '';

    switch (urlProperties.mode) {
        case MODE.LIST_FEATURED:
            return makeDispatchEvents([retrieveFeaturedList], [clearList]);
        case MODE.LIST_MALE:
            return makeDispatchEvents([() => retrieveMaleList(page)], [clearList], [page]);
        case MODE.LIST_FEMALE:
            return makeDispatchEvents([() => retrieveFemaleList(page)], [clearList], [page]);
        case MODE.LIST_UNISEX:
            return makeDispatchEvents([() => retrieveUnisexList(page)], [clearList], [page]);
        case MODE.LIST_SEARCHED:
            return makeDispatchEvents([() => retrieveSearchedList(keyword, page)], [clearList], [keyword, page]);
        case MODE.LIST_ALL:
            return makeDispatchEvents([() => retrieveAllList(page)], [clearList], [page]);
        case MODE.LIST_ON_SALE:
            return makeDispatchEvents([() => retrieveOnSaleList(page)], [clearList], [page]);
        case MODE.DETAILS:
            return makeDispatchEvents([() => retrieveDetails(productId)], [clearDetails], [productId]);
        default:
            throw new Error(`Unexpected mode "${urlProperties.mode}" provided in perfumes callbackGetDispatchEvents.`);
    }
};

export const useRedirectWhenLastOnPageDestroyed = (queryParameters, status) => {
    // This hook makes sure that when user destroys the last product in the list of page,
    // he doesn't get redirected back on non-existent page number, instead he gets redirected to
    // the last page number - 1.

    const history = useHistory();
    const {destroySwitcher} = useSelector(state => state.perfume);

    useEffect(() => {
        if (process.env.__CLIENT__ && +queryParameters.page > 1 && status === STATUS.RESPONSE_NOT_FOUND) {
            const {pathname} = window.location;
            const nextQueryParameters = {...queryParameters, page: queryParameters.page - 1};

            (async () => {
                const queryString = await import('query-string');
                history.replace(
                    nextQueryParameters,
                    null, // Current browsers don't support this option anyway.
                    `${pathname}?${queryString.stringify(nextQueryParameters)}`
                );
            })();
        }
    }, [destroySwitcher, queryParameters, status, history]);
};
