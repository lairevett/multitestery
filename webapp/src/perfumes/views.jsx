/* eslint-disable react/prop-types */
import React from 'react';

import Content from '../app/templates/__partials__/content';
import PerfumesDetails from './templates/details';
import PerfumesList from './templates/list';
import {submitServer as submitServerAddProductToCart} from './forms/process_add_product_to_cart';
import Loading from '../app/templates/__partials__/loading';
import Suspense from '../app/utils/api/suspense';
import {useRedirectWhenLastOnPageDestroyed} from './utils';

/*
 * GET /perfumes/all[?page={page}]
 * GET /perfumes/male[?page={page}]
 * GET /perfumes/female[?page={page}]
 * GET /perfumes/unisex[?page={page}]
 * GET /perfumes/search?keyword={keyword}[&page={page}]
 * GET /perfumes/on-sale[?page={page}]
 */
export const PerfumesListView = ({title, match: {path}, queryParameters, state}) => {
    const {
        previous,
        next,
        results,
        __meta__: {status},
    } = state.perfume.list;

    useRedirectWhenLastOnPageDestroyed(queryParameters, status);

    return (
        <Content status={status} title={title}>
            <Suspense status={status} fallback={<Loading />}>
                <PerfumesList list={results} paginationData={{previous, next, path, queryParameters}} />
            </Suspense>
        </Content>
    );
};

/*
 * GET /perfumes/all/:productId/:productStringForSEO?
 * POST /perfumes/all/:productId/:productStringForSEO?add_product_to_cart
 */
export const PerfumesDetailsView = ({state}) => {
    const isUserStaff = state.auth.user.is_staff;

    const {details} = state.perfume;
    const {
        brand,
        model,
        __meta__: {status},
    } = details;

    // Suspense is on top of content here because of dynamic title.
    return (
        <Suspense status={status} fallback={<Loading />}>
            <Content status={status} title={`${brand} ${model}`}>
                <PerfumesDetails details={details} isUserStaff={isUserStaff} />
            </Content>
        </Suspense>
    );
};

PerfumesDetailsView.willDispatchEvents = (request, response) => {
    if (request.method === 'POST' && 'add_product_to_cart' in request.query) {
        return submitServerAddProductToCart(request, response);
    }
};
