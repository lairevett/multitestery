import {ACCESS, registerUrl} from '../app/utils/router';
import {PerfumesDetailsView, PerfumesListView} from './views';

import {MODE} from './constants';
import {callbackGetDispatchEvents} from './utils';

registerUrl('perfumesAllList', '/perfumes/all', ['get'], PerfumesListView, ACCESS.ALLOW_ANY, {
    title: 'Wszystkie produkty',
    mode: MODE.LIST_ALL,
    callbackGetDispatchEvents,
});

registerUrl('perfumesMaleList', '/perfumes/male', ['get'], PerfumesListView, ACCESS.ALLOW_ANY, {
    title: 'Perfumy męskie',
    mode: MODE.LIST_MALE,
    callbackGetDispatchEvents,
});

registerUrl('perfumesFemaleList', '/perfumes/female', ['get'], PerfumesListView, ACCESS.ALLOW_ANY, {
    title: 'Perfumy damskie',
    mode: MODE.LIST_FEMALE,
    callbackGetDispatchEvents,
});

registerUrl('perfumesUnisexList', '/perfumes/unisex', ['get'], PerfumesListView, ACCESS.ALLOW_ANY, {
    title: 'Perfumy unisex',
    mode: MODE.LIST_UNISEX,
    callbackGetDispatchEvents,
});

registerUrl('perfumesSearchedList', '/perfumes/search', ['get'], PerfumesListView, ACCESS.ALLOW_ANY, {
    title: 'Szukaj',
    mode: MODE.LIST_SEARCHED,
    callbackGetDispatchEvents,
});

registerUrl('perfumesOnSaleList', '/perfumes/on-sale', ['get'], PerfumesListView, ACCESS.ALLOW_ANY, {
    title: 'Aktualne promocje',
    mode: MODE.LIST_ON_SALE,
    callbackGetDispatchEvents,
});

registerUrl(
    'perfumesDetails',
    '/perfumes/all/:productId(\\d+)/:productStringForSEO?',
    ['get', 'post'],
    PerfumesDetailsView,
    ACCESS.ALLOW_ANY,
    {title: 'Szczegóły produktu', mode: MODE.DETAILS, callbackGetDispatchEvents}
);
