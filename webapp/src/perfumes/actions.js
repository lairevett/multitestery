import {ACTION, DETAILS_INITIAL_STATE, ENDPOINT, LIST_INITIAL_STATE} from './constants';
import {apiDeleteRequest, apiGetListRequest, apiGetRequest} from '../app/utils/api/requests';

export const retrieveFeaturedList = () =>
    apiGetListRequest(ACTION.RETRIEVE_FEATURED_LIST, LIST_INITIAL_STATE, ENDPOINT.LIST_FEATURED);

export const retrieveMaleList = page =>
    apiGetListRequest(ACTION.RETRIEVE_MALE_LIST, LIST_INITIAL_STATE, ENDPOINT.LIST_MALE(page));

export const retrieveFemaleList = page =>
    apiGetListRequest(ACTION.RETRIEVE_FEMALE_LIST, LIST_INITIAL_STATE, ENDPOINT.LIST_FEMALE(page));

export const retrieveUnisexList = page =>
    apiGetListRequest(ACTION.RETRIEVE_UNISEX_LIST, LIST_INITIAL_STATE, ENDPOINT.LIST_UNISEX(page));

export const retrieveSearchedList = (keyword, page) =>
    apiGetListRequest(ACTION.RETRIEVE_SEARCHED_LIST, LIST_INITIAL_STATE, ENDPOINT.LIST_SEARCHED(keyword, page));

export const retrieveAllList = page =>
    apiGetListRequest(ACTION.RETRIEVE_ALL_LIST, LIST_INITIAL_STATE, ENDPOINT.LIST_ALL(page));

export const retrieveOnSaleList = page =>
    apiGetListRequest(ACTION.RETRIEVE_ON_SALE_LIST, LIST_INITIAL_STATE, ENDPOINT.LIST_ON_SALE(page));

export const clearList = () => ({type: ACTION.CLEAR_LIST});

export const retrieveDetails = productId =>
    apiGetRequest(ACTION.RETRIEVE_DETAILS, DETAILS_INITIAL_STATE, ENDPOINT.DETAILS(productId));

export const clearDetails = () => ({type: ACTION.CLEAR_DETAILS});

// Required for perfumes details admin actions page.
export const destroy = productId => apiDeleteRequest(ACTION.DESTROY, {}, ENDPOINT.DETAILS(productId), {id: productId});
