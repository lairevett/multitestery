import React from 'react';

import PropTypes from 'prop-types';
import Card from './__partials__/card';
import Cards from './__partials__/cards';
import {LIST_PROP_TYPES} from '../constants';
import {PAGINATION_DATA_PROP_TYPES} from '../../app/constants';

const PerfumesList = ({list, paginationData}) => (
    <Cards paginationData={paginationData}>
        {list.map(details => (
            <Card key={details.id} details={details} />
        ))}
    </Cards>
);

PerfumesList.propTypes = {
    list: LIST_PROP_TYPES(PropTypes).isRequired,
    paginationData: PAGINATION_DATA_PROP_TYPES(PropTypes),
};

PerfumesList.defaultProps = {
    paginationData: undefined,
};

export default PerfumesList;
