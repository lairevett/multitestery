import React from 'react';

import PropTypes from 'prop-types';
import DetailsTableAdminActionsRow from './details_table_admin_actions_row';
import DetailsTablePriceRow from './details_table_price_row';
import DetailsTableStatusRow from './details_table_status_row';

const DetailsTable = ({id, brand, model, category, amount, quantity, unitPrice, percentOff, isUserStaff}) => (
    <table className="table mb-5">
        <tbody>
            <tr>
                <td>Marka</td>
                <td className="text-right">{brand}</td>
            </tr>
            <tr>
                <td>Model</td>
                <td className="text-right">{model}</td>
            </tr>
            <tr>
                <td>Zapach dla</td>
                <td className="text-right">
                    {category === 0 ? 'Mężczyzn' : category === 1 ? 'Kobiet' : 'Uniwersalny'}
                </td>
            </tr>
            <tr>
                <td>Pojemność</td>
                <td className="text-right">{amount}ml</td>
            </tr>
            <DetailsTablePriceRow unitPrice={unitPrice} percentOff={percentOff} />
            <DetailsTableStatusRow quantity={quantity} />
            <DetailsTableAdminActionsRow
                id={id}
                brand={brand}
                model={model}
                amount={amount}
                isUserStaff={isUserStaff}
            />
        </tbody>
    </table>
);

DetailsTable.propTypes = {
    id: PropTypes.number.isRequired,
    brand: PropTypes.string.isRequired,
    model: PropTypes.string.isRequired,
    category: PropTypes.number.isRequired,
    amount: PropTypes.number.isRequired,
    quantity: PropTypes.number.isRequired,
    unitPrice: PropTypes.number.isRequired,
    percentOff: PropTypes.number.isRequired,
    isUserStaff: PropTypes.bool.isRequired,
};

export default DetailsTable;
