import React from 'react';
import PropTypes from 'prop-types';

const DetailsTableStatusRow = ({quantity}) => (
    <tr>
        <td>Stan</td>
        <td className="text-right">
            {quantity > 0 ? (
                <>
                    <span className="available">Dostępny</span> {`(${quantity} na magazynie)`}
                </>
            ) : (
                <span className="unavailable">Niedostępny</span>
            )}
        </td>
    </tr>
);

DetailsTableStatusRow.propTypes = {
    quantity: PropTypes.number.isRequired,
};

export default DetailsTableStatusRow;
