import PropTypes from 'prop-types';
import React from 'react';
import {getPrice} from '../../utils';
import {CURRENCY_FORMATTER} from '../../../app/constants';

const CardBody = ({title, unitPrice, quantity, percentOff}) => {
    const formattedPrice = CURRENCY_FORMATTER.format(getPrice(unitPrice, percentOff));

    return (
        <div className="card-body perfume">
            <h6 className="card-title">{title}</h6>
            Cena:{' '}
            {!!percentOff ? (
                <>
                    <del className="discount-old-price">{CURRENCY_FORMATTER.format(getPrice(unitPrice, 0))}</del>{' '}
                    <ins className="discount-new-price">{formattedPrice}</ins>
                </>
            ) : (
                formattedPrice
            )}
            {quantity > 0 ? <p className="available">Dostępny</p> : <p className="unavailable">Niedostępny</p>}
        </div>
    );
};

CardBody.propTypes = {
    title: PropTypes.string.isRequired,
    unitPrice: PropTypes.number.isRequired,
    quantity: PropTypes.number.isRequired,
    percentOff: PropTypes.number.isRequired,
};

export default CardBody;
