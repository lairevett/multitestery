import React from 'react';

import FadeIn from 'react-lazyload-fadein';
import ImageMagnify from 'react-image-magnify';
import PropTypes from 'prop-types';

const DetailsImg = ({photoMedium, photoBig, brand, model}) => {
    const imgAlt = `Tester ${brand} ${model}`;

    return (
        <>
            <FadeIn duration={256} once>
                {onload => (
                    <ImageMagnify
                        imageStyle={{border: '1px solid #dee2e6'}}
                        enlargedImageContainerStyle={{zIndex: 1, border: '1px solid #dee2e6'}}
                        enlargedImagePosition="over"
                        smallImage={{
                            src: photoMedium,
                            alt: imgAlt,
                            isFluidWidth: true,
                            onLoad: onload,
                        }}
                        largeImage={{src: photoBig, width: 1024, height: 1024}}
                    />
                )}
            </FadeIn>
            <noscript>
                <img src={photoMedium} className="w-100 h-auto" style={{border: '1px solid #dee2e6'}} alt={imgAlt} />
            </noscript>
        </>
    );
};

DetailsImg.propTypes = {
    photoMedium: PropTypes.string.isRequired,
    photoBig: PropTypes.string.isRequired,
    brand: PropTypes.string.isRequired,
    model: PropTypes.string.isRequired,
};

export default DetailsImg;
