import React from 'react';
import PropTypes from 'prop-types';
import {getPrice} from '../../utils';
import {CURRENCY_FORMATTER} from '../../../app/constants';

const DetailsTablePriceRow = ({unitPrice, percentOff}) => {
    const formattedPrice = CURRENCY_FORMATTER.format(getPrice(unitPrice, percentOff));

    return (
        <tr>
            <td>Cena</td>
            <td className="text-right">
                {!!percentOff ? (
                    <>
                        <del className="discount-old-price">{CURRENCY_FORMATTER.format(getPrice(unitPrice, 0))}</del>{' '}
                        <ins className="discount-new-price">{formattedPrice}</ins>
                    </>
                ) : (
                    formattedPrice
                )}
            </td>
        </tr>
    );
};

DetailsTablePriceRow.propTypes = {
    unitPrice: PropTypes.number.isRequired,
    percentOff: PropTypes.number.isRequired,
};

export default DetailsTablePriceRow;
