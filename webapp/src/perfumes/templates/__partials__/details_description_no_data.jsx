import React from 'react';

const DetailsDescriptionNoData = () => <span style={{fontStyle: 'italic'}}>Brak informacji.</span>;

export default DetailsDescriptionNoData;
