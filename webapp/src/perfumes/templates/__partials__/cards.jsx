import React from 'react';

import PropTypes from 'prop-types';
import Pagination from '../../../app/templates/__partials__/pagination';
import {PAGINATION_DATA_PROP_TYPES} from '../../../app/constants';

const Cards = ({paginationData, children}) => (
    <>
        <div className="row">{children}</div>
        {paginationData && (
            <div className="d-flex justify-content-center">
                <Pagination data={paginationData} />
            </div>
        )}
    </>
);

Cards.propTypes = {
    paginationData: PAGINATION_DATA_PROP_TYPES(PropTypes),
    children: PropTypes.node.isRequired,
};

Cards.defaultProps = {
    paginationData: undefined,
};

export default Cards;
