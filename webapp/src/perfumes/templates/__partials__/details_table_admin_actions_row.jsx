import React, {useCallback} from 'react';
import {useHistory} from 'react-router';
import {useDispatch} from 'react-redux';

import {reverse} from 'named-urls';
import PropTypes from 'prop-types';
import Modal from '../../../app/templates/__partials__/modal';
import {retrieveUrl} from '../../../app/utils/router';
import {destroy} from '../../actions';
import {makeYesNoModal} from '../../../app/utils/modal';
import {MODAL_DESTROY_PERFUME} from '../../constants';
import DeleteButton from '../../../app/templates/__partials__/delete_button';
import EditButton from '../../../app/templates/__partials__/edit_button';

const DetailsTableAdminActionsRow = ({id, brand, model, amount, isUserStaff}) => {
    const dispatch = useDispatch();
    const history = useHistory();

    const dispatchDestroy = useCallback(async () => {
        dispatch(await destroy(id));
        history.goBack();
    }, [dispatch, history, id]);

    return (
        isUserStaff && (
            <tr>
                <td>Akcje administratora</td>
                <td className="text-right">
                    <EditButton
                        className="btn-sm"
                        href={reverse(retrieveUrl('adminPerfumesUpdate').path.replace('(\\d+)', ''), {
                            productId: id,
                        })}
                    />
                    {/* This component is not used in a loop, so modal can be defined here. */}
                    <Modal id={MODAL_DESTROY_PERFUME} />
                    <DeleteButton
                        className="btn-sm ml-2"
                        onClick={() => {
                            makeYesNoModal(
                                MODAL_DESTROY_PERFUME,
                                'Usuń',
                                `Czy na pewno chcesz usunąć produkt "${brand} ${model} ${amount}ml"?`,
                                dispatchDestroy
                            );
                        }}
                    />
                </td>
            </tr>
        )
    );
};

DetailsTableAdminActionsRow.propTypes = {
    id: PropTypes.number.isRequired,
    brand: PropTypes.string.isRequired,
    model: PropTypes.string.isRequired,
    amount: PropTypes.number.isRequired,
    isUserStaff: PropTypes.bool.isRequired,
};

export default DetailsTableAdminActionsRow;
