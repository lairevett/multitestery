import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import React from 'react';
import {reverse} from 'named-urls';
import {convertTitleToUrlFriendlyString, makeTitle} from '../../utils';
import CardImg from './card_img';
import CardBody from './card_body';
import {DETAILS_PROP_TYPES} from '../../constants';
import {retrieveUrl} from '../../../app/utils/router';

const Card = ({details: {id, photo_small, brand, model, unit_price, amount, quantity, percent_off}}) => {
    const title = makeTitle(brand, model, amount);

    return (
        <Link
            to={reverse(retrieveUrl('perfumesDetails').path.replace('(\\d+)', ''), {
                productId: id,
                productStringForSEO: convertTitleToUrlFriendlyString(title),
            })}
            className="perfume col-xs-12 col-sm-6 col-md-4 col-lg-3 mb-4"
        >
            <div className="card perfume">
                <CardImg title={title} photoSmall={photo_small} />
                <CardBody title={title} unitPrice={unit_price} quantity={quantity} percentOff={percent_off} />
            </div>
        </Link>
    );
};

Card.propTypes = {
    details: DETAILS_PROP_TYPES(PropTypes).isRequired,
};

export default Card;
