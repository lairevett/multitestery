import FadeIn from 'react-lazyload-fadein';
import PropTypes from 'prop-types';
import React from 'react';

const CardImg = ({title, photoSmall}) => (
    <div className="img-wrapper">
        <FadeIn duration={256} once>
            {onload => (
                <img src={photoSmall} alt={`Tester ${title}`} className="img-card-top perfume" onLoad={onload} />
            )}
        </FadeIn>
        <noscript>
            <img src={photoSmall} alt={`Tester ${title}`} className="img-card-top perfume" />
        </noscript>
    </div>
);

CardImg.propTypes = {
    title: PropTypes.string.isRequired,
    photoSmall: PropTypes.string.isRequired,
};

export default CardImg;
