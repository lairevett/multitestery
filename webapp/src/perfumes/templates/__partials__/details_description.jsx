import React from 'react';

import Markdown from 'react-markdown';
import PropTypes from 'prop-types';
import DetailsDescriptionNoData from './details_description_no_data';

const DetailsDataDescription = ({description, topNotes, heartNotes, baseNotes}) => (
    <div className="col-12 mt-3">
        <h4>Opis</h4>
        {description ? (
            <Markdown source={description} />
        ) : (
            <p>
                <DetailsDescriptionNoData />
            </p>
        )}
        <h5>Nuty zapachowe</h5>
        <h6>Głowa</h6>
        <p>{topNotes || <DetailsDescriptionNoData />}</p>
        <h6>Serce</h6>
        <p>{heartNotes || <DetailsDescriptionNoData />}</p>
        <h6>Głębia</h6>
        <p>{baseNotes || <DetailsDescriptionNoData />}</p>
        <hr />
        <p>
            <strong>Tester</strong> to pełnowartościowy produkt o tym samym składzie, co oryginalne perfumy, lecz w
            opakowaniu zastępczym. Kupując swoje ulubione perfumy w postaci <strong>testerów</strong> płacisz{' '}
            <strong>nawet do 70% mniej</strong>! Flakon może być pozbawiony korka na atomizerze.
        </p>
    </div>
);

DetailsDataDescription.propTypes = {
    description: PropTypes.string.isRequired,
    topNotes: PropTypes.string.isRequired,
    heartNotes: PropTypes.string.isRequired,
    baseNotes: PropTypes.string.isRequired,
};

export default DetailsDataDescription;
