import React from 'react';

import {Helmet} from 'react-helmet';
import PropTypes from 'prop-types';
import DetailsDescription from './__partials__/details_description';
import DetailsImg from './__partials__/details_img';
import DetailsTable from './__partials__/details_table';
import {DETAILS_PROP_TYPES} from '../constants';
import AddToCartForm from '../forms/add_product_to_cart';

const PerfumesDetails = ({details, isUserStaff}) => (
    <>
        {details.description && (
            <Helmet>
                <meta name="description" content={details.description} />
            </Helmet>
        )}
        <div className="row">
            <div className="d-flex col-md-5 p-0">
                <DetailsImg
                    photoMedium={details.photo_medium}
                    photoBig={details.photo_big}
                    brand={details.brand}
                    model={details.model}
                />
            </div>
            <div className="col-md-7">
                <DetailsTable
                    id={details.id}
                    brand={details.brand}
                    model={details.model}
                    category={details.category}
                    unitPrice={details.unit_price}
                    amount={details.amount}
                    quantity={details.quantity}
                    percentOff={details.percent_off}
                    isUserStaff={isUserStaff}
                />
                <AddToCartForm initialValues={{productId: details.id, quantity: 1}} />
            </div>
            <div className="col-12">
                <DetailsDescription
                    description={details.description}
                    topNotes={details.top_notes}
                    heartNotes={details.heart_notes}
                    baseNotes={details.base_notes}
                />
            </div>
        </div>
    </>
);

PerfumesDetails.propTypes = {
    details: DETAILS_PROP_TYPES(PropTypes).isRequired,
    isUserStaff: PropTypes.bool.isRequired,
};

export default PerfumesDetails;
