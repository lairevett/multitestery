import {validateFieldMinValue, validateFieldRequired} from '../../app/utils/validation';

import {
    getOrCreateClientCartEntries,
    getServerCartEntries,
    setClientCartEntries,
    setServerCartEntries,
} from '../../cart/utils';
import {setEntries} from '../../cart/actions';
import {logError} from '../../app/utils/logger';

export const validate = ({productId, quantity}) => {
    const errors = {};

    errors.productId = validateFieldRequired(productId) || validateFieldMinValue(productId, 1);
    errors.quantity = validateFieldRequired(quantity) || validateFieldMinValue(quantity, 0);

    return errors;
};

export const submit = async ({productId, quantity}, dispatch) => {
    const entries = getOrCreateClientCartEntries();
    entries[`_${productId}`] = quantity;

    setClientCartEntries(entries);
    dispatch(setEntries(entries));
};

export const submitServer = (request, response) => {
    try {
        const {productId, quantity} = request.body;
        const parsedProductId = +productId;
        const parsedQuantity = +quantity;

        if (Object.values(validate({productId: parsedProductId, quantity: parsedQuantity})).every(value => !value)) {
            const entries = getServerCartEntries(request);
            entries[`_${productId}`] = parsedQuantity;
            setServerCartEntries(response, entries);

            return () => response.redirect('?added_product_to_cart');
        }
    } catch (error) {
        logError(error.stack);
    }

    return () => response.redirect('?error');
};
