import {reset} from 'redux-form';
import {FORM} from '../constants';
import {history, retrieveUrl} from '../../app/utils/router';

export const submit = ({keyword}, dispatch) => {
    history.push(`${retrieveUrl('perfumesSearchedList').path}?keyword=${encodeURIComponent(keyword)}`);
    dispatch(reset(FORM.SEARCH));
};
