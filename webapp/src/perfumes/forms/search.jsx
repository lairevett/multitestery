import {Field, Form, reduxForm, propTypes} from 'redux-form';

import React from 'react';
import {FORM} from '../constants';
import {FormField} from '../../app/templates/__partials__/form';
import SubmitButton from '../../app/templates/__partials__/submit_button';
import {submit} from './process_search';
import {retrieveUrl} from '../../app/utils/router';

const SearchForm = ({handleSubmit, valid, submitting}) => (
    <Form
        method="get"
        action={retrieveUrl('perfumesSearchedList').path}
        className="form-inline mr-2 d-none d-md-flex"
        onSubmit={handleSubmit(submit)}
    >
        <div className="form-group">
            <Field
                id="keyword"
                name="keyword"
                type="text"
                placeholder="Szukaj perfum..."
                small
                doNotValidate
                component={FormField}
                aria-label="Pole ze słowem kluczowym do szukania perfum"
            />
            <SubmitButton
                className="btn-sm btn-light btn-outline-dark"
                noScriptValue="Szukaj"
                value={<i className="icon-search" />}
                disabled={!valid || submitting}
                aria-label="Przycisk szukaj"
            />
        </div>
    </Form>
);

SearchForm.propTypes = propTypes;

export default reduxForm({form: FORM.SEARCH, initialValues: {keyword: ''}})(SearchForm);
