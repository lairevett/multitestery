import {Field, Form, reduxForm, propTypes} from 'redux-form';
import React from 'react';
import {useSelector} from 'react-redux';
import PropTypes from 'prop-types';
import {FormField, FormHorizontalField} from '../../app/templates/__partials__/form';
import {submit, validate} from './process_add_product_to_cart';

import {FORM} from '../constants';
import SubmitButton from '../../app/templates/__partials__/submit_button';

const AddProductToCartForm = ({handleSubmit, valid, submitting, submitSucceeded, initialValues: {productId}}) => {
    const cartEntries = useSelector(state => state.cart.entries);

    return (
        <Form
            method="post"
            action="?add_product_to_cart"
            className="border p-3 float-right col-lg-7"
            onSubmit={handleSubmit(submit)}
        >
            <Field id="productId" name="productId" type="hidden" parse={value => +value} component={FormField} />
            <Field
                id="quantity"
                name="quantity"
                label="Sztuk"
                type="number"
                min="1"
                parse={value => +value}
                component={FormHorizontalField}
                aria-labelledby="quantity"
            />
            <SubmitButton
                className="btn-block"
                disabled={
                    !valid || submitting || submitSucceeded || typeof cartEntries[`_${productId}`] !== 'undefined'
                }
                noScriptValue="Dodaj do koszyka"
                value={
                    submitSucceeded || typeof cartEntries[`_${productId}`] !== 'undefined'
                        ? 'Dodano do koszyka'
                        : 'Dodaj do koszyka'
                }
                aria-label="Przycisk dodający produkt do koszyka"
            />
        </Form>
    );
};

AddProductToCartForm.propTypes = {
    ...propTypes,
    initialValues: PropTypes.exact({
        productId: PropTypes.string.isRequired,
    }).isRequired,
};

export default reduxForm({
    form: FORM.ADD_PRODUCT_TO_CART,
    enableReinitialize: true,
    validate,
    shouldValidate: () => process.env.__CLIENT__,
})(AddProductToCartForm);
