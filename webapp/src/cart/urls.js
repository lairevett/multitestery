import {ACCESS, registerUrl} from '../app/utils/router';
import {CartCheckoutView, CartIndexView} from './views';
import {MODE} from './constants';
import {callbackGetDispatchEvents} from './utils';

registerUrl('cartIndex', '/cart', ['get', 'post'], CartIndexView, ACCESS.ALLOW_ANY, {
    title: 'Koszyk',
    mode: MODE.CART,
    callbackGetDispatchEvents,
});

registerUrl('cartCheckout', '/cart/checkout', ['get', 'post'], CartCheckoutView, ACCESS.REQUIRE_AUTH, {
    title: 'Kasa',
    mode: MODE.CART_CHECKOUT,
    callbackGetDispatchEvents,
});
