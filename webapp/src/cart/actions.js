import {ACTION, ENDPOINT, PRODUCTS_INITIAL_STATE} from './constants';

import {apiBatchGetRequests} from '../app/utils/api/requests';
import {logDispatchAPIErrors} from '../app/utils/logger';
import {DETAILS_INITIAL_STATE} from '../perfumes/constants';

export const setCart = (entries, products) => ({type: ACTION.SET_CART, payload: {entries, products}});

export const setEntries = entries => ({type: ACTION.SET_ENTRIES, payload: entries});

export const retrieveProducts = entries => {
    try {
        const resources = Object.keys(entries).map(productId => ENDPOINT.PRODUCT_DETAILS(productId.slice(1)));
        return apiBatchGetRequests(
            ACTION.RETRIEVE_PRODUCTS,
            DETAILS_INITIAL_STATE,
            PRODUCTS_INITIAL_STATE,
            resources,
            {}
        );
    } catch (error) {
        logDispatchAPIErrors(error);
    }
};

export const clearProducts = () => ({type: ACTION.CLEAR_PRODUCTS});

export const removeProduct = productId => ({type: ACTION.REMOVE_PRODUCT, context: {productId}});
