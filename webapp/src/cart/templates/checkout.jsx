import React from 'react';

import PropTypes from 'prop-types';
import {Redirect} from 'react-router-dom';
import CheckoutForm from '../forms/checkout';
import {ENTRIES_PROP_TYPES} from '../constants';
import {LIST_PROP_TYPES as FORWARDING_ADDRESSES_LIST_PROP_TYPES} from '../../auth/constants/forwarding_addresses';
import OrderEntriesTable from '../../orders/templates/__partials__/order_entries_table';
import {LIST_PROP_TYPES as PRODUCTS_LIST_PROP_TYPES} from '../../perfumes/constants';
import {retrieveUrl} from '../../app/utils/router';

const CartCheckout = ({entries, products, forwardingAddresses, isError}) =>
    Object.keys(entries).length > 0 ? (
        <>
            <OrderEntriesTable entries={entries} products={products} isMutable={false} />
            <CheckoutForm forwardingAddresses={forwardingAddresses} isError={isError} />
        </>
    ) : (
        <Redirect to={retrieveUrl('cartIndex').path} />
    );

CartCheckout.propTypes = {
    entries: ENTRIES_PROP_TYPES(PropTypes).isRequired,
    products: PRODUCTS_LIST_PROP_TYPES(PropTypes).isRequired,
    forwardingAddresses: FORWARDING_ADDRESSES_LIST_PROP_TYPES(PropTypes).isRequired,
    isError: PropTypes.bool.isRequired,
};

export default CartCheckout;
