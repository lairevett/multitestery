import React from 'react';

import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import ClearCartForm from '../forms/clear_cart';
import OrderEntriesTable from '../../orders/templates/__partials__/order_entries_table';
import {ENTRIES_PROP_TYPES} from '../constants';
import {LIST_PROP_TYPES as PERFUME_LIST_PROP_TYPES} from '../../perfumes/constants';
import {retrieveUrl} from '../../app/utils/router';

const CartIndex = ({entries, products}) =>
    Object.keys(entries).length > 0 ? (
        <>
            <OrderEntriesTable entries={entries} products={products} isMutable />
            <div className="d-flex justify-content-between">
                <ClearCartForm />
                <Link to={retrieveUrl('cartCheckout').path} className="btn btn-primary">
                    Przejdź do kasy
                </Link>
            </div>
        </>
    ) : (
        <p>Twój koszyk jest pusty.</p>
    );

CartIndex.propTypes = {
    entries: ENTRIES_PROP_TYPES(PropTypes).isRequired,
    products: PERFUME_LIST_PROP_TYPES(PropTypes).isRequired,
};

export default CartIndex;
