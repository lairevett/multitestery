import Cookies from 'js-cookie';
import {useDispatch, useSelector} from 'react-redux';
import {useCallback, useEffect} from 'react';
import {clearProducts, retrieveProducts, setCart} from './actions';
import {API_URL, STATUS} from '../app/utils/api/constants';
import {MODE} from './constants';
import {makeDispatchEvents} from '../app/utils/dispatch_events';
import {retrieveAllList as retrieveAllForwardingAddressesList} from '../auth/actions/forwarding_addresses';
import {logError} from '../app/utils/logger';

export const callbackGetDispatchEvents = urlProperties => (urlParameters, queryParameters, state, context) => {
    const userId = state.auth.user.id;
    const {entries} = state.cart;

    const dispatchEvents = makeDispatchEvents([() => retrieveProducts(entries)], [clearProducts]);

    switch (urlProperties.mode) {
        case MODE.CART:
            break;
        case MODE.CART_CHECKOUT:
            dispatchEvents.onInit.push(() => retrieveAllForwardingAddressesList(userId, context));
            break;
        default:
            throw new Error(`Unexpected mode ${urlProperties.mode} provided in cart callbackGetDispatchEvents.`);
    }

    return dispatchEvents;
};

// Filters out bad cart entries.
const filterCartEntries = entries => {
    if (typeof entries === 'object' && entries !== null) {
        return Object.fromEntries(
            Object.entries(entries).filter(
                ([productId, quantity]) => +productId.slice?.(1) > 0 && Number.isSafeInteger(quantity) && quantity > 0
            )
        );
    }

    return {};
};

// Client-side cart state handlers.
export const getOrCreateClientCartEntries = () => {
    try {
        const cart = Cookies.get('cart');
        if (typeof cart === 'undefined') return {};

        return filterCartEntries(JSON.parse(unescape(cart)));
    } catch (error) {
        logError(error.stack);
        return {};
    }
};

export const setClientCartEntries = cart => {
    if (typeof cart !== 'undefined' && cart != null) {
        try {
            Cookies.set('cart', escape(JSON.stringify(cart)), {
                sameSite: 'strict',
                secure: API_URL.startsWith('https'),
            });
        } catch (error) {
            logError(error.stack);
        }
    }
};

// Server-side cart state handlers.
export const getServerCartEntries = request => {
    try {
        const cart = request.cookies?.cart;
        if (typeof cart === 'undefined') return {};

        return filterCartEntries(JSON.parse(unescape(cart)));
    } catch (error) {
        logError(error.stack);

        return {};
    }
};

export const setServerCartEntries = (response, cart) => {
    if (typeof cart !== 'undefined' && cart != null) {
        try {
            response.cookie('cart', escape(JSON.stringify(cart)), {
                sameSite: 'strict',
                secure: API_URL.startsWith('https'),
            });
        } catch (error) {
            logError(error.stack);
        }
    }
};

const getFilteredCartState = (entries, products) => {
    const entriesKeys = Object.keys(entries);
    const entriesKeysToDelete = [];

    const newProductsResultsState = products.results.filter(({__meta__: {status}}, i) => {
        if (status !== STATUS.OK) {
            // Can't remove by product id, it's 0 if the API returns response with non-2xx status.
            // eslint-disable-next-line security/detect-object-injection
            entriesKeysToDelete.push(entriesKeys[i]);

            return false;
        }

        return true;
    });

    let newEntriesState = null;
    if (entriesKeysToDelete.length > 0) {
        newEntriesState = {...entries};

        for (const key of entriesKeysToDelete) {
            // eslint-disable-next-line security/detect-object-injection
            delete newEntriesState[key];
        }
    }

    return [newEntriesState || entries, {...products, results: newProductsResultsState}];
};

export const useFilterCartState = () => {
    const willServerDispatchNextEvents = useSelector(state => state.__server__.willDispatchNextEvents);
    const {entries, products} = useSelector(state => state.cart);
    const dispatch = useDispatch();

    const dispatchSetCart = useCallback((_entries, _products) => dispatch(setCart(_entries, _products)), [dispatch]);

    useEffect(() => {
        // The first cart filter dispatch is made server-side,
        // no need to filter the cart again client-side.
        if (willServerDispatchNextEvents) return;

        const cart = getFilteredCartState(entries, products);
        const [newEntriesState, newProductsState] = cart;

        if (Object.keys(entries).length !== Object.keys(newEntriesState).length) {
            setClientCartEntries(newEntriesState);
            dispatchSetCart(newEntriesState, newProductsState);
        }
    }, [willServerDispatchNextEvents, entries, products, dispatchSetCart]);
};

export const filterCartState = ({entries, products}, dispatch) => {
    let cart = [{}, {...products, results: []}];

    try {
        cart = getFilteredCartState(entries, products);
    } catch (error) {
        logError(error.stack);
    }

    const [newEntriesState, newProductsState] = cart;

    if (Object.keys(entries).length !== Object.keys(newEntriesState).length) {
        setClientCartEntries(newEntriesState);
        dispatch(setCart(newEntriesState, newProductsState));
    }
};
