import {ACTION, INITIAL_STATE} from './constants';

const cartReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ACTION.SET_CART:
            return action.payload;
        case ACTION.SET_ENTRIES:
            return {
                ...state,
                entries: action.payload,
            };
        case ACTION.RETRIEVE_PRODUCTS:
            return {
                ...state,
                products: action.payload,
            };
        case ACTION.CLEAR_PRODUCTS:
            return {
                ...state,
                products: INITIAL_STATE.products,
            };
        case ACTION.REMOVE_PRODUCT: {
            const newEntriesState = {...state.entries};
            delete newEntriesState[`_${action.context.productId}`];

            return {
                ...state,
                entries: newEntriesState,
                products: {
                    ...state.products,
                    results: state.products.results.filter(({id}) => id !== +action.context.productId),
                },
            };
        }
        default:
            return state;
    }
};

export default cartReducer;
