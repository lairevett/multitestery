import {Form, reduxForm, propTypes} from 'redux-form';

import React from 'react';
import {FORM} from '../constants';
import SubmitButton from '../../app/templates/__partials__/submit_button';
import {submit} from './process_clear_cart';

const ClearCartForm = ({handleSubmit}) => (
    <Form method="post" action="?clear" onSubmit={handleSubmit(submit)}>
        <SubmitButton value="Wyczyść koszyk" aria-label="Przycisk czyszczący koszyk" />
    </Form>
);

ClearCartForm.propTypes = propTypes;

export default reduxForm({form: FORM.CLEAR_CART})(ClearCartForm);
