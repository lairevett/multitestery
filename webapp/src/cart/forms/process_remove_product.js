import {validateFieldRequired} from '../../app/utils/validation';

import {getOrCreateClientCartEntries, getServerCartEntries, setClientCartEntries, setServerCartEntries} from '../utils';
import {removeProduct} from '../actions';
import {logError} from '../../app/utils/logger';

export const validate = ({productId}) => ({productId: validateFieldRequired(productId)});

export const submit = ({productId}, dispatch) => {
    const entries = getOrCreateClientCartEntries();
    delete entries[`_${productId}`];

    setClientCartEntries(entries);
    dispatch(removeProduct(productId));
};

export const submitServer = async (request, response, dispatch) => {
    try {
        const {productId} = request.body;
        const parsedProductId = +productId;

        if (Object.values(validate({productId: parsedProductId})).every(value => !value)) {
            const entries = getServerCartEntries(request);
            delete entries[`_${productId}`];

            setServerCartEntries(response, entries);
            dispatch(removeProduct(productId));

            return () => response.redirect('?removed_product');
        }
    } catch (error) {
        logError(error.stack);
    }

    return () => response.redirect('?error');
};
