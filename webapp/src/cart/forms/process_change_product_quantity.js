import {validateFieldMinValue, validateFieldRequired} from '../../app/utils/validation';

import {getOrCreateClientCartEntries, getServerCartEntries, setClientCartEntries, setServerCartEntries} from '../utils';
import {OPERATION} from '../../orders/constants';
import {setEntries} from '../actions';
import {logError} from '../../app/utils/logger';

export const validate = ({productId, quantity}) => {
    const errors = {};

    errors.productId = validateFieldRequired(productId);
    errors.quantity = validateFieldRequired(quantity) || validateFieldMinValue(quantity, 0);

    return errors;
};

export const submit = ({productId}, dispatch, operation) => {
    const entries = getOrCreateClientCartEntries();

    switch (operation) {
        case OPERATION.SUB_QUANTITY:
            if (entries?.[`_${productId}`] > 1) {
                entries[`_${productId}`] -= 1;
                setClientCartEntries(entries);

                dispatch(setEntries(entries));
            }
            break;
        case OPERATION.ADD_QUANTITY:
            if (entries?.[`_${productId}`]) {
                entries[`_${productId}`] += 1;
                setClientCartEntries(entries);

                dispatch(setEntries(entries));
            }
            break;
        default:
            logError(`Unexpected operation ${operation} provided in ChangeProductQuantityForm submit.`);
    }
};

export const submitServer = async (request, response, dispatch, operation) => {
    try {
        const {productId, quantity} = request.body;
        const parsedProductId = +productId;
        const parsedQuantity = +quantity;

        if (Object.values(validate({productId: parsedProductId, quantity: parsedQuantity})).every(value => !value)) {
            const entries = getServerCartEntries(request);

            switch (operation) {
                case OPERATION.SUB_QUANTITY:
                    if (entries?.[`_${productId}`] > 1) {
                        entries[`_${productId}`] -= 1;
                        await setServerCartEntries(response, entries);

                        return () => response.redirect('?subbed_quantity');
                    }
                    break;
                case OPERATION.ADD_QUANTITY:
                    if (entries?.[`_${productId}`]) {
                        entries[`_${productId}`] += 1;
                        await setServerCartEntries(response, entries);

                        return () => response.redirect('?added_quantity');
                    }
                    break;
                default:
                    logError(`Unexpected operation ${operation} provided in ChangeProductQuantityForm submitServer.`);
            }
        }
    } catch (error) {
        logError(error.stack);
    }

    return () => response.redirect('?error');
};
