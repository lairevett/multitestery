import {SubmissionError} from 'redux-form';
import {getOrCreateClientCartEntries, getServerCartEntries, setClientCartEntries, setServerCartEntries} from '../utils';
import {validateFieldMaxValue, validateFieldMinValue, validateFieldRequired} from '../../app/utils/validation';

import {SHIPPING} from '../../orders/constants';
import {setCart} from '../actions';
import {create as createOrder} from '../../orders/actions';
import {history, retrieveUrl} from '../../app/utils/router';
import {PRODUCTS_INITIAL_STATE} from '../constants';
import {STATUS} from '../../app/utils/api/constants';
import {logDispatchAPIErrors} from '../../app/utils/logger';

export const validate = ({forwardingAddress, shipping}) => {
    const errors = {};

    errors.forwardingAddress = validateFieldRequired(forwardingAddress);

    errors.shipping =
        validateFieldRequired(shipping) ||
        validateFieldMinValue(SHIPPING.CASH_ON_DELIVERY) ||
        validateFieldMaxValue(SHIPPING.CASH_UP_FRONT);

    return errors;
};

export const submit = async ({forwardingAddress, shipping}, dispatch) => {
    // Remove prepended "_" character from productIds before sending request and map it to API serializer fields.
    const entries = Object.entries(getOrCreateClientCartEntries()).map(([productId, quantity]) => ({
        product: +productId.slice(1),
        quantity,
    }));

    const {payload} = dispatch(await createOrder(entries, forwardingAddress, shipping));

    if (payload.errors) {
        throw new SubmissionError(payload.errors);
    } else if (payload.__meta__.status !== STATUS.OK) {
        throw SubmissionError;
    }

    setClientCartEntries({});
    dispatch(setCart({}, PRODUCTS_INITIAL_STATE));
    history.push(retrieveUrl('ordersNotPaidList').path);
};

export const submitServer = async (request, response, dispatch) => {
    try {
        const {forwardingAddress, shipping} = request.body;
        const parsedForwardingAddress = +forwardingAddress;
        const parsedShipping = +shipping;

        const parsedValues = {
            forwardingAddress: parsedForwardingAddress,
            shipping: parsedShipping,
        };

        if (Object.values(validate(parsedValues)).every(value => !value)) {
            // Remove prepended "_" character from productIds before sending request and map it to API serializer fields.
            const entries = Object.entries(getServerCartEntries(request)).map(([productId, quantity]) => ({
                product: +productId.slice(1),
                quantity,
            }));

            const {payload} = dispatch(
                await createOrder(entries, parsedForwardingAddress, parsedShipping, {headers: request.headers})
            );

            if (!payload.errors && payload.__meta__.status === STATUS.OK) {
                setServerCartEntries(response, {});

                return () => response.redirect(retrieveUrl('ordersNotPaidList').path);
            }
        }
    } catch (error) {
        logDispatchAPIErrors(error);
    }

    return () => response.redirect('?error');
};
