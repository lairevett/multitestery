import {Field, Form, reduxForm, propTypes} from 'redux-form';
import React from 'react';
import {submit, validate} from './process_change_product_quantity';

import {FormField} from '../../app/templates/__partials__/form';
import SubmitButton from '../../app/templates/__partials__/submit_button';
import {OPERATION} from '../../orders/constants';

const ChangeProductQuantityForm = ({handleSubmit}) => (
    <Form method="post" onSubmit={() => false} className="form-inline">
        <Field name="productId" type="hidden" parse={value => +value} component={FormField} />
        <div className="d-flex justify-content-center">
            <SubmitButton
                className="btn-sm btn-light btn-outline-primary"
                formAction="?sub_quantity"
                noScriptValue="-"
                value={<i className="icon-minus" />}
                onClick={handleSubmit((values, dispatch) => submit(values, dispatch, OPERATION.SUB_QUANTITY))}
                aria-label="Przycisk do zmniejszania ilości produktu"
            />
            <Field
                name="quantity"
                type="text"
                required
                readOnly
                plainText
                formGroupClassNames="col-3"
                inputClassNames="text-center"
                parse={value => +value}
                component={FormField}
                aria-label="Pole z ilością produktu"
            />
            <SubmitButton
                className="btn-sm btn-light btn-outline-primary"
                formAction="?add_quantity"
                noScriptValue="+"
                value={<i className="icon-plus" />}
                onClick={handleSubmit((values, dispatch) => submit(values, dispatch, OPERATION.ADD_QUANTITY))}
                aria-label="Przycisk do zwiększania ilości produktu"
            />
        </div>
    </Form>
);

ChangeProductQuantityForm.propTypes = propTypes;

// Form name is being set dynamically, no need setting it here.
export default reduxForm({
    enableReinitialize: true,
    validate,
    shouldValidate: () => process.env.__CLIENT__,
})(ChangeProductQuantityForm);
