import {setClientCartEntries, setServerCartEntries} from '../utils';
import {PRODUCTS_INITIAL_STATE} from '../constants';
import {setCart} from '../actions';
import {STATUS} from '../../app/utils/api/constants';
import {logError} from '../../app/utils/logger';

export const submit = (_, dispatch) => {
    setClientCartEntries({});
    dispatch(setCart({}, {...PRODUCTS_INITIAL_STATE, __meta__: {status: STATUS.OK}}));
};

export const submitServer = async response => {
    try {
        setServerCartEntries(response, {});

        return () => response.redirect('?cleared');
    } catch (error) {
        logError(error.stack);
    }

    return () => response.redirect('?error');
};
