import {Field, Form, reduxForm, propTypes} from 'redux-form';
import {Link} from 'react-router-dom';
import React from 'react';
import PropTypes from 'prop-types';
import {submit, validate} from './process_checkout';

import ErrorAlert from '../../app/templates/__partials__/error_alert';
import {FORM} from '../constants';
import {FormField} from '../../app/templates/__partials__/form';
import {SHIPPING} from '../../orders/constants';
import SubmitButton from '../../app/templates/__partials__/submit_button';
import {formatForwardingAddress} from '../../orders/utils';
import {retrieveUrl} from '../../app/utils/router';
import {LIST_PROP_TYPES} from '../../auth/constants/forwarding_addresses';

const CheckoutForm = ({handleSubmit, valid, submitting, submitSucceeded, forwardingAddresses, isError}) => (
    <Form method="post" action="?" onSubmit={handleSubmit(submit)}>
        {isError && <ErrorAlert />}
        <Field
            id="forwardingAddress"
            name="forwardingAddress"
            type="select"
            label="Adres wysyłki"
            parse={value => +value}
            component={FormField}
            aria-labelledby="forwardingAddress"
        >
            <option value="" disabled>
                Wybierz adres wysyłki...
            </option>
            {forwardingAddresses.map(forwardingAddress => (
                <option key={forwardingAddress.id} value={forwardingAddress.id}>
                    {formatForwardingAddress(forwardingAddress)}
                </option>
            ))}
        </Field>
        <Field
            id="shipping"
            name="shipping"
            type="select"
            label="Sposób płatności"
            parse={value => +value}
            component={FormField}
            aria-labelledby="shipping"
        >
            <option value="" disabled>
                Wybierz sposób płatności...
            </option>
            <option value={SHIPPING.CASH_ON_DELIVERY}>Płatność przy odbiorze (przesyłka za pobraniem)</option>
            <option value={SHIPPING.CASH_UP_FRONT}>Płatność z góry (przesyłka kurierska)</option>
        </Field>
        <div className="d-flex justify-content-between">
            <Link to={retrieveUrl('cartIndex').path} className="btn btn-primary">
                Wróć do koszyka
            </Link>
            <SubmitButton
                value="Zatwierdź zamówienie"
                disabled={!valid || submitting || submitSucceeded}
                aria-label="Przycisk zatwierdzający zamówienie"
            />
        </div>
    </Form>
);

CheckoutForm.propTypes = {
    ...propTypes,
    forwardingAddresses: LIST_PROP_TYPES(PropTypes).isRequired,
};

export default reduxForm({form: FORM.CHECKOUT, validate, shouldValidate: () => process.env.__CLIENT__})(CheckoutForm);
