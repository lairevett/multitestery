import {Field, Form, reduxForm, propTypes} from 'redux-form';
import React from 'react';
import {submit, validate} from './process_remove_product';

import {FormField} from '../../app/templates/__partials__/form';
import SubmitButton from '../../app/templates/__partials__/submit_button';

const RemoveProductForm = ({handleSubmit}) => (
    <Form
        method="post"
        action="?remove_product"
        onSubmit={handleSubmit(submit)}
        className="d-flex justify-content-center"
    >
        <Field name="productId" type="hidden" parse={value => +value} component={FormField} />
        <SubmitButton
            className="btn-sm btn-light btn-outline-danger"
            noScriptValue="x"
            value={<i className="icon-cancel" />}
            aria-label="Przycisk usuwający produkt z koszyka"
        />
    </Form>
);

RemoveProductForm.propTypes = propTypes;

// Form name is being set dynamically, no need to setting it here.
export default reduxForm({
    enableReinitialize: true,
    validate,
    shouldValidate: () => process.env.__CLIENT__,
})(RemoveProductForm);
