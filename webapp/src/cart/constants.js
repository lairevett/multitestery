import {ENDPOINT as PERFUME_ENDPOINT} from '../perfumes/constants';
import {DEFAULT_STATUS_INITIAL_STATE} from '../app/utils/api/constants';

// API endpoints.
export const ENDPOINT = {
    PRODUCT_DETAILS: productId => PERFUME_ENDPOINT.DETAILS(productId),
};

// Redux action types.
export const ACTION = {
    SET_CART: '@cart/SET',
    SET_ENTRIES: '@cart/SET_ENTRIES',
    RETRIEVE_PRODUCTS: '@cart/RETRIEVE_PRODUCTS',
    CLEAR_PRODUCTS: '@cart/CLEAR_PRODUCTS',
    REMOVE_PRODUCT: '@cart/REMOVE_PRODUCT',
};

// Redux form names.
export const FORM = {
    CHANGE_PRODUCT_QUANTITY: 'FORM_CHANGE_PRODUCT_QUANTITY',
    REMOVE_PRODUCT: 'FORM_REMOVE_PRODUCT',
    CLEAR_CART: 'FORM_CLEAR_CART',
    CHECKOUT: 'FORM_CHECKOUT',
};

// Reducer initial states.
export const PRODUCTS_INITIAL_STATE = {
    results: [],
    ...DEFAULT_STATUS_INITIAL_STATE,
};

export const INITIAL_STATE = {
    entries: {},
    products: PRODUCTS_INITIAL_STATE,
};

// Prop types.
export const ENTRIES_PROP_TYPES = PropertyTypes => PropertyTypes.objectOf(PropertyTypes.number.isRequired);

// Template modes.
export const MODE = {
    CART: 'MODE_CART',
    CART_CHECKOUT: 'MODE_CART_CHECKOUT',
};
