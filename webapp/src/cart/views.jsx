/* eslint-disable react/prop-types */
import React from 'react';

import CartIndex from './templates';
import CartCheckout from './templates/checkout';
import Content from '../app/templates/__partials__/content';
import {filterCartState, useFilterCartState} from './utils';
import {submitServer as submitServerChangeQuantity} from './forms/process_change_product_quantity';
import {submitServer as submitServerClearCart} from './forms/process_clear_cart';
import {submitServer as submitServerCreateOrder} from './forms/process_checkout';
import {submitServer as submitServerRemoveProduct} from './forms/process_remove_product';
import Loading from '../app/templates/__partials__/loading';
import Suspense from '../app/utils/api/suspense';
import {OPERATION} from '../orders/constants';

/*
 * GET /cart
 * POST /cart?clear
 * POST /cart?sub_quantity
 * POST /cart?add_quantity
 * POST /cart?remove_product
 */
export const CartIndexView = ({title, state}) => {
    const {entries} = state.cart;
    useFilterCartState();

    const {
        results,
        __meta__: {status},
    } = state.cart.products;

    return (
        <Content status={status} title={title}>
            <Suspense status={status} fallback={<Loading />}>
                <CartIndex entries={entries} products={results} />
            </Suspense>
        </Content>
    );
};

CartIndexView.willDispatchEvents = (request, response, urlProperties, state, dispatch) => {
    if (request.method === 'POST') {
        if ('clear' in request.query) {
            return submitServerClearCart(response);
        }

        if ('sub_quantity' in request.query) {
            return submitServerChangeQuantity(request, response, dispatch, OPERATION.SUB_QUANTITY);
        }

        if ('add_quantity' in request.query) {
            return submitServerChangeQuantity(request, response, dispatch, OPERATION.ADD_QUANTITY);
        }

        if ('remove_product' in request.query) {
            return submitServerRemoveProduct(request, response, dispatch);
        }
    }
};

CartIndexView.didDispatchEvents = (request, response, urlProperties, state, dispatch) => {
    filterCartState(state.cart, dispatch);
};

/*
 * GET /cart/checkout
 * POST /cart/checkout
 */
export const CartCheckoutView = ({title, queryParameters, state}) => {
    const isError = 'error' in queryParameters;

    const {entries} = state.cart;
    useFilterCartState();

    const {
        results: products,
        __meta__: {status},
    } = state.cart.products;

    const {results: forwardingAddresses} = state.auth.forwardingAddresses;

    return (
        <Content status={status} title={title}>
            <Suspense status={status} fallback={<Loading />}>
                <CartCheckout
                    entries={entries}
                    products={products}
                    forwardingAddresses={forwardingAddresses}
                    isError={isError}
                />
            </Suspense>
        </Content>
    );
};

CartCheckoutView.willDispatchEvents = (request, response, urlProperties, state, dispatch) => {
    if (request.method === 'POST') {
        return submitServerCreateOrder(request, response, dispatch);
    }
};

CartCheckoutView.didDispatchEvents = (request, response, urlProperties, state, dispatch) => {
    filterCartState(state.cart, dispatch);
};
