import {makeDispatchEvents} from '../app/utils/dispatch_events';
import {clearDetails, retrieveDetails} from './actions';

export const callbackGetDispatchEvents = () => urlParameters => {
    // Cannot set /:slug url for route, it catches all 404 not found pages.
    const slug = process.env.__CLIENT__ ? window.location.pathname : urlParameters[0];

    return makeDispatchEvents([() => retrieveDetails(slug.replace('/', ''))], [clearDetails]);
};
