import React from 'react';
import ReactMarkdown from 'react-markdown';
import PropTypes from 'prop-types';

const PagesDetails = ({content}) => <ReactMarkdown source={content} />;

PagesDetails.propTypes = {
    content: PropTypes.string.isRequired,
};

export default PagesDetails;
