import {DEFAULT_STATUS_INITIAL_STATE} from '../app/utils/api/constants';

// API endpoints.
export const ENDPOINT = {
    LIST_ACTIVE: '/v1/pages/?active=1',
    DETAILS: pageSlug => `/v1/pages/${pageSlug}/`,
};

// Redux action types.
export const ACTION = {
    RETRIEVE_ACTIVE_LIST: '@page/RETRIEVE_ACTIVE_LIST',
    RETRIEVE_DETAILS: '@page/RETRIEVE_DETAILS',
    CLEAR_DETAILS: '@page/CLEAR_DETAILS',
};

// Reducer initial states.
export const LIST_INITIAL_STATE = {
    results: [],
    ...DEFAULT_STATUS_INITIAL_STATE,
};

export const DETAILS_INITIAL_STATE = {
    id: 0,
    slug: '',
    priority: 0,
    title: '',
    content: '',
    is_active: false,
    ...DEFAULT_STATUS_INITIAL_STATE,
};

export const INITIAL_STATE = {
    list: LIST_INITIAL_STATE,
    details: DETAILS_INITIAL_STATE,
};
