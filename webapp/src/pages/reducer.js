import {ACTION, INITIAL_STATE} from './constants';

const pageReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ACTION.RETRIEVE_ACTIVE_LIST:
            return {
                ...state,
                list: action.payload,
            };
        case ACTION.RETRIEVE_DETAILS:
            return {
                ...state,
                details: action.payload,
            };
        case ACTION.CLEAR_DETAILS:
            return {
                ...state,
                details: INITIAL_STATE.details,
            };
        default:
            return state;
    }
};

export default pageReducer;
