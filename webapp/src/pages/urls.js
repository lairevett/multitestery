import {ACCESS, registerUrl} from '../app/utils/router';
import {PagesDetailsView} from './views';
import {callbackGetDispatchEvents} from './utils';

export const registerPagesUrls = pages =>
    pages.forEach(({slug, title}) =>
        registerUrl(`pages${slug}Details`, `/${slug}`, ['get'], PagesDetailsView, ACCESS.ALLOW_ANY, {
            title,
            callbackGetDispatchEvents,
        })
    );
