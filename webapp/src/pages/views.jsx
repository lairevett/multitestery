/* eslint-disable react/prop-types */
import React from 'react';
import Content from '../app/templates/__partials__/content';
import Suspense from '../app/utils/api/suspense';
import Loading from '../app/templates/__partials__/loading';
import PagesDetails from './templates/details';

/*
 * GET /{slug}
 */
export const PagesDetailsView = ({state}) => {
    const {
        title,
        content,
        __meta__: {status},
    } = state.page.details;

    return (
        <Content status={status} title={title}>
            <Suspense status={status} fallback={<Loading />}>
                <PagesDetails content={content} />
            </Suspense>
        </Content>
    );
};
