import {apiGetListRequest, apiGetRequest} from '../app/utils/api/requests';
import {ACTION, DETAILS_INITIAL_STATE, ENDPOINT, LIST_INITIAL_STATE} from './constants';

export const retrieveActiveList = () =>
    apiGetListRequest(ACTION.RETRIEVE_ACTIVE_LIST, LIST_INITIAL_STATE, ENDPOINT.LIST_ACTIVE);

export const retrieveDetails = pageSlug =>
    apiGetRequest(ACTION.RETRIEVE_DETAILS, DETAILS_INITIAL_STATE, ENDPOINT.DETAILS(pageSlug));

export const clearDetails = () => ({type: ACTION.CLEAR_DETAILS});
