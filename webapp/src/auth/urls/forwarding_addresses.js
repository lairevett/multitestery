import {ACCESS, registerUrl} from '../../app/utils/router';
import {AuthForwardingAddressesCreateView, AuthForwardingAddressesListView} from '../views/forwarding_addresses';
import {callbackGetDispatchEvents} from '../utils/forwarding_addresses';

registerUrl(
    'authForwardingAddressesCreate',
    '/settings/forwarding-addresses/create',
    ['get', 'post'],
    AuthForwardingAddressesCreateView,
    ACCESS.REQUIRE_AUTH,
    {title: 'Nowy adres', callbackGetDispatchEvents}
);

registerUrl(
    'authForwardingAddressesAllList',
    '/settings/forwarding-addresses',
    ['get', 'post'],
    AuthForwardingAddressesListView,
    ACCESS.REQUIRE_AUTH,
    {title: 'Dane kontaktowe', callbackGetDispatchEvents}
);
