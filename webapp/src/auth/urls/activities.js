import {ACCESS, registerUrl} from '../../app/utils/router';
import {
    AuthActivitiesActivateView,
    AuthActivitiesResendActivationLinkView,
    AuthActivitiesSignInView,
    AuthActivitiesSignOutView,
    AuthActivitiesSignUpView,
} from '../views/activities';

registerUrl(
    'authActivitiesActivate',
    '/activate/:uid/:token',
    ['get', 'post'],
    AuthActivitiesActivateView,
    ACCESS.DISALLOW_AUTH,
    {
        title: 'Aktywuj konto',
    }
);

registerUrl(
    'authActivitiesResendActivationLink',
    '/resend-activation-link',
    ['get', 'post'],
    AuthActivitiesResendActivationLinkView,
    ACCESS.DISALLOW_AUTH,
    {
        title: 'Wyślij link aktywacyjny',
    }
);

registerUrl('authActivitiesSignUp', '/sign-up', ['get', 'post'], AuthActivitiesSignUpView, ACCESS.DISALLOW_AUTH, {
    title: 'Zarejestruj się',
});

registerUrl('authActivitiesSignIn', '/sign-in', ['get', 'post'], AuthActivitiesSignInView, ACCESS.DISALLOW_AUTH, {
    title: 'Zaloguj się',
});

registerUrl('authActivitiesSignOut', '/sign-out', ['get', 'post'], AuthActivitiesSignOutView, ACCESS.REQUIRE_AUTH);
