import {ACCESS, registerUrl} from '../../app/utils/router';
import {
    AuthUserDestroyView,
    AuthUserEmailUpdateView,
    AuthUserNewslettersUpdateView,
    AuthUserPasswordUpdateView,
} from '../views/user';

registerUrl('authUserEmailUpdate', '/settings/email', ['get', 'post'], AuthUserEmailUpdateView, ACCESS.REQUIRE_AUTH, {
    title: 'Zmień email',
});

registerUrl(
    'authUserPasswordUpdate',
    '/settings/password',
    ['get', 'post'],
    AuthUserPasswordUpdateView,
    ACCESS.REQUIRE_AUTH,
    {
        title: 'Zmień hasło',
    }
);

registerUrl(
    'authUserNewslettersUpdate',
    '/settings/newsletters',
    ['get', 'post'],
    AuthUserNewslettersUpdateView,
    ACCESS.REQUIRE_AUTH,
    {title: 'Newslettery'}
);

registerUrl('authUserDestroy', '/settings/delete-user', ['get', 'post'], AuthUserDestroyView, ACCESS.REQUIRE_AUTH, {
    title: 'Usuń konto',
});
