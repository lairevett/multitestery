import {ACTION, DETAILS_INITIAL_STATE, ENDPOINT} from '../constants/user';
import {apiGetRequest, apiPatchRequest} from '../../app/utils/api/requests';

export const updateEmail = (password, newEmail, context) =>
    apiPatchRequest(
        ACTION.UPDATE_EMAIL,
        DETAILS_INITIAL_STATE,
        ENDPOINT.UPDATE_EMAIL,
        {password, new_email: newEmail},
        context
    );

export const updatePassword = (oldPassword, newPassword, context) =>
    apiPatchRequest(
        ACTION.UPDATE_PASSWORD,
        DETAILS_INITIAL_STATE,
        ENDPOINT.UPDATE_PASSWORD,
        {old_password: oldPassword, new_password: newPassword},
        context
    );

export const updateWantsNewsletters = (wantsNewsletters, context) =>
    apiPatchRequest(
        ACTION.UPDATE_WANTS_NEWSLETTERS,
        DETAILS_INITIAL_STATE,
        ENDPOINT.UPDATE_WANTS_NEWSLETTERS,
        {wants_newsletters: wantsNewsletters},
        context
    );

export const retrieveDetails = context =>
    apiGetRequest(ACTION.RETRIEVE_DETAILS, DETAILS_INITIAL_STATE, ENDPOINT.DETAILS, context);

export const clearDetails = () => ({type: ACTION.CLEAR_DETAILS});

export const destroy = (password, context) =>
    apiPatchRequest(ACTION.DESTROY, DETAILS_INITIAL_STATE, ENDPOINT.DESTROY, {password}, context);
