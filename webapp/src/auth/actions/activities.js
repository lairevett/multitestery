import {apiDeleteRequest, apiPostRequest} from '../../app/utils/api/requests';
import {DEFAULT_STATUS_INITIAL_STATE} from '../../app/utils/api/constants';
import {ACTION, ENDPOINT, SIGN_IN_OUT_INITIAL_STATE} from '../constants/activities';

export const activate = (uid, activationToken, context) =>
    apiPostRequest(
        ACTION.ACTIVATE,
        DEFAULT_STATUS_INITIAL_STATE,
        ENDPOINT.ACTIVATE,
        {uid, token: activationToken},
        context
    );

export const resendActivationLink = (email, context) =>
    apiPostRequest(
        ACTION.RESEND_ACTIVATION_LINK,
        DEFAULT_STATUS_INITIAL_STATE,
        ENDPOINT.RESEND_ACTIVATION_LINK,
        {email},
        context
    );

export const signUp = (username, email, password, context) =>
    apiPostRequest(
        ACTION.SIGN_UP,
        DEFAULT_STATUS_INITIAL_STATE,
        ENDPOINT.SIGN_UP,
        {username, email, password},
        context
    );

export const signIn = (username, password, context) =>
    apiPostRequest(ACTION.SIGN_IN, SIGN_IN_OUT_INITIAL_STATE, ENDPOINT.SIGN_IN, {username, password}, context);

export const signOut = context =>
    apiDeleteRequest(ACTION.SIGN_OUT, SIGN_IN_OUT_INITIAL_STATE, ENDPOINT.SIGN_OUT, context);

export const setSignInOut = (isAuthenticated, additionalPayload) => ({
    type: ACTION.SET_SIGN_IN_OUT,
    payload: {is_authenticated: isAuthenticated, ...additionalPayload},
});
