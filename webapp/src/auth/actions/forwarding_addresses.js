import {ACTION, ENDPOINT, INITIAL_STATE} from '../constants/forwarding_addresses';
import {apiDeleteRequest, apiGetRequest, apiPostRequest} from '../../app/utils/api/requests';

export const create = (requestBody, context) =>
    apiPostRequest(ACTION.CREATE, INITIAL_STATE, ENDPOINT.CREATE, requestBody, context);

export const retrieveAllList = (userId, context) =>
    apiGetRequest(ACTION.RETRIEVE_ALL_LIST, INITIAL_STATE, ENDPOINT.LIST_ALL(userId), context);

export const clearList = () => ({type: ACTION.CLEAR_LIST});

export const destroy = (userId, forwardingAddressId, context = {}) =>
    apiDeleteRequest(ACTION.DESTROY, INITIAL_STATE, ENDPOINT.DETAILS(userId, forwardingAddressId), {
        ...context,
        id: forwardingAddressId,
    });
