import {Field, Form, reduxForm, propTypes} from 'redux-form';
import React from 'react';
import {FormCard, FormField} from '../../../app/templates/__partials__/form';
import {submit, validate} from './process_email_update';

import ErrorAlert from '../../../app/templates/__partials__/error_alert';
import {FORM} from '../../constants/user';
import SubmitButton from '../../../app/templates/__partials__/submit_button';
import SuccessAlert from '../../../app/templates/__partials__/success_alert';

const AuthUserEmailUpdateForm = ({handleSubmit, valid, pristine, submitting, submitSucceeded, isSuccess, isError}) => (
    <FormCard>
        {(submitSucceeded || isSuccess) && (
            <SuccessAlert message="Przypisany do konta adres email został zaktualizowany pomyślnie." />
        )}
        {isError && <ErrorAlert />}
        <Form method="post" action="?" onSubmit={handleSubmit(submit)}>
            <Field
                id="password"
                name="password"
                type="password"
                label="Hasło"
                component={FormField}
                aria-labelledby="password"
            />
            <Field
                id="newEmail"
                name="newEmail"
                type="email"
                label="Nowy email"
                component={FormField}
                aria-labelledby="newEmail"
            />
            <SubmitButton
                className="mt-3"
                value="Zapisz"
                disabled={!valid || pristine || submitting}
                aria-label="Przycisk zapisz"
            />
        </Form>
    </FormCard>
);

AuthUserEmailUpdateForm.propTypes = propTypes;

export default reduxForm({form: FORM.UPDATE_EMAIL, validate, shouldValidate: () => process.env.__CLIENT__})(
    AuthUserEmailUpdateForm
);
