import {Field, Form, reduxForm, propTypes} from 'redux-form';
import React from 'react';
import {FormCard, FormField} from '../../../app/templates/__partials__/form';
import {submit, validate} from './process_destroy';

import ErrorAlert from '../../../app/templates/__partials__/error_alert';
import {FORM} from '../../constants/user';
import SubmitButton from '../../../app/templates/__partials__/submit_button';

const AuthUserDestroyForm = ({handleSubmit, valid, pristine, submitting, isError}) => (
    <FormCard>
        {isError && <ErrorAlert />}
        <Form method="post" action="?" onSubmit={handleSubmit(submit)}>
            <Field
                id="password"
                name="password"
                type="password"
                label="Hasło"
                component={FormField}
                aria-labelledby="password"
            />
            <Field
                id="confirmDeletion"
                name="confirmDeletion"
                type="checkbox"
                label="Potwierdzam, że chcę usunąć konto."
                component={FormField}
                value={value => !!value}
                aria-labelledby="confirmDeletion"
            />
            <SubmitButton
                className="mt-3"
                value="Usuń konto"
                disabled={!valid || pristine || submitting}
                aria-label="Przycisk usuń konto"
            />
        </Form>
    </FormCard>
);

AuthUserDestroyForm.propTypes = propTypes;

export default reduxForm({form: FORM.DESTROY_USER, validate, shouldValidate: () => process.env.__CLIENT__})(
    AuthUserDestroyForm
);
