import {SubmissionError} from 'redux-form';
import {validateFieldRequired, validatePassword} from '../../../app/utils/validation';
import {clearDetails as clearUserDetails, destroy} from '../../actions/user';
import {history} from '../../../app/utils/router';
import {setSignInOut} from '../../actions/activities';
import {STATUS} from '../../../app/utils/api/constants';
import {logDispatchAPIErrors} from '../../../app/utils/logger';

export const validate = ({confirmDeletion, password}) => {
    const errors = {};

    errors.password = validatePassword(password);
    errors.confirmDeletion = validateFieldRequired(confirmDeletion);

    return errors;
};

export const submit = async (values, dispatch) => {
    const {payload} = dispatch(await destroy(values.password));

    if (payload.errors) {
        // Password is the only field that can throw error, so password errors are payload.errors.
        throw new SubmissionError({password: payload.errors});
    } else if (payload.__meta__.status !== STATUS.OK) {
        throw SubmissionError;
    }

    dispatch(setSignInOut(false, {__meta__: payload.__meta__}));
    dispatch(clearUserDetails());
    history.push('/');
};

export const submitServer = async (request, response, dispatch) => {
    try {
        if (Object.values(validate(request.body)).every(value => !value)) {
            const {payload, context} = dispatch(await destroy(request.body.password, {headers: request.headers}));

            if (payload.__meta__.status === STATUS.OK) {
                const setCookieHeaders = context?.setCookieHeaders;
                if (setCookieHeaders) {
                    Object.values(setCookieHeaders).forEach(({name, value, ...options}) =>
                        response.cookie(name, value, options)
                    );
                }

                dispatch(setSignInOut(false, {__meta__: payload.__meta__}));
                dispatch(clearUserDetails());
                return () => response.redirect('/');
            }
        }
    } catch (error) {
        logDispatchAPIErrors(error);
    }

    return () => response.redirect('?error');
};
