import {Field, Form, reduxForm, propTypes} from 'redux-form';
import React from 'react';
import {FormCard, FormField} from '../../../app/templates/__partials__/form';

import ErrorAlert from '../../../app/templates/__partials__/error_alert';
import {FORM} from '../../constants/user';
import SubmitButton from '../../../app/templates/__partials__/submit_button';
import SuccessAlert from '../../../app/templates/__partials__/success_alert';
import {submit} from './process_newsletters_update';

const AuthUserNewslettersUpdateForm = ({
    handleSubmit,
    valid,
    pristine,
    submitting,
    submitSucceeded,
    isSuccess,
    isError,
}) => (
    <FormCard>
        {(submitSucceeded || isSuccess) && (
            <SuccessAlert message="Ustawienia newsletterów zostały zaktualizowane pomyślnie." />
        )}
        {isError && <ErrorAlert />}
        <Form method="post" action="?" onSubmit={handleSubmit(submit)}>
            <Field
                id="wantsNewsletters"
                name="wantsNewsletters"
                type="checkbox"
                label="Chcę otrzymywać maile informujące o nowych promocjach na stronie."
                component={FormField}
                value={value => !!value}
                aria-labelledby="wantsNewsletters"
            />
            <SubmitButton
                className="mt-3"
                value="Zapisz"
                disabled={!valid || pristine || submitting}
                aria-label="Przycisk zapisz"
            />
        </Form>
    </FormCard>
);

AuthUserNewslettersUpdateForm.propTypes = propTypes;

export default reduxForm({form: FORM.UPDATE_NEWSLETTERS, enableReinitialize: true})(AuthUserNewslettersUpdateForm);
