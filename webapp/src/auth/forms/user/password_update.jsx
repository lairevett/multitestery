import {Field, Form, reduxForm, propTypes} from 'redux-form';
import React from 'react';
import {FormCard, FormField} from '../../../app/templates/__partials__/form';
import {submit, validate} from './process_password_update';

import ErrorAlert from '../../../app/templates/__partials__/error_alert';
import {FORM} from '../../constants/user';
import SubmitButton from '../../../app/templates/__partials__/submit_button';

const AuthUserPasswordUpdateForm = ({handleSubmit, valid, pristine, submitting, isError}) => (
    <FormCard>
        {isError && <ErrorAlert />}
        <Form method="post" action="?" onSubmit={handleSubmit(submit)}>
            <Field
                id="oldPassword"
                name="oldPassword"
                type="password"
                label="Stare hasło"
                component={FormField}
                aria-labelledby="oldPassword"
            />
            <Field
                id="newPassword"
                name="newPassword"
                type="password"
                label="Nowe hasło"
                component={FormField}
                aria-labelledby="newPassword"
            />
            <Field
                id="repeatNewPassword"
                name="repeatNewPassword"
                type="password"
                label="Powtórz nowe hasło"
                component={FormField}
                aria-labelledby="repeatNewPassword"
            />
            <SubmitButton
                className="mt-3"
                value="Zapisz"
                disabled={!valid || pristine || submitting}
                aria-label="Przycisk zapisz"
            />
        </Form>
    </FormCard>
);

AuthUserPasswordUpdateForm.propTypes = propTypes;

export default reduxForm({form: FORM.UPDATE_PASSWORD, validate, shouldValidate: () => process.env.__CLIENT__})(
    AuthUserPasswordUpdateForm
);
