import {SubmissionError} from 'redux-form';
import {validateEmail, validatePassword} from '../../../app/utils/validation';
import {updateEmail} from '../../actions/user';
import {STATUS} from '../../../app/utils/api/constants';
import {logDispatchAPIErrors} from '../../../app/utils/logger';

export const validate = ({password, newEmail}) => {
    const errors = {};

    errors.password = validatePassword(password);
    errors.newEmail = validateEmail(newEmail);

    return errors;
};

export const submit = async ({password, newEmail}, dispatch) => {
    const {payload} = dispatch(await updateEmail(password, newEmail));

    if (payload.errors) {
        const {email, ...restOfErrors} = payload.errors;

        throw new SubmissionError({newEmail: email, ...restOfErrors});
    } else if (payload.__meta__.status !== STATUS.OK) {
        throw SubmissionError;
    }
};

export const submitServer = async (request, response, dispatch) => {
    try {
        if (Object.values(validate(request.body)).every(value => !value)) {
            const {password, newEmail} = request.body;
            const {payload} = dispatch(await updateEmail(password, newEmail, {headers: request.headers}));

            if (payload.__meta__.status === STATUS.OK) {
                return () => response.redirect('?success');
            }
        }
    } catch (error) {
        logDispatchAPIErrors(error);
    }

    return () => response.redirect('?error');
};
