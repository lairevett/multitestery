import {SubmissionError} from 'redux-form';
import {validateFieldEquals, validatePassword} from '../../../app/utils/validation';

import {history, retrieveUrl} from '../../../app/utils/router';
import {setSignInOut} from '../../actions/activities';
import {clearDetails as clearUserDetails, updatePassword} from '../../actions/user';
import {STATUS} from '../../../app/utils/api/constants';
import {logDispatchAPIErrors} from '../../../app/utils/logger';

export const validate = ({oldPassword, newPassword, repeatNewPassword}) => {
    const errors = {};

    errors.oldPassword = validatePassword(oldPassword);
    errors.newPassword = validatePassword(newPassword) || validateFieldEquals(newPassword, repeatNewPassword);
    errors.repeatNewPassword =
        validatePassword(repeatNewPassword) || validateFieldEquals(repeatNewPassword, newPassword);

    return errors;
};

export const submit = async ({oldPassword, newPassword}, dispatch) => {
    const {payload} = dispatch(await updatePassword(oldPassword, newPassword));

    if (payload.errors) {
        // OldPassword is the only field that can throw error, so password errors are payload.errors.
        throw new SubmissionError({oldPassword: payload.errors});
    } else if (payload.__meta__.status !== STATUS.OK) {
        throw SubmissionError;
    }

    dispatch(setSignInOut(false, {__meta__: payload.__meta__}));
    dispatch(clearUserDetails());
    history.push(retrieveUrl('authActivitiesSignIn').path);
};

export const submitServer = async (request, response, dispatch) => {
    try {
        if (Object.values(validate(request.body)).every(value => !value)) {
            const {oldPassword, newPassword} = request.body;
            const {payload, context} = dispatch(
                await updatePassword(oldPassword, newPassword, {headers: request.headers})
            );

            if (payload.__meta__.status === STATUS.OK) {
                const setCookieHeaders = context?.setCookieHeaders;
                if (setCookieHeaders) {
                    Object.values(setCookieHeaders).forEach(({name, value, ...options}) =>
                        response.cookie(name, value, options)
                    );
                }

                dispatch(setSignInOut(false, {__meta__: payload.__meta__}));
                dispatch(clearUserDetails());
                return () => response.redirect(retrieveUrl('authActivitiesSignIn').path);
            }
        }
    } catch (error) {
        logDispatchAPIErrors(error);
    }

    return () => response.redirect('?error');
};
