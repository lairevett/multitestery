import {SubmissionError} from 'redux-form';
import {updateWantsNewsletters} from '../../actions/user';
import {logDispatchAPIErrors} from '../../../app/utils/logger';
import {STATUS} from '../../../app/utils/api/constants';

export const submit = async ({wantsNewsletters}, dispatch) => {
    const {payload} = dispatch(await updateWantsNewsletters(wantsNewsletters));

    if (payload.errors) {
        // WantsNewsletters is the only field that can throw error, so password errors are payload.errors.
        throw new SubmissionError({wantsNewsletters: payload.errors});
    } else if (payload.__meta__.status !== STATUS.OK) {
        throw SubmissionError;
    }
};

export const submitServer = async (request, response, dispatch) => {
    try {
        const {wantsNewsletters} = request.body;
        const {payload} = dispatch(await updateWantsNewsletters(!!wantsNewsletters, {headers: request.headers}));

        if (payload.__meta__.status === STATUS.OK) {
            return () => response.redirect('?success');
        }
    } catch (error) {
        logDispatchAPIErrors(error);
    }

    return () => response.redirect('?error');
};
