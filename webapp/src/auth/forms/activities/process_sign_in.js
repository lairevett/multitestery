import {SubmissionError} from 'redux-form';
import {validatePassword, validateUsername} from '../../../app/utils/validation';

import {history} from '../../../app/utils/router';
import {retrieveDetails as retrieveUserDetails} from '../../actions/user';
import {signIn} from '../../actions/activities';
import {STATUS} from '../../../app/utils/api/constants';
import {logDispatchAPIErrors} from '../../../app/utils/logger';

export const validate = ({username, password}) => {
    const errors = {};

    errors.username = validateUsername(username);
    errors.password = validatePassword(password);

    return errors;
};

export const submit = async ({username, password}, dispatch) => {
    const {payload} = dispatch(await signIn(username, password));

    if (payload.errors) {
        throw new SubmissionError(payload.errors);
    } else if (payload.__meta__.status !== STATUS.OK) {
        throw SubmissionError;
    }

    dispatch(await retrieveUserDetails());
    history.push('/');
};

export const submitServer = async (request, response, dispatch) => {
    try {
        if (Object.values(validate(request.body)).every(value => !value)) {
            const {username, password} = request.body;
            const {payload, context} = dispatch(await signIn(username, password, {headers: request.headers}));

            if (payload.__meta__.status === STATUS.OK) {
                // Forward cookies from context to client.
                const {setCookieHeaders} = context;
                if (setCookieHeaders) {
                    Object.values(setCookieHeaders).forEach(({name, value, ...options}) =>
                        response.cookie(name, value, options)
                    );
                }

                return () => response.redirect('/');
            }
        }
    } catch (error) {
        logDispatchAPIErrors(error);
    }

    return () => response.redirect('?error');
};
