import React from 'react';
import {Form, reduxForm, propTypes} from 'redux-form';
import {submit} from './process_sign_out';
import {FORM} from '../../constants/activities';
import {retrieveUrl} from '../../../app/utils/router';

const AuthActivitiesSignOutForm = ({handleSubmit}) => (
    <Form method="post" action={retrieveUrl('authActivitiesSignOut').path} onSubmit={handleSubmit(submit)}>
        <button className="dropdown-item btn-link" type="submit">
            Wyloguj się
        </button>
    </Form>
);

AuthActivitiesSignOutForm.propTypes = propTypes;

export default reduxForm({form: FORM.SIGN_OUT})(AuthActivitiesSignOutForm);
