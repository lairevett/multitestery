import {Field, Form, propTypes, reduxForm} from 'redux-form';
import React from 'react';
import PropTypes from 'prop-types';
import {FormCard, FormField} from '../../../app/templates/__partials__/form';
import {submit, validate} from './process_resend_activation_link';
import ErrorAlert from '../../../app/templates/__partials__/error_alert';
import {FORM} from '../../constants/activities';
import SubmitButton from '../../../app/templates/__partials__/submit_button';
import SuccessAlert from '../../../app/templates/__partials__/success_alert';

const AuthActivitiesResendActivationLinkForm = ({
    handleSubmit,
    valid,
    pristine,
    submitting,
    submitSucceeded,
    submitFailed,
    isSuccess,
    isError,
}) => (
    <FormCard>
        {(submitSucceeded || isSuccess) && (
            <SuccessAlert message="Email z linkiem aktywacyjnym został wysłany pomyślnie." />
        )}
        {(submitFailed || isError) && (
            <ErrorAlert
                message="Próba wysłania linku aktywacyjnego nie powiodła się, twoje konto jest już aktywne lub użytkownik o
                podanym adresie email nie istnieje."
            />
        )}
        <Form method="post" action="?" onSubmit={handleSubmit(submit)}>
            <Field
                id="email"
                name="email"
                type="email"
                label="Adres email"
                component={FormField}
                aria-labelledby="email"
            />
            <SubmitButton
                className="mt-3"
                value="Wyślij"
                disabled={!valid || pristine || submitting}
                aria-label="Przycisk wyślij"
            />
        </Form>
    </FormCard>
);

AuthActivitiesResendActivationLinkForm.propTypes = {
    ...propTypes,
    isSuccess: PropTypes.bool.isRequired,
    isError: PropTypes.bool.isRequired,
};

export default reduxForm({
    form: FORM.RESEND_ACTIVATION_LINK,
    validate,
    shouldValidate: () => process.env.__CLIENT__,
})(AuthActivitiesResendActivationLinkForm);
