import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import AuthActivitiesActivateForm from '../activate';
import {render} from '../../../../app/utils/tests';
import {FORM} from '../../../constants/activities';
import * as router from '../../../../app/utils/router';
import {testSubmitButtonDisabledWhenSubmitting} from '../../../../app/utils/tests/form_buttons';
import {testMatchesLastSnapshot} from '../../../../app/utils/tests/snapshot';
import {
    testErrorAlertsOnIsError,
    testErrorAlertsOnSubmitFailed,
    testSuccessAlertsOnIsSuccess,
    testSuccessAlertsOnSubmitSucceeded,
} from '../../../../app/utils/tests/form_alerts';

describe('AuthActivitiesActivateForm', () => {
    const renderComponent = (isSuccess, isError, initialState = {}) =>
        render(<AuthActivitiesActivateForm isSuccess={isSuccess} isError={isError} />, {
            initialState,
            urls: ['authActivitiesSignIn'],
        });

    testMatchesLastSnapshot(() => renderComponent(true, true));
    testSuccessAlertsOnIsSuccess(renderComponent);
    testSuccessAlertsOnSubmitSucceeded(FORM.ACTIVATE, renderComponent);
    testErrorAlertsOnIsError(renderComponent);
    testErrorAlertsOnSubmitFailed(FORM.ACTIVATE, renderComponent);
    testSubmitButtonDisabledWhenSubmitting(FORM.ACTIVATE, 'Aktywuj', renderComponent);

    it('displays success alert with link to sign in page after successful submission', () => {
        const retrieveUrl = jest.spyOn(router, 'retrieveUrl');

        const {getByText} = renderComponent(false, false, {
            form: {
                [FORM.ACTIVATE]: {
                    submitSucceeded: true,
                },
            },
        });

        expect(retrieveUrl).toBeCalledWith('authActivitiesSignIn');

        // This is the sign in link's text content.
        expect(getByText('się zalogować').href).toBe(
            window.location.origin + router.retrieveUrl('authActivitiesSignIn').path
        );
    });
});
