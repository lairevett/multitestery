import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import {render} from '../../../../app/utils/tests';
import {testMatchesLastSnapshot} from '../../../../app/utils/tests/snapshot';
import {
    testSubmitButtonDisabledWhenFormIsNotValid,
    testSubmitButtonDisabledWhenFormIsPristine,
    testSubmitButtonDisabledWhenSubmitting,
} from '../../../../app/utils/tests/form_buttons';
import {FORM} from '../../../constants/activities';
import AuthActivitiesSignInForm from '../sign_in';
import {testErrorAlertsOnIsError, testErrorAlertsOnSubmitFailed} from '../../../../app/utils/tests/form_alerts';
import {fireEvent} from '@testing-library/react';

describe('AuthActivitiesSignInForm', () => {
    const submitButtonText = 'Zaloguj się';

    const renderComponent = (isSuccess, isError, initialState = {}) =>
        render(<AuthActivitiesSignInForm isSuccess={isSuccess} isError={isError} />, {initialState});

    const renderValidComponent = (isSuccess, isError, initialState) => {
        const component = renderComponent(isSuccess, isError, initialState);
        const {getByLabelText} = component;
        fireEvent.change(getByLabelText('Nazwa użytkownika'), {target: {value: 'johndoe'}});
        fireEvent.change(getByLabelText('Hasło'), {target: {value: 'password'}});
        return component;
    };

    testMatchesLastSnapshot(() => renderComponent(true, true));
    testErrorAlertsOnIsError(renderComponent);
    testErrorAlertsOnSubmitFailed(FORM.SIGN_IN, renderComponent);
    testSubmitButtonDisabledWhenFormIsNotValid(FORM.SIGN_IN, submitButtonText, renderComponent);
    testSubmitButtonDisabledWhenFormIsPristine(FORM.SIGN_IN, submitButtonText, renderComponent);
    testSubmitButtonDisabledWhenSubmitting(FORM.SIGN_IN, submitButtonText, renderValidComponent);
});
