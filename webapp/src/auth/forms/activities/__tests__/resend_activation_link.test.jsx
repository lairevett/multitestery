import React from 'react';
import {render} from '../../../../app/utils/tests';
import '@testing-library/jest-dom/extend-expect';
import AuthActivitiesResendActivationLinkForm from '../resend_activation_link';
import {FORM} from '../../../constants/activities';
import {
    testSubmitButtonDisabledWhenFormIsNotValid,
    testSubmitButtonDisabledWhenFormIsPristine,
    testSubmitButtonDisabledWhenSubmitting,
} from '../../../../app/utils/tests/form_buttons';
import {testMatchesLastSnapshot} from '../../../../app/utils/tests/snapshot';
import {
    testErrorAlertsOnIsError,
    testErrorAlertsOnSubmitFailed,
    testSuccessAlertsOnIsSuccess,
    testSuccessAlertsOnSubmitSucceeded,
} from '../../../../app/utils/tests/form_alerts';
import {fireEvent} from '@testing-library/react';

describe('AuthActivitiesResendActivationLinkForm', () => {
    const submitButtonText = 'Wyślij';

    const renderComponent = (isSuccess, isError, initialState = {}) =>
        render(<AuthActivitiesResendActivationLinkForm isSuccess={isSuccess} isError={isError} />, {initialState});

    const renderValidComponent = (isSuccess, isError, initialState) => {
        const component = renderComponent(isSuccess, isError, initialState);
        const {getByLabelText} = component;
        fireEvent.change(getByLabelText('Adres email'), {target: {value: 'johndoe@email.com'}});
        return component;
    };

    testMatchesLastSnapshot(() => renderComponent(true, true));
    testSuccessAlertsOnIsSuccess(renderComponent);
    testSuccessAlertsOnSubmitSucceeded(FORM.RESEND_ACTIVATION_LINK, renderComponent);
    testErrorAlertsOnIsError(renderComponent);
    testErrorAlertsOnSubmitFailed(FORM.RESEND_ACTIVATION_LINK, renderComponent);
    testSubmitButtonDisabledWhenFormIsNotValid(FORM.RESEND_ACTIVATION_LINK, submitButtonText, renderComponent);
    testSubmitButtonDisabledWhenFormIsPristine(FORM.RESEND_ACTIVATION_LINK, submitButtonText, renderComponent);
    testSubmitButtonDisabledWhenSubmitting(FORM.RESEND_ACTIVATION_LINK, submitButtonText, renderValidComponent);
});
