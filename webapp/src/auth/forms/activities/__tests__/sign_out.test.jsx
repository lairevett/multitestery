import React from 'react';
import {render} from '../../../../app/utils/tests';
import AuthActivitiesSignOutForm from '../sign_out';
import '@testing-library/jest-dom/extend-expect';
import {testMatchesLastSnapshot} from '../../../../app/utils/tests/snapshot';
import {retrieveUrl} from '../../../../app/utils/router';

describe('AuthActivitiesSignOutForm', () => {
    const submitButtonText = 'Wyloguj się';
    const renderComponent = () => render(<AuthActivitiesSignOutForm />, {urls: ['authActivitiesSignOut']});

    testMatchesLastSnapshot(renderComponent);

    it('uses post request while signing out', () => {
        const {getByText} = renderComponent();
        expect(getByText(submitButtonText).closest('form').method).toBe('post');
    });

    it('redirects user to sign out route', () => {
        const {getByText} = renderComponent();

        expect(getByText(submitButtonText).closest('form').action).toBe(
            window.location.origin + retrieveUrl('authActivitiesSignOut').path
        );
    });
});
