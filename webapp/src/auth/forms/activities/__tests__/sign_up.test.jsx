import React from 'react';
import {render} from '../../../../app/utils/tests';
import '@testing-library/jest-dom/extend-expect';
import {fireEvent} from '@testing-library/react';
import {FORM} from '../../../constants/activities';
import {
    testSubmitButtonDisabledWhenFormIsNotValid,
    testSubmitButtonDisabledWhenFormIsPristine,
    testSubmitButtonDisabledWhenSubmitting,
} from '../../../../app/utils/tests/form_buttons';
import {testMatchesLastSnapshot} from '../../../../app/utils/tests/snapshot';
import {
    testErrorAlertsOnIsError,
    testSuccessAlertsOnIsSuccess,
    testSuccessAlertsOnSubmitSucceeded,
} from '../../../../app/utils/tests/form_alerts';
import AuthActivitiesSignUpForm from '../sign_up';
import * as router from '../../../../app/utils/router';

describe('AuthActivitiesSignUpForm', () => {
    const submitButtonText = 'Stwórz konto';

    const renderComponent = (isSuccess, isError, initialState = {}) =>
        render(<AuthActivitiesSignUpForm isSuccess={isSuccess} isError={isError} />, {
            initialState,
            urls: ['authActivitiesResendActivationLink'],
        });

    const renderValidComponent = (isSuccess, isError, initialState) => {
        const component = renderComponent(isSuccess, isError, initialState);
        const {getByLabelText} = component;
        fireEvent.change(getByLabelText('Nazwa użytkownika'), {target: {value: 'johndoe'}});
        fireEvent.change(getByLabelText('Adres email'), {target: {value: 'johndoe@email.com'}});
        fireEvent.change(getByLabelText('Hasło'), {target: {value: 'password'}});
        fireEvent.click(getByLabelText('Akceptuję regulamin serwisu.'));
        return component;
    };

    testMatchesLastSnapshot(() => renderComponent(true, true));
    testSuccessAlertsOnIsSuccess(renderComponent);
    testSuccessAlertsOnSubmitSucceeded(FORM.SIGN_UP, renderComponent);
    testErrorAlertsOnIsError(renderComponent);
    testSubmitButtonDisabledWhenFormIsNotValid(FORM.SIGN_UP, submitButtonText, renderComponent);
    testSubmitButtonDisabledWhenFormIsPristine(FORM.SIGN_UP, submitButtonText, renderComponent);
    testSubmitButtonDisabledWhenSubmitting(FORM.SIGN_UP, submitButtonText, renderValidComponent);

    it('displays success alert with link to resend activation link page after successful submission', () => {
        const retrieveUrl = jest.spyOn(router, 'retrieveUrl');

        const {getByText} = renderComponent(false, false, {
            form: {
                [FORM.SIGN_UP]: {
                    submitSucceeded: true,
                },
            },
        });

        expect(retrieveUrl).toBeCalledWith('authActivitiesResendActivationLink');

        expect(getByText('wyślij link aktywacyjny jeszcze raz').href).toBe(
            window.location.origin + router.retrieveUrl('authActivitiesResendActivationLink').path
        );
    });
});
