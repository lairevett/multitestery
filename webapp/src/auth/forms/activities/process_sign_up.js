import {reset, SubmissionError} from 'redux-form';
import {validateEmail, validateFieldRequired, validatePassword, validateUsername} from '../../../app/utils/validation';

import {FORM} from '../../constants/activities';
import {signUp} from '../../actions/activities';
import {STATUS} from '../../../app/utils/api/constants';
import {logDispatchAPIErrors} from '../../../app/utils/logger';

export const validate = ({username, email, password, rules, recaptcha}) => {
    const errors = {};

    errors.username = validateUsername(username);
    errors.email = validateEmail(email);
    errors.password = validatePassword(password);
    errors.rules = validateFieldRequired(rules, 'Musisz zaakceptować regulamin serwisu.');

    if (process?.env?.JEST_WORKER_ID !== 'undefined') {
        // Skip captcha if in test mode.
        errors.recaptcha = validateFieldRequired(recaptcha, 'Musisz potwierdzić, że nie jesteś robotem.');
    }

    return errors;
};

export const submit = async ({username, email, password}, dispatch) => {
    const {payload} = dispatch(await signUp(username, email, password));

    if (payload.errors) {
        throw new SubmissionError(payload.errors);
    } else if (payload.__meta__.status !== STATUS.OK) {
        throw SubmissionError;
    }

    dispatch(reset(FORM.SIGN_UP));
};

export const submitServer = async (request, response, dispatch) => {
    try {
        if (Object.values(validate(request.body)).every(value => !value)) {
            const {username, email, password} = request.body;
            const {payload} = dispatch(await signUp(username, email, password, {headers: request.headers}));

            if (payload.__meta__.status === STATUS.OK) {
                return () => response.redirect('?success');
            }
        }
    } catch (error) {
        logDispatchAPIErrors(error);
    }

    return () => response.redirect('?error');
};
