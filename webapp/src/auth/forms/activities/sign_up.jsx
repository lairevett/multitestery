import {Field, Form, reduxForm, propTypes} from 'redux-form';
import {Link} from 'react-router-dom';
import React from 'react';
import PropTypes from 'prop-types';
import {FormCard, FormField} from '../../../app/templates/__partials__/form';
import {submit, validate} from './process_sign_up';
import ErrorAlert from '../../../app/templates/__partials__/error_alert';
import {FORM} from '../../constants/activities';
import SubmitButton from '../../../app/templates/__partials__/submit_button';
import SuccessAlert from '../../../app/templates/__partials__/success_alert';
import ReCAPTCHA from '../../templates/__partials__/recaptcha';
import {retrieveUrl} from '../../../app/utils/router';

const AuthActivitiesSignUpForm = ({handleSubmit, valid, pristine, submitting, submitSucceeded, isSuccess, isError}) => {
    const isTestMode = process?.env?.JEST_WORKER_ID !== 'undefined';

    return (
        <FormCard>
            {(submitSucceeded || isSuccess) && (
                <SuccessAlert>
                    Link aktywacyjny został wysłany pomyślnie. Jeśli nadal nie widzisz wiadomości, sprawdź folder spam
                    lub{' '}
                    <Link to={retrieveUrl('authActivitiesResendActivationLink').path}>
                        wyślij link aktywacyjny jeszcze raz
                    </Link>
                    .
                </SuccessAlert>
            )}
            {isError && <ErrorAlert />}
            <Form method="post" action="?" onSubmit={handleSubmit(submit)}>
                <Field
                    id="username"
                    name="username"
                    type="text"
                    label="Nazwa użytkownika"
                    component={FormField}
                    aria-labelledby="username"
                />
                <Field
                    id="email"
                    name="email"
                    type="email"
                    label="Adres email"
                    component={FormField}
                    aria-labelledby="email"
                />
                <Field
                    id="password"
                    name="password"
                    type="password"
                    label="Hasło"
                    component={FormField}
                    aria-labelledby="password"
                />
                <Field
                    id="rules"
                    name="rules"
                    type="checkbox"
                    label="Akceptuję regulamin serwisu."
                    component={FormField}
                    parse={value => !!value}
                    aria-labelledby="rules"
                />
                {isTestMode && (
                    <Field id="recaptcha" name="recaptcha" component={ReCAPTCHA} aria-label="Nie jestem robotem." />
                )}
                <SubmitButton
                    className="mt-3"
                    value="Stwórz konto"
                    disabled={!valid || pristine || submitting}
                    noScriptDisabled
                    aria-label="Przycisk stwórz konto"
                />
            </Form>
        </FormCard>
    );
};

AuthActivitiesSignUpForm.propTypes = {
    ...propTypes,
    isSuccess: PropTypes.bool.isRequired,
    isError: PropTypes.bool.isRequired,
};

export default reduxForm({form: FORM.SIGN_UP, validate, shouldValidate: () => process.env.__CLIENT__})(
    AuthActivitiesSignUpForm
);
