import {Field, Form, reduxForm, propTypes} from 'redux-form';
import {Link} from 'react-router-dom';
import React from 'react';
import PropTypes from 'prop-types';
import {FormCard, FormField} from '../../../app/templates/__partials__/form';
import {submit} from './process_activate';
import ErrorAlert from '../../../app/templates/__partials__/error_alert';
import {FORM} from '../../constants/activities';
import SubmitButton from '../../../app/templates/__partials__/submit_button';
import SuccessAlert from '../../../app/templates/__partials__/success_alert';
import {retrieveUrl} from '../../../app/utils/router';

const AuthActivitiesActivateForm = ({handleSubmit, submitting, submitSucceeded, submitFailed, isSuccess, isError}) => (
    <FormCard>
        {(submitSucceeded || isSuccess) && (
            <SuccessAlert>
                Aktywacja konta została zakończona pomyślnie, możesz teraz{' '}
                <Link to={retrieveUrl('authActivitiesSignIn').path}>się zalogować</Link>.
            </SuccessAlert>
        )}
        {(submitFailed || isError) && (
            <ErrorAlert message="Aktywacja konta nie powiodła się, twoje konto jest już aktywne lub link jest niepoprawny." />
        )}
        <Form method="post" action="?" onSubmit={handleSubmit(submit)}>
            <Field id="uid" name="uid" type="hidden" component={FormField} />
            <Field id="token" name="token" type="hidden" component={FormField} />
            <SubmitButton disabled={submitting} value="Aktywuj" aria-label="Przycisk aktywuj" />
        </Form>
    </FormCard>
);

AuthActivitiesActivateForm.propTypes = {
    ...propTypes,
    isSuccess: PropTypes.bool.isRequired,
    isError: PropTypes.bool.isRequired,
};

export default reduxForm({
    form: FORM.ACTIVATE,
    enableReinitialize: true,
})(AuthActivitiesActivateForm);
