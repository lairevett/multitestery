import {reset, SubmissionError} from 'redux-form';

import {FORM} from '../../constants/activities';
import {resendActivationLink} from '../../actions/activities';
import {validateEmail} from '../../../app/utils/validation';
import {STATUS} from '../../../app/utils/api/constants';
import {logDispatchAPIErrors} from '../../../app/utils/logger';

export const validate = ({email}) => ({email: validateEmail(email)});

export const submit = async ({email}, dispatch) => {
    const {payload} = dispatch(await resendActivationLink(email));

    if (payload.errors) {
        throw new SubmissionError(payload.errors);
    } else if (payload.__meta__.status !== STATUS.OK) {
        throw SubmissionError;
    }

    dispatch(reset(FORM.RESEND_ACTIVATION_LINK));
};

export const submitServer = async (request, response, dispatch) => {
    try {
        if (Object.values(validate(request.body)).every(value => !value)) {
            const {email} = request.body;
            const {payload} = dispatch(await resendActivationLink(email, {headers: request.headers}));

            if (payload.__meta__.status === STATUS.OK) {
                return () => response.redirect('?success');
            }
        }
    } catch (error) {
        logDispatchAPIErrors(error);
    }

    return () => response.redirect('?error');
};
