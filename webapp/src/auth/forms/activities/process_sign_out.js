import {SubmissionError} from 'redux-form';
import {signOut} from '../../actions/activities';
import {clearDetails as clearUserDetails} from '../../actions/user';
import {history, retrieveUrl} from '../../../app/utils/router';
import {STATUS} from '../../../app/utils/api/constants';
import {logDispatchAPIErrors} from '../../../app/utils/logger';

export const submit = async (_, dispatch) => {
    const {payload} = dispatch(await signOut());

    if (payload.__meta__.status !== STATUS.OK) {
        throw SubmissionError;
    }

    dispatch(clearUserDetails());
    history.push(retrieveUrl('authActivitiesSignIn').path);
};

export const submitServer = async (request, response, dispatch) => {
    try {
        const {payload, context} = dispatch(await signOut({headers: request.headers}));

        if (payload.__meta__.status === STATUS.OK) {
            dispatch(clearUserDetails());

            const setCookieHeaders = context?.setCookieHeaders;
            if (setCookieHeaders) {
                Object.values(setCookieHeaders).forEach(({name, value, ...options}) =>
                    response.cookie(name, value, options)
                );
            }

            return () => response.redirect(retrieveUrl('authActivitiesSignIn').path);
        }
    } catch (error) {
        logDispatchAPIErrors(error);
    }

    return () => response.redirect('/');
};
