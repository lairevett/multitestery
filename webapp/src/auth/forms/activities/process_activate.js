import {reset, SubmissionError} from 'redux-form';

import {FORM} from '../../constants/activities';
import {activate} from '../../actions/activities';
import {validateFieldRequired} from '../../../app/utils/validation';
import {STATUS} from '../../../app/utils/api/constants';
import {logDispatchAPIErrors} from '../../../app/utils/logger';

export const validate = ({uid, token}) => {
    const errors = {};

    errors.uid = validateFieldRequired(uid);
    errors.token = validateFieldRequired(token);

    return errors;
};

export const submit = async ({uid, token}, dispatch) => {
    const {payload} = dispatch(await activate(uid, token));

    if (payload.errors) {
        throw new SubmissionError(payload.errors);
    } else if (payload.__meta__.status !== STATUS.OK) {
        throw SubmissionError;
    }

    dispatch(reset(FORM.ACTIVATE));
};

export const submitServer = async (request, response, dispatch) => {
    try {
        if (Object.values(validate(request.body)).every(value => !value)) {
            const {uid, token} = request.body;
            const {payload} = dispatch(await activate(uid, token, {headers: request.headers}));

            if (payload.__meta__.status === STATUS.OK) {
                return () => response.redirect('?success');
            }
        }
    } catch (error) {
        logDispatchAPIErrors(error);
    }

    return () => response.redirect('?error');
};
