import {Field, Form, reduxForm, propTypes} from 'redux-form';

import React from 'react';
import {FORM} from '../../constants/forwarding_addresses';
import SubmitButton from '../../../app/templates/__partials__/submit_button';
import {submit} from './process_destroy';
import {FormField} from '../../../app/templates/__partials__/form';

const AuthForwardingAddressesDestroyForm = ({valid, submitting, handleSubmit}) => (
    <Form method="post" action="?" onSubmit={handleSubmit(submit)}>
        <Field id="userId" name="userId" type="hidden" parse={value => +value} component={FormField} />
        <Field
            id="forwardingAddressId"
            name="forwardingAddressId"
            type="hidden"
            parse={value => +value}
            component={FormField}
        />
        <SubmitButton className="card-link" value="Usuń" disabled={!valid || submitting} aria-label="Przycisk usuń" />
    </Form>
);

AuthForwardingAddressesDestroyForm.propTypes = propTypes;

export default reduxForm({form: FORM.DESTROY, enableReinitialize: true})(AuthForwardingAddressesDestroyForm);
