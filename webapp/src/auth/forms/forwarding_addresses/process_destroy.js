import {SubmissionError} from 'redux-form';
import {validateFieldMinValue, validateFieldRequired} from '../../../app/utils/validation';
import {destroy} from '../../actions/forwarding_addresses';
import {STATUS} from '../../../app/utils/api/constants';
import {logDispatchAPIErrors} from '../../../app/utils/logger';

export const validate = ({userId, forwardingAddressId}) => {
    const errors = {};

    errors.userId = validateFieldRequired(userId) || validateFieldMinValue(userId, 1);

    errors.forwardingAddressId =
        validateFieldRequired(forwardingAddressId) || validateFieldMinValue(forwardingAddressId, 1);

    return errors;
};

export const submit = async ({userId, forwardingAddressId}, dispatch) => {
    const {payload} = dispatch(await destroy(userId, forwardingAddressId));

    if (payload.errors) {
        throw new SubmissionError(payload.errors);
    } else if (payload.__meta__.status !== STATUS.OK) {
        throw SubmissionError;
    }
};

export const submitServer = async (request, response, dispatch) => {
    try {
        const {userId, forwardingAddressId} = request.body;
        const parsedUserId = +userId;
        const parsedForwardingAddressId = +forwardingAddressId;

        const parsedValues = {
            userId: parsedUserId,
            forwardingAddressId: parsedForwardingAddressId,
        };

        if (Object.values(validate(parsedValues)).every(value => !value)) {
            const {payload} = dispatch(
                await destroy(parsedUserId, parsedForwardingAddressId, {headers: request.headers})
            );

            if (payload.__meta__.status === STATUS.OK) {
                return () => response.redirect('?success');
            }
        }
    } catch (error) {
        logDispatchAPIErrors(error);
    }

    return () => response.redirect('?error');
};
