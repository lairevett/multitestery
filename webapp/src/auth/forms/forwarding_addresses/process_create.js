import {SubmissionError} from 'redux-form';
import {
    validateFieldContainsNoWhitespace,
    validateFieldMaxLength,
    validateFieldMinLength,
    validateFieldMinValue,
    validateFieldPhoneNumber,
    validateFieldPostalCode,
    validateFieldRequired,
    validateFieldStartsWithCapitalLetter,
} from '../../../app/utils/validation';

import {create} from '../../actions/forwarding_addresses';
import {history, retrieveUrl} from '../../../app/utils/router';
import {makeRequestBody} from '../../utils/forwarding_addresses';
import {STATUS} from '../../../app/utils/api/constants';
import {logDispatchAPIErrors} from '../../../app/utils/logger';

export const validate = ({firstName, lastName, phone, street, homeNumber, aptNumber, city, postalCode}) => {
    const errors = {};

    errors.firstName =
        validateFieldRequired(firstName) ||
        validateFieldContainsNoWhitespace(firstName) ||
        validateFieldStartsWithCapitalLetter(firstName) ||
        validateFieldMinLength(firstName, 3) ||
        validateFieldMaxLength(firstName, 30);

    errors.lastName =
        validateFieldRequired(lastName) ||
        validateFieldContainsNoWhitespace(lastName) ||
        validateFieldStartsWithCapitalLetter(lastName) ||
        validateFieldMinLength(lastName, 3) ||
        validateFieldMaxLength(lastName, 150);

    errors.phone =
        validateFieldRequired(phone) || validateFieldContainsNoWhitespace(phone) || validateFieldPhoneNumber(phone);

    errors.street =
        validateFieldRequired(street) || validateFieldMinLength(street, 3) || validateFieldMaxLength(street, 200);

    errors.homeNumber = validateFieldRequired(homeNumber);
    errors.aptNumber = aptNumber ? validateFieldMinValue(aptNumber, 1) : null;

    errors.city =
        validateFieldRequired(city) ||
        validateFieldStartsWithCapitalLetter(city) ||
        validateFieldMinLength(city, 3) ||
        validateFieldMaxLength(city, 35);

    errors.postalCode =
        validateFieldRequired(postalCode) ||
        validateFieldContainsNoWhitespace(postalCode) ||
        validateFieldMinLength(postalCode, 6) ||
        validateFieldMaxLength(postalCode, 6) ||
        validateFieldPostalCode(postalCode);

    return errors;
};

export const submit = async (values, dispatch) => {
    const requestBody = makeRequestBody(values);

    const {payload} = dispatch(await create(requestBody));
    if (payload.errors) {
        const {first_name, last_name, home_number, apt_number, postal_code, ...restOfErrors} = payload.errors;

        // Map field error names to their form input equivalents.
        throw new SubmissionError({
            firstName: first_name,
            lastName: last_name,
            homeNumber: home_number,
            aptNumber: apt_number,
            postalCode: postal_code,
            ...restOfErrors,
        });
    } else if (payload.__meta__.status !== STATUS.OK) {
        throw SubmissionError;
    }

    history.push(retrieveUrl('authForwardingAddressesAllList').path);
};

export const submitServer = async (request, response, dispatch) => {
    try {
        const {aptNumber, ...restOfValues} = request.body;
        const parsedValues = {
            aptNumber: aptNumber ? +aptNumber : null,
            ...restOfValues,
        };

        if (Object.values(validate(parsedValues)).every(value => !value)) {
            const requestBody = makeRequestBody(parsedValues);

            const {payload} = dispatch(await create(requestBody, {headers: request.headers}));
            if (payload.__meta__.status === STATUS.OK) {
                return () => response.redirect(retrieveUrl('authForwardingAddressesAllList').path);
            }
        }
    } catch (error) {
        logDispatchAPIErrors(error);
    }

    return () => response.redirect('?error');
};
