import {Field, Form, reduxForm, propTypes} from 'redux-form';
import React from 'react';
import {FormCard, FormField, FormHorizontalField} from '../../../app/templates/__partials__/form';
import {submit, validate} from './process_create';

import ErrorAlert from '../../../app/templates/__partials__/error_alert';
import {FORM} from '../../constants/forwarding_addresses';
import SubmitButton from '../../../app/templates/__partials__/submit_button';

const AuthForwardingAddressesCreateForm = ({handleSubmit, valid, pristine, submitting, isError}) => (
    <FormCard>
        {isError && <ErrorAlert />}
        <Form method="post" action="?" className="form" onSubmit={handleSubmit(submit)}>
            <Field
                id="firstName"
                name="firstName"
                type="text"
                label="Imię"
                component={FormField}
                aria-labelledby="firstName"
            />
            <Field
                id="lastName"
                name="lastName"
                type="text"
                label="Nazwisko"
                component={FormField}
                aria-labelledby="lastName"
            />
            <Field
                id="phone"
                name="phone"
                type="tel"
                label="Numer telefonu"
                placeholder="np. +48123456789"
                component={FormField}
                aria-labelledby="phone"
            />
            <Field id="street" name="street" type="text" label="Ulica" component={FormField} aria-labelledby="street" />
            <Field
                id="homeNumber"
                name="homeNumber"
                type="text"
                label="Numer bloku/domu"
                component={FormHorizontalField}
                aria-labelledby="homeNumber"
            />
            <Field
                id="aptNumber"
                name="aptNumber"
                type="number"
                label="Numer mieszkania"
                component={FormHorizontalField}
                parse={value => (value ? +value : undefined)}
                aria-labelledby="aptNumber"
            />
            <Field id="city" name="city" type="text" label="Miasto" component={FormField} aria-labelledby="city" />
            <Field
                id="postalCode"
                name="postalCode"
                type="text"
                label="Kod pocztowy"
                component={FormField}
                aria-labelledby="postalCode"
            />
            <SubmitButton
                className="mt-3"
                value="Dodaj"
                disabled={!valid || pristine || submitting}
                aria-label="Przycisk dodaj"
            />
        </Form>
    </FormCard>
);

AuthForwardingAddressesCreateForm.propTypes = propTypes;

export default reduxForm({form: FORM.CREATE, validate, shouldValidate: () => process.env.__CLIENT__})(
    AuthForwardingAddressesCreateForm
);
