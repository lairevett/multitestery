/* eslint-disable react/prop-types */
import React from 'react';

import {Redirect} from 'react-router';
import AuthActivitiesActivate from '../templates/activities/activate';
import Content from '../../app/templates/__partials__/content';
import AuthActivitiesResendActivationLink from '../templates/activities/resend_activation_link';
import AuthActivitiesSignIn from '../templates/activities/sign_in';
import AuthActivitiesSignUp from '../templates/activities/sign_up';
import {submitServer as submitServerActivate} from '../forms/activities/process_activate';
import {submitServer as submitServerResendActivationLink} from '../forms/activities/process_resend_activation_link';
import {submitServer as submitServerSignUp} from '../forms/activities/process_sign_up';
import {submitServer as submitServerSignIn} from '../forms/activities/process_sign_in';
import {submitServer as submitServerSignOut} from '../forms/activities/process_sign_out';

/*
 * GET /activate/:uid/:token
 * POST /activate/:uid/:token
 */
export const AuthActivitiesActivateView = ({title, urlParameters, queryParameters, state}) => {
    const {uid, token} = urlParameters;

    const isSuccess = 'success' in queryParameters;
    const isError = 'error' in queryParameters;

    const {status} = state.auth.activities.activate.__meta__;

    return (
        <Content status={status} title={title} ignoreBadRequest>
            <AuthActivitiesActivate uid={uid} token={token} isSuccess={isSuccess} isError={isError} />
        </Content>
    );
};

AuthActivitiesActivateView.willDispatchEvents = (request, response, urlProperties, state, dispatch) => {
    if (request.method === 'POST') {
        return submitServerActivate(request, response, dispatch);
    }
};

/*
 * GET /resend-activation-link
 * POST /resend-activation-link
 */
export const AuthActivitiesResendActivationLinkView = ({title, queryParameters, state}) => {
    const isSuccess = 'success' in queryParameters;
    const isError = 'error' in queryParameters;

    const {status} = state.auth.activities.resendActivationLink.__meta__;

    return (
        <Content status={status} title={title} ignoreBadRequest>
            <AuthActivitiesResendActivationLink isSuccess={isSuccess} isError={isError} />
        </Content>
    );
};

AuthActivitiesResendActivationLinkView.willDispatchEvents = (request, response, urlProperties, state, dispatch) => {
    if (request.method === 'POST') {
        return submitServerResendActivationLink(request, response, dispatch);
    }
};

/*
 * GET /sign-up
 * POST /sign-up
 */
export const AuthActivitiesSignUpView = ({title, queryParameters}) => {
    const isSuccess = 'success' in queryParameters;
    const isError = 'error' in queryParameters;

    const {status} = state => state.auth.activities.signUp.__meta__;

    return (
        <Content status={status} title={title} ignoreBadRequest>
            <AuthActivitiesSignUp isSuccess={isSuccess} isError={isError} />
        </Content>
    );
};

AuthActivitiesSignUpView.willDispatchEvents = (request, response, urlProperties, state, dispatch) => {
    if (request.method === 'POST') {
        return submitServerSignUp(request, response, dispatch);
    }
};

/*
 * GET /sign-in
 * POST /sign-in
 */
export const AuthActivitiesSignInView = ({title, queryParameters, state}) => {
    const isError = 'error' in queryParameters;

    const {status} = state.auth.activities.signInOut.__meta__;

    return (
        <Content status={status} title={title} ignoreBadRequest>
            <AuthActivitiesSignIn isError={isError} />
        </Content>
    );
};

AuthActivitiesSignInView.willDispatchEvents = (request, response, urlProperties, state, dispatch) => {
    if (request.method === 'POST') {
        return submitServerSignIn(request, response, dispatch);
    }
};

/*
 * POST /sign-out
 */
export const AuthActivitiesSignOutView = () => <Redirect to="/" />;

AuthActivitiesSignOutView.willDispatchEvents = (request, response, urlProperties, state, dispatch) => {
    if (request.method === 'POST') {
        return submitServerSignOut(request, response, dispatch);
    }
};
