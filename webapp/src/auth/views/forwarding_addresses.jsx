/* eslint-disable react/prop-types */
import React from 'react';

import {Redirect} from 'react-router-dom';
import {APP_MAX_FORWARDING_ADDRESSES} from '../../app/constants';
import Content from '../../app/templates/__partials__/content';
import AuthForwardingAddressesCreate from '../templates/forwarding_addresses/create';
import AuthForwardingAddressesList from '../templates/forwarding_addresses/list';
import {submitServer as submitServerCreateForwardingAddress} from '../forms/forwarding_addresses/process_create';
import {submitServer as submitServerDeleteForwardingAddress} from '../forms/forwarding_addresses/process_destroy';
import Suspense from '../../app/utils/api/suspense';
import Loading from '../../app/templates/__partials__/loading';
import Menu from '../templates/__partials__/menu';
import {retrieveUrl} from '../../app/utils/router';

/*
 * GET /settings/forwarding-addresses/create
 * POST /settings/forwarding-addresses/create
 */
export const AuthForwardingAddressesCreateView = ({title, queryParameters, state}) => {
    const isError = 'error' in queryParameters;

    const {
        results,
        __meta__: {status},
    } = state.auth.forwardingAddresses;

    return (
        <Content status={status} title={title} menu={<Menu />}>
            <Suspense status={status} fallback={<Loading />}>
                {results.length <= APP_MAX_FORWARDING_ADDRESSES ? (
                    <AuthForwardingAddressesCreate isError={isError} />
                ) : (
                    <Redirect to={retrieveUrl('authForwardingAddressesAllList').path} />
                )}
            </Suspense>
        </Content>
    );
};

AuthForwardingAddressesCreateView.willDispatchEvents = (request, response, urlProperties, state, dispatch) => {
    if (request.method === 'POST') {
        return submitServerCreateForwardingAddress(request, response, dispatch);
    }
};

/*
 * GET /settings/forwarding-addresses
 * POST /settings/forwarding-addresses
 */
export const AuthForwardingAddressesListView = ({title, queryParameters, state}) => {
    const isError = 'error' in queryParameters;

    const userId = state.auth.user.id;

    const {
        results,
        __meta__: {status},
    } = state.auth.forwardingAddresses;

    return (
        <Content status={status} title={title} menu={<Menu />}>
            <Suspense status={status} fallback={<Loading />}>
                <AuthForwardingAddressesList userId={userId} list={results} isError={isError} />
            </Suspense>
        </Content>
    );
};

AuthForwardingAddressesListView.willDispatchEvents = (request, response, urlProperties, state, dispatch) => {
    if (request.method === 'POST') {
        return submitServerDeleteForwardingAddress(request, response, dispatch);
    }
};
