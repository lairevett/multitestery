/* eslint-disable react/prop-types */
import React from 'react';

import Content from '../../app/templates/__partials__/content';
import AuthUserDestroy from '../templates/user/destroy';
import AuthUserNewslettersUpdate from '../templates/user/newsletters_update';
import AuthUserEmailUpdate from '../templates/user/email_update';
import AuthUserPasswordUpdate from '../templates/user/password_update';
import {submitServer as submitServerChangeEmail} from '../forms/user/process_email_update';
import {submitServer as submitServerChangePassword} from '../forms/user/process_password_update';
import {submitServer as submitServerDeleteUser} from '../forms/user/process_destroy';
import {submitServer as submitServerToggleNewsletters} from '../forms/user/process_newsletters_update';
import Menu from '../templates/__partials__/menu';

/*
 * GET /settings/email
 * POST /settings/email
 */
export const AuthUserEmailUpdateView = ({title, queryParameters, state}) => {
    const isSuccess = 'success' in queryParameters;
    const isError = 'error' in queryParameters;

    const {status} = state.auth.user.__meta__;

    return (
        <Content status={status} title={title} menu={<Menu />}>
            <AuthUserEmailUpdate isSuccess={isSuccess} isError={isError} />
        </Content>
    );
};

AuthUserEmailUpdateView.willDispatchEvents = (request, response, urlProperties, state, dispatch) => {
    if (request.method === 'POST') {
        return submitServerChangeEmail(request, response, dispatch);
    }
};

/*
 * GET /settings/password
 * POST /settings/password
 */
export const AuthUserPasswordUpdateView = ({title, queryParameters, state}) => {
    const isError = 'error' in queryParameters;

    const {status} = state.auth.user.__meta__;

    return (
        <Content status={status} title={title} menu={<Menu />}>
            <AuthUserPasswordUpdate isError={isError} />
        </Content>
    );
};

AuthUserPasswordUpdateView.willDispatchEvents = (request, response, urlProperties, state, dispatch) => {
    if (request.method === 'POST') {
        return submitServerChangePassword(request, response, dispatch);
    }
};

/*
 * GET /settings/newsletters
 * POST /settings/newsletters
 */
export const AuthUserNewslettersUpdateView = ({title, queryParameters, state}) => {
    const isSuccess = 'success' in queryParameters;
    const isError = 'error' in queryParameters;

    const {
        wants_newsletters,
        __meta__: {status},
    } = state.auth.user;

    return (
        <Content status={status} title={title} menu={<Menu />}>
            <AuthUserNewslettersUpdate wantsNewsletters={wants_newsletters} isSuccess={isSuccess} isError={isError} />
        </Content>
    );
};

AuthUserNewslettersUpdateView.willDispatchEvents = (request, response, urlProperties, state, dispatch) => {
    if (request.method === 'POST') {
        return submitServerToggleNewsletters(request, response, dispatch);
    }
};

/*
 * GET /settings/delete-user
 * POST /settings/delete-user
 */
export const AuthUserDestroyView = ({title, queryParameters, state}) => {
    const isError = 'error' in queryParameters;

    const {
        is_staff,
        __meta__: {status},
    } = state.auth.user;

    return (
        <Content status={status} title={title} menu={<Menu />}>
            <AuthUserDestroy isStaff={is_staff} isError={isError} />
        </Content>
    );
};

AuthUserDestroyView.willDispatchEvents = (request, response, urlProperties, state, dispatch) => {
    if (request.method === 'POST') {
        return submitServerDeleteUser(request, response, dispatch);
    }
};
