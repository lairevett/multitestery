import {ACTION, INITIAL_STATE} from '../constants/forwarding_addresses';

const authForwardingAddressesReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ACTION.RETRIEVE_ALL_LIST:
            return {
                ...state,
                ...action.payload,
            };
        case ACTION.CLEAR_LIST:
            return INITIAL_STATE;
        case ACTION.DESTROY:
            return {
                ...state,
                results: state.results.filter(result => result.id !== action.context.id),
                __meta__: {...state.__meta__, ...action.payload.__meta__},
            };
        default:
            return state;
    }
};

export default authForwardingAddressesReducer;
