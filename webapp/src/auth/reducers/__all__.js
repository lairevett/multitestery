import {combineReducers} from 'redux';
import authActivitiesReducer from './activities';
import authForwardingAddressesReducer from './forwarding_addresses';
import authUserReducer from './user';

const authReducer = combineReducers({
    activities: authActivitiesReducer,
    user: authUserReducer,
    forwardingAddresses: authForwardingAddressesReducer,
});

export default authReducer;
