import {ACTION, INITIAL_STATE, SIGN_IN_OUT_INITIAL_STATE} from '../constants/activities';

const authActivitiesReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ACTION.ACTIVATE:
            return {
                ...state,
                activate: action.payload,
            };
        case ACTION.RESEND_ACTIVATION_LINK:
            return {
                ...state,
                resendActivationLink: action.payload,
            };
        case ACTION.SIGN_UP:
            return {
                ...state,
                signUp: action.payload,
            };
        case ACTION.SIGN_IN:
        case ACTION.SET_SIGN_IN_OUT:
            return {
                ...state,
                signInOut: action.payload,
            };
        case ACTION.SIGN_OUT:
            return {
                ...state,
                signInOut: SIGN_IN_OUT_INITIAL_STATE,
            };
        default:
            return state;
    }
};

export default authActivitiesReducer;
