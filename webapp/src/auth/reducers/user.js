import {ACTION, DETAILS_INITIAL_STATE as INITIAL_STATE} from '../constants/user';

const authUserReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ACTION.UPDATE_EMAIL:
        case ACTION.UPDATE_PASSWORD:
        case ACTION.UPDATE_WANTS_NEWSLETTERS:
            if (action.payload.errors) {
                return state;
            }

            return {
                ...state,
                ...action.payload,
            };
        case ACTION.RETRIEVE_DETAILS:
            return {
                ...state,
                ...action.payload,
            };
        case ACTION.CLEAR_DETAILS:
            return INITIAL_STATE;
        default:
            return state;
    }
};

export default authUserReducer;
