import {makeDispatchEvents} from '../../app/utils/dispatch_events';
import {clearList, retrieveAllList} from '../actions/forwarding_addresses';

export const callbackGetDispatchEvents = () => (urlParameters, queryParameters, state, context) => {
    const userId = state.auth.user.id;

    return makeDispatchEvents([() => retrieveAllList(userId, context)], [clearList]);
};

export const makeRequestBody = values => {
    const {firstName, lastName, homeNumber, aptNumber, postalCode, ...restOfValues} = values;

    return {
        first_name: firstName,
        last_name: lastName,
        home_number: homeNumber,
        apt_number: aptNumber,
        postal_code: postalCode,
        ...restOfValues,
    };
};
