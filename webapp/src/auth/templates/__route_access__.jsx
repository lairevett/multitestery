import React, {memo} from 'react';
import {Redirect, Route} from 'react-router-dom';
import {useSelector} from 'react-redux';
import PropTypes from 'prop-types';
import {
    ADMIN_REQUIRED_ROUTE_REDIRECT_URL,
    AUTH_DISALLOWED_ROUTE_REDIRECT_URL,
    AUTH_REQUIRED_ROUTE_REDIRECT_URL,
} from '../../app/constants';
import {STATUS} from '../../app/utils/api/constants';

export const AuthRequiredRoute = memo(({render, ...rest}) => {
    const isAuthenticated = useSelector(state => state.auth.activities.signInOut.is_authenticated);

    return (
        <Route {...rest} render={isAuthenticated ? render : () => <Redirect to={AUTH_REQUIRED_ROUTE_REDIRECT_URL} />} />
    );
});

AuthRequiredRoute.propTypes = {
    render: PropTypes.func.isRequired,
};

export const AuthDisallowedRoute = memo(({render, ...rest}) => {
    const isAuthenticated = useSelector(state => state.auth.activities.signInOut.is_authenticated);

    return (
        <Route
            {...rest}
            render={!isAuthenticated ? render : () => <Redirect to={AUTH_DISALLOWED_ROUTE_REDIRECT_URL} />}
        />
    );
});

AuthDisallowedRoute.propTypes = {
    render: PropTypes.func.isRequired,
};

export const AdminRequiredRoute = memo(({render, ...rest}) => {
    const isAuthenticated = useSelector(state => state.auth.activities.signInOut.is_authenticated);
    const user = useSelector(state => state.auth.user);

    return isAuthenticated ? (
        user.__meta__.status === STATUS.OK && (
            <Route
                {...rest}
                render={user.is_staff ? render : () => <Redirect to={ADMIN_REQUIRED_ROUTE_REDIRECT_URL} />}
            />
        )
    ) : (
        <Redirect to={ADMIN_REQUIRED_ROUTE_REDIRECT_URL} />
    );
});

AdminRequiredRoute.propTypes = {
    render: PropTypes.func.isRequired,
};
