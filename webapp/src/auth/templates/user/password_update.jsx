import React from 'react';
import PropTypes from 'prop-types';
import AuthUserPasswordUpdateForm from '../../forms/user/password_update';

const AuthUserPasswordUpdate = ({isError}) => <AuthUserPasswordUpdateForm isError={isError} />;

AuthUserPasswordUpdate.propTypes = {
    isError: PropTypes.bool.isRequired,
};

export default AuthUserPasswordUpdate;
