import PropTypes from 'prop-types';
import React from 'react';
import AuthUserNewslettersUpdateForm from '../../forms/user/newsletters_update';

const AuthUserNewslettersUpdate = ({wantsNewsletters, isSuccess, isError}) => (
    <AuthUserNewslettersUpdateForm isSuccess={isSuccess} isError={isError} initialValues={{wantsNewsletters}} />
);

AuthUserNewslettersUpdate.propTypes = {
    wantsNewsletters: PropTypes.bool.isRequired,
    isSuccess: PropTypes.bool.isRequired,
    isError: PropTypes.bool.isRequired,
};

export default AuthUserNewslettersUpdate;
