import PropTypes from 'prop-types';
import React from 'react';
import AuthUserDestroyForm from '../../forms/user/destroy';

const AuthUserDestroy = ({isStaff, isError}) =>
    isStaff ? <p>Administrator nie może usunąć swojego konta.</p> : <AuthUserDestroyForm isError={isError} />;

AuthUserDestroy.propTypes = {
    isStaff: PropTypes.bool.isRequired,
    isError: PropTypes.bool.isRequired,
};

export default AuthUserDestroy;
