import PropTypes from 'prop-types';
import React from 'react';
import AuthUserEmailUpdateForm from '../../forms/user/email_update';

const AuthUserEmailUpdate = ({isSuccess, isError}) => (
    <AuthUserEmailUpdateForm isSuccess={isSuccess} isError={isError} />
);

AuthUserEmailUpdate.propTypes = {
    isSuccess: PropTypes.bool.isRequired,
    isError: PropTypes.bool.isRequired,
};

export default AuthUserEmailUpdate;
