import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import {render} from '../../../../app/utils/tests';
import {testMatchesLastSnapshot} from '../../../../app/utils/tests/snapshot';
import AuthActivitiesSignUp from '../sign_up';

describe('AuthActivitiesSignUp', () => {
    const renderComponent = (isSuccess, isError) =>
        render(<AuthActivitiesSignUp isSuccess={isSuccess} isError={isError} />, {
            urls: ['authActivitiesResendActivationLink'],
        });

    testMatchesLastSnapshot(() => renderComponent(true, true));
});
