import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import AuthActivitiesActivate from '../activate';
import {render} from '../../../../app/utils/tests';
import {testMatchesLastSnapshot} from '../../../../app/utils/tests/snapshot';

describe('AuthActivitiesActivate', () => {
    const renderComponent = (uid, token, isSuccess = false, isError = false) =>
        render(<AuthActivitiesActivate uid={uid} token={token} isSuccess={isSuccess} isError={isError} />, {
            urls: ['authActivitiesSignIn'],
        });

    testMatchesLastSnapshot(() => renderComponent('uid', 'token', true, true));

    it("sets form's uid initial value", () => {
        const uid = 'hidden uid value';
        const {container} = renderComponent(uid, 'hidden token value');
        expect(container.querySelector('#uid').value).toBe(uid);
    });

    it("sets form's token initial value", () => {
        const token = 'hidden token value';
        const {container} = renderComponent('hidden uid value', token);
        expect(container.querySelector('#token').value).toBe(token);
    });
});
