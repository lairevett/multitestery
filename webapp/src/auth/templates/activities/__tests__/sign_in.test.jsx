import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import {render} from '../../../../app/utils/tests';
import AuthActivitiesSignIn from '../sign_in';
import {testMatchesLastSnapshot} from '../../../../app/utils/tests/snapshot';

describe('AuthActivitiesSignIn', () => {
    const renderComponent = isError => render(<AuthActivitiesSignIn isError={isError} />);

    testMatchesLastSnapshot(() => renderComponent(true));
});
