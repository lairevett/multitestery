import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import {render} from '../../../../app/utils/tests';
import AuthActivitiesResendActivationLink from '../resend_activation_link';
import {testMatchesLastSnapshot} from '../../../../app/utils/tests/snapshot';

describe('AuthActivitiesResendActivationLink', () => {
    const renderComponent = (isSuccess, isError) =>
        render(<AuthActivitiesResendActivationLink isSuccess={isSuccess} isError={isError} />);

    testMatchesLastSnapshot(() => renderComponent(true, true));
});
