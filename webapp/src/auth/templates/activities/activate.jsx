import PropTypes from 'prop-types';
import React from 'react';
import AuthActivitiesActivateForm from '../../forms/activities/activate';

const AuthActivitiesActivate = ({uid, token, isSuccess, isError}) => (
    <AuthActivitiesActivateForm initialValues={{uid, token}} isSuccess={isSuccess} isError={isError} />
);

AuthActivitiesActivate.propTypes = {
    uid: PropTypes.string.isRequired,
    token: PropTypes.string.isRequired,
    isSuccess: PropTypes.bool.isRequired,
    isError: PropTypes.bool.isRequired,
};

export default AuthActivitiesActivate;
