import PropTypes from 'prop-types';
import React from 'react';
import AuthActivitiesResendActivationLinkForm from '../../forms/activities/resend_activation_link';

const AuthActivitiesResendActivationLink = ({isSuccess, isError}) => (
    <AuthActivitiesResendActivationLinkForm isSuccess={isSuccess} isError={isError} />
);

AuthActivitiesResendActivationLink.propTypes = {
    isSuccess: PropTypes.bool.isRequired,
    isError: PropTypes.bool.isRequired,
};

export default AuthActivitiesResendActivationLink;
