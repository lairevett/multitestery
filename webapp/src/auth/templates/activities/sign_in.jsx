import PropTypes from 'prop-types';
import React from 'react';
import AuthActivitiesSignInForm from '../../forms/activities/sign_in';

const AuthActivitiesSignIn = ({isError}) => <AuthActivitiesSignInForm isError={isError} />;

AuthActivitiesSignIn.propTypes = {
    isError: PropTypes.bool.isRequired,
};

export default AuthActivitiesSignIn;
