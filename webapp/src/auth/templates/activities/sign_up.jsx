import PropTypes from 'prop-types';
import React from 'react';
import AuthActivitiesSignUpForm from '../../forms/activities/sign_up';

const AuthActivitiesSignUp = ({isSuccess, isError}) => (
    <AuthActivitiesSignUpForm isSuccess={isSuccess} isError={isError} />
);

AuthActivitiesSignUp.propTypes = {
    isSuccess: PropTypes.bool.isRequired,
    isError: PropTypes.bool.isRequired,
};

export default AuthActivitiesSignUp;
