import PropTypes from 'prop-types';
import React from 'react';
import {DETAILS_PROP_TYPES} from '../../../constants/forwarding_addresses';

const ForwardingAddressCardInfo = ({
    details: {first_name, last_name, phone, street, home_number, apt_number, postal_code, city},
}) => (
    <>
        <div className="card-title">
            <h5>
                {first_name} {last_name}
            </h5>
        </div>
        <h6 className="card-subtitle text-muted mb-2">{phone}</h6>
        <p className="card-text">
            {street} {home_number}
            {apt_number && ` m. ${apt_number}`},<br />
            {postal_code} {city}
        </p>
    </>
);

ForwardingAddressCardInfo.propTypes = {
    details: DETAILS_PROP_TYPES(PropTypes).isRequired,
};

export default ForwardingAddressCardInfo;
