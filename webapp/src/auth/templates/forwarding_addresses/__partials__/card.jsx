import PropTypes from 'prop-types';
import React from 'react';

const ForwardingAddressCard = ({children}) => (
    <div className="card col-sm-12 col-md-5 mb-3">
        <div className="card-body">{children}</div>
    </div>
);

ForwardingAddressCard.propTypes = {
    children: PropTypes.node.isRequired,
};

export default ForwardingAddressCard;
