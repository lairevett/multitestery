import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import React from 'react';
import {APP_MAX_FORWARDING_ADDRESSES} from '../../../../app/constants';
import {retrieveUrl} from '../../../../app/utils/router';

const ForwardingAddressCreateButton = ({count}) => (
    <div
        className="col-sm-12 text-right"
        data-toggle="tooltip"
        data-placement="bottom"
        title={
            count >= APP_MAX_FORWARDING_ADDRESSES ? 'Osiągnięto maksymalną ilość adresów przypisanych do konta.' : ''
        }
    >
        <Link
            to={retrieveUrl('authForwardingAddressesCreate').path}
            className={`btn btn-primary${count >= APP_MAX_FORWARDING_ADDRESSES ? ' disabled' : ''}`}
        >
            Dodaj adres
        </Link>
    </div>
);

ForwardingAddressCreateButton.propTypes = {
    count: PropTypes.number.isRequired,
};

export default ForwardingAddressCreateButton;
