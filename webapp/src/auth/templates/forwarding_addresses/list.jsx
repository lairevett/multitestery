import React from 'react';

import PropTypes from 'prop-types';
import ForwardingAddressCreateButton from './__partials__/create_button';
import AuthForwardingAddressesDestroyForm from '../../forms/forwarding_addresses/destroy';
import ErrorAlert from '../../../app/templates/__partials__/error_alert';
import ForwardingAddressCard from './__partials__/card';
import ForwardingAddressCardInfo from './__partials__/card_info';
import {LIST_PROP_TYPES} from '../../constants/forwarding_addresses';

const AuthForwardingAddressesList = ({userId, list, isError}) => (
    <>
        {isError && <ErrorAlert message="Coś poszło nie tak, spróbuj ponownie później." />}
        <div className="row no-gutters justify-content-md-between">
            {list.length > 0 ? (
                list.map(details => (
                    <ForwardingAddressCard key={`_${details.id}`}>
                        <ForwardingAddressCardInfo details={details} />
                        <AuthForwardingAddressesDestroyForm initialValues={{userId, forwardingAddressId: details.id}} />
                    </ForwardingAddressCard>
                ))
            ) : (
                <p>Nie posiadasz aktualnie żadnych adresów przypisanych do konta.</p>
            )}
            <ForwardingAddressCreateButton count={list.length} />
        </div>
    </>
);

AuthForwardingAddressesList.propTypes = {
    userId: PropTypes.number.isRequired,
    list: LIST_PROP_TYPES(PropTypes).isRequired,
    isError: PropTypes.bool.isRequired,
};

export default AuthForwardingAddressesList;
