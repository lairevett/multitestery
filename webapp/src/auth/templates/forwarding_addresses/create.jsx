import PropTypes from 'prop-types';
import React from 'react';
import AuthForwardingAddressesCreateForm from '../../forms/forwarding_addresses/create';

const AuthForwardingAddressesCreate = ({isError}) => <AuthForwardingAddressesCreateForm isError={isError} />;

AuthForwardingAddressesCreate.propTypes = {
    isError: PropTypes.bool.isRequired,
};

export default AuthForwardingAddressesCreate;
