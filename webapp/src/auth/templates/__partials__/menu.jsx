import React from 'react';
import MenuList from '../../../app/templates/__partials__/menu_list';
import MenuListItem from '../../../app/templates/__partials__/menu_list_item';
import {retrieveUrl} from '../../../app/utils/router';

const Menu = () => (
    <MenuList>
        <MenuListItem url={retrieveUrl('authForwardingAddressesAllList')} />
        <MenuListItem url={retrieveUrl('authUserEmailUpdate')} exact />
        <MenuListItem url={retrieveUrl('authUserPasswordUpdate')} exact />
        <MenuListItem url={retrieveUrl('authUserNewslettersUpdate')} exact />
        <MenuListItem url={retrieveUrl('authUserDestroy')} exact />
    </MenuList>
);

export default Menu;
