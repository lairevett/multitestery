import React from 'react';
import ReactReCAPTCHA from 'react-google-recaptcha';
import {Helmet} from 'react-helmet';
import {APP_RECAPTCHA_SITE_KEY} from '../../../app/constants';

const ReCAPTCHA = properties => (
    <>
        <Helmet
            style={[
                {
                    cssText: `
                #centered-recaptcha > div > div > div {
                    margin-left: auto;
                    margin-right: auto;
                }
            `,
                },
            ]}
        />
        <div id="centered-recaptcha" className="mt-3">
            <ReactReCAPTCHA sitekey={APP_RECAPTCHA_SITE_KEY} onChange={properties.input.onChange} />
        </div>
    </>
);

export default ReCAPTCHA;
