import {INITIAL_STATE as ACTIVITIES_INITIAL_STATE} from './activities';
import {DETAILS_INITIAL_STATE as USER_INITIAL_STATE} from './user';
import {INITIAL_STATE as FORWARDING_ADDRESSES_INITIAL_STATE} from './forwarding_addresses';

// Auth reducer initial state.
export const INITIAL_STATE = {
    ...ACTIVITIES_INITIAL_STATE,
    user: USER_INITIAL_STATE,
    forwardingAddresses: FORWARDING_ADDRESSES_INITIAL_STATE,
};
