import {DEFAULT_STATUS_INITIAL_STATE} from '../../app/utils/api/constants';

// API endpoints.
export const ENDPOINT = {
    UPDATE_EMAIL: '/v1/auth/change-email/',
    UPDATE_PASSWORD: '/v1/auth/change-password/',
    UPDATE_WANTS_NEWSLETTERS: '/v1/auth/change-wants-newsletters/',
    DETAILS: '/v1/auth/user/',
    DESTROY: '/v1/auth/delete-user/',
};

// Redux action types.
export const ACTION = {
    UPDATE_EMAIL: '@auth_user/UPDATE_EMAIL',
    UPDATE_PASSWORD: '@auth_user/UPDATE_PASSWORD',
    UPDATE_WANTS_NEWSLETTERS: '@auth_user/UPDATE_WANTS_NEWSLETTERS',
    RETRIEVE_DETAILS: '@auth_user/RETRIEVE_DETAILS',
    CLEAR_DETAILS: '@auth_user/CLEAR_DETAILS',
    DESTROY: '@auth_user/DESTROY',
};

// Redux form names.
export const FORM = {
    UPDATE_EMAIL: 'FORM_AUTH_USER_UPDATE_EMAIL',
    UPDATE_PASSWORD: 'FORM_AUTH_USER_UPDATE_PASSWORD',
    UPDATE_NEWSLETTERS: 'FORM_AUTH_USER_UPDATE_NEWSLETTERS',
    DESTROY_USER: 'FORM_AUTH_USER_DESTROY_USER',
};

// Reducer initial states.
export const DETAILS_INITIAL_STATE = {
    id: 0,
    username: '',
    email: '',
    wants_newsletters: true,
    is_staff: false,
    is_superuser: false,
    is_active: true,
    date_joined: '',
    ...DEFAULT_STATUS_INITIAL_STATE,
};

// Prop types.
export const DETAILS_PROP_TYPES = PropertyTypes =>
    PropertyTypes.exact({
        id: PropertyTypes.number.isRequired,
        username: PropertyTypes.string.isRequired,
        email: PropertyTypes.string.isRequired,
        wants_newsletters: PropertyTypes.bool.isRequired,
        is_staff: PropertyTypes.bool.isRequired,
        is_superuser: PropertyTypes.bool.isRequired,
        is_active: PropertyTypes.bool.isRequired,
        date_joined: PropertyTypes.string.isRequired,
    }).isRequired;
