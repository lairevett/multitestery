import {DEFAULT_STATUS_INITIAL_STATE} from '../../app/utils/api/constants';

// API endpoints.
export const ENDPOINT = {
    ACTIVATE: '/v1/auth/activate/',
    RESEND_ACTIVATION_LINK: '/v1/auth/resend-activation-link/',
    SIGN_UP: '/v1/auth/sign-up/',
    SIGN_IN: '/v1/auth/sign-in/',
    SIGN_OUT: '/v1/auth/sign-out/',
};

// Redux action types.
export const ACTION = {
    ACTIVATE: '@auth_activities/ACTIVATE',
    RESEND_ACTIVATION_LINK: '@auth_activities/RESEND_ACTIVATION_LINK',
    SIGN_UP: '@auth_activities/SIGN_UP',
    SIGN_IN: '@auth_activities/SIGN_IN',
    SET_SIGN_IN_OUT: '@auth_activities/SET_SIGN_IN_OUT',
    SIGN_OUT: '@auth_activities/SIGN_OUT',
};

// Redux form names.
export const FORM = {
    ACTIVATE: 'FORM_AUTH_ACTIVITIES_ACTIVATE',
    RESEND_ACTIVATION_LINK: 'FORM_AUTH_ACTIVITIES_RESEND_ACTIVATION_LINK',
    SIGN_UP: 'FORM_AUTH_ACTIVITIES_SIGN_UP',
    SIGN_IN: 'FORM_AUTH_ACTIVITIES_SIGN_IN',
    SIGN_OUT: 'FORM_AUTH_ACTIVITIES_SIGN_OUT',
};

// Reducer initial states.
export const SIGN_IN_OUT_INITIAL_STATE = {
    is_authenticated: false,
    ...DEFAULT_STATUS_INITIAL_STATE,
};

export const INITIAL_STATE = {
    activate: DEFAULT_STATUS_INITIAL_STATE,
    resendActivationLink: DEFAULT_STATUS_INITIAL_STATE,
    signUp: DEFAULT_STATUS_INITIAL_STATE,
    signInOut: SIGN_IN_OUT_INITIAL_STATE,
};
