import {DEFAULT_STATUS_INITIAL_STATE} from '../../app/utils/api/constants';

// API endpoints.
export const ENDPOINT = {
    CREATE: '/v1/users/forwarding-addresses/',
    LIST_ALL: userId => `/v1/users/${userId}/forwarding-addresses/`,
    DETAILS: (userId, forwardingAddressId) => `/v1/users/${userId}/forwarding-addresses/${forwardingAddressId}/`,
};

// Redux action types.
export const ACTION = {
    CREATE: '@auth_forwarding_addresses/CREATE',
    RETRIEVE_ALL_LIST: '@auth_forwarding_addresses/RETRIEVE_ALL_LIST',
    CLEAR_LIST: '@auth_forwarding_addresses/CLEAR_LIST',
    DESTROY: '@auth_forwarding_addresses/DESTROY',
};

// Redux form names.
export const FORM = {
    CREATE: 'FORM_AUTH_FORWARDING_ADDRESSES_CREATE',
    DESTROY: 'FORM_AUTH_FORWARDING_ADDRESSES_DESTROY',
};

// Reducer initial states.
export const INITIAL_STATE = {
    results: [],
    ...DEFAULT_STATUS_INITIAL_STATE,
};

export const DETAILS_INITIAL_STATE = {
    id: 0,
    first_name: '',
    last_name: '',
    phone: '',
    street: '',
    home_number: '',
    apt_number: 0,
    city: '',
    postal_code: '',
};

// Prop types.
export const LIST_PROP_TYPES = PropertyTypes => PropertyTypes.arrayOf(DETAILS_PROP_TYPES(PropertyTypes));

export const DETAILS_PROP_TYPES = PropertyTypes =>
    PropertyTypes.exact({
        id: PropertyTypes.number.isRequired,
        first_name: PropertyTypes.string.isRequired,
        last_name: PropertyTypes.string.isRequired,
        phone: PropertyTypes.string.isRequired,
        street: PropertyTypes.string.isRequired,
        home_number: PropertyTypes.string.isRequired,
        apt_number: PropertyTypes.number,
        city: PropertyTypes.string.isRequired,
        postal_code: PropertyTypes.string.isRequired,
    });
