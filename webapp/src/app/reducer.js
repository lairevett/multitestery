import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';
import announcementReducer from '../announcements/reducer';
import authReducer from '../auth/reducers/__all__';
import cartReducer from '../cart/reducer';
import orderReducer from '../orders/reducer';
import pageReducer from '../pages/reducer';
import perfumeReducer from '../perfumes/reducer';
import adminReducer from '../admin/reducers/admin';
import __server__Reducer from '../__server__/reducer';

const reducers = {
    form: formReducer,
    auth: authReducer,
    perfume: perfumeReducer,
    cart: cartReducer,
    order: orderReducer,
    admin: adminReducer,
    announcement: announcementReducer,
    page: pageReducer,
    __server__: __server__Reducer,
};

const makeAppReducer = () => combineReducers(reducers);

export default makeAppReducer;
