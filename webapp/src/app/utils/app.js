import {makeDispatchEvents} from './dispatch_events';
import {retrieveActiveList} from '../../announcements/actions';
import {clearList, retrieveFeaturedList} from '../../perfumes/actions';

export const callbackGetDispatchEvents = () => () =>
    makeDispatchEvents([retrieveActiveList, retrieveFeaturedList], [clearList]);

export const getPolishPluralForPositiveNumber = (count, commonDenominator) =>
    count === 1 ? `${commonDenominator}ę` : count >= 2 && count <= 4 ? `${commonDenominator}i` : commonDenominator;
