export default class Factory {
    #fields = {};

    getOne = () => ({...this.#fields});

    getMultiple = count => {
        const elements = [];
        for (let i = 0; i < count; i++) elements.push(this.getOne());
        return elements;
    };
}
