import {applyMiddleware, createStore} from 'redux';

import {composeWithDevTools} from 'redux-devtools-extension/developmentOnly';
import thunk from 'redux-thunk';
import makeAppReducer from '../reducer';

const makeStore = initialState =>
    createStore(makeAppReducer(), initialState, composeWithDevTools(applyMiddleware(thunk)));

export default makeStore;
