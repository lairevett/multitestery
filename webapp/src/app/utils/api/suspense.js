import PropTypes from 'prop-types';
import {STATUS} from './constants';

const Suspense = ({status, fallback, children}) => (status === STATUS.INITIALIZED ? fallback : children);

Suspense.propTypes = {
    status: PropTypes.oneOf(Object.values(STATUS)).isRequired,
    fallback: PropTypes.node.isRequired,
    children: PropTypes.node.isRequired,
};

export default Suspense;
