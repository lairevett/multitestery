// API address.
export const {API_URL} = process.env;

// Allowed reducer meta status values.
export const STATUS = {
    INITIALIZED: 'STATUS_INITIALIZED',
    RESPONSE_BAD_REQUEST: 'STATUS_RESPONSE_BAD_REQUEST',
    RESPONSE_FORBIDDEN: 'STATUS_FORBIDDEN',
    RESPONSE_NOT_FOUND: 'STATUS_RESPONSE_NOT_FOUND',
    RESPONSE_INTERNAL_SERVER_ERROR: 'STATUS_RESPONSE_INTERNAL_SERVER_ERROR',
    RESPONSE_OTHER: 'STATUS_OTHER',
    API_CONNECTION_ERROR: 'STATUS_API_CONNECTION_ERROR',
    UNKNOWN_ERROR: 'STATUS_UNKNOWN_ERROR',
    OK: 'STATUS_OK',
    OK_NO_RESULTS: 'STATUS_OK_NO_RESULTS',
};

export const ERROR_STATUS_CODES_MAPPED_TO_STATUS_STRINGS = new Map([
    [400, STATUS.RESPONSE_BAD_REQUEST],
    [401, STATUS.RESPONSE_FORBIDDEN],
    [404, STATUS.RESPONSE_NOT_FOUND],
    [500, STATUS.RESPONSE_INTERNAL_SERVER_ERROR],
]);

// Common reducer initial states.
export const DEFAULT_STATUS_INITIAL_STATE = {
    __meta__: {
        status: STATUS.INITIALIZED,
    },
};

export const DEFAULT_LIST_INITIAL_STATE = {
    count: 0,
    next: null,
    previous: null,
    results: [],
    ...DEFAULT_STATUS_INITIAL_STATE,
};
