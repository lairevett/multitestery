import axios from 'axios';
import setCookie from 'set-cookie-parser';
import {API_URL, ERROR_STATUS_CODES_MAPPED_TO_STATUS_STRINGS, STATUS} from './constants';
import {APP_CSRF_COOKIE_NAME, APP_CSRF_HEADER_NAME} from '../../constants';
import {logDispatchAPIErrors} from '../logger';

const makeRequestHeaders = contextHeaders => {
    const headers = {
        accept: 'application/json',
    };

    // If client's JS is disabled or if it's the first request to the server,
    // axios acts as reverse proxy, headers from request need to be forwarded manually.
    // client => nginx => express => axios => api.
    const referer = contextHeaders?.referer;
    if (referer) {
        headers.referer = referer;
    }

    const cookie = contextHeaders?.cookie;
    if (cookie) {
        const csrfToken = cookie.match(`(^|[^;]+)\\s*${APP_CSRF_COOKIE_NAME}\\s*=\\s*([^;]+)`);
        // eslint-disable-next-line security/detect-object-injection
        if (csrfToken) headers[APP_CSRF_HEADER_NAME] = csrfToken.pop(); // Server-side equivalent of xsrfHeaderName and xsrfCookieName

        headers.cookie = cookie; // Server-side equivalent of withCredentials.
    }

    return headers;
};

const makeResponse = (action, state, status, context) => ({
    type: action,
    payload: {
        ...state,
        __meta__: {
            status,
        },
    },
    context,
});

const makeErrorResponses = (action, initialState, context, error) => {
    if (error?.response) {
        // Keeps errors from server-side validation if bad request.
        const state =
            error.response?.status === 400 && typeof error.response.data === 'object' && error.response.data !== null
                ? {...initialState, ...error.response.data}
                : initialState;

        return makeResponse(
            action,
            state,
            ERROR_STATUS_CODES_MAPPED_TO_STATUS_STRINGS.get(error.response.status) ?? STATUS.RESPONSE_OTHER,
            context
        );
    }

    if (error?.request) {
        return makeResponse(action, initialState, STATUS.API_CONNECTION_ERROR, context);
    }

    return makeResponse(action, initialState, STATUS.UNKNOWN_ERROR, context);
};

export const apiGetRequest = async (action, initialState, resource, context = {}) => {
    try {
        const response = await axios({
            method: 'get',
            url: API_URL + resource,
            headers: makeRequestHeaders(context.headers),
            withCredentials: process.env.__CLIENT__,
        });

        const setCookieHeaders = setCookie(response.headers['set-cookie'], {map: true});
        return makeResponse(action, response.data, STATUS.OK, {...context, setCookieHeaders});
    } catch (error) {
        logDispatchAPIErrors(error);
        return makeErrorResponses(action, initialState, context, error);
    }
};

export const apiBatchGetRequests = async (
    action,
    singleInitialState,
    multipleInitialState,
    resources,
    context = {}
) => {
    try {
        const responses = resources.map(resource => apiGetRequest(action, singleInitialState, resource, context, true));
        const payload = (await Promise.all(responses)).map(response => response.payload);

        return makeResponse(action, {results: payload}, STATUS.OK, context);
    } catch (error) {
        logDispatchAPIErrors(error);
        return makeErrorResponses(action, multipleInitialState, context, error);
    }
};

export const apiGetListRequest = async (action, initialState, resource, context = {}) => {
    try {
        const response = await axios({
            method: 'get',
            url: API_URL + resource,
            headers: makeRequestHeaders(context.headers),
            withCredentials: process.env.__CLIENT__,
        });

        const setCookieHeaders = setCookie(response.headers['set-cookie'], {map: true});
        return (typeof response.data.count === 'undefined' && response.data?.results?.length > 0) ||
            response.data.count > 0
            ? makeResponse(action, response.data, STATUS.OK, {...context, setCookieHeaders})
            : makeResponse(action, initialState, STATUS.OK_NO_RESULTS, {...context, setCookieHeaders});
    } catch (error) {
        logDispatchAPIErrors(error);
        return makeErrorResponses(action, initialState, context, error);
    }
};

export const apiPostRequest = async (action, initialState, resource, requestBody, context = {}) => {
    try {
        const requestHeaders = makeRequestHeaders(context.headers);

        const response = await axios({
            method: 'post',
            url: API_URL + resource,
            data: requestBody,
            headers: requestBody.getHeaders?.(requestHeaders) || requestHeaders,
            xsrfHeaderName: APP_CSRF_HEADER_NAME,
            xsrfCookieName: APP_CSRF_COOKIE_NAME,
            withCredentials: process.env.__CLIENT__,
        });

        const setCookieHeaders = setCookie(response.headers['set-cookie'], {map: true});
        return makeResponse(action, response.data, STATUS.OK, {...context, setCookieHeaders});
    } catch (error) {
        logDispatchAPIErrors(error);
        return makeErrorResponses(action, initialState, context, error);
    }
};

export const apiPatchRequest = async (action, initialState, resource, requestBody, context = {}) => {
    try {
        const requestHeaders = makeRequestHeaders(context.headers);

        const response = await axios({
            method: 'patch',
            url: API_URL + resource,
            data: requestBody,
            headers: requestBody.getHeaders?.(requestHeaders) || requestHeaders,
            xsrfHeaderName: APP_CSRF_HEADER_NAME,
            xsrfCookieName: APP_CSRF_COOKIE_NAME,
            withCredentials: process.env.__CLIENT__,
        });

        const setCookieHeaders = setCookie(response.headers['set-cookie'], {map: true});
        return makeResponse(action, response.data, STATUS.OK, {...context, setCookieHeaders});
    } catch (error) {
        logDispatchAPIErrors(error);
        return makeErrorResponses(action, initialState, context, error);
    }
};

export const apiDeleteRequest = async (action, initialState, resource, context = {}) => {
    try {
        const response = await axios({
            method: 'delete',
            url: API_URL + resource,
            headers: makeRequestHeaders(context.headers),
            xsrfHeaderName: APP_CSRF_HEADER_NAME,
            xsrfCookieName: APP_CSRF_COOKIE_NAME,
            withCredentials: process.env.__CLIENT__,
        });

        const setCookieHeaders = setCookie(response.headers['set-cookie'], {map: true});
        return makeResponse(action, response.data, STATUS.OK, {...context, setCookieHeaders});
    } catch (error) {
        logDispatchAPIErrors(error);
        return makeErrorResponses(action, initialState, context, error);
    }
};
