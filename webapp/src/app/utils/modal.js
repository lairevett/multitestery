export const makeYesNoModal = (id, title, body, onYesClick, isYesButtonDisabled = false) => {
    // noinspection JSUnresolvedFunction
    const modal = new window.BSN.Modal(document.getElementById(id), {
        content: makeYesNoModalContent(id, title, body, isYesButtonDisabled),
        keyboard: true,
    });

    onModalYesClick(id, () => {
        modal.hide();
        onYesClick();
        // The modal.hide() method doesn't hide the backdrop for some reason.
        document.querySelector('body').classList.remove('modal-open');
        document.querySelector('.modal-backdrop').remove();
    });

    modal.show();
    return modal;
};

// noinspection
export const makeYesNoModalContent = (id, title, body, isYesButtonDisabled) => `
    <div class="modal-header">
        <h5 id="${id}-title" class="modal-title">
            ${title}
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Zamknij">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body text-left">
        <p>${body}</p>
    </div>
    <div class="modal-footer">
        <button id="${id}-no-btn" type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Przycisk nie">
            Nie
        </button>
        <button id="${id}-yes-btn" type="button" class="btn btn-primary" aria-label="Przycisk tak"
                ${isYesButtonDisabled && ' disabled'}>
            Tak
        </button>
    </div>
`;

export const onModalYesClick = (id, callback) => {
    document.querySelector(`#${id}-yes-btn`).addEventListener('click', callback);
};
