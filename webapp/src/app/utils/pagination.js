import queryString from 'query-string';

export const getQueryStringWithPreviousPage = queryParameters =>
    queryString.stringify({...queryParameters, page: (+queryParameters.page || 1) - 1});

export const getQueryStringWithNextPage = queryParameters =>
    queryString.stringify({...queryParameters, page: (+queryParameters.page || 1) + 1});
