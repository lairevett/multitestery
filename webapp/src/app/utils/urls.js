import '../urls';
import '../../auth/urls/activities';
import '../../auth/urls/forwarding_addresses';
import '../../auth/urls/user';
import '../../perfumes/urls';
import '../../cart/urls';
import '../../orders/urls';
import '../../admin/urls/summary';
import '../../admin/urls/announcements';
import '../../admin/urls/perfumes';
import '../../admin/urls/orders';
import '../../admin/urls/users';
import '../../admin/urls/pages';

export const registerDynamicUrls = async pageList => {
    try {
        const urls = await import('../../pages/urls');
        urls.registerPagesUrls(pageList);
    } catch (error) {
        console.error(error);
    }
};
