export const validateFieldRequired = (field, message = 'Pole jest wymagane.') => {
    if (
        typeof field === 'undefined' ||
        (typeof field === 'string' && field.replace(/ /g, '').length === 0) ||
        (typeof field === 'boolean' && !field)
    ) {
        return message;
    }

    return null;
};

export const validateFieldContainsNoWhitespace = (field, message = 'Pole nie może zawierać spacji.') => {
    if (typeof field !== 'undefined' && / /g.test(field)) {
        return message;
    }

    return null;
};

export const validateFieldStartsWithCapitalLetter = (field, message = 'Pole musi zaczynać się dużą literą.') => {
    if (typeof field === 'string' && field.charAt(0) !== field.charAt(0).toUpperCase()) {
        return message;
    }

    return null;
};

export const validateFieldMinLength = (field, min, customMessage = null) => {
    if (
        (typeof field === 'string' && field.replace(/ /g, '').length === 0) ||
        (typeof field !== 'undefined' && field.length < min)
    ) {
        if (customMessage) return customMessage;
        return `Pole musi zawierać przynajmniej ${min} ${min < 2 ? 'znak' : min < 5 ? 'znaki' : 'znaków'}.`;
    }
    return null;
};

export const validateFieldMaxLength = (field, max, customMessage = null) => {
    if (
        (typeof field === 'string' && field.replace(/ /g, '').length === 0) ||
        (typeof field !== 'undefined' && field.length > max)
    ) {
        if (customMessage) return customMessage;
        return `Pole może zawierać maksymalnie ${max} ${max < 2 ? 'znak' : max < 5 ? 'znaki' : 'znaków'}.`;
    }
    return null;
};

export const validateFieldMinValue = (field, min, customMessage = null) => {
    if (typeof field !== 'undefined' && +field < min) {
        if (customMessage) return customMessage;
        return `Minimalna wartość pola to ${min}.`;
    }
    return null;
};

export const validateFieldMaxValue = (field, max, customMessage = null) => {
    if (typeof field !== 'undefined' && +field > max) {
        if (customMessage) return customMessage;
        return `Maksymalna wartość pola to ${max}.`;
    }
    return null;
};

export const validateFieldPhoneNumber = (field, message = 'Numer telefonu jest niepoprawny.') => {
    if (typeof field !== 'undefined' && (!field.startsWith('+') || field.length !== 12)) {
        return message;
    }

    return null;
};

export const validateFieldEquals = (field, value, message = 'Pola nie są takie same.') => {
    if (typeof field !== 'undefined' && field !== value) {
        return message;
    }

    return null;
};

export const validateFieldEmail = email => {
    if (
        typeof email !== 'undefined' &&
        // eslint-disable-next-line security/detect-unsafe-regex
        /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
            email
        )
    ) {
        return 'Adres email jest niepoprawny.';
    }

    return null;
};

export const validateFieldPostalCode = postalCode => {
    if (typeof postalCode !== 'undefined' && !postalCode.match('[0-9][0-9]-[0-9][0-9][0-9]')) {
        return 'Kod pocztowy jest niepoprawny.';
    }

    return null;
};

export const validateUsername = username =>
    validateFieldRequired(username) ||
    validateFieldContainsNoWhitespace(username) ||
    validateFieldMinLength(username, 3) ||
    validateFieldMaxLength(username, 16);

export const validateEmail = email =>
    validateFieldRequired(email) ||
    validateFieldContainsNoWhitespace(email) ||
    validateFieldMaxLength(email, 100) ||
    validateFieldEmail(email);

export const validatePassword = password =>
    validateFieldRequired(password) ||
    validateFieldContainsNoWhitespace(password) ||
    validateFieldMinLength(password, 8) ||
    validateFieldMaxLength(password, 100);
