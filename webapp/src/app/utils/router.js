import {Route} from 'react-router-dom';
import {createBrowserHistory} from 'history';
import {AdminRequiredRoute, AuthDisallowedRoute, AuthRequiredRoute} from '../../auth/templates/__route_access__';
import {makeDispatchEvents} from './dispatch_events';

const historyPolyfill = {
    push: _ => {},
    goBack: () => {},
};
export const history = process.env.__CLIENT__ ? createBrowserHistory() : historyPolyfill;

export const ACCESS = {
    ALLOW_ANY: Route,
    REQUIRE_AUTH: AuthRequiredRoute,
    DISALLOW_AUTH: AuthDisallowedRoute,
    REQUIRE_ADMIN: AdminRequiredRoute,
};

const _urls = {};

export const makeUrl = (path, methods, view, access, properties = {}) => {
    const {callbackGetDispatchEvents, ...restOfProperties} = properties;

    return {
        path,
        methods,
        view,
        access,
        properties: {
            callbackGetDispatchEvents:
                typeof callbackGetDispatchEvents === 'function'
                    ? callbackGetDispatchEvents(restOfProperties)
                    : () => makeDispatchEvents(),
            ...restOfProperties,
        },
    };
};

export const registerUrl = (name, path, methods, view, access, properties = {}) => {
    if (_urls?.[name] === undefined) {
        // eslint-disable-next-line security/detect-object-injection
        _urls[name] = makeUrl(path, methods, view, access, properties);
    } else if (process?.env?.NODE_ENV === 'development') {
        console.warn(`Route ${name} at path ${path} is already registered.`);
    }
};

export const retrieveUrl = name => {
    if (_urls?.[name] !== undefined) {
        // eslint-disable-next-line security/detect-object-injection
        return _urls[name];
    }

    throw new Error(`URL with name ${name} wasn't registered.`);
};

export const retrieveUrls = () => _urls;
