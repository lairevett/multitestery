import React from 'react';
import {render as rtlRender} from '@testing-library/react';
import {Provider} from 'react-redux';
import {BrowserRouter} from 'react-router-dom';
import makeStore from './store';
import {ACCESS, registerUrl} from './router';

const render = (component, {initialState = {}, urls = [], ...renderOptions} = {}) => {
    const store = makeStore(initialState);

    // eslint-disable-next-line react/prop-types
    const Wrapper = ({children}) => {
        return (
            <Provider store={store}>
                <BrowserRouter>{children}</BrowserRouter>
            </Provider>
        );
    };

    urls.forEach(url => registerUrl(url, '/test-path', [], null, ACCESS.ALLOW_ANY));

    return rtlRender(component, {wrapper: Wrapper, ...renderOptions});
};

// Re-export everything.
export * from '@testing-library/react';

// Override render method.
export {render};
