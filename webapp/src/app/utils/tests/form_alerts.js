/* eslint-disable jest/no-export */

export const testSuccessAlertsOnIsSuccess = callbackRenderComponent => {
    it("doesn't display success alert when isSuccess is false", () => {
        const {container} = callbackRenderComponent(false, false);
        expect(container.querySelector('.alert-success')).not.toBeInTheDocument();
    });

    it('displays success alert when isSuccess is true', () => {
        const {container} = callbackRenderComponent(true, false);
        expect(container.querySelector('.alert-success')).toBeInTheDocument();
    });
};

export const testSuccessAlertsOnSubmitSucceeded = (formName, callbackRenderComponent) => {
    it("doesn't display success alert when submitSucceeded is false", () => {
        const {container} = callbackRenderComponent(false, false, {
            form: {
                [formName]: {
                    submitSucceeded: false,
                },
            },
        });

        expect(container.querySelector('.alert-success')).not.toBeInTheDocument();
    });

    it('displays success alert when submitSucceeded is true', () => {
        const {container} = callbackRenderComponent(false, false, {
            form: {
                [formName]: {
                    submitSucceeded: true,
                },
            },
        });

        expect(container.querySelector('.alert-success')).toBeInTheDocument();
    });
};

export const testErrorAlertsOnIsError = callbackRenderComponent => {
    it("doesn't display error alert when isError is false", () => {
        const {container} = callbackRenderComponent(false, false);
        expect(container.querySelector('.alert-danger')).not.toBeInTheDocument();
    });

    it('displays error alert when isError is true', () => {
        const {container} = callbackRenderComponent(false, true);
        expect(container.querySelector('.alert-danger')).toBeInTheDocument();
    });
};

export const testErrorAlertsOnSubmitFailed = (formName, callbackRenderComponent) => {
    it("doesn't display error alert when submitFailed is false", () => {
        const {container} = callbackRenderComponent(false, false, {
            form: {
                [formName]: {
                    submitFailed: false,
                },
            },
        });

        expect(container.querySelector('.alert-danger')).not.toBeInTheDocument();
    });

    it('displays error alert when submitFailed is true', () => {
        const {container} = callbackRenderComponent(false, false, {
            form: {
                [formName]: {
                    submitFailed: true,
                },
            },
        });

        expect(container.querySelector('.alert-danger')).toBeInTheDocument();
    });
};
