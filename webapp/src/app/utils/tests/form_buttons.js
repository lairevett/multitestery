/* eslint-disable jest/no-export */

export const testSubmitButtonDisabledWhenFormIsNotValid = (formName, submitButtonText, callbackRenderComponent) => {
    it('makes the submit button disabled if the form is not valid', () => {
        const {getByText} = callbackRenderComponent(false, false, {
            form: {
                [formName]: {
                    valid: false,
                },
            },
        });

        expect(getByText(submitButtonText)).toBeDisabled();
    });
};

export const testSubmitButtonDisabledWhenFormIsPristine = (formName, submitButtonText, callbackRenderComponent) => {
    it('makes the submit button disabled if the form is pristine', () => {
        const {getByText} = callbackRenderComponent(false, false, {
            form: {
                [formName]: {
                    pristine: true,
                },
            },
        });

        expect(getByText(submitButtonText)).toBeDisabled();
    });
};

export const testSubmitButtonDisabledWhenSubmitting = (formName, submitButtonText, callbackRenderComponent) => {
    it('makes the submit button disabled if the form is being submitted', () => {
        const {getByText} = callbackRenderComponent(false, false, {
            form: {
                [formName]: {
                    submitting: true,
                },
            },
        });

        expect(getByText(submitButtonText)).toBeDisabled();
    });
};
