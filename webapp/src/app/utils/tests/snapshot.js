/* eslint-disable jest/no-export */

export const testMatchesLastSnapshot = callbackRenderComponent => {
    it('matches last snapshot', () => {
        expect(callbackRenderComponent()).toMatchSnapshot();
    });
};
