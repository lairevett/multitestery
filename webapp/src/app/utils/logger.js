import {getArgumentValue} from '../../__server__/utils/argv';

export const logError = (message, time = null) =>
    console.error(`${new Date(time || Date.now()).toLocaleString('pl-PL')} ${message}`);

export const logDispatchAPIErrors = error => {
    const time = Date.now();
    const minLogStatus = +getArgumentValue('min-log-status') || 500;
    const isClient = process.env.__CLIENT__;

    if (error.config) {
        if (error.response) {
            if (
                isClient ||
                process.env.NODE_ENV === 'development' ||
                (process.env.NODE_ENV === 'production' && error.response.status >= minLogStatus)
            ) {
                logError(
                    `Request to ${error.config.method} ${error.config.url} received response with non-2xx status (${error.response.status}).`,
                    time
                );

                logError('-- Response contents --', time);
                try {
                    logError(JSON.stringify(error.response.data), time);
                } catch (error_) {
                    logError('JSON.stringify(error.response.data) failed.', time);
                }
            }
        } else if (error.request) {
            // Request is instance of XMLHttpRequest when sending from client and ClientRequest when sending from node.
            logError(`Request to ${error.config.method} ${error.config.url} didn't receive any response.`, time);
        } else {
            logError(`Unknown error occurred while sending request: ${error.message}`, time);
        }
    }

    logError('-- Stack trace --', time);
    logError(error.stack, time);
};
