export const APP_NAME = 'MultiTestery';
export const APP_DESCRIPTION = 'perfumeria internetowa, testery perfum męskie, damskie, unisex.';

export const APP_AUTH_COOKIE_NAME = '_at';
export const APP_CSRF_HEADER_NAME = 'X-CSRF-Token';
export const APP_CSRF_COOKIE_NAME = '_ct';

export const APP_MAX_FORWARDING_ADDRESSES = 5;

// noinspection SpellCheckingInspection
export const APP_RECAPTCHA_SITE_KEY = '6LdJctkUAAAAAPvgCFBUzvNPIGIoH1ERprzzCQ9S';

export const AUTH_REQUIRED_ROUTE_REDIRECT_URL = '/sign-in';
export const AUTH_DISALLOWED_ROUTE_REDIRECT_URL = '/';
export const ADMIN_REQUIRED_ROUTE_REDIRECT_URL = '/';

export const CURRENCY_FORMATTER = new Intl.NumberFormat('pl-PL', {style: 'currency', currency: 'PLN'});

// Prop types.
export const PAGINATION_DATA_PROP_TYPES = PropertyTypes =>
    PropertyTypes.exact({
        previous: PropertyTypes.string,
        next: PropertyTypes.string,
        path: PropertyTypes.string,
        queryParameters: PropertyTypes.object.isRequired,
    });
