/* eslint-disable react/prop-types */
import React from 'react';

import Carousel from '../announcements/templates/__partials__/carousel';
import Content from './templates/__partials__/content';
import PerfumesList from '../perfumes/templates/list';
import Loading from './templates/__partials__/loading';
import Suspense from './utils/api/suspense';

/*
 * GET /
 */
export const IndexView = ({title, state}) => {
    const announcementsResults = state.announcement.list.results;

    const {
        results: perfumesResults,
        __meta__: {status},
    } = state.perfume.list;

    return (
        <Content status={status} title={title} prepend={<Carousel list={announcementsResults} />}>
            <Suspense status={status} fallback={<Loading />}>
                <PerfumesList list={perfumesResults} />
            </Suspense>
        </Content>
    );
};
