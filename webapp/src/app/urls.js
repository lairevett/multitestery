import {ACCESS, registerUrl} from './utils/router';

import {IndexView} from './views';
import {callbackGetDispatchEvents} from './utils/app';

registerUrl('index', '/', ['get'], IndexView, ACCESS.ALLOW_ANY, {title: 'Polecane perfumy', callbackGetDispatchEvents});
