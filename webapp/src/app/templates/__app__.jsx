import React from 'react';

import Layout from './__layout__';
import Routes from '../routes';

const App = () => (
    <Layout>
        <Routes />
    </Layout>
);

export default App;
