import React from 'react';
import PropTypes from 'prop-types';
import NavBar from './__partials__/nav_bar';
import Footer from './__partials__/footer';

const Layout = ({children}) => (
    <>
        <NavBar />
        <main role="main" className="container mt-3">
            {children}
        </main>
        <Footer />
    </>
);

Layout.propTypes = {
    children: PropTypes.node.isRequired,
};

export default Layout;
