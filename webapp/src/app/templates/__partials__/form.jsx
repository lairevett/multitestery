/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import {handleFileChange} from '../../utils/form';

export const FormCard = ({children}) => (
    <div className="card col-xs-12 col-sm-9 col-md-6 mx-auto form-card">
        <div className="card-body text-center">{children}</div>
    </div>
);

FormCard.propTypes = {
    children: PropTypes.node.isRequired,
};

export const FormField = ({
    id,
    input,
    label,
    type,
    placeholder,
    doNotValidate,
    meta: {touched, error},
    small,
    children,
    rows,
    plainText,
    disabled,
    readOnly,
    required,
    formGroupClassNames,
    inputClassNames,
    value,
}) =>
    type === 'checkbox' ? (
        <div className="form-check">
            <input
                id={id}
                className={`form-check-input${touched ? (error ? ' is-invalid' : '') : ''}`}
                {...input}
                type="checkbox"
                value={value}
            />
            <label htmlFor={id} className="form-check-label">
                {label}
            </label>
            <div className="invalid-feedback">{error}</div>
        </div>
    ) : type === 'hidden' ? (
        <input id={id} {...input} type="hidden" required readOnly />
    ) : (
        <div className={`form-group${typeof formGroupClassNames !== 'undefined' ? ` ${formGroupClassNames}` : ''}`}>
            {label ? <label htmlFor={id}>{label}</label> : ''}
            {type === 'textarea' ? (
                <textarea
                    id={id}
                    className={`form-control${
                        doNotValidate ? '' : touched ? (error ? ' is-invalid' : ' is-valid') : ''
                    }`}
                    {...input}
                    rows={rows}
                    placeholder={placeholder}
                >
                    {children}
                </textarea>
            ) : type === 'select' ? (
                <select
                    id={id}
                    className={`custom-select${
                        doNotValidate ? '' : touched ? (error ? ' is-invalid' : ' is-valid') : ''
                    }`}
                    {...input}
                >
                    {children}
                </select>
            ) : type !== 'select' && type !== 'textarea' ? (
                <input
                    id={id}
                    {...input}
                    className={`${plainText ? 'form-control-plaintext' : 'form-control'}${
                        typeof inputClassNames !== 'undefined' ? ` ${inputClassNames}` : ''
                    }${doNotValidate ? '' : touched ? (error ? ' is-invalid' : ' is-valid') : ''}${
                        small ? ' form-control-sm' : ''
                    }`}
                    type={type}
                    disabled={disabled}
                    readOnly={readOnly}
                    required={required}
                    placeholder={placeholder}
                />
            ) : (
                'Zły typ..'
            )}
            <div className="invalid-feedback">{error}</div>
        </div>
    );

export const FormHorizontalField = ({id, input, label, type, min, max, maxLength, meta: {touched, error}}) => (
    <div className="form-group row">
        <label htmlFor={id} className="col col-form-label">
            {label}
        </label>
        <div className="col">
            <input
                id={id}
                {...input}
                type={type}
                className={`form-control${touched ? (error ? ' is-invalid' : '') : ''}`}
                min={min}
                max={max}
                maxLength={maxLength}
            />
        </div>
    </div>
);

export const FormFileField = ({
    input: {onChange, onBlur, value: omitValue, ...inputProperties},
    meta: {touched, error},
    id,
    label,
    placeholder,
    ...properties
}) => (
    <div className="form-group">
        {label ? <label htmlFor={id}>{label}</label> : ''}
        <input
            id={id}
            className={`form-control-file${touched ? (error ? ' is-invalid' : ' is-valid') : ''}`}
            {...inputProperties}
            {...properties}
            type="file"
            onChange={handleFileChange(onChange)}
            onBlur={handleFileChange(onBlur)}
        />
        <div className="invalid-feedback">{error}</div>
    </div>
);
