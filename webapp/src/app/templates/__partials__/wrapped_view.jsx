import React, {memo} from 'react';
import {useSelector} from 'react-redux';
import queryString from 'query-string';
import {useClientDispatchEvents} from '../../utils/dispatch_events';

const WrappedView = memo(properties => {
    const {
        match: {params: urlParameters},
        location: {search},
    } = properties;

    const queryParameters = queryString.parse(search);
    // eslint-disable-next-line no-shadow
    const state = useSelector(state => state);

    const {callbackGetDispatchEvents, view: View, ...restOfProperties} = properties;
    useClientDispatchEvents(callbackGetDispatchEvents(urlParameters, queryParameters, state, {}));

    return <View urlParameters={urlParameters} queryParameters={queryParameters} state={state} {...restOfProperties} />;
});

export default WrappedView;
