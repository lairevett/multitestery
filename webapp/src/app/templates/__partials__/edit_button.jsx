import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

const EditButton = ({className, href}) => (
    <Link to={href} className={`btn btn-primary ${className ?? ''}`} aria-label="Edytuj">
        <noscript>Edytuj</noscript>
        <i className="icon-pencil only-js-inline" />
    </Link>
);

EditButton.propTypes = {
    className: PropTypes.string,
    href: PropTypes.string.isRequired,
};

EditButton.defaultProps = {
    className: '',
};

export default EditButton;
