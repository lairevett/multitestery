import React from 'react';
import PropTypes from 'prop-types';

const MenuList = ({children}) => (
    <ul className="nav nav-pills flex-column col-sm-12 col-md-3 text-center text-md-left text-lg-left text-xl-left">
        {children}
    </ul>
);

MenuList.propTypes = {
    children: PropTypes.node.isRequired,
};

export default MenuList;
