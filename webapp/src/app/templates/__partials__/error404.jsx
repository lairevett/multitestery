import React from 'react';
import Content from './content';
import {makeDispatchEvents, useClientDispatchEvents} from '../../utils/dispatch_events';

const Error404 = () => {
    // This allows client to make the next dispatch after user is routed to 404 page.
    useClientDispatchEvents(makeDispatchEvents());

    return (
        <Content title="404 Not Found">
            <p>Strony, której szukasz nie znaleziono.</p>
        </Content>
    );
};

export default Error404;
