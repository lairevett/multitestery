import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

const AddButton = ({className, href}) => (
    <Link to={href} className={`btn btn-primary ${className}`}>
        Dodaj
    </Link>
);

AddButton.propTypes = {
    className: PropTypes.string,
    href: PropTypes.string.isRequired,
};

AddButton.defaultProps = {
    className: '',
};

export default AddButton;
