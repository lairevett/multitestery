/* eslint-disable jsx-a11y/no-noninteractive-tabindex */
// Without the tabIndex="0" client and server DOMs aren't synchronized, hydrate() adds it for some reason implicitly.
import {Link, NavLink} from 'react-router-dom';

import React, {memo} from 'react';
import {reverse} from 'named-urls';
import {useSelector} from 'react-redux';
import {APP_NAME} from '../../constants';
import SearchForm from '../../../perfumes/forms/search';
import AuthActivitiesSignOutForm from '../../../auth/forms/activities/sign_out';
import {retrieveUrl} from '../../utils/router';

const NavBar = memo(() => {
    const isAuthenticated = useSelector(state => state.auth.activities.signInOut.is_authenticated);
    const user = useSelector(state => state.auth.user);
    const cartEntriesCount = useSelector(state => Object.keys(state.cart.entries).length);
    const pages = useSelector(state => state.page.list.results);

    return (
        <nav className="navbar navbar-expand navbar-light bg-white border-bottom shadow-sm">
            <div className="container">
                <Link to={retrieveUrl('index').path} className="navbar-brand">
                    {APP_NAME}
                </Link>
                <div className="d-flex">
                    <ul className="navbar-nav mr-2 d-none d-lg-flex">
                        <li className="nav-item">
                            <NavLink
                                to={reverse(retrieveUrl('perfumesMaleList').path)}
                                activeClassName="active"
                                className="nav-link"
                            >
                                Męskie
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink
                                to={reverse(retrieveUrl('perfumesFemaleList').path)}
                                activeClassName="active"
                                className="nav-link"
                            >
                                Damskie
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink
                                to={reverse(retrieveUrl('perfumesUnisexList').path)}
                                activeClassName="active"
                                className="nav-link"
                            >
                                Unisex
                            </NavLink>
                        </li>
                    </ul>
                    <SearchForm />
                    <div className="dropdown">
                        <button
                            className="btn btn-default dropdown-toggle"
                            type="button"
                            id="dropdownMenuButton"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                            aria-label="Konto"
                        >
                            <noscript>Konto</noscript>
                            <i className="icon-user only-js-inline" />
                        </button>
                        <div
                            className="dropdown-menu dropdown-menu-right"
                            aria-labelledby="dropdownMenuButton"
                            tabIndex="0"
                        >
                            {!isAuthenticated ? (
                                <>
                                    <Link to={retrieveUrl('authActivitiesSignIn').path} className="dropdown-item">
                                        {retrieveUrl('authActivitiesSignIn').properties.title}
                                    </Link>
                                    <Link to={retrieveUrl('authActivitiesSignUp').path} className="dropdown-item">
                                        {retrieveUrl('authActivitiesSignUp').properties.title}
                                    </Link>
                                </>
                            ) : (
                                <>
                                    <h6 className="dropdown-header">{user.username}</h6>
                                    {user.is_staff && (
                                        <Link to={retrieveUrl('adminSummary').path} className="dropdown-item">
                                            Panel administratora
                                        </Link>
                                    )}
                                    <Link to={reverse(retrieveUrl('ordersAllList').path)} className="dropdown-item">
                                        {retrieveUrl('ordersAllList').properties.title}
                                    </Link>
                                    <Link
                                        to={retrieveUrl('authForwardingAddressesAllList').path}
                                        className="dropdown-item"
                                    >
                                        Ustawienia
                                    </Link>
                                    <div className="dropdown-divider" />
                                    <AuthActivitiesSignOutForm />
                                </>
                            )}
                        </div>
                    </div>
                    <div className="dropdown">
                        <button
                            className="btn btn-default dropdown-toggle"
                            type="button"
                            id="dropdownMenuButton"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                            aria-label="Menu"
                        >
                            <noscript>Menu</noscript>
                            <i className="icon-menu only-js-inline" />
                        </button>
                        <div
                            className="dropdown-menu dropdown-menu-right"
                            aria-labelledby="dropdownMenuButton"
                            tabIndex="0"
                        >
                            <Link to={retrieveUrl('cartIndex').path} className="dropdown-item">
                                {retrieveUrl('cartIndex').properties.title} ({cartEntriesCount})
                            </Link>
                            <div className="dropdown-divider" />
                            <Link to={reverse(retrieveUrl('perfumesAllList').path)} className="dropdown-item">
                                {retrieveUrl('perfumesAllList').properties.title}
                            </Link>
                            <Link to={reverse(retrieveUrl('perfumesMaleList').path)} className="dropdown-item">
                                {retrieveUrl('perfumesMaleList').properties.title}
                            </Link>
                            <Link to={reverse(retrieveUrl('perfumesFemaleList').path)} className="dropdown-item">
                                {retrieveUrl('perfumesFemaleList').properties.title}
                            </Link>
                            <Link to={reverse(retrieveUrl('perfumesUnisexList').path)} className="dropdown-item">
                                {retrieveUrl('perfumesUnisexList').properties.title}
                            </Link>
                            <Link to={reverse(retrieveUrl('perfumesOnSaleList').path)} className="dropdown-item">
                                {retrieveUrl('perfumesOnSaleList').properties.title}
                            </Link>
                            <div className="dropdown-divider" />
                            {pages.map(({slug, title}) => (
                                <Link key={slug} to={`/${slug}`} className="dropdown-item">
                                    {title}
                                </Link>
                            ))}
                            <a href="/sitemap.xml" className="dropdown-item" target="_blank" rel="noreferrer noopener">
                                Mapa strony
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    );
});

export default NavBar;
