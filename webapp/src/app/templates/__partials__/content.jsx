import React from 'react';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';
import {APP_DESCRIPTION, APP_NAME} from '../../constants';
import {STATUS} from '../../utils/api/constants';

const Content = ({status, prepend, title, menu, children, ignoreNoResults, ignoreBadRequest}) => {
    // This should prevent blank titles on not found products.
    const titleToDisplay = title.trim().length === 0 ? 'Nie znaleziono' : title;

    return (
        <>
            <Helmet
                title={titleToDisplay}
                titleTemplate={`%s | ${APP_NAME} - ${APP_DESCRIPTION}`}
                defaultTitle={`${APP_NAME} - ${APP_DESCRIPTION}`}
            >
                <meta
                    name="description"
                    content="Perfumeria internetowa z testerami perfum popularnych marek. Zamów swoje ulubione perfumy nawet do 70% taniej z darmową dostawą do domu!"
                />
            </Helmet>
            <div className="container">
                {status === STATUS.INITIALIZED ||
                status === STATUS.OK ||
                (ignoreNoResults && status === STATUS.OK_NO_RESULTS) ||
                (ignoreBadRequest && status === STATUS.RESPONSE_BAD_REQUEST) ? (
                    <>
                        {prepend && prepend}
                        <h3 className={prepend ? 'mb-2' : ''}>{title}</h3>
                        {menu ? (
                            <div className="row no-gutters">
                                {menu}
                                <div className="col-sm-12 col-md-9 pl-md-5 p-3">{children}</div>
                            </div>
                        ) : (
                            children
                        )}
                    </>
                ) : status === STATUS.OK_NO_RESULTS || status === STATUS.RESPONSE_NOT_FOUND ? (
                    <>
                        {prepend && prepend}
                        <h3 className={prepend ? 'mb-2' : ''}>{titleToDisplay}</h3>
                        <p>Brak wyników.</p>
                    </>
                ) : (
                    <>
                        <h3>Coś poszło nie tak</h3>
                        <p>Spróbuj ponownie później.</p>
                    </>
                )}
            </div>
        </>
    );
};

Content.propTypes = {
    status: PropTypes.string,
    prepend: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    title: PropTypes.string.isRequired,
    menu: PropTypes.node,
    children: PropTypes.node.isRequired,
    ignoreNoResults: PropTypes.bool,
    ignoreBadRequest: PropTypes.bool,
};

Content.defaultProps = {
    status: STATUS.OK,
    prepend: null,
    menu: null,
    ignoreNoResults: false,
    ignoreBadRequest: false,
};

export default Content;
