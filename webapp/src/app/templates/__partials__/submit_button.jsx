import PropTypes from 'prop-types';
import React from 'react';

const SubmitButton = ({
    className,
    noScriptClassName,
    value,
    noScriptValue,
    disabled,
    noScriptDisabled,
    title,
    children,
    ...properties
}) => (
    <>
        <noscript>
            <button
                type="submit"
                className={`btn btn-primary ${noScriptClassName ?? className}`}
                disabled={noScriptDisabled}
                title={noScriptDisabled ? title ?? 'Twoja przeglądarka musi obsługiwać JavaScript.' : ''}
                {...properties}
            >
                {noScriptValue ?? value ?? children ?? 'Zatwierdź'}
            </button>
        </noscript>
        <button
            type="submit"
            className={`btn btn-primary ${className} only-js-inline`}
            disabled={disabled}
            {...properties}
        >
            {value ?? children ?? 'Zatwierdź'}
        </button>
    </>
);

SubmitButton.propTypes = {
    className: PropTypes.string,
    noScriptClassName: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    noScriptValue: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    disabled: PropTypes.bool,
    noScriptDisabled: PropTypes.bool,
    title: PropTypes.string,
    children: PropTypes.node,
};

SubmitButton.defaultProps = {
    className: '',
    noScriptClassName: null,
    value: null,
    noScriptValue: null,
    disabled: false,
    noScriptDisabled: false,
    title: null,
    children: null,
};

export default SubmitButton;
