import React from 'react';
import PropTypes from 'prop-types';
import Button from './button';

const DeleteButton = ({className, noScriptHref, title, onClick, disabled}) => (
    <Button
        className={className}
        noScriptHref={noScriptHref}
        title={title}
        onClick={onClick}
        disabled={disabled}
        noScriptValue="Usuń"
        value={<i className="icon-cancel" />}
    />
);

DeleteButton.propTypes = {
    className: PropTypes.string,
    noScriptHref: PropTypes.string,
    title: PropTypes.string,
    // eslint-disable-next-line react/require-default-props
    onClick: PropTypes.func,
    disabled: PropTypes.bool,
};

DeleteButton.defaultProps = {
    className: '',
    noScriptHref: null,
    title: null,
    disabled: false,
};

export default DeleteButton;
