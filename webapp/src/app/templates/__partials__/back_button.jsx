import PropTypes from 'prop-types';
import React from 'react';
import {Link} from 'react-router-dom';
import {history} from '../../utils/router';

const BackButton = ({className, href}) => (
    <>
        <noscript>
            <a href={href ?? '{{ REFERER }}'} className={`btn btn-primary ${className}`}>
                Wróć
            </a>
        </noscript>
        {href ? (
            <Link to={href} className={`btn btn-primary ${className} only-js-inline`}>
                Wróć
            </Link>
        ) : (
            <button
                type="button"
                className={`btn btn-primary ${className} only-js-inline`}
                onClick={history.goBack}
                aria-label="Wróć"
            >
                Wróć
            </button>
        )}
    </>
);

BackButton.propTypes = {
    className: PropTypes.string,
    href: PropTypes.string,
};

BackButton.defaultProps = {
    className: '',
    href: '',
};

export default BackButton;
