import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import React from 'react';
import {PAGINATION_DATA_PROP_TYPES} from '../../constants';
import {getQueryStringWithPreviousPage, getQueryStringWithNextPage} from '../../utils/pagination';

const Pagination = ({data: {previous, next, path, queryParameters}}) => (
    <nav>
        <ul className="pagination justify-content-center">
            {previous && (
                <li className="page-item">
                    <Link
                        to={`${path}?${getQueryStringWithPreviousPage(queryParameters)}`}
                        className="page-link"
                        aria-label="Poprzednia strona"
                    >
                        <span aria-hidden="true">&laquo;</span>
                        <span className="sr-only">Poprzednia strona</span>
                    </Link>
                </li>
            )}
            {next && (
                <li className="page-item">
                    <Link
                        to={`${path}?${getQueryStringWithNextPage(queryParameters)}`}
                        className="page-link"
                        aria-label="Następna strona"
                    >
                        <span aria-hidden="true">&raquo;</span>
                        <span className="sr-only">Następna strona</span>
                    </Link>
                </li>
            )}
        </ul>
    </nav>
);

Pagination.propTypes = {
    data: PAGINATION_DATA_PROP_TYPES(PropTypes).isRequired,
};

export default Pagination;
