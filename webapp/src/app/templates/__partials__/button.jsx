import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

const Button = ({className, noScriptHref, title, onClick, disabled, noScriptValue, value, children}) => {
    const noScriptFinalValue = noScriptValue ?? value ?? children ?? 'Przycisk';
    const finalValue = value ?? children ?? 'Przycisk';

    return (
        <>
            <noscript>
                {noScriptHref ? (
                    <Link
                        to={noScriptHref}
                        className={`btn btn-primary ${className ?? ''} ${disabled ? 'disabled' : ''}`}
                    >
                        {noScriptFinalValue}
                    </Link>
                ) : (
                    <button
                        type="button"
                        className={`btn btn-primary ${className}`}
                        title={title ?? 'Twoja przeglądarka musi obsługiwać JavaScript.'}
                        disabled
                        aria-label={noScriptFinalValue}
                    >
                        {noScriptFinalValue}
                    </button>
                )}
            </noscript>
            <button
                type="button"
                className={`btn btn-primary ${className} only-js-inline`}
                title={title}
                onClick={onClick}
                disabled={disabled}
                aria-label={finalValue === noScriptFinalValue ? finalValue : noScriptFinalValue}
            >
                {finalValue}
            </button>
        </>
    );
};

Button.propTypes = {
    className: PropTypes.string,
    noScriptHref: PropTypes.string,
    title: PropTypes.string,
    // eslint-disable-next-line react/require-default-props
    onClick: PropTypes.func,
    disabled: PropTypes.bool,
    noScriptValue: PropTypes.string,
    // eslint-disable-next-line react/require-default-props
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    // eslint-disable-next-line react/require-default-props
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
};

Button.defaultProps = {
    className: '',
    noScriptHref: null,
    title: null,
    disabled: false,
    noScriptValue: null,
};

export default Button;
