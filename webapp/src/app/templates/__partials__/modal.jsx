import React from 'react';
import PropTypes from 'prop-types';

const Modal = ({id}) => (
    <div id={id} className="modal fade" tabIndex="-1" role="dialog" aria-labelledby={id} aria-hidden="true">
        <div className="modal-dialog" role="document">
            <div className="modal-content" />
        </div>
    </div>
);

Modal.propTypes = {
    id: PropTypes.string.isRequired,
};

export default Modal;
