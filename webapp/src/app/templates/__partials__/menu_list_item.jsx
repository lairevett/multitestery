import React from 'react';
import {NavLink} from 'react-router-dom';
import PropTypes from 'prop-types';

const MenuListItem = ({url: {path, properties}, exact}) => (
    <li className="nav-item">
        <NavLink className="nav-link" activeClassName="active" to={path} exact={exact}>
            {properties.title}
        </NavLink>
    </li>
);

MenuListItem.propTypes = {
    url: PropTypes.exact({
        path: PropTypes.string.isRequired,
        methods: PropTypes.arrayOf(PropTypes.oneOf(['get', 'post'])).isRequired,
        view: PropTypes.func.isRequired,
        access: PropTypes.object.isRequired,
        properties: PropTypes.shape({title: PropTypes.string.isRequired, mode: PropTypes.string}).isRequired,
    }).isRequired,
    exact: PropTypes.bool,
};

MenuListItem.defaultProps = {
    exact: false,
};

export default MenuListItem;
