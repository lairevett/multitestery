import PropTypes from 'prop-types';
import React from 'react';

const SuccessAlert = ({message, children}) => (
    <div className="alert alert-success" role="alert">
        {message ?? children ?? 'Formularz wysłany pomyślnie.'}
    </div>
);

SuccessAlert.propTypes = {
    message: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
};

SuccessAlert.defaultProps = {
    message: null,
    children: null,
};

export default SuccessAlert;
