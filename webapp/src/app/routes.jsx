import {Route, Switch} from 'react-router-dom';

import React, {memo, useEffect, useState} from 'react';
import {useSelector} from 'react-redux';
import Error404 from './templates/__partials__/error404';
import Suspense from './utils/api/suspense';
import Loading from './templates/__partials__/loading';
import WrappedView from './templates/__partials__/wrapped_view';
import {retrieveUrls} from './utils/router';
import {registerDynamicUrls} from './utils/urls';
import {STATUS} from './utils/api/constants';

const Routes = memo(() => {
    const [dynamicUrlsRegistrationStatus, setDynamicUrlsRegistrationStatus] = useState(STATUS.INITIALIZED);
    const {results: pagesList} = useSelector(state => state.page.list);

    useEffect(() => {
        // Static URLs are registered automatically in app/utils/urls.js.
        // CSR and SSR URLs are independent of each other, so registering dynamic routes is necessary here, too.
        (async () => {
            await registerDynamicUrls(pagesList);
            setDynamicUrlsRegistrationStatus(STATUS.OK);
        })();
    }, [pagesList]);

    // Server registers dynamic routes in engage_matcher middleware, so Suspense is only needed in CSR-mode.
    const status = process.env.__CLIENT__ ? dynamicUrlsRegistrationStatus : STATUS.OK;

    return (
        <Suspense status={status} fallback={<Loading />}>
            <Switch>
                {Object.values(retrieveUrls()).map(url => (
                    <url.access
                        key={url.path}
                        exact
                        path={url.path}
                        render={properties => <WrappedView {...properties} {...url.properties} view={url.view} />}
                    />
                ))}
                <Route
                    render={({staticContext}) => {
                        if (staticContext) staticContext.status = 404;
                        return <Error404 />;
                    }}
                />
            </Switch>
        </Suspense>
    );
});

export default Routes;
