import Factory from '../app/utils/factory';

export default class AnnouncementFactory extends Factory {
    static #id = 1;

    static #priority = 1;

    #fields = {
        id: 0,
        priority: 0,
        image: 'http://via.placeholder.com/1100x500',
        title: 'Some title',
        description: 'Description below the title.',
        is_active: true,
    };

    getOne = () => ({
        ...this.#fields,
        id: AnnouncementFactory.#id++,
        priority: AnnouncementFactory.#priority++,
    });
}
