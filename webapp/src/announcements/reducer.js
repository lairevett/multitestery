import {ACTION, INITIAL_STATE} from './constants';

const announcementReducer = (state = INITIAL_STATE, action) => {
    if (action.type === ACTION.RETRIEVE_ACTIVE_LIST) {
        return {
            ...state,
            list: action.payload,
        };
    }

    return state;
};

export default announcementReducer;
