import {ACTION, ENDPOINT, LIST_INITIAL_STATE} from './constants';

import {apiGetListRequest} from '../app/utils/api/requests';

export const retrieveActiveList = () =>
    apiGetListRequest(ACTION.RETRIEVE_ACTIVE_LIST, LIST_INITIAL_STATE, ENDPOINT.LIST_ACTIVE);
