import {DEFAULT_STATUS_INITIAL_STATE} from '../app/utils/api/constants';

// API endpoints.
export const ENDPOINT = {
    LIST_ACTIVE: '/v1/announcements/?active=1',
};

// Redux action types.
export const ACTION = {
    RETRIEVE_ACTIVE_LIST: '@announcement/RETRIEVE_ACTIVE_LIST',
};

// Reducer initial states.
export const LIST_INITIAL_STATE = {
    results: [],
    ...DEFAULT_STATUS_INITIAL_STATE,
};

export const INITIAL_STATE = {
    list: LIST_INITIAL_STATE,
};

// Prop types.
export const DETAILS_PROP_TYPES = PropertyTypes =>
    PropertyTypes.exact({
        id: PropertyTypes.number.isRequired,
        priority: PropertyTypes.number.isRequired,
        image: PropertyTypes.string.isRequired,
        title: PropertyTypes.string.isRequired,
        description: PropertyTypes.string.isRequired,
        is_active: PropertyTypes.bool.isRequired,
    }).isRequired;

export const LIST_PROP_TYPES = PropertyTypes => PropertyTypes.arrayOf(DETAILS_PROP_TYPES(PropertyTypes));
