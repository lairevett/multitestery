import React from 'react';
import PropTypes from 'prop-types';
import {LIST_PROP_TYPES} from '../../constants';

const CarouselIndicators = ({list}) => (
    <ol className="carousel-indicators">
        {list.map(({id}, i) => (
            <li key={`_${id}`} data-target="#carousel" data-slide-to={i} className={i === 0 ? 'active' : ''} />
        ))}
    </ol>
);

CarouselIndicators.propTypes = {
    list: LIST_PROP_TYPES(PropTypes).isRequired,
};

export default CarouselIndicators;
