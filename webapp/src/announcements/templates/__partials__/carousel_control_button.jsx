import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

const CarouselControlButton = ({isPrevious}) => {
    const direction = isPrevious ? 'prev' : 'next';

    return (
        <Link to="#carousel" className={`carousel-control-${direction}`} role="button" data-slide={direction}>
            <span className={`carousel-control-${direction}-icon`} aria-hidden="true" />
            <span className="sr-only">{isPrevious ? 'Poprzednie' : 'Następne'} ogłoszenie</span>
        </Link>
    );
};

CarouselControlButton.propTypes = {
    isPrevious: PropTypes.bool,
};

CarouselControlButton.defaultProps = {
    isPrevious: false,
};

export default CarouselControlButton;
