import React, {useEffect} from 'react';

import PropTypes from 'prop-types';
import CarouselIndicators from './carousel_indicators';
import CarouselInner from './carousel_inner';
import {LIST_PROP_TYPES} from '../../constants';
import CarouselControlButton from './carousel_control_button';

const Carousel = ({list}) => {
    useEffect(() => {
        if (process.env.__CLIENT__ && list.length > 0) {
            /* eslint-disable no-new */
            // noinspection JSUnresolvedFunction
            new window.BSN.Carousel('#carousel', {
                interval: 5000,
                pause: false,
                keyboard: false,
            });
        }
    }, [list.length]);

    return list.length > 0 ? (
        <div id="carousel" className="carousel slide carousel-fade mb-3" data-ride="carousel">
            <CarouselIndicators list={list} />
            <CarouselInner list={list} />
            <CarouselControlButton isPrevious />
            <CarouselControlButton />
        </div>
    ) : null;
};

Carousel.propTypes = {
    list: LIST_PROP_TYPES(PropTypes).isRequired,
};

export default Carousel;
