import React from 'react';
import PropTypes from 'prop-types';
import {LIST_PROP_TYPES} from '../../constants';

// noinspection SpellCheckingInspection
const CarouselInner = ({list}) => (
    <div className="carousel-inner" role="list">
        {list.map(({id, image, description, title}, i) => (
            <div key={`_${id}`} className={`carousel-item ${i === 0 ? 'active' : ''}`} role="listitem">
                <div className="view">
                    <img className="d-block w-100 h-auto" src={image} alt={description} />
                    <div className="mask rgba-black-light" />
                </div>
                <div className="carousel-caption" style={{backgroundColor: '#00000055'}}>
                    <h3 className="h3-responsive text-light">{title}</h3>
                    <p>{description}</p>
                </div>
            </div>
        ))}
    </div>
);

CarouselInner.propTypes = {
    list: LIST_PROP_TYPES(PropTypes).isRequired,
};

export default CarouselInner;
