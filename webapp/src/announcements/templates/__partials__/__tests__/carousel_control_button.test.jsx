import React from 'react';
import {render} from '../../../../app/utils/tests';
import '@testing-library/jest-dom/extend-expect';
import CarouselControlButton from '../carousel_control_button';
import {testMatchesLastSnapshot} from '../../../../app/utils/tests/snapshot';

describe('CarouselControlButton', () => {
    const renderComponent = isPrevious => render(<CarouselControlButton isPrevious={isPrevious} />);

    testMatchesLastSnapshot(() => renderComponent(false));
    testMatchesLastSnapshot(() => renderComponent(true));

    it("prev button's link class is carousel-control-prev", () => {
        const {container} = renderComponent(true);
        // noinspection JSUnresolvedVariable
        expect(container.firstChild.classList).toContain('carousel-control-prev');
    });

    it("prev buttons's data-slide is equal to prev", () => {
        const {container} = renderComponent(true);
        expect(container.firstChild).toHaveAttribute('data-slide', 'prev');
    });

    it("prev button's span icon has carousel-control-prev-icon classname", () => {
        const {container} = renderComponent(true);
        expect(container.querySelector('.carousel-control-prev-icon')).toBeTruthy();
    });

    it('screen reader text says "Poprzednie ogłoszenie" when direction is equal to prev', () => {
        const {getByText} = renderComponent(true);
        expect(getByText(/Poprzednie ogłoszenie/i)).toBeInTheDocument();
    });

    it("next button's link class is carousel-control-next", () => {
        const {container} = renderComponent(false);
        // noinspection JSUnresolvedVariable
        expect(container.firstChild.classList).toContain('carousel-control-next');
    });

    it("next buttons's data-slide is equal to next", () => {
        const {container} = renderComponent(false);
        expect(container.firstChild).toHaveAttribute('data-slide', 'next');
    });

    it("next button 's span icon has carousel-control-next-icon classname", () => {
        const {container} = renderComponent(false);
        expect(container.querySelector('.carousel-control-next-icon')).toBeTruthy();
    });

    it('screen reader text says "Następne" when direction is equal to next', () => {
        const {getByText} = renderComponent(false);
        expect(getByText(/Następne ogłoszenie/i)).toBeInTheDocument();
    });
});
