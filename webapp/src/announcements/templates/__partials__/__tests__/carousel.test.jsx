import React from 'react';
import {render} from '../../../../app/utils/tests';
import '@testing-library/jest-dom/extend-expect';
import Carousel from '../carousel';
import AnnouncementFactory from '../../../factory';
import {testMatchesLastSnapshot} from '../../../../app/utils/tests/snapshot';

describe('Carousel', () => {
    const factory = new AnnouncementFactory();
    const announcement = factory.getOne();
    const renderComponent = properties => render(<Carousel {...properties} />);

    testMatchesLastSnapshot(() => renderComponent({list: [announcement]}));

    it("doesn't render the carousel when there aren't any announcements", () => {
        const {container} = renderComponent({list: []});
        expect(container.firstChild).toBeNull();
    });

    it('renders the carousel when there are one or more elements in the list', () => {
        const {getByText} = renderComponent({list: [announcement]});
        expect(getByText(announcement.title)).toBeInTheDocument();
    });
});
