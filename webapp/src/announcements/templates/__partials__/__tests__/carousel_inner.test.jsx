import React from 'react';
import {render} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import AnnouncementFactory from '../../../factory';
import CarouselInner from '../carousel_inner';
import {testMatchesLastSnapshot} from '../../../../app/utils/tests/snapshot';

describe('CarouselInner', () => {
    const factory = new AnnouncementFactory();
    const renderComponent = announcements => render(<CarouselInner list={announcements} />);

    testMatchesLastSnapshot(() => renderComponent(factory.getMultiple(2)));

    it('displays correct number of announcements', () => {
        const announcements = [];

        for (let i = 1; i <= 5; i++) {
            announcements.push(factory.getOne());
            const {container} = renderComponent(announcements);
            expect(container.querySelector('.carousel-inner').childElementCount).toBe(i);
        }
    });

    it('displays only one active element at a time', () => {
        const announcements = factory.getMultiple(5);
        const {container} = renderComponent(announcements);
        expect(container.querySelectorAll('.active').length).toBe(1);
    });
});
