import React from 'react';
import {render} from '@testing-library/react';
import CarouselIndicators from '../carousel_indicators';
import '@testing-library/jest-dom/extend-expect';
import AnnouncementFactory from '../../../factory';
import {testMatchesLastSnapshot} from '../../../../app/utils/tests/snapshot';

describe('CarouselIndicators', () => {
    const factory = new AnnouncementFactory();
    const renderComponent = announcements => render(<CarouselIndicators list={announcements} />);

    testMatchesLastSnapshot(() => renderComponent(factory.getMultiple(2)));

    it('displays correct number of indicators', () => {
        const announcements = [];

        for (let i = 1; i <= 5; i++) {
            announcements.push(factory.getOne());
            const {container} = renderComponent(announcements);
            expect(container.querySelector('.carousel-indicators').childElementCount).toBe(i);
        }
    });

    it('displays only one active element at a time', () => {
        const announcements = factory.getMultiple(5);
        const {container} = renderComponent(announcements);
        expect(container.querySelectorAll('.active').length).toBe(1);
    });
});
