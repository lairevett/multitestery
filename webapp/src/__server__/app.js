/* eslint-disable security/detect-non-literal-fs-filename */
import cors from 'cors';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import express from 'express';
import expressStaticGzip from 'express-static-gzip'; // eslint-disable-line import/no-extraneous-dependencies
import path from 'path';
import {handleRequest} from './utils/request';
import {makeRoomForSocket, getStreamInfo} from './utils/stream';
import {createPIDFile} from './utils/pid';
import handleMultipartFormData from './middleware/handle_multipart_form_data';
import createPipelineData from './middleware/create_pipeline_data';
import makeReduxStore from './middleware/make_redux_store';
import pingAPI from './middleware/ping_api';
import engageMatcher from './middleware/engage_matcher';
import forwardCSRFToken from './middleware/forward_csrf_token';
import retrieveUserData from './middleware/retrieve_user_data';
import retrievePagesData from './middleware/retrieve_pages_data';
import syncCartEntries from './middleware/sync_cart_entries';
import {handleViewEvents} from './middleware/handle_view_events';
import {retrieveUrls} from '../app/utils/router';
import '../app/utils/urls';

const isProduction = process.env?.NODE_ENV === 'production';

export const createMaster = () => {
    if (isProduction) createPIDFile();
    const streamInfo = getStreamInfo();
    makeRoomForSocket(streamInfo[1]);

    return streamInfo;
};

export const createSlave = () => {
    const streamInfo = getStreamInfo();
    const slave = express();
    slave.disable('x-powered-by'); // Don't send X-Powered-By: Express header.

    if (!isProduction) {
        // Serve static files from express when in development mode.
        // Static files in production are being served by nginx.
        const root = path.resolve(__dirname, '../../public');
        // noinspection JSCheckFunctionSignatures
        slave.use('/', expressStaticGzip(root, {index: false, enableBrotli: true}));
    }

    [
        cors(),
        cookieParser(),
        bodyParser.json(),
        bodyParser.urlencoded({extended: true}),
        createPipelineData,
        makeReduxStore,
        pingAPI,
        forwardCSRFToken,
        retrieveUserData,
        retrievePagesData,
        syncCartEntries,
        engageMatcher,
    ].forEach(middleware => slave.use(middleware));

    // For automatic colon params parsing in views.
    Object.values(retrieveUrls()).forEach(url => {
        if (url.methods.includes('get')) {
            // noinspection JSUnresolvedFunction
            slave.get(url.path, handleViewEvents, handleRequest);
        }

        if (url.methods.includes('post')) {
            // noinspection JSUnresolvedFunction
            slave.post(url.path, handleMultipartFormData(url), handleViewEvents, handleRequest);
        }
    });

    // noinspection JSUnresolvedFunction
    slave.get('*', handleViewEvents, handleRequest);

    const server = slave.listen(streamInfo[1], () => {
        console.log(`Listening at ${streamInfo[0]} ${streamInfo[1]}...`);
    });

    ['error', 'SIGINT', 'SIGTERM'].forEach(signal => {
        process.on(signal, () => {
            console.info('Worker received kill signal.');

            if (typeof server !== 'undefined' && server != null) {
                server.close(() => {
                    console.info('Worker closed all connections and was shut down gracefully.');
                    process.exit(128);
                });
            } else {
                process.exit(128);
            }

            setTimeout(() => {
                console.info('Worker failed to close all connections and was shut down forcefully.');
                process.exit(129);
            }, 10000);
        });
    });
};
