require('../../babel_loader');
const cluster = require('cluster');
const app = require('./app');
const {getArgumentValue} = require('./utils/argv');
const {removeSocket} = require('./utils/stream');

(() => {
    if (cluster.isMaster) {
        let timeout = null;
        const streamInfo = app.createMaster();
        const workersCount = +getArgumentValue('workers') || 3;
        let activeWorkers = [];

        for (let i = 0; i < workersCount; i++) {
            activeWorkers.push(cluster.fork());
        }

        cluster.on('online', worker => {
            console.info(`Worker started (${worker.process.pid}).`);
        });

        cluster.on('exit', (worker, code, signal) => {
            activeWorkers = activeWorkers.filter(activeWorker => activeWorker.process.pid !== worker.process.pid);

            if (timeout !== null && activeWorkers.length === 0) {
                clearTimeout(timeout);
            }

            if (code === 128 || code === 129 || signal === 'SIGINT') {
                return;
            }

            console.info(`Worker ${worker.process.pid} died with code ${code} and signal ${signal}.`);
            activeWorkers.push(cluster.fork());
        });

        ['error', 'SIGINT', 'SIGTERM'].forEach(signal => {
            process.on(signal, () => {
                console.info('Master process received kill signal, shutting down gracefully.');

                timeout = setTimeout(() => {
                    console.info('Master process timed out while waiting for children, shutting down forcefully.');
                    removeSocket(streamInfo[1]);
                    process.exit(1);
                }, 11000);
            });
        });
    } else {
        app.createSlave();
    }
})();
