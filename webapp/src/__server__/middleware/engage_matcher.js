import {matchPath} from 'react-router-dom';
import {makeErrorResponse} from '../utils/responses';
import {logError} from '../../app/utils/logger';
import {retrieveUrls} from '../../app/utils/router';
import {registerDynamicUrls} from '../../app/utils/urls';

const engageMatcher = async (request, response, next) => {
    const {store} = request.__PIPELINE_DATA__;

    try {
        // Static URLs are registered automatically in app/utils/urls.js.
        // CSR and SSR URLs are independent of each other, so registering dynamic routes is necessary here, too.
        await registerDynamicUrls(store.getState().page.list.results);
        request.__PIPELINE_DATA__.match = Object.values(retrieveUrls()).find(url =>
            matchPath(request.path, {path: url.path, exact: true})
        );

        next();
    } catch (error) {
        logError(error.stack);
        return makeErrorResponse(response, 500, 'Something went wrong while trying to match route.');
    }
};

export default engageMatcher;
