import {retrieveActiveList as retrieveActivePagesList} from '../../pages/actions';
import {logDispatchAPIErrors} from '../../app/utils/logger';
import {makeErrorResponse} from '../utils/responses';

const retrievePagesData = async (request, response, next) => {
    try {
        request.__PIPELINE_DATA__.store.dispatch(await retrieveActivePagesList());

        next();
    } catch (error) {
        logDispatchAPIErrors(error);
        return makeErrorResponse(response, 500, 'Failed to retrieve pages from the API.');
    }
};

export default retrievePagesData;
