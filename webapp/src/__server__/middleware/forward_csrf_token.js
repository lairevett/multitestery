import {APP_CSRF_COOKIE_NAME} from '../../app/constants';
import {logError} from '../../app/utils/logger';
import {makeErrorResponse} from '../utils/responses';

const forwardCSRFToken = (request, response, next) => {
    try {
        // Forward the token only if there isn't already one in browser's cookies.
        const csrfToken = request.cookies?.[APP_CSRF_COOKIE_NAME];

        if (!csrfToken) {
            const options =
                process.env.NODE_ENV === 'production'
                    ? {domain: process.env.API_URL.replace('https://www.api', ''), secure: true, sameSite: 'Lax'}
                    : {};

            response.cookie(APP_CSRF_COOKIE_NAME, request.__PIPELINE_DATA__.csrfToken, options);
        }

        next();
    } catch (error) {
        logError(error.stack);
        return makeErrorResponse(response, 500, "Failed to forward CSRF token to client's browser.");
    }
};

export default forwardCSRFToken;
