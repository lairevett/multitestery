import {getServerCartEntries} from '../../cart/utils';
import {setEntries} from '../../cart/actions';
import {logError} from '../../app/utils/logger';
import {makeErrorResponse} from '../utils/responses';

const syncCartEntries = (request, response, next) => {
    try {
        const entries = getServerCartEntries(request);
        if (entries) request.__PIPELINE_DATA__.store.dispatch(setEntries(entries));

        next();
    } catch (error) {
        logError(error.stack);
        return makeErrorResponse(response, 500, 'Failed to sync cart entries between client and server.');
    }
};

export default syncCartEntries;
