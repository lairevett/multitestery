import {logDispatchAPIErrors, logError} from '../../app/utils/logger';
import {makeErrorResponse} from '../utils/responses';

export const handleViewEvents = async (request, response, next) => {
    const {match, store} = request.__PIPELINE_DATA__;

    if (typeof match !== 'undefined') {
        const {view, properties: urlProperties} = match;

        try {
            if (typeof view.willDispatchEvents === 'function') {
                const dispatchResponse = await view.willDispatchEvents(
                    request,
                    response,
                    urlProperties,
                    store.getState(),
                    store.dispatch
                );

                if (typeof dispatchResponse === 'function') return dispatchResponse();
            }
        } catch (error) {
            logError(error.stack);
            return makeErrorResponse(response, 500, 'Failed to execute willDispatchEvents().');
        }

        try {
            if (typeof urlProperties.callbackGetDispatchEvents === 'function') {
                const dispatchEvents = urlProperties.callbackGetDispatchEvents(
                    request.params,
                    request.query,
                    store.getState(),
                    {
                        headers: request.headers,
                    }
                );

                if (dispatchEvents) {
                    const unwrappedOnInitEvents = dispatchEvents.onInit.map(onInitEvent => onInitEvent());
                    await Promise.allSettled(unwrappedOnInitEvents)
                        .then(promises => promises.forEach(promise => store.dispatch(promise.value)))
                        .catch(error => console.error(error));
                }
            }
        } catch (error) {
            logDispatchAPIErrors(error);
            return makeErrorResponse(response, 500, 'Failed to execute callbackGetDispatchEvents().');
        }

        try {
            if (typeof view.didDispatchEvents === 'function') {
                const dispatchResponse = view.didDispatchEvents(
                    request,
                    response,
                    urlProperties,
                    store.getState(),
                    store.dispatch
                );

                if (typeof dispatchResponse === 'function') return dispatchResponse();
            }
        } catch (error) {
            logError(error.stack);
            return makeErrorResponse(response, 500, 'Failed to execute didDispatchEvents().');
        }
    }

    next();
};
