import multer from 'multer';
import {logError} from '../../app/utils/logger';
import {makeErrorResponse} from '../utils/responses';

const validate = url => (request, file, callback) => {
    const options = url.properties.formFileFields[file.fieldname];

    if (!options.accept.includes(file.mimetype)) {
        return callback(
            new Error(`Wrong file mimetype (${file.mimetype}), accepting only ${options.accept.join(', ')}`)
        );
    }

    return callback(null, true);
};

const handleMultipartFormData = url => (request, response, next) => {
    if (request.headers['content-type'].includes('multipart/form-data')) {
        if (typeof url.properties.formFileFields === 'undefined') {
            logError(`Form file fields for ${url.path} aren't defined.`);
            return makeErrorResponse(response, 500);
        }

        const multerInstance = multer({fileFilter: validate(url)});

        const fields = Object.entries(url.properties.formFileFields).map(([name, {maxCount}]) => ({name, maxCount}));
        const handle = multerInstance.fields(fields);

        handle(request, response, error => {
            if (error instanceof multer.MulterError) {
                return makeErrorResponse(response, 500);
            }
            if (error) {
                return makeErrorResponse(response, 400, error.message);
            }

            next();
        });
    } else {
        next();
    }
};

export default handleMultipartFormData;
