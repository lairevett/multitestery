const createPipelineData = (request, response, next) => {
    request.__PIPELINE_DATA__ = {};
    next();
};

export default createPipelineData;
