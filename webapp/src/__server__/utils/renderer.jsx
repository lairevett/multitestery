import Helmet from 'react-helmet';
import {Provider} from 'react-redux';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import {StaticRouter} from 'react-router-dom';
import serialize from 'serialize-javascript';
import App from '../../app/templates/__app__';

export const render = (request, response, indexContent) => {
    const context = {};
    const {store} = request.__PIPELINE_DATA__;
    const {referer} = request.headers; // Required for back button links to work (href to referer).

    const jsx = (
        <Provider store={store}>
            <StaticRouter location={request.url} context={context}>
                <App />
            </StaticRouter>
        </Provider>
    );

    // noinspection JSUnresolvedFunction
    const appContent = ReactDOMServer.renderToString(jsx).replace('{{ REFERER }}', referer ?? '#');

    // noinspection JSUnresolvedFunction
    const helmetTags = Helmet.renderStatic();

    // noinspection JSUnresolvedVariable
    return [
        // eslint-disable-next-line xss/no-mixed-html
        indexContent
            .replace('{{ HELMET_TITLE }}', helmetTags.title.toString())
            .replace('{{ HELMET_META }}', helmetTags.meta.toString())
            .replace(
                '{{ __REDUX_PRELOADED_STATE__ }}',
                `<script>window.__REDUX_PRELOADED_STATE__ = ${serialize(store.getState(), {
                    isJSON: true,
                })};</script>`
            )
            .replace('<div id="app"></div>', `<div id="app">${appContent}</div>`),
        context,
    ];
};
