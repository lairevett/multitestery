export const makeErrorResponse = (response, statusCode, message = null) =>
    response
        .set({'content-type': 'text/plain; charset=utf8'})
        .status(statusCode)
        .end(`Coś poszło nie tak, spróbuj ponownie później.${message ? `\nMessage: ${message}` : ''}`);

export const makeSuccessResponse = (response, rendererData) => {
    if (rendererData[1].url) return response.redirect(rendererData[1].url);

    return response
        .set({'content-type': 'text/html'})
        .status(rendererData[1].status || 200)
        .end(rendererData[0]);
};
