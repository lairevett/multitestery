/* eslint-disable security/detect-non-literal-fs-filename */
import fs from 'fs';
import {getArgumentValue} from './argv';

export const createPIDFile = () => {
    const pidFile = getArgumentValue('pid');
    if (pidFile === null) {
        return;
    }

    const PID = `${process.pid.toString()}\n`;
    const buffer = Buffer.allocUnsafe(PID.length);
    buffer.fill(PID);

    // noinspection JSCheckFunctionSignatures
    fs.open(pidFile, 'w', 0o660, (error, fd) => {
        if (error) {
            throw error;
        }

        // eslint-disable-next-line no-shadow
        fs.write(fd, buffer, 0, buffer.length, error => {
            if (error) {
                throw error;
            }

            fs.close(fd, () => {
                console.info('PID file created successfully.');
            });
        });
    });
};
