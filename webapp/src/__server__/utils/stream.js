/* eslint-disable security/detect-non-literal-fs-filename */
import fs from 'fs';
import {getArgumentValue} from './argv';

export const removeSocket = path => {
    if (path !== null && `${path}`.startsWith('/')) {
        try {
            fs.unlinkSync(path);
            console.info('Socket file removed successfully.');
        } catch (error) {
            console.error('Failed to remove socket file.');
        }
    }
};

export const makeRoomForSocket = path => {
    if (path !== null && `${path}`.startsWith('/')) {
        try {
            const stats = fs.statSync(path);
            if (stats && stats.isSocket()) {
                console.warn('Provided socket address is already in use, trying to remove it...');
                removeSocket(path);
            }
            // eslint-disable-next-line no-empty
        } catch {}
    }
};

export const getStreamInfo = () => {
    const bind = getArgumentValue('bind');
    if (bind === null) {
        return ['port', 3000];
    }

    const streamType = bind.startsWith('/') ? 'socket' : 'port';
    const stream = streamType === 'port' ? +bind || 3000 : bind;

    return [streamType, stream];
};
