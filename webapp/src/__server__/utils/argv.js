export const getArgumentValue = argument => {
    const argv = process.argv.slice(2);
    const index = argv.indexOf(`--${argument}`);
    if (index === -1) {
        return null;
    }

    const value = argv[index + 1];
    if (typeof value == 'undefined' || value == null || value.startsWith('-')) {
        return null;
    }

    return value;
};
