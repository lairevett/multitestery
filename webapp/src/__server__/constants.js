// API endpoints.
export const ENDPOINT = {
    PING: '/ping/',
};

// Redux action types.
export const ACTION = {
    PING: '@__server__/PING',
    SET_WILL_DISPATCH_NEXT_EVENTS: '@__server__/SET_WILL_DISPATCH_NEXT_EVENTS',
};

// Reducer initial states.
export const INITIAL_STATE = {
    willDispatchNextEvents: true,
};
