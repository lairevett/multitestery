import {ACTION, DETAILS_INITIAL_STATE, ENDPOINT, LIST_INITIAL_STATE} from './constants';
import {apiGetListRequest, apiGetRequest, apiPostRequest} from '../app/utils/api/requests';

export const create = (entries, forwardingAddressId, shipping, context) =>
    apiPostRequest(
        ACTION.CREATE,
        {},
        ENDPOINT.CREATE,
        {entries, forwarding_address: forwardingAddressId, shipping},
        context
    );

export const retrieveAllList = (userId, page, context) =>
    apiGetListRequest(ACTION.RETRIEVE_ALL_LIST, LIST_INITIAL_STATE, ENDPOINT.LIST_ALL(userId, page), context);

export const retrieveNotPaidList = (userId, page, context) =>
    apiGetListRequest(ACTION.RETRIEVE_NOT_PAID_LIST, LIST_INITIAL_STATE, ENDPOINT.LIST_NOT_PAID(userId, page), context);

export const retrievePaidList = (userId, page, context) =>
    apiGetListRequest(ACTION.RETRIEVE_PAID_LIST, LIST_INITIAL_STATE, ENDPOINT.LIST_PAID(userId, page), context);

export const retrieveCanceledList = (userId, page, context) =>
    apiGetListRequest(ACTION.RETRIEVE_CANCELED_LIST, LIST_INITIAL_STATE, ENDPOINT.LIST_CANCELED(userId, page), context);

export const retrieveSentList = (userId, page, context) =>
    apiGetListRequest(ACTION.RETRIEVE_SENT_LIST, LIST_INITIAL_STATE, ENDPOINT.LIST_SENT(userId, page), context);

export const clearList = () => ({type: ACTION.CLEAR_LIST});

export const retrieveDetails = (userId, orderId, context) =>
    apiGetRequest(ACTION.RETRIEVE_DETAILS, DETAILS_INITIAL_STATE, ENDPOINT.DETAILS(userId, orderId), context);

export const cancel = (userId, orderId, context) =>
    apiPostRequest(ACTION.CANCEL, DETAILS_INITIAL_STATE, ENDPOINT.CANCEL(userId, orderId), {}, context);

export const clearDetails = () => ({type: ACTION.CLEAR_DETAILS});
