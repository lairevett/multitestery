import React from 'react';

import PropTypes from 'prop-types';
import OrdersTable from './__partials__/orders_table';
import {LIST_PROP_TYPES} from '../constants';
import {PAGINATION_DATA_PROP_TYPES} from '../../app/constants';
import Pagination from '../../app/templates/__partials__/pagination';

const OrdersList = ({list, paginationData}) => (
    <>
        <OrdersTable list={list} isAdminPanel={false} />
        <Pagination data={paginationData} />
    </>
);

OrdersList.propTypes = {
    list: LIST_PROP_TYPES(PropTypes).isRequired,
    paginationData: PAGINATION_DATA_PROP_TYPES(PropTypes).isRequired,
};

export default OrdersList;
