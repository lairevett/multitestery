import React from 'react';

import PropTypes from 'prop-types';
import BackButton from '../../app/templates/__partials__/back_button';
import CancelOrderButton from './__partials__/cancel_order_button';
import OrderData from './__partials__/order_data';
import OrderEntriesTable from './__partials__/order_entries_table';
import {DETAILS_PROP_TYPES, STATUS} from '../constants';
import {retrieveUrl} from '../../app/utils/router';

const OrdersDetails = ({userId, details: {status, id, created_at, forwarding_address, entries, total, shipping}}) => (
    <>
        <OrderData createdAt={created_at} shipping={shipping} status={status} forwardingAddress={forwarding_address} />
        <OrderEntriesTable entries={entries} total={total} isMutable={false} />
        <div className="d-flex justify-content-between">
            <BackButton href={retrieveUrl('ordersAllList').path} />
            {status === STATUS.PLACED && <CancelOrderButton userId={userId} orderId={id} />}
        </div>
    </>
);

OrdersDetails.propTypes = {
    userId: PropTypes.number.isRequired,
    details: DETAILS_PROP_TYPES(PropTypes).isRequired,
};

export default OrdersDetails;
