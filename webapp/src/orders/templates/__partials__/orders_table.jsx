import React from 'react';
import PropTypes from 'prop-types';
import OrdersTableBody from './orders_table_body';
import OrdersTableHead from './orders_table_head';
import {LIST_PROP_TYPES} from '../../constants';

const OrdersTable = ({list, isAdminPanel}) => (
    <div className="table-responsive">
        <table className="table table-hover">
            <OrdersTableHead />
            <OrdersTableBody list={list} isAdminPanel={isAdminPanel} />
        </table>
    </div>
);

OrdersTable.propTypes = {
    list: LIST_PROP_TYPES(PropTypes).isRequired,
    isAdminPanel: PropTypes.bool.isRequired,
};

export default OrdersTable;
