import React from 'react';

import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {reverse} from 'named-urls';
import {DETAILS_PROP_TYPES as PERFUME_DETAILS_PROP_TYPES} from '../../../perfumes/constants';
import {convertTitleToUrlFriendlyString, makeTitle} from '../../../perfumes/utils';
import {retrieveUrl} from '../../../app/utils/router';

const OrderEntriesTableNameColumn = ({details: {id, brand, model, amount, is_deleted}}) => {
    const title = makeTitle(brand, model, amount);

    return (
        <td className="text-center">
            {!is_deleted ? (
                <Link
                    to={reverse(retrieveUrl('perfumesDetails').path.replace('(\\d+)', ''), {
                        productId: id,
                        productStringForSEO: convertTitleToUrlFriendlyString(title),
                    })}
                >
                    {title}
                </Link>
            ) : (
                title
            )}
        </td>
    );
};

OrderEntriesTableNameColumn.propTypes = {
    details: PERFUME_DETAILS_PROP_TYPES(PropTypes).isRequired,
};

export default OrderEntriesTableNameColumn;
