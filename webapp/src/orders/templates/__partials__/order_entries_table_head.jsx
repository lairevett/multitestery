import PropTypes from 'prop-types';
import React from 'react';

const OrderEntriesTableHead = ({isMutable}) => (
    <thead>
        <tr>
            <td className="text-center">#</td>
            <td className="text-center">Nazwa</td>
            <td className="text-center">Sztuk</td>
            <td className="text-center">Cena</td>
            {isMutable && <td className="text-center">Usuń</td>}
        </tr>
    </thead>
);

OrderEntriesTableHead.propTypes = {
    isMutable: PropTypes.bool.isRequired,
};

export default OrderEntriesTableHead;
