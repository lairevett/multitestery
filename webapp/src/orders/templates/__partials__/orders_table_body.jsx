import {Link} from 'react-router-dom';
import React from 'react';
import {reverse} from 'named-urls';
import PropTypes from 'prop-types';
import {LIST_PROP_TYPES} from '../../constants';
import {CURRENCY_FORMATTER} from '../../../app/constants';
import {retrieveUrl} from '../../../app/utils/router';
import {getPrice} from '../../../perfumes/utils';

const OrdersTableBody = ({list, isAdminPanel}) => (
    <tbody>
        {list.map(({id, total, created_at}) => (
            <tr key={`_${id}`}>
                <td className="text-center">{id}</td>
                <td className="text-center">{new Date(created_at).toLocaleDateString('pl-PL')}</td>
                <td className="text-center">{CURRENCY_FORMATTER.format(getPrice(total, 0))}</td>
                <td className="text-center">
                    <Link
                        to={reverse(
                            isAdminPanel
                                ? retrieveUrl('adminOrdersDetails').path.replace('(\\d+)', '')
                                : retrieveUrl('ordersDetails').path.replace('(\\d+)', ''),
                            {orderId: id}
                        )}
                        className="btn btn-sm btn-primary"
                        aria-label="Detale zamówienia"
                    >
                        <noscript>*</noscript>
                        <i className="icon-info only-js-inline" />
                    </Link>
                </td>
            </tr>
        ))}
    </tbody>
);

OrdersTableBody.propTypes = {
    list: LIST_PROP_TYPES(PropTypes).isRequired,
    isAdminPanel: PropTypes.bool.isRequired,
};

export default OrdersTableBody;
