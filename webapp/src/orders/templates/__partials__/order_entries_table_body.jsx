/* eslint-disable security/detect-object-injection */
import React from 'react';
import PropTypes from 'prop-types';
import {DETAILS_ENTRIES_PROP_TYPES} from '../../constants';
import {ENTRIES_PROP_TYPES as CART_ENTRIES_PROP_TYPES} from '../../../cart/constants';
import {LIST_PROP_TYPES as PERFUME_LIST_PROP_TYPES} from '../../../perfumes/constants';
import OrderEntriesTableRow from './order_entries_table_row';

const OrderEntriesTableBody = ({entries, products, isMutable}) => {
    const isCart = typeof products !== 'undefined';

    return (
        <tbody>
            {isCart
                ? Object.entries(entries).map(([_, quantity], i) => (
                      <OrderEntriesTableRow
                          key={`_${products[i].id}`}
                          index={i}
                          product={products[i]}
                          quantity={quantity}
                          isMutable={isMutable}
                      />
                  ))
                : entries.map((entry, i) => (
                      <OrderEntriesTableRow
                          key={`_${entry.product.id}`}
                          index={i}
                          product={entry.product}
                          quantity={entry.quantity}
                          subtotal={entry.subtotal}
                          isMutable={isMutable}
                      />
                  ))}
        </tbody>
    );
};

OrderEntriesTableBody.propTypes = {
    entries: PropTypes.oneOfType([CART_ENTRIES_PROP_TYPES(PropTypes), DETAILS_ENTRIES_PROP_TYPES(PropTypes)])
        .isRequired,
    products: PERFUME_LIST_PROP_TYPES(PropTypes),
    isMutable: PropTypes.bool.isRequired,
};

OrderEntriesTableBody.defaultProps = {
    products: undefined,
};

export default OrderEntriesTableBody;
