import React from 'react';
import PropTypes from 'prop-types';
import {getPrice} from '../../utils';
import {CURRENCY_FORMATTER} from '../../../app/constants';

// If subtotal is provided, then data is coming from order reducer, otherwise it's coming from cart.
const OrderEntriesTablePriceColumn = ({unitPrice, quantity, percentOff, subtotal}) => {
    // If it's the whole subtotal, just get the formatted price (subtotal * 1, 0% off).
    const subtotalToDisplay = subtotal ? getPrice(subtotal, 1, 0) : getPrice(unitPrice, quantity, percentOff);

    return (
        <td className="text-center" style={{width: '16.66%'}}>
            {CURRENCY_FORMATTER.format(subtotalToDisplay)}
        </td>
    );
};

OrderEntriesTablePriceColumn.propTypes = {
    unitPrice: PropTypes.number,
    quantity: PropTypes.number.isRequired,
    percentOff: PropTypes.number,
    subtotal: PropTypes.number,
};

OrderEntriesTablePriceColumn.defaultProps = {
    unitPrice: undefined,
    percentOff: undefined,
    subtotal: undefined,
};

export default OrderEntriesTablePriceColumn;
