import React from 'react';
import MenuList from '../../../app/templates/__partials__/menu_list';
import MenuListItem from '../../../app/templates/__partials__/menu_list_item';
import {retrieveUrl} from '../../../app/utils/router';

const Menu = () => (
    <MenuList>
        <MenuListItem url={retrieveUrl('ordersAllList')} />
        <MenuListItem url={retrieveUrl('ordersNotPaidList')} />
        <MenuListItem url={retrieveUrl('ordersPaidList')} />
        <MenuListItem url={retrieveUrl('ordersCanceledList')} />
        <MenuListItem url={retrieveUrl('ordersSentList')} />
    </MenuList>
);

export default Menu;
