import React from 'react';

import PropTypes from 'prop-types';
import OrderEntriesTableBody from './order_entries_table_body';
import OrderEntriesTableFoot from './order_entries_table_foot';
import OrderEntriesTableHead from './order_entries_table_head';
import {DETAILS_ENTRIES_PROP_TYPES} from '../../constants';
import {ENTRIES_PROP_TYPES as CART_ENTRIES_PROP_TYPES} from '../../../cart/constants';
import {LIST_PROP_TYPES as PERFUME_LIST_PROP_TYPES} from '../../../perfumes/constants';

const OrderEntriesTable = ({entries, products, total, isMutable}) => (
    <div className="table-responsive">
        <table className="table table-hover">
            <OrderEntriesTableHead isMutable={isMutable} />
            <OrderEntriesTableBody entries={entries} products={products} isMutable={isMutable} />
            <OrderEntriesTableFoot entries={entries} products={products} total={total} />
        </table>
    </div>
);

OrderEntriesTable.propTypes = {
    entries: PropTypes.oneOfType([CART_ENTRIES_PROP_TYPES(PropTypes), DETAILS_ENTRIES_PROP_TYPES(PropTypes)])
        .isRequired,
    products: PERFUME_LIST_PROP_TYPES(PropTypes),
    total: PropTypes.number,
    isMutable: PropTypes.bool.isRequired,
};

OrderEntriesTable.defaultProps = {
    products: undefined,
    total: undefined,
};

export default OrderEntriesTable;
