import React from 'react';
import PropTypes from 'prop-types';
import {getPrice} from '../../utils';
import {DETAILS_ENTRIES_PROP_TYPES} from '../../constants';
import {ENTRIES_PROP_TYPES as CART_ENTRIES_PROP_TYPES} from '../../../cart/constants';
import {LIST_PROP_TYPES as PERFUME_LIST_PROP_TYPES} from '../../../perfumes/constants';
import {CURRENCY_FORMATTER} from '../../../app/constants';

// If total is provided, then data is coming from order reducer,
// otherwise it's coming from cart and the total needs to be calculated.
const OrderEntriesTableFoot = ({entries, products, total}) => {
    let totalToDisplay;

    if (total) {
        totalToDisplay = getPrice(total, 1, 0);
    } else {
        totalToDisplay = Object.values(entries).reduce(
            (sum, quantity, i) => sum + getPrice(+products?.[i]?.unit_price, quantity, +products?.[i]?.percent_off),
            0
        );
    }

    return (
        <tfoot>
            <tr>
                <td colSpan="5" className="text-right">
                    Razem: {CURRENCY_FORMATTER.format(totalToDisplay)}
                </td>
            </tr>
        </tfoot>
    );
};

OrderEntriesTableFoot.propTypes = {
    entries: PropTypes.oneOfType([CART_ENTRIES_PROP_TYPES(PropTypes), DETAILS_ENTRIES_PROP_TYPES(PropTypes)])
        .isRequired,
    products: PERFUME_LIST_PROP_TYPES(PropTypes),
    total: PropTypes.number,
};

OrderEntriesTableFoot.defaultProps = {
    products: undefined,
    total: undefined,
};

export default OrderEntriesTableFoot;
