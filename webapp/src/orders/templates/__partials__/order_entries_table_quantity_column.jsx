import PropTypes from 'prop-types';
import React from 'react';
import ChangeQuantityForm from '../../../cart/forms/change_product_quantity';
import {FORM as CART_FORM} from '../../../cart/constants';

const OrderEntriesTableQuantityColumn = ({index, productId, quantity, isMutable}) => {
    const formName = `${CART_FORM.CHANGE_PRODUCT_QUANTITY}_${index}`;

    return (
        <td className="text-center">
            {isMutable ? (
                <div className="d-flex justify-content-center">
                    <ChangeQuantityForm form={formName} initialValues={{productId, quantity}} />
                </div>
            ) : (
                quantity
            )}
        </td>
    );
};

OrderEntriesTableQuantityColumn.propTypes = {
    index: PropTypes.number.isRequired,
    productId: PropTypes.number.isRequired,
    quantity: PropTypes.number.isRequired,
    isMutable: PropTypes.bool.isRequired,
};

export default OrderEntriesTableQuantityColumn;
