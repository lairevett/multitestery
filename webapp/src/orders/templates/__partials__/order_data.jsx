import React from 'react';
import PropTypes from 'prop-types';
import {SHIPPING, STATUS} from '../../constants';
import {DETAILS_PROP_TYPES as FORWARDING_ADDRESS_DETAILS_PROP_TYPES} from '../../../auth/constants/forwarding_addresses';

const OrderData = ({status, shipping, createdAt, forwardingAddress}) => {
    const {first_name, last_name, phone, street, home_number, apt_number, postal_code, city} = forwardingAddress;

    return (
        <div className="text-center d-md-flex justify-content-between">
            <div>
                <h5>Stan</h5>
                <p>
                    {status === STATUS.CANCELED
                        ? 'Anulowane'
                        : status === STATUS.PLACED
                        ? 'Złożone'
                        : status === STATUS.PAID
                        ? 'Zapłacone'
                        : status === STATUS.SENT
                        ? 'Wysłane'
                        : 'Nie udało się pobrać stanu.'}
                </p>
            </div>
            <div>
                <h5>Data złożenia</h5>
                <p>{createdAt !== '' && new Date(createdAt).toLocaleString('pl-PL')}</p>
            </div>
            <div>
                <h5>Dane wysyłki</h5>
                <ul className="list-unstyled">
                    <li>{shipping === SHIPPING.CASH_ON_DELIVERY ? 'Płatność za pobraniem' : 'Płatność z góry'}</li>
                    <li>
                        <h6 className="m-0">
                            {first_name} {last_name}
                        </h6>
                    </li>
                    <li className="text-muted">{phone}</li>
                    <li>
                        {street} {home_number}
                        {apt_number && ` m. ${apt_number}`}
                    </li>
                    <li>
                        {postal_code} {city}
                    </li>
                </ul>
            </div>
        </div>
    );
};

OrderData.propTypes = {
    status: PropTypes.number.isRequired,
    shipping: PropTypes.number.isRequired,
    createdAt: PropTypes.string.isRequired,
    forwardingAddress: FORWARDING_ADDRESS_DETAILS_PROP_TYPES(PropTypes).isRequired,
};

export default OrderData;
