import React from 'react';
import PropTypes from 'prop-types';
import OrderEntriesTableNameColumn from './order_entries_table_name_column';
import OrderEntriesTableQuantityColumn from './order_entries_table_quantity_column';
import RemoveProductForm from '../../../cart/forms/remove_product';
import {FORM as CART_FORM} from '../../../cart/constants';
import {DETAILS_PROP_TYPES} from '../../../perfumes/constants';
import OrderEntriesTablePriceColumn from './order_entries_table_price_column';

const OrderEntriesTableRow = ({index, product, quantity, subtotal, isMutable}) => {
    const formName = `${CART_FORM.REMOVE_PRODUCT}_${index}`;

    return (
        <tr>
            <td className="text-center">{index + 1}</td>
            <OrderEntriesTableNameColumn details={product} />
            <OrderEntriesTableQuantityColumn
                index={index}
                productId={product.id}
                quantity={quantity}
                isMutable={isMutable}
            />
            <OrderEntriesTablePriceColumn
                unitPrice={product.unit_price}
                quantity={quantity}
                percentOff={product.percent_off}
                subtotal={subtotal}
            />
            {isMutable && (
                <td className="text-center">
                    <RemoveProductForm form={formName} initialValues={{productId: product.id}} />
                </td>
            )}
        </tr>
    );
};

OrderEntriesTableRow.propTypes = {
    index: PropTypes.number.isRequired,
    product: DETAILS_PROP_TYPES(PropTypes).isRequired,
    quantity: PropTypes.number.isRequired,
    subtotal: PropTypes.number,
    isMutable: PropTypes.bool.isRequired,
};

OrderEntriesTableRow.defaultProps = {
    subtotal: undefined,
};

export default OrderEntriesTableRow;
