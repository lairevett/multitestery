import React, {useCallback} from 'react';
import PropTypes from 'prop-types';
import {useDispatch} from 'react-redux';
import {reverse} from 'named-urls';
import {cancel} from '../../actions';
import {makeYesNoModal} from '../../../app/utils/modal';
import {MODAL_CANCEL_ORDER} from '../../constants';
import CancelButton from '../../../app/templates/__partials__/cancel_button';
import Modal from '../../../app/templates/__partials__/modal';
import {retrieveUrl} from '../../../app/utils/router';

const CancelOrderButton = ({userId, orderId}) => {
    const dispatch = useDispatch();
    const dispatchCancelOrder = useCallback(async () => dispatch(await cancel(userId, orderId)), [
        dispatch,
        userId,
        orderId,
    ]);

    return (
        <>
            {/* This component is not used in a loop, so modal can be defined here. */}
            <Modal id={MODAL_CANCEL_ORDER} />
            <CancelButton
                noScriptHref={reverse(retrieveUrl('ordersCancel').path.replace('(\\d+)', ''), {orderId})}
                onClick={() =>
                    makeYesNoModal(
                        MODAL_CANCEL_ORDER,
                        'Anuluj',
                        `Czy na pewno chcesz anulować Zamówienie #${orderId}?`,
                        dispatchCancelOrder
                    )
                }
            />
        </>
    );
};

CancelOrderButton.propTypes = {
    userId: PropTypes.number.isRequired,
    orderId: PropTypes.number.isRequired,
};

export default CancelOrderButton;
