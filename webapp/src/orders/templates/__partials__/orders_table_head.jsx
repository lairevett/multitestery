import React from 'react';

const OrdersTableHead = () => (
    <thead>
        <tr>
            <td className="text-center">#</td>
            <td className="text-center">Data złożenia</td>
            <td className="text-center">Suma</td>
            <td className="text-center">Szczegóły</td>
        </tr>
    </thead>
);

export default OrdersTableHead;
