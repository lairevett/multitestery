import React from 'react';
import PropTypes from 'prop-types';
import CancelOrderForm from '../forms/cancel_order';

const OrdersDetailsCancel = ({userId, orderId}) => <CancelOrderForm initialValues={{userId, orderId}} />;

OrdersDetailsCancel.propTypes = {
    userId: PropTypes.number.isRequired,
    orderId: PropTypes.number.isRequired,
};

export default OrdersDetailsCancel;
