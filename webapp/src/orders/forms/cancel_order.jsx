import React from 'react';
import {Field, Form, reduxForm, propTypes} from 'redux-form';
import {submit, validate} from './process_cancel_order';
import SubmitButton from '../../app/templates/__partials__/submit_button';
import {FormCard, FormField} from '../../app/templates/__partials__/form';
import BackButton from '../../app/templates/__partials__/back_button';
import {FORM} from '../constants';

const CancelOrderForm = ({handleSubmit}) => (
    <FormCard>
        <Form method="post" action="?" onSubmit={handleSubmit(submit)}>
            <p>Czy na pewno chcesz anulować to zamówienie?</p>
            <Field id="userId" name="userId" type="hidden" parse={value => +value} component={FormField} />
            <Field id="orderId" name="orderId" type="hidden" parse={value => +value} component={FormField} />
            <BackButton className="mr-3" value="Nie" aria-label="Przycisk nie" />
            <SubmitButton value="Tak" aria-label="Przycisk tak" />
        </Form>
    </FormCard>
);

CancelOrderForm.propTypes = propTypes;

export default reduxForm({
    form: FORM.CANCEL_ORDER,
    enableReinitialize: true,
    validate,
    shouldValidate: () => process.env.__CLIENT__,
})(CancelOrderForm);
