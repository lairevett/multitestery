import {reverse} from 'named-urls';
import {SubmissionError} from 'redux-form';
import {validateFieldRequired} from '../../app/utils/validation';
import {cancel} from '../actions';
import {history, retrieveUrl} from '../../app/utils/router';
import {STATUS} from '../../app/utils/api/constants';
import {logDispatchAPIErrors} from '../../app/utils/logger';

export const validate = ({userId, orderId}) => {
    const errors = {};

    errors.userId = validateFieldRequired(userId);
    errors.orderId = validateFieldRequired(orderId);

    return errors;
};

export const submit = async ({userId, orderId}, dispatch) => {
    const {payload} = dispatch(await cancel(userId, orderId));

    if (payload.errors) {
        throw new SubmissionError(payload.errors);
    } else if (payload.__meta__.status !== STATUS.OK) {
        throw SubmissionError;
    }

    history.push(reverse(retrieveUrl('ordersDetails').path.replace('(\\d+)', ''), {orderId}));
};

export const submitServer = async (request, response, dispatch) => {
    const {userId, orderId} = request.body;
    const parsedUserId = +userId;
    const parsedOrderId = +orderId;

    const parsedValues = {
        userId: parsedUserId,
        orderId: parsedOrderId,
    };

    try {
        if (Object.values(validate(parsedValues)).every(value => !value)) {
            const {payload} = dispatch(await cancel(parsedUserId, parsedOrderId, {headers: request.headers}));

            if (payload.__meta__.status === STATUS.OK) {
                return () =>
                    response.redirect(
                        `${reverse(retrieveUrl('ordersDetails').path.replace('(\\d+)', ''), {orderId: parsedOrderId})}`
                    );
            }
        }
    } catch (error) {
        logDispatchAPIErrors(error);
    }

    return () =>
        response.redirect(
            `${reverse(retrieveUrl('ordersDetails').path.replace('(\\d+)', ''), {orderId: parsedOrderId})}?cancel_error`
        );
};
