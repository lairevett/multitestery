import {DEFAULT_LIST_INITIAL_STATE, DEFAULT_STATUS_INITIAL_STATE} from '../app/utils/api/constants';
import {
    DETAILS_INITIAL_STATE as AUTH_FORWARDING_ADDRESSES_DETAILS_INITIAL_STATE,
    DETAILS_PROP_TYPES as AUTH_FORWARDING_ADDRESSES_DETAILS_PROP_TYPES,
} from '../auth/constants/forwarding_addresses';
import {DETAILS_PROP_TYPES as PERFUME_DETAILS_PROP_TYPES} from '../perfumes/constants';

// API endpoints.
export const ENDPOINT = {
    CREATE: '/v1/users/orders/',
    LIST_ALL: (userId, page) => `/v1/users/${userId}/orders/?page=${page}`,
    LIST_NOT_PAID: (userId, page) => `/v1/users/${userId}/orders/?paid=0&page=${page}`,
    LIST_PAID: (userId, page) => `/v1/users/${userId}/orders/?paid=1&page=${page}`,
    LIST_CANCELED: (userId, page) => `/v1/users/${userId}/orders/?canceled=1&page=${page}`,
    LIST_SENT: (userId, page) => `/v1/users/${userId}/orders/?sent=1&page=${page}`,
    DETAILS: (userId, orderId) => `/v1/users/${userId}/orders/${orderId}/`,
    CANCEL: (userId, orderId) => `/v1/users/${userId}/orders/${orderId}/cancel/`,
};

// Redux action types.
export const ACTION = {
    CREATE: '@order/CREATE',
    RETRIEVE_ALL_LIST: '@order/RETRIEVE_ALL_LIST',
    RETRIEVE_NOT_PAID_LIST: '@order/RETRIEVE_NOT_PAID_LIST',
    RETRIEVE_PAID_LIST: '@order/RETRIEVE_PAID_LIST',
    RETRIEVE_SENT_LIST: '@order/RETRIEVE_SENT_LIST',
    RETRIEVE_CANCELED_LIST: '@order/RETRIEVE_CANCELED_LIST',
    CLEAR_LIST: '@order/CLEAR_LIST',
    RETRIEVE_DETAILS: '@order/RETRIEVE_DETAILS',
    CANCEL: '@order/CANCEL',
    CLEAR_DETAILS: '@order/CLEAR_DETAILS',
};

// Redux form names.
export const FORM = {
    CANCEL_ORDER: 'FORM_CANCEL_ORDER',
};

// Reducer initial states.
export const LIST_INITIAL_STATE = {
    ...DEFAULT_LIST_INITIAL_STATE,
    ...DEFAULT_STATUS_INITIAL_STATE,
};

export const DETAILS_INITIAL_STATE = {
    id: 0,
    name: '',
    entries: [],
    forwarding_address: AUTH_FORWARDING_ADDRESSES_DETAILS_INITIAL_STATE,
    total: 0,
    shipping: 0,
    status: 0,
    created_at: '',
    updated_at: '',
    ...DEFAULT_STATUS_INITIAL_STATE,
};

export const INITIAL_STATE = {
    list: LIST_INITIAL_STATE,
    details: DETAILS_INITIAL_STATE,
};

// Prop types.
export const LIST_PROP_TYPES = PropertyTypes => PropertyTypes.arrayOf(DETAILS_PROP_TYPES(PropertyTypes));

export const DETAILS_PROP_TYPES = PropertyTypes =>
    PropertyTypes.exact({
        id: PropertyTypes.number.isRequired,
        name: PropertyTypes.string.isRequired,
        entries: DETAILS_ENTRIES_PROP_TYPES(PropertyTypes),
        forwarding_address: AUTH_FORWARDING_ADDRESSES_DETAILS_PROP_TYPES(PropertyTypes),
        total: PropertyTypes.number.isRequired,
        shipping: PropertyTypes.number.isRequired,
        status: PropertyTypes.number.isRequired,
        created_at: PropertyTypes.string.isRequired,
        updated_at: PropertyTypes.string.isRequired,
        __meta__: PropertyTypes.exact({status: PropertyTypes.string.isRequired}),
    });

export const DETAILS_ENTRY_PROP_TYPES = PropertyTypes =>
    PropertyTypes.exact({
        id: PropertyTypes.number.isRequired,
        product: PERFUME_DETAILS_PROP_TYPES(PropertyTypes),
        quantity: PropertyTypes.number.isRequired,
        subtotal: PropertyTypes.number.isRequired,
    }).isRequired;

export const DETAILS_ENTRIES_PROP_TYPES = PropertyTypes =>
    PropertyTypes.arrayOf(DETAILS_ENTRY_PROP_TYPES(PropertyTypes));

// Template modes.
export const MODE = {
    LIST_ALL: 'MODE_LIST_ALL',
    LIST_NOT_PAID: 'MODE_LIST_NOT_PAID',
    LIST_PAID: 'MODE_LIST_PAID',
    LIST_CANCELED: 'MODE_LIST_CANCELED',
    LIST_SENT: 'MODE_LIST_SENT',
    DETAILS: 'MODE_DETAILS',
};

// Order status.
export const SHIPPING = {
    CASH_ON_DELIVERY: 0,
    CASH_UP_FRONT: 1,
};

export const STATUS = {
    CANCELED: -1,
    PLACED: 0,
    PAID: 1,
    SENT: 2,
};

// Submit operations.
export const OPERATION = {
    SUB_QUANTITY: 'OPERATION_SUB_QUANTITY',
    ADD_QUANTITY: 'OPERATION_ADD_QUANTITY',
};

// Modal ids.
export const MODAL_CANCEL_ORDER = 'cancel-order';
