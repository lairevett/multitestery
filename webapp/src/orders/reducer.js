import {ACTION, DETAILS_INITIAL_STATE, INITIAL_STATE, LIST_INITIAL_STATE} from './constants';

const orderReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ACTION.RETRIEVE_ALL_LIST:
        case ACTION.RETRIEVE_NOT_PAID_LIST:
        case ACTION.RETRIEVE_PAID_LIST:
        case ACTION.RETRIEVE_CANCELED_LIST:
        case ACTION.RETRIEVE_SENT_LIST:
            return {
                ...state,
                list: action.payload,
            };
        case ACTION.CLEAR_LIST:
            return {
                ...state,
                list: LIST_INITIAL_STATE,
            };
        case ACTION.RETRIEVE_DETAILS:
        case ACTION.CANCEL:
            return {
                ...state,
                details: action.payload,
            };
        case ACTION.CLEAR_DETAILS:
            return {
                ...state,
                details: DETAILS_INITIAL_STATE,
            };
        default:
            return state;
    }
};

export default orderReducer;
