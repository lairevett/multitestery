import {ACCESS, registerUrl} from '../app/utils/router';
import {OrdersCancelView, OrdersDetailsView, OrdersListView} from './views';

import {MODE} from './constants';
import {callbackGetDispatchEvents} from './utils';

registerUrl('ordersAllList', '/orders/all', ['get'], OrdersListView, ACCESS.REQUIRE_AUTH, {
    title: 'Wszystkie zamówienia',
    mode: MODE.LIST_ALL,
    callbackGetDispatchEvents,
});

registerUrl('ordersNotPaidList', '/orders/not-paid', ['get'], OrdersListView, ACCESS.REQUIRE_AUTH, {
    title: 'Nieopłacone zamówienia',
    mode: MODE.LIST_NOT_PAID,
    callbackGetDispatchEvents,
});

registerUrl('ordersPaidList', '/orders/paid', ['get'], OrdersListView, ACCESS.REQUIRE_AUTH, {
    title: 'Opłacone zamówienia',
    mode: MODE.LIST_PAID,
    callbackGetDispatchEvents,
});

registerUrl('ordersCanceledList', '/orders/canceled', ['get'], OrdersListView, ACCESS.REQUIRE_AUTH, {
    title: 'Anulowane zamówienia',
    mode: MODE.LIST_CANCELED,
    callbackGetDispatchEvents,
});

registerUrl('ordersSentList', '/orders/sent', ['get'], OrdersListView, ACCESS.REQUIRE_AUTH, {
    title: 'Wysłane zamówienia',
    mode: MODE.LIST_SENT,
    callbackGetDispatchEvents,
});

registerUrl('ordersDetails', '/orders/all/:orderId(\\d+)', ['get'], OrdersDetailsView, ACCESS.REQUIRE_AUTH, {
    title: 'Szczegóły zamówienia',
    mode: MODE.DETAILS,
    callbackGetDispatchEvents,
});

registerUrl(
    'ordersCancel',
    '/orders/all/:orderId(\\d+)/cancel',
    ['get', 'post'],
    OrdersCancelView,
    ACCESS.REQUIRE_AUTH,
    {
        title: 'Anuluj zamówienie',
    }
);
