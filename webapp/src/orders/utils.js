import {
    clearDetails,
    clearList,
    retrieveAllList,
    retrieveCanceledList,
    retrieveDetails,
    retrieveNotPaidList,
    retrievePaidList,
    retrieveSentList,
} from './actions';

import {MODE} from './constants';
import {makeDispatchEvents} from '../app/utils/dispatch_events';

export const formatForwardingAddress = forwardingAddress =>
    `${forwardingAddress.first_name} ${forwardingAddress.last_name}, \
    ${forwardingAddress.phone}, \
    ${forwardingAddress.street}, ${forwardingAddress.postal_code} ${forwardingAddress.city}`;

export const getPrice = (unitPrice, quantity, percentOff) => {
    const price = percentOff ? Math.ceil(unitPrice - unitPrice * (percentOff / 100)) : unitPrice;
    return (price / 100) * quantity;
};

export const callbackGetDispatchEvents = urlProperties => (urlParameters, queryParameters, state, context) => {
    const orderId = +urlParameters.orderId;
    const userId = state.auth.user.id;
    const page = +queryParameters.page || 1;

    switch (urlProperties.mode) {
        case MODE.LIST_ALL:
            return makeDispatchEvents([() => retrieveAllList(userId, page, context)], [clearList], [page]);
        case MODE.LIST_NOT_PAID:
            return makeDispatchEvents([() => retrieveNotPaidList(userId, page, context)], [clearList], [page]);
        case MODE.LIST_PAID:
            return makeDispatchEvents([() => retrievePaidList(userId, page, context)], [clearList], [page]);
        case MODE.LIST_CANCELED:
            return makeDispatchEvents([() => retrieveCanceledList(userId, page, context)], [clearList], [page]);
        case MODE.LIST_SENT:
            return makeDispatchEvents([() => retrieveSentList(userId, page, context)], [clearList], [page]);
        case MODE.DETAILS:
            return makeDispatchEvents([() => retrieveDetails(userId, orderId, context)], [clearDetails]);
        default:
            throw new Error(`Unexpected mode "${urlProperties.mode}" provided in orders callbackGetDispatchEvents.`);
    }
};
