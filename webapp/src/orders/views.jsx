/* eslint-disable react/prop-types */
import React from 'react';

import Content from '../app/templates/__partials__/content';
import OrdersDetails from './templates/details';
import OrdersList from './templates/list';
import Loading from '../app/templates/__partials__/loading';
import OrdersDetailsCancel from './templates/details_cancel';
import {submitServer as submitServerCancelOrder} from './forms/process_cancel_order';
import Suspense from '../app/utils/api/suspense';
import Menu from './templates/__partials__/menu';

/*
 * GET /orders/all[?page={page}]
 * GET /orders/not-paid[?page={page}]
 * GET /orders/paid[?page={page}]
 * GET /orders/canceled[?page={page}]
 * GET /orders/sent[?page={page}]
 */
export const OrdersListView = ({title, match: {path}, queryParameters, state}) => {
    const {list} = state.order;
    const {
        previous,
        next,
        results,
        __meta__: {status},
    } = list;

    return (
        <Content status={status} title={title} menu={<Menu />} ignoreNoResults>
            <Suspense status={status} fallback={<Loading />}>
                <OrdersList list={results} paginationData={{previous, next, path, queryParameters}} />
            </Suspense>
        </Content>
    );
};

/*
 * GET /orders/all/:orderId(\d+)
 */
export const OrdersDetailsView = ({title, state}) => {
    const userId = state.auth.user.id;

    const {details} = state.order;
    const {
        __meta__: {status},
    } = details;

    return (
        <Content status={status} title={title} menu={<Menu />}>
            <Suspense status={status} fallback={<Loading />}>
                <OrdersDetails userId={userId} details={details} />
            </Suspense>
        </Content>
    );
};

/*
 * GET /orders/all/:orderId(\d+)/cancel
 * POST /orders/all/:orderId(\d+)/cancel
 */
export const OrdersCancelView = ({title, urlParameters, state}) => {
    const orderId = +urlParameters.orderId;

    const {
        id,
        __meta__: {status},
    } = state.auth.user;

    return (
        <Content status={status} title={title} menu={<Menu />}>
            <Suspense status={status} fallback={<Loading />}>
                <OrdersDetailsCancel userId={id} orderId={orderId} />
            </Suspense>
        </Content>
    );
};

OrdersCancelView.willDispatchEvents = (request, response, urlProperties, state, dispatch) => {
    if (request.method === 'POST') {
        return submitServerCancelOrder(request, response, dispatch);
    }
};
