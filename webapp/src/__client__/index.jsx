import {Provider} from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';
import ReactGA from 'react-ga';
import {Router} from 'react-router-dom';
import App from '../app/templates/__app__';
import {history} from '../app/utils/router';
import makeStore from '../app/utils/store';
import '../app/utils/urls';

import './assets/fonts/font-awesome.woff2';
import './assets/fonts/parisienne.woff2';

import './assets/css/font-awesome.css';
import './assets/css/bootstrap_lux.css';
import './assets/css/style.css';

import BSN from './assets/js/bootstrap-native.min';

// Make Bootstrap.Native usable outside by attaching it to the window.
window.BSN = BSN;

// Sync client's redux state with state created on server.
// noinspection JSUnresolvedVariable
const preloadedState = window.__REDUX_PRELOADED_STATE__;
// noinspection JSUnresolvedVariable
delete window.__PRELOADED_STATE__;
const store = makeStore(preloadedState);

// Initialize Google Analytics.
ReactGA.initialize('UA-150971718-1');
history.listen(location => {
    // Scroll to the top of the site on location change.
    window.scrollTo(0, 0);

    // Monitor which pages are viewed mostly.
    ReactGA.pageview(location.pathname + location.search);
});

// Hydrate DOM with server-rendered HTML.
ReactDOM.hydrate(
    <Provider store={store}>
        <Router history={history}>
            <App />
        </Router>
    </Provider>,
    document.getElementById('app')
);

if ('serviceWorker' in navigator) {
    window.addEventListener('load', async () => {
        try {
            await navigator.serviceWorker.register('/service_worker.js');
            console.log('Service worker registered!');
        } catch (error) {
            console.warn(error);
        }
    });
}
