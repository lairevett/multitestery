import React from 'react';
import PropTypes from 'prop-types';
import UsersTable from './__partials__/users_table';
import {LIST_PROP_TYPES, MODAL_DESTROY_USER} from '../../constants/users';
import {PAGINATION_DATA_PROP_TYPES} from '../../../app/constants';
import Modal from '../../../app/templates/__partials__/modal';
import Pagination from '../../../app/templates/__partials__/pagination';

const AdminUsersList = ({list, userId, paginationData}) => (
    <>
        <Modal id={MODAL_DESTROY_USER} />
        <UsersTable list={list} userId={userId} />
        <Pagination data={paginationData} />
    </>
);

AdminUsersList.propTypes = {
    list: LIST_PROP_TYPES(PropTypes).isRequired,
    userId: PropTypes.number.isRequired,
    paginationData: PAGINATION_DATA_PROP_TYPES(PropTypes).isRequired,
};

export default AdminUsersList;
