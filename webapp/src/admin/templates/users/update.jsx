import React from 'react';
import PropTypes from 'prop-types';
import AdminUserUpdateForm from '../../forms/user_update';
import {DETAILS_PROP_TYPES} from '../../../auth/constants/user';

const AdminUsersUpdate = ({details: {id, username, email, is_staff, is_superuser}, isSuccess, isError}) => (
    <AdminUserUpdateForm
        initialValues={{id, username, email, isStaff: is_staff}}
        isSuperUser={is_superuser}
        isSuccess={isSuccess}
        isError={isError}
    />
);

AdminUsersUpdate.propTypes = {
    details: DETAILS_PROP_TYPES(PropTypes).isRequired,
    isSuccess: PropTypes.bool.isRequired,
    isError: PropTypes.bool.isRequired,
};

export default AdminUsersUpdate;
