import React from 'react';
import PropTypes from 'prop-types';
import UsersTableHead from './users_table_head';
import UsersTableBody from './users_table_body';
import {LIST_PROP_TYPES} from '../../../constants/users';

const UsersTable = ({list, userId}) => (
    <div className="table-responsive">
        <table className="table table-hover">
            <UsersTableHead />
            <UsersTableBody list={list} userId={userId} />
        </table>
    </div>
);

UsersTable.propTypes = {
    list: LIST_PROP_TYPES(PropTypes).isRequired,
    userId: PropTypes.number.isRequired,
};

export default UsersTable;
