import React from 'react';

const UsersTableHead = () => (
    <thead>
        <tr>
            <td className="text-center">#</td>
            <td className="text-center">Nazwa użytkownika</td>
            <td className="text-center">Adres email</td>
            <td className="text-center">Data dołączenia</td>
            <td className="text-center">Edytuj</td>
            <td className="text-center">Usuń</td>
        </tr>
    </thead>
);

export default UsersTableHead;
