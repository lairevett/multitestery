import React from 'react';
import PropTypes from 'prop-types';
import UsersTableDeleteButton from './users_table_delete_button';

// If currently logged in user's id is equal to user id in table, then forbid deleting.
const UsersTableDeleteButtonColumn = ({userId, id, username, isSuperUser}) => (
    <td
        className="text-center"
        data-toggle="tooltip"
        data-placement="bottom"
        title={
            id === userId
                ? 'Nie możesz usunąć swojego konta.'
                : isSuperUser
                ? 'Nie możesz usunąć konta superużytkownika.'
                : ''
        }
    >
        <UsersTableDeleteButton userId={userId} id={id} username={username} isSuperUser={isSuperUser} />
    </td>
);

UsersTableDeleteButtonColumn.propTypes = {
    userId: PropTypes.number.isRequired,
    id: PropTypes.number.isRequired,
    username: PropTypes.string.isRequired,
    isSuperUser: PropTypes.bool.isRequired,
};

export default UsersTableDeleteButtonColumn;
