import React from 'react';
import PropTypes from 'prop-types';
import UsersTableEditButton from './users_table_edit_button';

// If currently logged in user's id is equal to user id in table, then forbid editing.
const UsersTableEditButtonColumn = ({userId, id, isSuperUser}) => (
    <td
        className="text-center"
        data-toggle="tooltip"
        data-placement="bottom"
        title={
            id === userId
                ? 'Swoje konto możesz edytować w ustawieniach.'
                : isSuperUser
                ? 'Nie możesz edytować konta superużytkownika.'
                : ''
        }
    >
        <UsersTableEditButton userId={userId} id={id} isSuperUser={isSuperUser} />
    </td>
);

UsersTableEditButtonColumn.propTypes = {
    userId: PropTypes.number.isRequired,
    id: PropTypes.number.isRequired,
    isSuperUser: PropTypes.bool.isRequired,
};

export default UsersTableEditButtonColumn;
