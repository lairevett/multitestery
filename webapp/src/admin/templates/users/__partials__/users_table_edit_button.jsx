import React from 'react';
import {reverse} from 'named-urls';
import PropTypes from 'prop-types';
import EditButton from '../../../../app/templates/__partials__/edit_button';
import {retrieveUrl} from '../../../../app/utils/router';

// If currently logged in user's id is equal to user id in table, then forbid editing.
const UsersTableEditButton = ({userId, id, isSuperUser}) => (
    <EditButton
        className={`btn-sm${id === userId || isSuperUser ? ' disabled' : ''}`}
        href={reverse(retrieveUrl('adminUsersUpdate').path.replace('(\\d+)', ''), {userId: id})}
    />
);

UsersTableEditButton.propTypes = {
    userId: PropTypes.number.isRequired,
    id: PropTypes.number.isRequired,
    isSuperUser: PropTypes.bool.isRequired,
};

export default UsersTableEditButton;
