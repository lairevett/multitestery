import React, {useCallback} from 'react';
import {useDispatch} from 'react-redux';
import PropTypes from 'prop-types';
import {makeYesNoModal} from '../../../../app/utils/modal';
import {destroy} from '../../../actions/users';
import {MODAL_DESTROY_USER} from '../../../constants/users';
import DeleteButton from '../../../../app/templates/__partials__/delete_button';

// If currently logged in user's id is equal to user id in table, then forbid deleting.
const UsersTableDeleteButton = ({userId, id, username, isSuperUser}) => {
    const dispatch = useDispatch();
    const dispatchDestroyUser = useCallback(async () => dispatch(await destroy(id)), [dispatch, id]);

    return (
        <DeleteButton
            className="btn-sm"
            disabled={id === userId || isSuperUser}
            onClick={() =>
                makeYesNoModal(
                    MODAL_DESTROY_USER,
                    'Usuń',
                    `Czy na pewno chcesz usunąć użytkownika "${username}"?`,
                    dispatchDestroyUser
                )
            }
        />
    );
};

UsersTableDeleteButton.propTypes = {
    userId: PropTypes.number.isRequired,
    id: PropTypes.number.isRequired,
    username: PropTypes.string.isRequired,
    isSuperUser: PropTypes.bool.isRequired,
};

export default UsersTableDeleteButton;
