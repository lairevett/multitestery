import React from 'react';
import PropTypes from 'prop-types';
import UsersTableEditButtonColumn from './users_table_edit_button_column';
import UsersTableDeleteButtonColumn from './users_table_delete_button_column';
import {LIST_PROP_TYPES} from '../../../constants/users';

const UsersTableBody = ({list, userId}) => (
    <tbody>
        {list.map(({id, username, email, is_superuser, date_joined}) => (
            <tr key={`_${id}`}>
                <td className="text-center">{id}</td>
                <td className="text-center">{username}</td>
                <td className="text-center">{email}</td>
                <td className="text-center">{new Date(date_joined).toLocaleDateString('pl-PL')}</td>
                <UsersTableEditButtonColumn userId={userId} id={id} isSuperUser={is_superuser} />
                <UsersTableDeleteButtonColumn userId={userId} id={id} username={username} isSuperUser={is_superuser} />
            </tr>
        ))}
    </tbody>
);

UsersTableBody.propTypes = {
    list: LIST_PROP_TYPES(PropTypes).isRequired,
    userId: PropTypes.number.isRequired,
};

export default UsersTableBody;
