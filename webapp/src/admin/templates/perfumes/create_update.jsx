import React from 'react';
import PropTypes from 'prop-types';
import AdminPerfumesCreateUpdateForm from '../../forms/perfumes_create_update';
import {MODE} from '../../constants/perfumes';
import {DETAILS_PROP_TYPES} from '../../../perfumes/constants';

const AdminPerfumesCreateUpdate = ({details, mode, formFileFields, isError}) => {
    const {
        id,
        photo,
        unit_price,
        top_notes,
        heart_notes,
        base_notes,
        percent_off,
        is_featured,
        ...restOfDetails
    } = details;

    return mode === MODE.CREATE ? (
        <AdminPerfumesCreateUpdateForm fileFields={formFileFields} isError={isError} />
    ) : (
        <AdminPerfumesCreateUpdateForm
            initialValues={{
                productId: details.id,
                unitPrice: unit_price,
                topNotes: top_notes,
                heartNotes: heart_notes,
                baseNotes: base_notes,
                percentOff: percent_off,
                isFeatured: is_featured,
                ...restOfDetails,
            }}
            fileFields={formFileFields}
            isError={isError}
        />
    );
};

AdminPerfumesCreateUpdate.propTypes = {
    // Could be null if mode is equal to MODE.CREATE.
    details: PropTypes.oneOfType([PropTypes.oneOf([null]).isRequired, DETAILS_PROP_TYPES(PropTypes)]).isRequired,
    mode: PropTypes.string.isRequired,
    formFileFields: PropTypes.objectOf(PropTypes.object).isRequired,
    isError: PropTypes.bool.isRequired,
};

export default AdminPerfumesCreateUpdate;
