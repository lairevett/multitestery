import React from 'react';
import PropTypes from 'prop-types';
import PerfumesTableHead from './perfumes_table_head';
import PerfumesTableBody from './perfumes_table_body';
import {LIST_PROP_TYPES} from '../../../../perfumes/constants';
import {MODE} from '../../../constants/perfumes';

const PerfumesTable = ({list, mode}) => (
    <div className="table-responsive">
        <table className="table table-hover">
            <PerfumesTableHead />
            <PerfumesTableBody list={list} mode={mode} />
        </table>
    </div>
);

PerfumesTable.propTypes = {
    list: LIST_PROP_TYPES(PropTypes).isRequired,
    mode: PropTypes.oneOf(Object.values(MODE)).isRequired,
};

export default PerfumesTable;
