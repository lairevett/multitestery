import React, {useCallback} from 'react';
import {useDispatch} from 'react-redux';
import PropTypes from 'prop-types';
import {addQuantity} from '../../../actions/perfumes';
import {MODE} from '../../../constants/perfumes';
import Button from '../../../../app/templates/__partials__/button';

const PerfumesTableAddQuantity = ({index, id, quantity, mode}) => {
    const dispatch = useDispatch();
    const dispatchAddQuantity = useCallback(async () => dispatch(await addQuantity(index, id, quantity, mode)), [
        dispatch,
        index,
        id,
        quantity,
        mode,
    ]);

    return (
        <Button
            className="btn btn-sm btn-primary ml-2"
            noScriptValue="+"
            value={<i className="icon-plus" />}
            onClick={dispatchAddQuantity}
        />
    );
};

PerfumesTableAddQuantity.propTypes = {
    index: PropTypes.number.isRequired,
    id: PropTypes.number.isRequired,
    quantity: PropTypes.number.isRequired,
    mode: PropTypes.oneOf(Object.values(MODE)).isRequired,
};

export default PerfumesTableAddQuantity;
