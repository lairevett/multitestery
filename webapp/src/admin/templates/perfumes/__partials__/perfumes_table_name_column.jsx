import React from 'react';
import {reverse} from 'named-urls';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {convertTitleToUrlFriendlyString} from '../../../../perfumes/utils';
import {retrieveUrl} from '../../../../app/utils/router';

const PerfumesTableNameColumn = ({id, title}) => (
    <td className="text-center">
        <Link
            to={reverse(retrieveUrl('perfumesDetails').path.replace('(\\d+)', ''), {
                productId: id,
                productStringForSEO: convertTitleToUrlFriendlyString(title),
            })}
        >
            {title}
        </Link>
    </td>
);

PerfumesTableNameColumn.propTypes = {
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
};

export default PerfumesTableNameColumn;
