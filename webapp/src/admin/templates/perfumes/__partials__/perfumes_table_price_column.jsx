import React from 'react';
import PropTypes from 'prop-types';
import {CURRENCY_FORMATTER} from '../../../../app/constants';
import {getPrice} from '../../../../perfumes/utils';

const PerfumesTablePriceColumn = ({unitPrice, percentOff}) => {
    const formattedPrice = CURRENCY_FORMATTER.format(getPrice(unitPrice, percentOff));

    return (
        <td className="text-center">
            {percentOff ? (
                <>
                    <del className="discount-old-price">{CURRENCY_FORMATTER.format(getPrice(unitPrice, 0))}</del>{' '}
                    <ins className="discount-new-price">{formattedPrice}</ins>
                </>
            ) : (
                formattedPrice
            )}
        </td>
    );
};

PerfumesTablePriceColumn.propTypes = {
    unitPrice: PropTypes.number.isRequired,
    percentOff: PropTypes.number.isRequired,
};

export default PerfumesTablePriceColumn;
