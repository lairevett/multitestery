import React from 'react';
import {reverse} from 'named-urls';
import PropTypes from 'prop-types';
import EditButton from '../../../../app/templates/__partials__/edit_button';
import {retrieveUrl} from '../../../../app/utils/router';

const PerfumesTableEditButton = ({id}) => (
    <EditButton
        className="btn-sm"
        href={reverse(retrieveUrl('adminPerfumesUpdate').path.replace('(\\d+)', ''), {productId: id})}
    />
);

PerfumesTableEditButton.propTypes = {
    id: PropTypes.number.isRequired,
};

export default PerfumesTableEditButton;
