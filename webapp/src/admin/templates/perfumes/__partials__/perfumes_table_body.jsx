import React from 'react';
import PropTypes from 'prop-types';
import PerfumesTableSubQuantityButton from './perfumes_table_sub_quantity_button';
import PerfumesTableAddQuantityButton from './perfumes_table_add_quantity_button';
import PerfumesTablePriceColumn from './perfumes_table_price_column';
import PerfumesTableNameColumn from './perfumes_table_name_column';
import PerfumesTableDeleteButton from './perfumes_table_delete_button';
import PerfumesTableEditButton from './perfumes_table_edit_button';
import {LIST_PROP_TYPES} from '../../../../perfumes/constants';
import {MODE} from '../../../constants/perfumes';
import {makeTitle} from '../../../../perfumes/utils';

const PerfumesTableBody = ({list, mode}) => (
    <tbody>
        {list.map(({id, brand, model, unit_price, amount, quantity, percent_off}, i) => (
            <tr key={`_${id}`} className={quantity === 0 ? 'table-danger' : ''}>
                <td className="text-center">{id}</td>
                <PerfumesTableNameColumn id={id} title={makeTitle(brand, model, amount)} />
                <td className="text-center">
                    <div className="d-flex">
                        <PerfumesTableSubQuantityButton index={i} id={id} quantity={quantity} mode={mode} />
                        {quantity}
                        <PerfumesTableAddQuantityButton index={i} id={id} quantity={quantity} mode={mode} />
                    </div>
                </td>
                <PerfumesTablePriceColumn unitPrice={unit_price} percentOff={percent_off} />
                <td className="text-center">
                    <PerfumesTableEditButton id={id} />
                </td>
                <td className="text-center">
                    <PerfumesTableDeleteButton index={i} id={id} mode={mode} title={makeTitle(brand, model, amount)} />
                </td>
            </tr>
        ))}
    </tbody>
);

PerfumesTableBody.propTypes = {
    list: LIST_PROP_TYPES(PropTypes).isRequired,
    mode: PropTypes.oneOf(Object.values(MODE)).isRequired,
};

export default PerfumesTableBody;
