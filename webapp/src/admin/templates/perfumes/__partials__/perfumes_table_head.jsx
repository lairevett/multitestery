import React from 'react';

const PerfumesTableHead = () => (
    <thead>
        <tr>
            <td className="text-center">#</td>
            <td className="text-center">Nazwa</td>
            <td className="text-center">Sztuk</td>
            <td className="text-center">Cena</td>
            <td className="text-center">Edytuj</td>
            <td className="text-center">Usuń</td>
        </tr>
    </thead>
);

export default PerfumesTableHead;
