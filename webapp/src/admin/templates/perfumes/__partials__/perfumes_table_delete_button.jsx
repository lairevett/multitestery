import React, {useCallback} from 'react';
import PropTypes from 'prop-types';
import {useDispatch} from 'react-redux';
import {makeYesNoModal} from '../../../../app/utils/modal';
import {destroy} from '../../../actions/perfumes';
import {MODAL_DESTROY_PERFUME, MODE} from '../../../constants/perfumes';
import DeleteButton from '../../../../app/templates/__partials__/delete_button';

const PerfumesTableDeleteButton = ({index, id, mode, title}) => {
    const dispatch = useDispatch();
    const dispatchDestroyPerfume = useCallback(async () => dispatch(await destroy(index, id, mode)), [
        dispatch,
        index,
        id,
        mode,
    ]);

    return (
        <DeleteButton
            className="btn-sm"
            onClick={() =>
                makeYesNoModal(
                    MODAL_DESTROY_PERFUME,
                    'Usuń',
                    `Czy na pewno chcesz usunąć produkt "${title}"?`,
                    dispatchDestroyPerfume
                )
            }
        />
    );
};

PerfumesTableDeleteButton.propTypes = {
    index: PropTypes.number.isRequired,
    id: PropTypes.number.isRequired,
    mode: PropTypes.oneOf(Object.values(MODE)).isRequired,
    title: PropTypes.string.isRequired,
};

export default PerfumesTableDeleteButton;
