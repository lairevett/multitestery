import React, {useCallback} from 'react';
import {useDispatch} from 'react-redux';
import PropTypes from 'prop-types';
import {subQuantity} from '../../../actions/perfumes';
import {MODE} from '../../../constants/perfumes';
import Button from '../../../../app/templates/__partials__/button';

const PerfumesTableSubQuantity = ({index, id, quantity, mode}) => {
    const dispatch = useDispatch();
    const dispatchSubQuantity = useCallback(async () => dispatch(await subQuantity(index, id, quantity, mode)), [
        dispatch,
        index,
        id,
        quantity,
        mode,
    ]);

    return (
        <Button
            className="btn btn-sm btn-primary mr-2"
            noScriptValue="-"
            value={<i className="icon-minus" />}
            onClick={dispatchSubQuantity}
        />
    );
};

PerfumesTableSubQuantity.propTypes = {
    index: PropTypes.number.isRequired,
    id: PropTypes.number.isRequired,
    quantity: PropTypes.number.isRequired,
    mode: PropTypes.oneOf(Object.values(MODE)).isRequired,
};

export default PerfumesTableSubQuantity;
