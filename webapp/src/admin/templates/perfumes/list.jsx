import React from 'react';
import PropTypes from 'prop-types';
import PerfumesTable from './__partials__/perfumes_table';
import {LIST_PROP_TYPES} from '../../../perfumes/constants';
import {MODAL_DESTROY_PERFUME, MODE} from '../../constants/perfumes';
import {PAGINATION_DATA_PROP_TYPES} from '../../../app/constants';
import Modal from '../../../app/templates/__partials__/modal';
import Pagination from '../../../app/templates/__partials__/pagination';
import AddButton from '../../../app/templates/__partials__/add_button';
import {retrieveUrl} from '../../../app/utils/router';

const AdminPerfumesList = ({list, mode, paginationData}) => (
    <>
        <Modal id={MODAL_DESTROY_PERFUME} />
        <PerfumesTable list={list} mode={mode} />
        {mode === MODE.LIST_ALL && (
            <div className="mt-3 d-flex justify-content-end">
                <AddButton href={retrieveUrl('adminPerfumesCreate').path} />
            </div>
        )}
        <Pagination data={paginationData} />
    </>
);

AdminPerfumesList.propTypes = {
    list: LIST_PROP_TYPES(PropTypes).isRequired,
    mode: PropTypes.oneOf(Object.values(MODE)).isRequired,
    paginationData: PAGINATION_DATA_PROP_TYPES(PropTypes).isRequired,
};

export default AdminPerfumesList;
