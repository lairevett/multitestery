import React from 'react';
import PropTypes from 'prop-types';
import PagesTable from './__partials__/pages_table';
import AddButton from '../../../app/templates/__partials__/add_button';
import Modal from '../../../app/templates/__partials__/modal';
import {LIST_PROP_TYPES, MODAL_DESTROY_PAGE} from '../../constants/pages';
import {retrieveUrl} from '../../../app/utils/router';

const AdminPagesList = ({list}) => (
    <>
        <Modal id={MODAL_DESTROY_PAGE} />
        <PagesTable list={list} />
        <div className="mt-3 d-flex justify-content-end">
            <AddButton href={retrieveUrl('adminPagesCreate').path} />
        </div>
    </>
);

AdminPagesList.propTypes = {
    list: LIST_PROP_TYPES(PropTypes).isRequired,
};

export default AdminPagesList;
