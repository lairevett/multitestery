import React from 'react';
import PropTypes from 'prop-types';
import PagesTableEditButton from './pages_table_edit_button';
import PagesTableDeleteButton from './pages_table_delete_button';
import {LIST_PROP_TYPES} from '../../../constants/pages';

const PagesTableBody = ({list}) => (
    <tbody>
        {list.map(({id, slug, priority, title, is_active}) => (
            <tr key={`_${id}`}>
                <td className="text-center">{id}</td>
                <td className="text-center">{priority}</td>
                <td className="text-center">/{slug}</td>
                <td className="text-center">{title}</td>
                <td className="text-center">{is_active ? 'Tak' : 'Nie'}</td>
                <td className="text-center">
                    <PagesTableEditButton slug={slug} />
                </td>
                <td className="text-center">
                    <PagesTableDeleteButton slug={slug} title={title} />
                </td>
            </tr>
        ))}
    </tbody>
);

PagesTableBody.propTypes = {
    list: LIST_PROP_TYPES(PropTypes).isRequired,
};

export default PagesTableBody;
