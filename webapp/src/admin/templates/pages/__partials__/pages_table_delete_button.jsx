import React, {useCallback} from 'react';
import {useDispatch} from 'react-redux';
import PropTypes from 'prop-types';
import DeleteButton from '../../../../app/templates/__partials__/delete_button';
import {destroy} from '../../../actions/pages';
import {makeYesNoModal} from '../../../../app/utils/modal';
import {MODAL_DESTROY_PAGE} from '../../../constants/pages';

const PagesTableDeleteButton = ({slug, title}) => {
    const dispatch = useDispatch();
    const dispatchDestroyPage = useCallback(async () => dispatch(await destroy(slug)), [dispatch, slug]);

    return (
        <DeleteButton
            className="btn-sm"
            onClick={() =>
                makeYesNoModal(
                    MODAL_DESTROY_PAGE,
                    'Usuń',
                    `Czy na pewno chcesz usunąć podstronę "${title}"?`,
                    dispatchDestroyPage
                )
            }
        />
    );
};

PagesTableDeleteButton.propTypes = {
    slug: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
};

export default PagesTableDeleteButton;
