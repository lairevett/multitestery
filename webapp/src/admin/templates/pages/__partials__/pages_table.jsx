import React from 'react';
import PropTypes from 'prop-types';
import PagesTableHead from './pages_table_head';
import PagesTableBody from './pages_table_body';
import {LIST_PROP_TYPES} from '../../../constants/pages';

const PagesTable = ({list}) => (
    <div className="table-responsive">
        <table className="table table-hover">
            <PagesTableHead />
            <PagesTableBody list={list} />
        </table>
    </div>
);

PagesTable.propTypes = {
    list: LIST_PROP_TYPES(PropTypes).isRequired,
};

export default PagesTable;
