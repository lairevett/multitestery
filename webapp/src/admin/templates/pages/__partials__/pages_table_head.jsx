import React from 'react';

const PagesTableHead = () => (
    <thead>
        <tr>
            <td className="text-center">#</td>
            <td className="text-center">Priorytet</td>
            <td className="text-center">Ścieżka</td>
            <td className="text-center">Tytuł</td>
            <td className="text-center">Aktywna</td>
            <td className="text-center">Edytuj</td>
            <td className="text-center">Usuń</td>
        </tr>
    </thead>
);

export default PagesTableHead;
