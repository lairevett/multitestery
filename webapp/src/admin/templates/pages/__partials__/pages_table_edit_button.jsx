import React from 'react';
import {reverse} from 'named-urls';
import PropTypes from 'prop-types';
import EditButton from '../../../../app/templates/__partials__/edit_button';
import {retrieveUrl} from '../../../../app/utils/router';

const PagesTableEditButton = ({slug}) => (
    <EditButton className="btn-sm" href={reverse(retrieveUrl('adminPagesUpdate').path, {pageSlug: slug})} />
);

PagesTableEditButton.propTypes = {
    slug: PropTypes.string.isRequired,
};

export default PagesTableEditButton;
