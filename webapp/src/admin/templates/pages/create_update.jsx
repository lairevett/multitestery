import React from 'react';
import PropTypes from 'prop-types';
import AdminPagesCreateUpdateForm from '../../forms/pages_create_update';
import {DETAILS_PROP_TYPES, MODE} from '../../constants/pages';

const AdminPagesCreateUpdate = ({details, mode, isError}) => {
    const {slug, is_active, ...restOfDetails} = details;

    return mode === MODE.CREATE ? (
        <AdminPagesCreateUpdateForm isError={isError} />
    ) : (
        <AdminPagesCreateUpdateForm
            initialValues={{
                pageSlug: slug,
                slug,
                isActive: is_active,
                ...restOfDetails,
            }}
            isError={isError}
        />
    );
};

AdminPagesCreateUpdate.propTypes = {
    // Could be null if mode is equal to MODE.CREATE.
    details: PropTypes.oneOfType([PropTypes.oneOf([null]).isRequired, DETAILS_PROP_TYPES(PropTypes)]).isRequired,
    mode: PropTypes.oneOf(Object.values(MODE)).isRequired,
    isError: PropTypes.bool.isRequired,
};

export default AdminPagesCreateUpdate;
