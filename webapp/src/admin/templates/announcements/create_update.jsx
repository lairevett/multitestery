import React from 'react';
import PropTypes from 'prop-types';
import AdminAnnouncementsCreateUpdateForm from '../../forms/announcements_create_update';
import {MODE} from '../../constants/announcements';
import {DETAILS_PROP_TYPES} from '../../../announcements/constants';

const AdminAnnouncementsCreateUpdate = ({
    details: {id, image, is_active, ...restOfDetails},
    mode,
    formFileFields,
    isError,
}) =>
    mode === MODE.CREATE ? (
        <AdminAnnouncementsCreateUpdateForm fileFields={formFileFields} isError={isError} />
    ) : (
        <AdminAnnouncementsCreateUpdateForm
            initialValues={{
                announcementId: id,
                isActive: is_active,
                ...restOfDetails,
            }}
            fileFields={formFileFields}
            isError={isError}
        />
    );

AdminAnnouncementsCreateUpdate.propTypes = {
    // Could be null if mode is equal to MODE.CREATE.
    details: PropTypes.oneOfType([PropTypes.oneOf([null]).isRequired, DETAILS_PROP_TYPES(PropTypes)]).isRequired,
    mode: PropTypes.oneOf(Object.values(MODE)).isRequired,
    formFileFields: PropTypes.objectOf(PropTypes.object).isRequired,
    isError: PropTypes.bool.isRequired,
};

export default AdminAnnouncementsCreateUpdate;
