import React from 'react';
import PropTypes from 'prop-types';
import AnnouncementsTableEditButton from './announcements_table_edit_button';
import AnnouncementsTableDeleteButton from './announcements_table_delete_button';
import {LIST_PROP_TYPES} from '../../../../announcements/constants';

const AnnouncementsTableBody = ({list}) => (
    <tbody>
        {list.map(({id, priority, title, is_active}) => (
            <tr key={`_${id}`}>
                <td className="text-center">{id}</td>
                <td className="text-center">{priority}</td>
                <td className="text-center">{title}</td>
                <td className="text-center">{is_active ? 'Tak' : 'Nie'}</td>
                <td className="text-center">
                    <AnnouncementsTableEditButton id={id} />
                </td>
                <td className="text-center">
                    <AnnouncementsTableDeleteButton id={id} title={title} />
                </td>
            </tr>
        ))}
    </tbody>
);

AnnouncementsTableBody.propTypes = {
    list: LIST_PROP_TYPES(PropTypes).isRequired,
};

export default AnnouncementsTableBody;
