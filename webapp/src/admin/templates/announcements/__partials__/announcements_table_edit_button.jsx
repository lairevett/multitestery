import React from 'react';
import {reverse} from 'named-urls';
import PropTypes from 'prop-types';
import EditButton from '../../../../app/templates/__partials__/edit_button';
import {retrieveUrl} from '../../../../app/utils/router';

const AnnouncementsTableEditButton = ({id}) => (
    <EditButton
        className="btn-sm"
        href={reverse(retrieveUrl('adminAnnouncementsUpdate').path.replace('(\\d+)', ''), {
            announcementId: id,
        })}
    />
);

AnnouncementsTableEditButton.propTypes = {
    id: PropTypes.number.isRequired,
};

export default AnnouncementsTableEditButton;
