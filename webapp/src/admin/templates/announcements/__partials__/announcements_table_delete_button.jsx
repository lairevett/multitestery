import React, {useCallback} from 'react';
import PropTypes from 'prop-types';
import {useDispatch} from 'react-redux';
import {makeYesNoModal} from '../../../../app/utils/modal';
import {MODAL_DESTROY_ANNOUNCEMENT} from '../../../constants/announcements';
import {destroy} from '../../../actions/announcements';
import DeleteButton from '../../../../app/templates/__partials__/delete_button';

const AnnouncementsTableDeleteButton = ({id, title}) => {
    const dispatch = useDispatch();
    const dispatchDestroyAnnouncement = useCallback(async () => dispatch(await destroy(id)), [dispatch, id]);

    return (
        <DeleteButton
            className="btn-sm"
            onClick={() =>
                makeYesNoModal(
                    MODAL_DESTROY_ANNOUNCEMENT,
                    'Usuń',
                    `Czy na pewno chcesz usunąć ogłoszenie "${title}"?`,
                    dispatchDestroyAnnouncement
                )
            }
        />
    );
};

AnnouncementsTableDeleteButton.propTypes = {
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
};

export default AnnouncementsTableDeleteButton;
