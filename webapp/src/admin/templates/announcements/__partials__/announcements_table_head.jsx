import React from 'react';

const AnnouncementsTableHead = () => (
    <thead>
        <tr>
            <td className="text-center">#</td>
            <td className="text-center">Priorytet</td>
            <td className="text-center">Tytuł</td>
            <td className="text-center">Aktywne</td>
            <td className="text-center">Edytuj</td>
            <td className="text-center">Usuń</td>
        </tr>
    </thead>
);

export default AnnouncementsTableHead;
