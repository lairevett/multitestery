import React from 'react';
import PropTypes from 'prop-types';
import AnnouncementsTableHead from './announcements_table_head';
import AnnouncementsTableBody from './announcements_table_body';
import {LIST_PROP_TYPES} from '../../../../announcements/constants';

const AnnouncementsTable = ({list}) => (
    <div className="table-responsive">
        <table className="table table-hover">
            <AnnouncementsTableHead />
            <AnnouncementsTableBody list={list} />
        </table>
    </div>
);

AnnouncementsTable.propTypes = {
    list: LIST_PROP_TYPES(PropTypes).isRequired,
};

export default AnnouncementsTable;
