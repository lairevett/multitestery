import React from 'react';
import PropTypes from 'prop-types';
import AddButton from '../../../app/templates/__partials__/add_button';
import AnnouncementsTable from './__partials__/announcements_table';
import {LIST_PROP_TYPES} from '../../../announcements/constants';
import {MODAL_DESTROY_ANNOUNCEMENT} from '../../constants/announcements';
import Modal from '../../../app/templates/__partials__/modal';
import {retrieveUrl} from '../../../app/utils/router';

const AdminAnnouncementsList = ({list}) => (
    <>
        <Modal id={MODAL_DESTROY_ANNOUNCEMENT} />
        <AnnouncementsTable list={list} />
        <div className="mt-3 d-flex justify-content-end">
            <AddButton href={retrieveUrl('adminAnnouncementsCreate').path} />
        </div>
    </>
);

AdminAnnouncementsList.propTypes = {
    list: LIST_PROP_TYPES(PropTypes).isRequired,
};

export default AdminAnnouncementsList;
