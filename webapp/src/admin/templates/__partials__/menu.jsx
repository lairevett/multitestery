import React from 'react';
import MenuList from '../../../app/templates/__partials__/menu_list';
import MenuListItem from '../../../app/templates/__partials__/menu_list_item';
import {retrieveUrl} from '../../../app/utils/router';

const Menu = () => (
    <MenuList>
        <MenuListItem url={retrieveUrl('adminSummary')} exact />
        <MenuListItem url={retrieveUrl('adminUsersAllList')} />
        <MenuListItem url={retrieveUrl('adminPerfumesAllList')} />
        <MenuListItem url={retrieveUrl('adminPerfumesNotAvailableList')} />
        <MenuListItem url={retrieveUrl('adminPerfumesAvailableList')} />
        <MenuListItem url={retrieveUrl('adminOrdersAllList')} />
        <MenuListItem url={retrieveUrl('adminOrdersNotPaidList')} />
        <MenuListItem url={retrieveUrl('adminOrdersPaidList')} />
        <MenuListItem url={retrieveUrl('adminOrdersCanceledList')} />
        <MenuListItem url={retrieveUrl('adminOrdersSentList')} />
        <MenuListItem url={retrieveUrl('adminAnnouncementsAllList')} />
        <MenuListItem url={retrieveUrl('adminPagesAllList')} />
    </MenuList>
);

export default Menu;
