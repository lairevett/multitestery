import React from 'react';
import PropTypes from 'prop-types';
import BackButton from '../../../app/templates/__partials__/back_button';
import OrderData from '../../../orders/templates/__partials__/order_data';
import OrderEntriesTable from '../../../orders/templates/__partials__/order_entries_table';
import MarkAsCanceledButton from './__partials__/mark_as_canceled_button';
import MarkAsPlacedButton from './__partials__/mark_as_placed_button';
import MarkAsPaidButton from './__partials__/mark_as_paid_button';
import MarkAsSentButton from './__partials__/mark_as_sent_button';
import {DETAILS_PROP_TYPES} from '../../../orders/constants';
import Modal from '../../../app/templates/__partials__/modal';
import {MODAL_MARK_ORDER_AS_SENT} from '../../constants/orders';

const AdminOrdersDetails = ({details: {id, entries, forwarding_address, total, shipping, status, created_at}}) => {
    return (
        <>
            <Modal id={MODAL_MARK_ORDER_AS_SENT} />
            <OrderData
                status={status}
                createdAt={created_at}
                shipping={shipping}
                forwardingAddress={forwarding_address}
            />
            <OrderEntriesTable entries={entries} total={total} isMutable={false} />
            <div className="d-flex justify-content-between align-items-start">
                <BackButton />
                <div className="d-flex flex-column">
                    <MarkAsCanceledButton orderId={id} orderStatus={status} />
                    <MarkAsPlacedButton orderId={id} orderStatus={status} />
                    <MarkAsPaidButton orderId={id} orderStatus={status} />
                    <MarkAsSentButton orderId={id} entries={entries} orderStatus={status} />
                </div>
            </div>
        </>
    );
};

AdminOrdersDetails.propTypes = {
    details: DETAILS_PROP_TYPES(PropTypes).isRequired,
};

export default AdminOrdersDetails;
