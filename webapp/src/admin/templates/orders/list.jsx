import React from 'react';
import PropTypes from 'prop-types';
import OrdersTable from '../../../orders/templates/__partials__/orders_table';
import {LIST_PROP_TYPES} from '../../../orders/constants';
import {PAGINATION_DATA_PROP_TYPES} from '../../../app/constants';
import Pagination from '../../../app/templates/__partials__/pagination';

const AdminOrdersList = ({list, paginationData}) => (
    <>
        <OrdersTable list={list} isAdminPanel />
        <Pagination data={paginationData} />
    </>
);

AdminOrdersList.propTypes = {
    list: LIST_PROP_TYPES(PropTypes).isRequired,
    paginationData: PAGINATION_DATA_PROP_TYPES(PropTypes).isRequired,
};

export default AdminOrdersList;
