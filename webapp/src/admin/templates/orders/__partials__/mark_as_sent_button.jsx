import React, {useCallback} from 'react';
import {useDispatch} from 'react-redux';
import PropTypes from 'prop-types';
import {DETAILS_ENTRIES_PROP_TYPES, STATUS} from '../../../../orders/constants';
import {updateStatus} from '../../../actions/orders';
import Button from '../../../../app/templates/__partials__/button';
import {makeYesNoModal} from '../../../../app/utils/modal';
import {MODAL_MARK_ORDER_AS_SENT} from '../../../constants/orders';
import {getMarkAsSentModalBodyTextAndButtonState} from '../../../utils/orders';

const MarkAsSentButton = ({orderId, entries, orderStatus}) => {
    const dispatch = useDispatch();
    const dispatchSetSent = useCallback(async () => dispatch(await updateStatus(orderId, STATUS.SENT)), [
        dispatch,
        orderId,
    ]);

    const [modalBodyText, isModalYesButtonDisabled] = getMarkAsSentModalBodyTextAndButtonState(entries);

    return (
        <Button
            className="btn"
            disabled={orderStatus === STATUS.SENT}
            onClick={() =>
                makeYesNoModal(
                    MODAL_MARK_ORDER_AS_SENT,
                    'Oznacz zamówienie jako wysłane',
                    modalBodyText,
                    dispatchSetSent,
                    isModalYesButtonDisabled
                )
            }
        >
            Oznacz jako wysłane
        </Button>
    );
};

MarkAsSentButton.propTypes = {
    orderId: PropTypes.number.isRequired,
    entries: DETAILS_ENTRIES_PROP_TYPES(PropTypes).isRequired,
    orderStatus: PropTypes.oneOf(Object.values(STATUS)).isRequired,
};

export default MarkAsSentButton;
