import React, {useCallback} from 'react';
import {useDispatch} from 'react-redux';
import PropTypes from 'prop-types';
import {updateStatus} from '../../../actions/orders';
import {STATUS} from '../../../../orders/constants';
import Button from '../../../../app/templates/__partials__/button';

const MarkAsPaidButton = ({orderId, orderStatus}) => {
    const dispatch = useDispatch();
    const dispatchSetPaid = useCallback(async () => dispatch(await updateStatus(orderId, STATUS.PAID)), [
        dispatch,
        orderId,
    ]);

    return (
        <Button className="btn btn-primary mb-3" disabled={orderStatus === STATUS.PAID} onClick={dispatchSetPaid}>
            Oznacz jako zapłacone
        </Button>
    );
};

MarkAsPaidButton.propTypes = {
    orderId: PropTypes.number.isRequired,
    orderStatus: PropTypes.oneOf(Object.values(STATUS)).isRequired,
};

export default MarkAsPaidButton;
