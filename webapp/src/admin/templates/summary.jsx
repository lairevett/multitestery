import React from 'react';
import PropTypes from 'prop-types';
import {SUMMARY_PROP_TYPES} from '../constants/summary';

const AdminSummary = ({summary}) => (
    <div className="table-responsive">
        <table className="table table-hover">
            <tbody>
                <tr>
                    <td>Liczba użytkowników</td>
                    <td>{summary.all_users}</td>
                </tr>
                <tr>
                    <td>Liczba perfum</td>
                    <td>{summary.all_products}</td>
                </tr>
                <tr>
                    <td>Liczba perfum niedostępnych</td>
                    <td>{summary.not_available_products}</td>
                </tr>
                <tr>
                    <td>Liczba perfum dostępnych</td>
                    <td>{summary.available_products}</td>
                </tr>
                <tr>
                    <td>Liczba zamówień</td>
                    <td>{summary.all_orders}</td>
                </tr>
                <tr>
                    <td>Liczba zamówień nieopłaconych</td>
                    <td>{summary.not_paid_orders}</td>
                </tr>
                <tr>
                    <td>Liczba zamówień opłaconych</td>
                    <td>{summary.paid_orders}</td>
                </tr>
                <tr>
                    <td>Liczba zamówień anulowanych</td>
                    <td>{summary.canceled_orders}</td>
                </tr>
                <tr>
                    <td>Liczba zamówień wysłanych</td>
                    <td>{summary.sent_orders}</td>
                </tr>
                <tr>
                    <td>Liczba ogłoszeń</td>
                    <td>{summary.all_announcements}</td>
                </tr>
                <tr>
                    <td>Liczba podstron</td>
                    <td>{summary.all_pages}</td>
                </tr>
            </tbody>
        </table>
    </div>
);

AdminSummary.propTypes = {
    summary: SUMMARY_PROP_TYPES(PropTypes).isRequired,
};

export default AdminSummary;
