import {DETAILS_INITIAL_STATE, LIST_INITIAL_STATE} from '../../perfumes/constants';

// API endpoints.
export const ENDPOINT = {
    BASE: '/v1/products/',
    LIST_NOT_AVAILABLE: page => `/v1/products/?available=0&page=${page}`,
    LIST_AVAILABLE: page => `/v1/products/?available=1&page=${page}`,
};

// Redux action types.
export const ACTION = {
    CREATE: '@admin_perfumes/CREATE',
    UPDATE: '@admin_perfumes/UPDATE',
    SUB_QUANTITY: '@admin_perfumes/SUB_QUANTITY',
    ADD_QUANTITY: '@admin_perfumes/ADD_QUANTITY',
    RETRIEVE_ALL_LIST: '@admin_perfumes/RETRIEVE_ALL_LIST',
    CLEAR_ALL_LIST: '@admin_perfumes/CLEAR_ALL_LIST',
    RETRIEVE_NOT_AVAILABLE_LIST: '@admin_perfumes/RETRIEVE_NOT_AVAILABLE_LIST',
    CLEAR_NOT_AVAILABLE_LIST: '@admin_perfumes/CLEAR_NOT_AVAILABLE_LIST',
    RETRIEVE_AVAILABLE_LIST: '@admin_perfumes/RETRIEVE_AVAILABLE_LIST',
    CLEAR_AVAILABLE_LIST: '@admin_perfumes/CLEAR_AVAILABLE_LIST',
    RETRIEVE_DETAILS: '@admin_perfumes/RETRIEVE_DETAILS',
    CLEAR_DETAILS: '@admin_perfumes/CLEAR_DETAILS',
    DESTROY: '@admin_perfumes/DESTROY',
};

// Redux form data.
export const FORM = {
    CREATE_UPDATE: 'FORM_ADMIN_PERFUMES_CREATE_UPDATE',
    CREATE_UPDATE__FILE_FIELDS: {
        photo: {
            accept: ['image/png', 'image/jpeg'],
            maxCount: 1,
        },
    },
};

// Reducer initial states.
export const INITIAL_STATE = {
    allList: LIST_INITIAL_STATE,
    notAvailableList: LIST_INITIAL_STATE,
    availableList: LIST_INITIAL_STATE,
    details: DETAILS_INITIAL_STATE,
};

// Template modes.
export const MODE = {
    CREATE: 'MODE_ADMIN_PERFUMES_CREATE',
    UPDATE: 'MODE_ADMIN_PERFUMES_UPDATE',
    LIST_ALL: 'MODE_ADMIN_PERFUMES_LIST_ALL',
    LIST_NOT_AVAILABLE: 'MODE_ADMIN_PERFUMES_LIST_NOT_AVAILABLE',
    LIST_AVAILABLE: 'MODE_ADMIN_PERFUMES_LIST_AVAILABLE',
};

// Modal ids.
export const MODAL_DESTROY_PERFUME = 'destroy-perfume';
