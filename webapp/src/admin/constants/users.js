import {DEFAULT_LIST_INITIAL_STATE, DEFAULT_STATUS_INITIAL_STATE} from '../../app/utils/api/constants';
import {DETAILS_INITIAL_STATE, DETAILS_PROP_TYPES} from '../../auth/constants/user';

// API endpoints.
export const ENDPOINT = {
    LIST_ALL: page => `/v1/users/?page=${page}`,
    DETAILS: userId => `/v1/users/${userId}/`,
};

// Redux action types.
export const ACTION = {
    UPDATE: '@admin_users/UPDATE',
    RETRIEVE_ALL_LIST: '@admin_users/RETRIEVE_ALL_LIST',
    CLEAR_LIST: '@admin_users/CLEAR_LIST',
    RETRIEVE_DETAILS: '@admin_users/RETRIEVE_DETAILS',
    CLEAR_DETAILS: '@admin_users/CLEAR_DETAILS',
    DESTROY: '@admin_users/DESTROY',
};

// Redux form names.
export const FORM = {
    UPDATE_USER: 'FORM_ADMIN_UPDATE_USER',
};

// Reducer initial states.
export const LIST_INITIAL_STATE = {
    ...DEFAULT_LIST_INITIAL_STATE,
    ...DEFAULT_STATUS_INITIAL_STATE,
};

export const INITIAL_STATE = {
    list: LIST_INITIAL_STATE,
    details: DETAILS_INITIAL_STATE,
};

// Prop types.
export const LIST_PROP_TYPES = PropertyTypes => PropertyTypes.arrayOf(DETAILS_PROP_TYPES(PropertyTypes));

// Template modes.
export const MODE = {
    UPDATE: 'MODE_ADMIN_USERS_UPDATE',
    LIST_ALL: 'MODE_ADMIN_USERS_LIST_ALL',
};

// Modal ids.
export const MODAL_DESTROY_USER = 'destroy-user';
