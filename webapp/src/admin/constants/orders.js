import {DETAILS_INITIAL_STATE, LIST_INITIAL_STATE} from '../../orders/constants';

// API endpoints.
export const ENDPOINT = {
    UPDATE_STATUS: orderId => `/v1/users/orders/${orderId}/update-status/`,
    LIST_ALL: page => `/v1/users/orders/?page=${page}`,
    LIST_NOT_PAID: page => `/v1/users/orders/?paid=0&page=${page}`,
    LIST_PAID: page => `/v1/users/orders/?paid=1&page=${page}`,
    LIST_CANCELED: page => `/v1/users/orders/?canceled=1&page=${page}`,
    LIST_SENT: page => `/v1/users/orders/?sent=1&page=${page}`,
    DETAILS: orderId => `/v1/users/orders/${orderId}/`,
};

// Redux action types.
export const ACTION = {
    UPDATE_STATUS: '@admin_orders/UPDATE_STATUS',
    RETRIEVE_ALL_LIST: '@admin_orders/RETRIEVE_ALL_LIST',
    RETRIEVE_NOT_PAID_LIST: '@admin_orders/RETRIEVE_NOT_PAID_LIST',
    RETRIEVE_PAID_LIST: '@admin_orders/RETRIEVE_PAID_LIST',
    RETRIEVE_CANCELED_LIST: '@admin_orders/RETRIEVE_CANCELED_LIST',
    RETRIEVE_SENT_LIST: '@admin_orders/RETRIEVE_SENT_LIST',
    RETRIEVE_DETAILS: '@admin_orders/RETRIEVE_DETAILS',
    CLEAR: '@admin_orders/CLEAR',
};

// Reducer initial states.
export const INITIAL_STATE = {
    allList: LIST_INITIAL_STATE,
    notPaidList: LIST_INITIAL_STATE,
    paidList: LIST_INITIAL_STATE,
    canceledList: LIST_INITIAL_STATE,
    sentList: LIST_INITIAL_STATE,
    details: DETAILS_INITIAL_STATE,
};

// Template modes.
export const MODE = {
    LIST_ALL: 'MODE_ADMIN_ORDERS_LIST_ALL',
    LIST_NOT_PAID: 'MODE_ADMIN_ORDERS_LIST_NOT_PAID',
    LIST_PAID: 'MODE_ADMIN_ORDERS_LIST_PAID',
    LIST_CANCELED: 'MODE_ADMIN_ORDERS_LIST_CANCELED',
    LIST_SENT: 'MODE_ADMIN_ORDERS_LIST_SENT',
    DETAILS: 'MODE_ADMIN_ORDERS_DETAILS',
};

// Modal ids.
export const MODAL_MARK_ORDER_AS_SENT = 'mark-order-as-sent';
