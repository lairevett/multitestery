import {DEFAULT_STATUS_INITIAL_STATE} from '../../app/utils/api/constants';

// API endpoints.
export const ENDPOINT = {
    BASE: '/v1/summary/',
};

// Redux action types.
export const ACTION = {
    RETRIEVE: '@admin_summary/RETRIEVE',
    CLEAR: '@admin_summary/CLEAR',
};

// Reducer initial states.
export const INITIAL_STATE = {
    all_users: 0,
    all_products: 0,
    not_available_products: 0,
    available_products: 0,
    all_orders: 0,
    not_paid_orders: 0,
    paid_orders: 0,
    canceled_orders: 0,
    sent_orders: 0,
    all_announcements: 0,
    all_pages: 0,
    ...DEFAULT_STATUS_INITIAL_STATE,
};

// Prop types.
export const SUMMARY_PROP_TYPES = PropertyTypes =>
    PropertyTypes.exact({
        all_users: PropertyTypes.number.isRequired,
        all_products: PropertyTypes.number.isRequired,
        not_available_products: PropertyTypes.number.isRequired,
        available_products: PropertyTypes.number.isRequired,
        all_orders: PropertyTypes.number.isRequired,
        not_paid_orders: PropertyTypes.number.isRequired,
        paid_orders: PropertyTypes.number.isRequired,
        canceled_orders: PropertyTypes.number.isRequired,
        sent_orders: PropertyTypes.number.isRequired,
        all_announcements: PropertyTypes.number.isRequired,
        all_pages: PropertyTypes.number.isRequired,
    });
