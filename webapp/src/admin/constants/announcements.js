import {DEFAULT_STATUS_INITIAL_STATE} from '../../app/utils/api/constants';
import {LIST_INITIAL_STATE} from '../../announcements/constants';

// API endpoints.
export const ENDPOINT = {
    BASE: '/v1/announcements/',
    DETAILS: announcementId => `/v1/announcements/${announcementId}/`,
};

// Redux action types.
export const ACTION = {
    CREATE: '@admin_announcements/CREATE',
    UPDATE: '@admin_announcements/UPDATE',
    RETRIEVE_ALL_LIST: '@admin_announcements/RETRIEVE_ALL_LIST',
    CLEAR_LIST: '@admin_announcements/CLEAR_LIST',
    RETRIEVE_DETAILS: '@admin_announcements/RETRIEVE_DETAILS',
    CLEAR_DETAILS: '@admin_announcements/CLEAR_DETAILS',
    DESTROY: '@admin_announcements/DESTROY',
};

// Redux form data.
export const FORM = {
    CREATE_UPDATE: 'FORM_ADMIN_ANNOUNCEMENTS_CREATE_UPDATE',
    CREATE_UPDATE__FILE_FIELDS: {
        image: {
            accept: ['image/png', 'image/jpeg'],
            maxCount: 1,
        },
    },
};

// Reducer initial states.
export const DETAILS_INITIAL_STATE = {
    id: 0,
    priority: 0,
    image: '',
    title: '',
    description: '',
    is_active: false,
    ...DEFAULT_STATUS_INITIAL_STATE,
};

export const INITIAL_STATE = {
    list: LIST_INITIAL_STATE,
    details: DETAILS_INITIAL_STATE,
};

// Template modes.
export const MODE = {
    CREATE: 'MODE_ADMIN_ANNOUNCEMENTS_CREATE',
    UPDATE: 'MODE_ADMIN_ANNOUNCEMENTS_UPDATE',
    LIST_ALL: 'MODE_ADMIN_ANNOUNCEMENTS_LIST_ALL',
};

// Modal ids.
export const MODAL_DESTROY_ANNOUNCEMENT = 'destroy-announcement';
