import {DETAILS_INITIAL_STATE, LIST_INITIAL_STATE} from '../../pages/constants';

// API endpoints.
export const ENDPOINT = {
    BASE: '/v1/pages/',
};

// Redux action types.
export const ACTION = {
    CREATE: '@admin_pages/CREATE',
    RETRIEVE_ALL_LIST: '@admin_pages/RETRIEVE_ALL_LIST',
    CLEAR_LIST: '@admin_pages/CLEAR_LIST',
    RETRIEVE_DETAILS: '@admin_pages/RETRIEVE_DETAILS',
    CLEAR_DETAILS: '@admin_pages/CLEAR_DETAILS',
    UPDATE: '@admin_pages/UPDATE',
    DESTROY: '@admin_pages/DESTROY',
};

// Redux form names.
export const FORM = {
    CREATE_UPDATE: 'FORM_ADMIN_PAGES_CREATE_UPDATE',
};

// Reducer initial states.
export const INITIAL_STATE = {
    list: LIST_INITIAL_STATE,
    details: DETAILS_INITIAL_STATE,
};

// Prop types.
export const DETAILS_PROP_TYPES = PropertyTypes =>
    PropertyTypes.exact({
        id: PropertyTypes.number.isRequired,
        slug: PropertyTypes.string.isRequired,
        priority: PropertyTypes.number.isRequired,
        title: PropertyTypes.string.isRequired,
        content: PropertyTypes.string,
        is_active: PropertyTypes.bool.isRequired,
    }).isRequired;

export const LIST_PROP_TYPES = PropertyTypes => PropertyTypes.arrayOf(DETAILS_PROP_TYPES(PropertyTypes));

// Template modes.
export const MODE = {
    CREATE: 'MODE_ADMIN_PAGES_CREATE',
    UPDATE: 'MODE_ADMIN_PAGES_UPDATE',
    LIST_ALL: 'MODE_ADMIN_PAGES_LIST_ALL',
    DETAILS: 'MODE_ADMIN_PAGES_DETAILS',
};

// Modal ids.
export const MODAL_DESTROY_PAGE = 'destroy-page';
