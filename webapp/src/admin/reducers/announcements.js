import {ACTION, INITIAL_STATE} from '../constants/announcements';

const adminAnnouncementsReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ACTION.CREATE:
        case ACTION.UPDATE:
            return state;
        case ACTION.RETRIEVE_ALL_LIST:
            return {
                ...state,
                list: action.payload,
            };
        case ACTION.CLEAR_LIST:
            return {
                ...state,
                list: INITIAL_STATE.list,
            };
        case ACTION.RETRIEVE_DETAILS:
            return {
                ...state,
                details: action.payload,
            };
        case ACTION.CLEAR_DETAILS:
            return {
                ...state,
                details: INITIAL_STATE.details,
            };
        case ACTION.DESTROY:
            return {
                ...state,
                list: {
                    ...state.list,
                    results: state.list.results.filter(result => result.id !== action.context.id),
                    __meta__: {...state.list.__meta__, ...action.payload.__meta__},
                },
            };
        default:
            return state;
    }
};

export default adminAnnouncementsReducer;
