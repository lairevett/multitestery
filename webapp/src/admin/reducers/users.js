import {ACTION, INITIAL_STATE} from '../constants/users';

const adminUsersReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ACTION.RETRIEVE_ALL_LIST:
            return {
                ...state,
                list: action.payload,
            };
        case ACTION.CLEAR_LIST:
            return {
                ...state,
                list: INITIAL_STATE.list,
            };
        case ACTION.UPDATE:
        case ACTION.RETRIEVE_DETAILS:
            return {
                ...state,
                details: action.payload,
            };
        case ACTION.CLEAR_DETAILS:
            return {
                ...state,
                details: INITIAL_STATE.details,
            };
        case ACTION.DESTROY:
            return {
                ...state,
                list: {
                    ...state.list,
                    count: state.list.count - 1,
                    results: state.list.results.filter(result => result.id !== action.context.id),
                    __meta__: {...state.list.__meta__, ...action.payload.__meta__},
                },
            };
        default:
            return state;
    }
};

export default adminUsersReducer;
