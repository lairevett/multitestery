import {ACTION, INITIAL_STATE} from '../constants/perfumes';
import {mapValueForMode} from '../utils/perfumes';

const adminPerfumesReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ACTION.UPDATE:
            return state;
        case ACTION.SUB_QUANTITY:
        case ACTION.ADD_QUANTITY: {
            const perfumesList = {
                ...mapValueForMode(action.context.mode, state.allList, state.notAvailableList, state.availableList),
            };

            perfumesList.results[action.context.index].quantity = action.context.newQuantity;
            perfumesList.__meta__ = action.payload.__meta__;

            return {
                ...state,
                ...mapValueForMode(
                    action.context.mode,
                    {allList: perfumesList},
                    {notAvailableList: perfumesList},
                    {availableList: perfumesList}
                ),
            };
        }
        case ACTION.RETRIEVE_ALL_LIST:
            return {
                ...state,
                allList: action.payload,
            };
        case ACTION.CLEAR_ALL_LIST:
            return {
                ...state,
                allList: INITIAL_STATE.allList,
            };
        case ACTION.RETRIEVE_NOT_AVAILABLE_LIST:
            return {
                ...state,
                notAvailableList: action.payload,
            };
        case ACTION.CLEAR_NOT_AVAILABLE_LIST:
            return {
                ...state,
                notAvailableList: INITIAL_STATE.notAvailableList,
            };
        case ACTION.RETRIEVE_AVAILABLE_LIST:
            return {
                ...state,
                availableList: action.payload,
            };
        case ACTION.CLEAR_AVAILABLE_LIST:
            return {
                ...state,
                availableList: INITIAL_STATE.availableList,
            };
        case ACTION.RETRIEVE_DETAILS:
            return {
                ...state,
                details: action.payload,
            };
        case ACTION.CLEAR_DETAILS:
            return {
                ...state,
                details: INITIAL_STATE.details,
            };
        case ACTION.DESTROY: {
            const perfumesList = {
                ...mapValueForMode(action.context.mode, state.allList, state.notAvailableList, state.availableList),
            };

            perfumesList.count -= 1;
            perfumesList.results.splice(action.context.index, 1);
            perfumesList.__meta__ = action.payload.__meta__;

            return {
                ...state,
                ...mapValueForMode(
                    action.context.mode,
                    {allList: perfumesList},
                    {notAvailableList: perfumesList},
                    {availableList: perfumesList}
                ),
            };
        }
        default:
            return state;
    }
};

export default adminPerfumesReducer;
