import {combineReducers} from 'redux';
import adminPerfumesReducer from './perfumes';
import adminOrdersReducer from './orders';
import adminAnnouncementsReducer from './announcements';
import adminUsersReducer from './users';
import adminPagesReducer from './pages';
import adminSummaryReducer from './summary';

const adminReducer = combineReducers({
    summary: adminSummaryReducer,
    users: adminUsersReducer,
    perfumes: adminPerfumesReducer,
    orders: adminOrdersReducer,
    announcements: adminAnnouncementsReducer,
    pages: adminPagesReducer,
});

export default adminReducer;
