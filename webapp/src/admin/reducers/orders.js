import {ACTION, INITIAL_STATE} from '../constants/orders';
import {mapValueForMode} from '../utils/orders';
import {STATUS} from '../../orders/constants';

const adminOrdersReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ACTION.UPDATE_STATUS: {
            let newEntriesState = state.details.entries;

            if (state.details.status === STATUS.SENT) {
                // Increment entry quantities if previous state is STATUS.SENT.
                newEntriesState = newEntriesState.map(entry => {
                    // eslint-disable-next-line no-param-reassign
                    entry.product.quantity += entry.quantity;
                    return entry;
                });
            } else if (action.payload.status === STATUS.SENT) {
                // Decrement entry quantities if next state is STATUS.SENT.
                newEntriesState = newEntriesState.map(entry => {
                    // eslint-disable-next-line no-param-reassign
                    entry.product.quantity -= entry.quantity;
                    return entry;
                });
            }

            return {
                ...state,
                details: {
                    ...state.details,
                    status: action.payload.status,
                    entries: newEntriesState,
                    __meta__: {
                        ...state.details.__meta__,
                        ...action.payload.__meta__,
                    },
                },
            };
        }
        case ACTION.RETRIEVE_ALL_LIST:
            return {
                ...state,
                allList: action.payload,
            };
        case ACTION.RETRIEVE_NOT_PAID_LIST:
            return {
                ...state,
                notPaidList: action.payload,
            };
        case ACTION.RETRIEVE_PAID_LIST:
            return {
                ...state,
                paidList: action.payload,
            };
        case ACTION.RETRIEVE_CANCELED_LIST:
            return {
                ...state,
                canceledList: action.payload,
            };
        case ACTION.RETRIEVE_SENT_LIST:
            return {
                ...state,
                sentList: action.payload,
            };
        case ACTION.RETRIEVE_DETAILS:
            return {
                ...state,
                details: action.payload,
            };
        case ACTION.CLEAR:
            return mapValueForMode(
                action.context.mode,
                {...state, allList: INITIAL_STATE.allList},
                {...state, notPaidList: INITIAL_STATE.notPaidList},
                {...state, paidList: INITIAL_STATE.paidList},
                {...state, canceledList: INITIAL_STATE.canceledList},
                {...state, sentList: INITIAL_STATE.sentList},
                {...state, details: INITIAL_STATE.details}
            );
        default:
            return state;
    }
};

export default adminOrdersReducer;
