import {ACTION, INITIAL_STATE} from '../constants/summary';

const adminSummaryReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ACTION.RETRIEVE:
            return {
                ...state,
                ...action.payload,
            };
        case ACTION.CLEAR:
            return {
                ...state,
                ...INITIAL_STATE,
            };
        default:
            return state;
    }
};

export default adminSummaryReducer;
