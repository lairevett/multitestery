import {SubmissionError} from 'redux-form';
import {create, update} from '../actions/perfumes';
import {validateFieldMaxValue, validateFieldMinValue, validateFieldRequired} from '../../app/utils/validation';

import {makeRequestBody} from '../utils/perfumes';
import {history, retrieveUrl} from '../../app/utils/router';
import {STATUS} from '../../app/utils/api/constants';
import {logDispatchAPIErrors} from '../../app/utils/logger';

export const validate = ({productId, photo, brand, model, category, unitPrice, amount, quantity, percentOff}) => {
    const errors = {};

    errors.photo = !productId && validateFieldRequired(photo?.file || photo?.[0]);
    errors.brand = validateFieldRequired(brand);
    errors.model = validateFieldRequired(model);

    errors.category =
        validateFieldRequired(category) || validateFieldMinValue(category, 0) || validateFieldMaxValue(category, 2);

    errors.unitPrice = validateFieldRequired(unitPrice) || validateFieldMinValue(unitPrice, 1);
    errors.amount = validateFieldRequired(amount) || validateFieldMinValue(amount, 1);
    errors.quantity = validateFieldRequired(quantity) || validateFieldMinValue(quantity, 0);

    errors.percentOff =
        validateFieldRequired(percentOff) ||
        validateFieldMinValue(percentOff, 0) ||
        validateFieldMaxValue(percentOff, 100);

    return errors;
};

export const submit = async ({productId, ...values}, dispatch) => {
    const requestBody = makeRequestBody(values);

    // If productId is not present, user tries to create new entry.
    const {payload} = dispatch(await (!productId ? create(requestBody) : update(productId, requestBody)));

    if (payload.errors) {
        const {
            photo_big,
            unit_price,
            top_notes,
            heart_notes,
            base_notes,
            percent_off,
            is_featured,
            ...restOfErrors
        } = payload.errors;

        // Map field error names to their form input equivalents.
        throw new SubmissionError({
            photo: photo_big,
            unitPrice: unit_price,
            top_notes,
            heart_notes,
            base_notes,
            percentOff: percent_off,
            isFeatured: is_featured,
            ...restOfErrors,
        });
    } else if (payload.__meta__.status !== STATUS.OK) {
        throw SubmissionError;
    }

    history.push(retrieveUrl('adminPerfumesAllList').path);
};

export const submitServer = async (request, response, dispatch) => {
    try {
        const files = request.files || {};
        const {productId, category, unitPrice, amount, quantity, percentOff, isFeatured, ...values} = request.body;

        const parsedValues = {
            productId: +productId,
            category: +category,
            unitPrice: +unitPrice,
            amount: +amount,
            quantity: +quantity,
            percentOff: +percentOff,
            isFeatured: !!isFeatured,
            ...values,
            ...files,
        };

        // noinspection DuplicatedCode
        if (Object.values(validate(parsedValues)).every(value => !value)) {
            // eslint-disable-next-line no-shadow
            const {productId, ...requestBodyValues} = parsedValues;
            const requestBody = makeRequestBody(requestBodyValues);
            const context = {headers: request.headers};

            const {payload} = dispatch(
                await (!productId ? create(requestBody, context) : update(productId, requestBody, context))
            );

            if (payload.__meta__.status === STATUS.OK) {
                return () => response.redirect(retrieveUrl('adminPerfumesAllList').path);
            }
        }
    } catch (error) {
        logDispatchAPIErrors(error);
    }

    return () => response.redirect('?error');
};
