import {SubmissionError} from 'redux-form';
import {create, update} from '../actions/announcements';
import {validateFieldMaxLength, validateFieldRequired} from '../../app/utils/validation';
import {makeRequestBody} from '../utils/announcements';
import {history, retrieveUrl} from '../../app/utils/router';
import {STATUS} from '../../app/utils/api/constants';
import {logDispatchAPIErrors} from '../../app/utils/logger';

export const validate = ({announcementId, image, priority, title, description}) => {
    const errors = {};

    errors.image = !announcementId && validateFieldRequired(image?.file || image?.[0]);
    errors.priority = validateFieldRequired(priority);
    errors.title = validateFieldRequired(title) || validateFieldMaxLength(title, 50);
    errors.description = validateFieldRequired(description);

    return errors;
};

export const submit = async ({announcementId, ...values}, dispatch) => {
    const requestBody = makeRequestBody(values);

    const {payload} = dispatch(await (!announcementId ? create(requestBody) : update(announcementId, requestBody)));

    if (payload.errors) {
        const {is_active, ...restOfErrors} = payload.errors;

        // Map field error names to their form input equivalents.
        throw new SubmissionError({isActive: is_active, ...restOfErrors});
    } else if (payload.__meta__.status !== STATUS.OK) {
        throw SubmissionError;
    }

    history.push(retrieveUrl('adminAnnouncementsAllList').path);
};

export const submitServer = async (request, response, dispatch) => {
    try {
        const files = request.files || {};
        const {announcementId, priority, isActive, ...body} = request.body;
        const parsedBody = {
            announcementId: +announcementId,
            priority: +priority,
            isActive: !!isActive,
            ...body,
        };

        const values = {...files, ...parsedBody};
        // noinspection DuplicatedCode
        if (Object.values(validate(values)).every(value => !value)) {
            const requestBody = makeRequestBody(values);
            const context = {headers: request.headers};

            const {payload} = dispatch(
                await (!announcementId ? create(requestBody, context) : update(announcementId, requestBody, context))
            );

            if (payload.__meta__.status === STATUS.OK) {
                return () => response.redirect(retrieveUrl('adminAnnouncementsAllList').path);
            }
        }
    } catch (error) {
        logDispatchAPIErrors(error);
    }

    return () => response.redirect('?error');
};
