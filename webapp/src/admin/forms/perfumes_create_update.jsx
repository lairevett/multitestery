import {Field, Form, reduxForm, propTypes} from 'redux-form';
import React from 'react';
import {FormCard, FormField, FormFileField} from '../../app/templates/__partials__/form';
import {submit, validate} from './process_perfumes_create_update';

import {FORM} from '../constants/perfumes';
import SubmitButton from '../../app/templates/__partials__/submit_button';
import ErrorAlert from '../../app/templates/__partials__/error_alert';

const AdminPerfumesCreateUpdateForm = ({
    handleSubmit,
    valid,
    pristine,
    submitting,
    submitFailed,
    fileFields,
    isError,
}) => (
    <FormCard>
        {(submitFailed || isError) && <ErrorAlert />}
        <Form method="post" action="?" encType="multipart/form-data" onSubmit={handleSubmit(submit)}>
            <Field id="productId" name="productId" type="hidden" parse={value => +value} component={FormField} />
            <Field
                id="photo"
                name="photo"
                type="file"
                accept={fileFields.photo.accept.join(', ')}
                multiple={fileFields.photo.maxCount > 1}
                label="Zdjęcie"
                component={FormFileField}
                aria-labelledby="photo"
            />
            <Field id="brand" name="brand" type="text" label="Marka" component={FormField} aria-labelledby="brand" />
            <Field id="model" name="model" type="text" label="Model" component={FormField} aria-labelledby="model" />
            <Field
                id="category"
                name="category"
                type="select"
                label="Kategoria"
                parse={value => +value}
                component={FormField}
                aria-labelledby="category"
            >
                <option value="" disabled>
                    Wybierz kategorię
                </option>
                <option value="0">Dla mężczyzn</option>
                <option value="1">Dla kobiet</option>
                <option value="2">Unisex</option>
            </Field>
            <Field
                id="unitPrice"
                name="unitPrice"
                type="number"
                label="Cena za sztukę (gr)"
                min="1"
                placeholder="np. 12099 (PLN 120,99)"
                parse={value => +value}
                component={FormField}
                aria-labelledby="unitPrice"
            />
            <Field
                id="amount"
                name="amount"
                type="number"
                label="Wielkość flakonu (ml)"
                min="1"
                parse={value => +value}
                component={FormField}
                aria-labelledby="amount"
            />
            <Field
                id="quantity"
                name="quantity"
                type="number"
                label="Sztuk"
                min="0"
                parse={value => +value}
                component={FormField}
                aria-labelledby="quantity"
            />
            <Field
                id="description"
                name="description"
                type="textarea"
                label="Opis"
                rows="15"
                placeholder="Pole niewymagane, opis produktu zapisany w Markdown."
                component={FormField}
                aria-labelledby="description"
            />
            <Field
                id="topNotes"
                name="topNotes"
                type="text"
                label="Nuty głowy"
                placeholder="Pole niewymagane."
                component={FormField}
                aria-labelledby="topNotes"
            />
            <Field
                id="heartNotes"
                name="heartNotes"
                type="text"
                label="Nuty serca"
                placeholder="Pole niewymagane."
                component={FormField}
                aria-labelledby="heartNotes"
            />
            <Field
                id="baseNotes"
                name="baseNotes"
                type="text"
                label="Nuty głębi"
                placeholder="Pole niewymagane."
                component={FormField}
                aria-labelledby="baseNotes"
            />
            <Field
                id="percentOff"
                name="percentOff"
                type="number"
                label="Przecena (%)"
                placeholder="np. 50"
                min="0"
                max="100"
                parse={value => +value}
                component={FormField}
                aria-labelledby="percentOff"
            />
            <Field
                id="isFeatured"
                name="isFeatured"
                type="checkbox"
                label="Produkt wyróżniony w polecanych."
                component={FormField}
                parse={value => !!value}
                aria-labelledby="isFeatured"
            />
            <SubmitButton
                className="mt-3"
                disabled={!valid || pristine || submitting}
                aria-label="Przycisk zatwierdź"
            />
        </Form>
    </FormCard>
);

AdminPerfumesCreateUpdateForm.propTypes = propTypes;

export default reduxForm({
    form: FORM.CREATE_UPDATE,
    enableReinitialize: true,
    validate,
    shouldValidate: () => process.env.__CLIENT__,
})(AdminPerfumesCreateUpdateForm);
