import {Field, Form, reduxForm, propTypes} from 'redux-form';
import React from 'react';
import {FormCard, FormField} from '../../app/templates/__partials__/form';
import {submit, validate} from './process_pages_create_update';

import {FORM} from '../constants/pages';
import SubmitButton from '../../app/templates/__partials__/submit_button';
import ErrorAlert from '../../app/templates/__partials__/error_alert';

const AdminPagesCreateUpdateForm = ({handleSubmit, valid, pristine, submitting, submitFailed, isError}) => (
    <FormCard>
        {(submitFailed || isError) && <ErrorAlert />}
        <Form method="post" action="?" onSubmit={handleSubmit(submit)}>
            {/* Use pageSlug instead of modifiable field slug when sending PATCH request. */}
            <Field id="pageSlug" name="pageSlug" type="hidden" component={FormField} parse={value => +value} />
            <Field
                id="slug"
                name="slug"
                type="text"
                label="Ścieżka (bez / na początku)"
                placeholder="np. about-us"
                component={FormField}
                aria-labelledby="slug"
            />
            <Field
                id="priority"
                name="priority"
                type="number"
                label="Priorytet"
                min="0"
                component={FormField}
                parse={value => +value}
                aria-labelledby="priority"
            />
            <Field id="title" name="title" type="text" label="Tytuł" component={FormField} aria-labelledby="title" />
            <Field
                id="content"
                name="content"
                type="textarea"
                label="Zawartość"
                placeholder="### Zawartość podstrony zapisana w Markdown"
                component={FormField}
                aria-labelledby="content"
            />
            <Field
                id="isActive"
                name="isActive"
                type="checkbox"
                label="Podstrona aktywna."
                component={FormField}
                parse={value => !!value}
                aria-labelledby="isActive"
            />
            <SubmitButton
                className="mt-3"
                disabled={!valid || pristine || submitting}
                aria-label="Przycisk zatwierdź"
            />
        </Form>
    </FormCard>
);

AdminPagesCreateUpdateForm.propTypes = propTypes;

export default reduxForm({
    form: FORM.CREATE_UPDATE,
    enableReinitialize: true,
    validate,
    shouldValidate: () => process.env.__CLIENT__,
})(AdminPagesCreateUpdateForm);
