import {Field, Form, reduxForm, propTypes} from 'redux-form';
import React from 'react';
import {FormCard, FormField} from '../../app/templates/__partials__/form';
import {submit, validate} from './process_user_update';

import {FORM} from '../constants/users';
import SubmitButton from '../../app/templates/__partials__/submit_button';
import SuccessAlert from '../../app/templates/__partials__/success_alert';
import ErrorAlert from '../../app/templates/__partials__/error_alert';

const AdminUserUpdateForm = ({
    handleSubmit,
    valid,
    pristine,
    submitting,
    submitSucceeded,
    submitFailed,
    isSuperUser,
    isSuccess,
    isError,
}) => (
    <FormCard>
        {(submitSucceeded || isSuccess) && <SuccessAlert message="Użytkownik został zaktualizowany pomyślnie." />}
        {(submitFailed || isError) && <ErrorAlert />}
        <Form method="post" action="?" onSubmit={handleSubmit(submit)}>
            <Field id="id" name="id" type="hidden" component={FormField} />
            <Field
                id="username"
                name="username"
                type="text"
                label="Nazwa użytkownika"
                component={FormField}
                aria-labelledby="username"
            />
            <Field
                id="email"
                name="email"
                type="email"
                label="Adres email"
                component={FormField}
                aria-labelledby="email"
            />
            {!isSuperUser && (
                <Field
                    id="isStaff"
                    name="isStaff"
                    type="checkbox"
                    label="Konto administratora."
                    component={FormField}
                    parse={value => !!value}
                    aria-labelledby="isStaff"
                />
            )}
            <SubmitButton
                className="btn btn-primary mt-3"
                disabled={!valid || pristine || submitting}
                value="Zapisz"
                aria-label="Przycisk zapisz"
            />
        </Form>
    </FormCard>
);

AdminUserUpdateForm.propTypes = propTypes;

export default reduxForm({
    form: FORM.UPDATE_USER,
    enableReinitialize: true,
    validate,
    shouldValidate: () => process.env.__CLIENT__,
})(AdminUserUpdateForm);
