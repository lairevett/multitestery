import {SubmissionError} from 'redux-form';
import {validateEmail, validateFieldRequired, validateUsername} from '../../app/utils/validation';

import {update} from '../actions/users';
import {STATUS} from '../../app/utils/api/constants';
import {logDispatchAPIErrors} from '../../app/utils/logger';

export const validate = ({id, username, email}) => {
    const errors = {};

    errors.id = validateFieldRequired(id);
    errors.username = validateUsername(username);
    errors.email = validateEmail(email);

    return errors;
};

export const submit = async ({id, username, email, isStaff}, dispatch) => {
    const {payload} = dispatch(await update(id, {username, email, is_staff: isStaff}));

    if (payload.errors) {
        const {is_staff, ...restOfErrors} = payload.errors;

        throw new SubmissionError({isStaff: is_staff, ...restOfErrors});
    } else if (payload.__meta__.status !== STATUS.OK) {
        throw SubmissionError;
    }
};

export const submitServer = async (request, response, dispatch) => {
    try {
        if (Object.values(validate(request.body)).every(value => !value)) {
            const {id, username, email, isStaff} = request.body;
            const {payload} = dispatch(
                await update(id, {username, email, is_staff: !!isStaff}, {headers: request.headers})
            );

            if (payload.__meta__.status === STATUS.OK) {
                return () => response.redirect('?success');
            }
        }
    } catch (error) {
        logDispatchAPIErrors(error);
    }

    return () => response.redirect('?error');
};
