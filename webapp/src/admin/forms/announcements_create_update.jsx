import {Field, Form, reduxForm, propTypes} from 'redux-form';
import React from 'react';
import {FormCard, FormField, FormFileField} from '../../app/templates/__partials__/form';
import {submit, validate} from './process_announcements_create_update';

import {FORM} from '../constants/announcements';
import SubmitButton from '../../app/templates/__partials__/submit_button';
import ErrorAlert from '../../app/templates/__partials__/error_alert';

const AdminAnnouncementsCreateUpdateForm = ({
    handleSubmit,
    valid,
    pristine,
    submitting,
    submitFailed,
    fileFields,
    isError,
}) => (
    <FormCard>
        {(submitFailed || isError) && <ErrorAlert />}
        <Form method="post" encType="multipart/form-data" onSubmit={handleSubmit(submit)}>
            <Field
                id="announcementId"
                name="announcementId"
                type="hidden"
                component={FormField}
                parse={value => +value}
            />
            <Field
                id="image"
                name="image"
                type="file"
                accept={fileFields.image.accept.join(', ')}
                multiple={fileFields.image.maxCount > 1}
                label="Zdjęcie"
                component={FormFileField}
                aria-labelledby="image"
            />
            <Field
                id="priority"
                name="priority"
                type="number"
                label="Priorytet"
                min="0"
                component={FormField}
                aria-labelledby="priority"
                parse={value => +value}
            />
            <Field id="title" name="title" type="text" label="Tytuł" component={FormField} aria-labelledby="title" />
            <Field
                id="description"
                name="description"
                type="textarea"
                label="Opis"
                component={FormField}
                aria-labelledby="description"
            />
            <Field
                id="isActive"
                name="isActive"
                type="checkbox"
                label="Ogłoszenie aktywne."
                component={FormField}
                parse={value => !!value}
                aria-labelledby="isActive"
            />
            <SubmitButton
                className="mt-3"
                disabled={!valid || pristine || submitting}
                aria-label="Przycisk zatwierdź"
            />
        </Form>
    </FormCard>
);

AdminAnnouncementsCreateUpdateForm.propTypes = propTypes;

export default reduxForm({
    form: FORM.CREATE_UPDATE,
    enableReinitialize: true,
    validate,
    shouldValidate: () => process.env.__CLIENT__,
})(AdminAnnouncementsCreateUpdateForm);
