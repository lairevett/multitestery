import {SubmissionError} from 'redux-form';
import {create, update} from '../actions/pages';
import {
    validateFieldContainsNoWhitespace,
    validateFieldMaxLength,
    validateFieldRequired,
} from '../../app/utils/validation';

import {history, retrieveUrl} from '../../app/utils/router';
import {STATUS} from '../../app/utils/api/constants';
import {logDispatchAPIErrors} from '../../app/utils/logger';

export const validate = ({slug, priority, title, content}) => {
    const errors = {};

    errors.slug = validateFieldRequired(slug) || validateFieldContainsNoWhitespace(slug);
    errors.priority = validateFieldRequired(priority);
    errors.title = validateFieldRequired(title) || validateFieldMaxLength(title, 50);
    errors.content = validateFieldRequired(content);

    return errors;
};

// noinspection DuplicatedCode
export const submit = async (values, dispatch) => {
    const {pageSlug, isActive, ...restOfValues} = values;
    const requestBody = {is_active: isActive, ...restOfValues};

    const {payload} = dispatch(await (!pageSlug ? create(requestBody) : update(pageSlug, requestBody)));

    if (payload.errors) {
        const {is_active, ...restOfErrors} = payload.errors;

        // Map field error names to their form input equivalents.
        throw new SubmissionError({isActive: is_active, ...restOfErrors});
    } else if (payload.__meta__.status !== STATUS.OK) {
        throw SubmissionError;
    }

    history.push(retrieveUrl('adminPagesAllList').path);
};

export const submitServer = async (request, response, dispatch) => {
    try {
        const {pageSlug, priority, isActive, ...values} = request.body;
        const parsedValues = {
            priority: +priority,
            isActive: !!isActive,
            ...values,
        };

        if (Object.values(validate(parsedValues)).every(value => !value)) {
            // eslint-disable-next-line no-shadow
            const {isActive, ...restOfValues} = parsedValues;
            const requestBody = {is_active: isActive, ...restOfValues};
            const context = {headers: request.headers};

            const {payload} = dispatch(
                await (!pageSlug ? create(requestBody, context) : update(pageSlug, requestBody, context))
            );

            if (payload.__meta__.status === STATUS.OK) {
                return () => response.redirect(retrieveUrl('adminPagesAllList').path);
            }
        }
    } catch (error) {
        logDispatchAPIErrors(error);
    }

    return () => response.redirect('?error');
};
