import {ACCESS, registerUrl} from '../../app/utils/router';
import {AdminSummaryView} from '../views/summary';
import {callbackGetDispatchEvents} from '../utils/summary';

registerUrl('adminSummary', '/admin/summary', ['get'], AdminSummaryView, ACCESS.REQUIRE_ADMIN, {
    title: 'Podsumowanie',
    callbackGetDispatchEvents,
});
