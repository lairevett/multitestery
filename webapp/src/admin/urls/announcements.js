import {ACCESS, registerUrl} from '../../app/utils/router';
import {AdminAnnouncementsCreateUpdateView, AdminAnnouncementsListView} from '../views/announcements';

import {FORM, MODE} from '../constants/announcements';
import {callbackGetDispatchEvents} from '../utils/announcements';

registerUrl(
    'adminAnnouncementsCreate',
    '/admin/announcements/create',
    ['get', 'post'],
    AdminAnnouncementsCreateUpdateView,
    ACCESS.REQUIRE_ADMIN,
    {
        title: 'Dodaj ogłoszenie',
        mode: MODE.CREATE,
        formFileFields: FORM.CREATE_UPDATE__FILE_FIELDS,
    }
);

registerUrl(
    'adminAnnouncementsUpdate',
    '/admin/announcements/:announcementId(\\d+)/update',
    ['get', 'post'],
    AdminAnnouncementsCreateUpdateView,
    ACCESS.REQUIRE_ADMIN,
    {
        title: 'Edytuj ogłoszenie',
        mode: MODE.UPDATE,
        callbackGetDispatchEvents,
        formFileFields: FORM.CREATE_UPDATE__FILE_FIELDS,
    }
);

registerUrl(
    'adminAnnouncementsAllList',
    '/admin/announcements',
    ['get'],
    AdminAnnouncementsListView,
    ACCESS.REQUIRE_ADMIN,
    {
        title: 'Ogłoszenia',
        mode: MODE.LIST_ALL,
        callbackGetDispatchEvents,
    }
);
