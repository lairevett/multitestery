import {ACCESS, registerUrl} from '../../app/utils/router';
import {AdminPagesCreateUpdateView, AdminPagesListView} from '../views/pages';

import {MODE} from '../constants/pages';
import {callbackGetDispatchEvents} from '../utils/pages';

registerUrl(
    'adminPagesCreate',
    '/admin/pages/create',
    ['get', 'post'],
    AdminPagesCreateUpdateView,
    ACCESS.REQUIRE_ADMIN,
    {
        title: 'Dodaj podstronę',
        mode: MODE.CREATE,
    }
);

registerUrl(
    'adminPagesUpdate',
    '/admin/pages/:pageSlug/update',
    ['get', 'post'],
    AdminPagesCreateUpdateView,
    ACCESS.REQUIRE_ADMIN,
    {
        title: 'Edytuj podstronę',
        mode: MODE.UPDATE,
        callbackGetDispatchEvents,
    }
);

registerUrl('adminPagesAllList', '/admin/pages', ['get'], AdminPagesListView, ACCESS.REQUIRE_ADMIN, {
    title: 'Podstrony',
    mode: MODE.LIST_ALL,
    callbackGetDispatchEvents,
});
