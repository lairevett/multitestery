import {ACCESS, registerUrl} from '../../app/utils/router';
import {FORM, MODE} from '../constants/perfumes';
import {AdminPerfumesCreateUpdateView, AdminPerfumesListView} from '../views/perfumes';
import {callbackGetDispatchEvents} from '../utils/perfumes';

registerUrl(
    'adminPerfumesCreate',
    '/admin/perfumes/all/create',
    ['get', 'post'],
    AdminPerfumesCreateUpdateView,
    ACCESS.REQUIRE_ADMIN,
    {
        title: 'Dodaj produkt',
        mode: MODE.CREATE,
        formFileFields: FORM.CREATE_UPDATE__FILE_FIELDS,
    }
);

registerUrl(
    'adminPerfumesUpdate',
    '/admin/perfumes/all/:productId(\\d+)/update',
    ['get', 'post'],
    AdminPerfumesCreateUpdateView,
    ACCESS.REQUIRE_ADMIN,
    {
        title: 'Edytuj produkt',
        mode: MODE.UPDATE,
        callbackGetDispatchEvents,
        formFileFields: FORM.CREATE_UPDATE__FILE_FIELDS,
    }
);

registerUrl('adminPerfumesAllList', '/admin/perfumes/all', ['get'], AdminPerfumesListView, ACCESS.REQUIRE_ADMIN, {
    title: 'Perfumy',
    mode: MODE.LIST_ALL,
    callbackGetDispatchEvents,
});

registerUrl(
    'adminPerfumesNotAvailableList',
    '/admin/perfumes/not-available',
    ['get'],
    AdminPerfumesListView,
    ACCESS.REQUIRE_ADMIN,
    {
        title: 'Niedostępne perfumy',
        mode: MODE.LIST_NOT_AVAILABLE,
        callbackGetDispatchEvents,
    }
);

registerUrl(
    'adminPerfumesAvailableList',
    '/admin/perfumes/available',
    ['get'],
    AdminPerfumesListView,
    ACCESS.REQUIRE_ADMIN,
    {
        title: 'Dostępne perfumy',
        mode: MODE.LIST_AVAILABLE,
        callbackGetDispatchEvents,
    }
);
