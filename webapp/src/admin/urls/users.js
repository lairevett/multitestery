import {ACCESS, registerUrl} from '../../app/utils/router';
import {UsersListView, UsersUpdateView} from '../views/users';
import {MODE} from '../constants/users';
import {callbackGetDispatchEvents} from '../utils/users';

registerUrl(
    'adminUsersUpdate',
    '/admin/users/:userId(\\d+)/update',
    ['get', 'post'],
    UsersUpdateView,
    ACCESS.REQUIRE_ADMIN,
    {
        title: 'Edytuj użytkownika',
        mode: MODE.UPDATE,
        callbackGetDispatchEvents,
    }
);

registerUrl('adminUsersAllList', '/admin/users', ['get'], UsersListView, ACCESS.REQUIRE_ADMIN, {
    title: 'Użytkownicy',
    mode: MODE.LIST_ALL,
    callbackGetDispatchEvents,
});
