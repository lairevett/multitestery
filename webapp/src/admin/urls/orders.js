import {ACCESS, registerUrl} from '../../app/utils/router';
import {MODE} from '../constants/orders';
import {AdminOrdersDetailsView, AdminOrdersListView} from '../views/orders';
import {callbackGetDispatchEvents} from '../utils/orders';

registerUrl('adminOrdersAllList', '/admin/orders/all', ['get'], AdminOrdersListView, ACCESS.REQUIRE_ADMIN, {
    title: 'Zamówienia',
    mode: MODE.LIST_ALL,
    callbackGetDispatchEvents,
});

registerUrl('adminOrdersNotPaidList', '/admin/orders/not-paid', ['get'], AdminOrdersListView, ACCESS.REQUIRE_ADMIN, {
    title: 'Nieopłacone zamówienia',
    mode: MODE.LIST_NOT_PAID,
    callbackGetDispatchEvents,
});

registerUrl('adminOrdersPaidList', '/admin/orders/paid', ['get'], AdminOrdersListView, ACCESS.REQUIRE_ADMIN, {
    title: 'Opłacone zamówienia',
    mode: MODE.LIST_PAID,
    callbackGetDispatchEvents,
});

registerUrl('adminOrdersCanceledList', '/admin/orders/canceled', ['get'], AdminOrdersListView, ACCESS.REQUIRE_ADMIN, {
    title: 'Anulowane zamówienia',
    mode: MODE.LIST_CANCELED,
    callbackGetDispatchEvents,
});

registerUrl('adminOrdersSentList', '/admin/orders/sent', ['get'], AdminOrdersListView, ACCESS.REQUIRE_ADMIN, {
    title: 'Wysłane zamówienia',
    mode: MODE.LIST_SENT,
    callbackGetDispatchEvents,
});

registerUrl(
    'adminOrdersDetails',
    '/admin/orders/all/:orderId(\\d+)',
    ['get'],
    AdminOrdersDetailsView,
    ACCESS.REQUIRE_ADMIN,
    {
        title: 'Szczegóły zamówienia',
        mode: MODE.DETAILS,
        callbackGetDispatchEvents,
    }
);
