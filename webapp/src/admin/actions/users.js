import {apiDeleteRequest, apiGetListRequest, apiGetRequest, apiPatchRequest} from '../../app/utils/api/requests';
import {ACTION, ENDPOINT, LIST_INITIAL_STATE} from '../constants/users';
import {DETAILS_INITIAL_STATE} from '../../auth/constants/user';

export const update = (userId, data, context) =>
    apiPatchRequest(ACTION.UPDATE, DETAILS_INITIAL_STATE, ENDPOINT.DETAILS(userId), data, context);

export const retrieveAllList = (page, context) =>
    apiGetListRequest(ACTION.RETRIEVE_ALL_LIST, LIST_INITIAL_STATE, ENDPOINT.LIST_ALL(page), context);

export const clearList = () => ({type: ACTION.CLEAR_LIST});

export const retrieveDetails = (userId, context) =>
    apiGetRequest(ACTION.RETRIEVE_DETAILS, DETAILS_INITIAL_STATE, ENDPOINT.DETAILS(userId), context);

export const clearDetails = () => ({type: ACTION.CLEAR_DETAILS});

export const destroy = (userId, context = {}) =>
    apiDeleteRequest(ACTION.DESTROY, {}, ENDPOINT.DETAILS(userId), {...context, id: userId});
