import {apiGetListRequest, apiGetRequest, apiPostRequest} from '../../app/utils/api/requests';
import {DETAILS_INITIAL_STATE, LIST_INITIAL_STATE} from '../../orders/constants';
import {ACTION, ENDPOINT} from '../constants/orders';

export const updateStatus = (orderId, newStatus) =>
    apiPostRequest(ACTION.UPDATE_STATUS, {}, ENDPOINT.UPDATE_STATUS(orderId), {new_status: newStatus});

export const retrieveAllList = (page, context) =>
    apiGetListRequest(ACTION.RETRIEVE_ALL_LIST, LIST_INITIAL_STATE, ENDPOINT.LIST_ALL(page), context);

export const retrieveNotPaidList = (page, context) =>
    apiGetListRequest(ACTION.RETRIEVE_NOT_PAID_LIST, LIST_INITIAL_STATE, ENDPOINT.LIST_NOT_PAID(page), context);

export const retrievePaidList = (page, context) =>
    apiGetListRequest(ACTION.RETRIEVE_PAID_LIST, LIST_INITIAL_STATE, ENDPOINT.LIST_PAID(page), context);

export const retrieveCanceledList = (page, context) =>
    apiGetListRequest(ACTION.RETRIEVE_CANCELED_LIST, LIST_INITIAL_STATE, ENDPOINT.LIST_CANCELED(page), context);

export const retrieveSentList = (page, context) =>
    apiGetListRequest(ACTION.RETRIEVE_SENT_LIST, LIST_INITIAL_STATE, ENDPOINT.LIST_SENT(page), context);

export const retrieveDetails = (orderId, context) =>
    apiGetRequest(ACTION.RETRIEVE_DETAILS, DETAILS_INITIAL_STATE, ENDPOINT.DETAILS(orderId), context);

export const clear = mode => ({type: ACTION.CLEAR, context: {mode}});
