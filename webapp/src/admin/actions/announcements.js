import {
    apiDeleteRequest,
    apiGetListRequest,
    apiGetRequest,
    apiPatchRequest,
    apiPostRequest,
} from '../../app/utils/api/requests';
import {LIST_INITIAL_STATE} from '../../announcements/constants';
import {ACTION, DETAILS_INITIAL_STATE, ENDPOINT} from '../constants/announcements';

export const create = (requestBody, context) => apiPostRequest(ACTION.CREATE, {}, ENDPOINT.BASE, requestBody, context);

export const update = (announcementId, requestBody, context) =>
    apiPatchRequest(ACTION.UPDATE, {}, ENDPOINT.DETAILS(announcementId), requestBody, context);

export const retrieveAllList = () => apiGetListRequest(ACTION.RETRIEVE_ALL_LIST, LIST_INITIAL_STATE, ENDPOINT.BASE);

export const clearList = () => ({type: ACTION.CLEAR_LIST});

export const retrieveDetails = announcementId =>
    apiGetRequest(ACTION.RETRIEVE_DETAILS, DETAILS_INITIAL_STATE, ENDPOINT.DETAILS(announcementId));

export const clearDetails = () => ({type: ACTION.CLEAR_DETAILS});

export const destroy = announcementId =>
    apiDeleteRequest(ACTION.DESTROY, {}, ENDPOINT.DETAILS(announcementId), {id: announcementId});
