import {
    apiDeleteRequest,
    apiGetListRequest,
    apiGetRequest,
    apiPatchRequest,
    apiPostRequest,
} from '../../app/utils/api/requests';
import {ACTION, ENDPOINT} from '../constants/pages';
import {DETAILS_INITIAL_STATE, ENDPOINT as ENDPOINT_PAGES, LIST_INITIAL_STATE} from '../../pages/constants';

export const create = (requestBody, context) => apiPostRequest(ACTION.CREATE, {}, ENDPOINT.BASE, requestBody, context);

export const update = (pageSlug, requestBody, context = {}) =>
    apiPatchRequest(ACTION.UPDATE, {}, ENDPOINT_PAGES.DETAILS(pageSlug), requestBody, {...context, slug: pageSlug});

export const retrieveAllList = () => apiGetListRequest(ACTION.RETRIEVE_ALL_LIST, LIST_INITIAL_STATE, ENDPOINT.BASE);

export const clearList = () => ({type: ACTION.CLEAR_LIST});

export const retrieveDetails = pageSlug =>
    apiGetRequest(ACTION.RETRIEVE_DETAILS, DETAILS_INITIAL_STATE, ENDPOINT_PAGES.DETAILS(pageSlug));

export const clearDetails = () => ({type: ACTION.CLEAR_DETAILS});

export const destroy = (pageSlug, context = {}) =>
    apiDeleteRequest(ACTION.DESTROY, DETAILS_INITIAL_STATE, ENDPOINT_PAGES.DETAILS(pageSlug), {
        ...context,
        slug: pageSlug,
    });
