import {apiGetRequest} from '../../app/utils/api/requests';
import {ACTION, ENDPOINT, INITIAL_STATE} from '../constants/summary';

export const retrieve = context => apiGetRequest(ACTION.RETRIEVE, INITIAL_STATE, ENDPOINT.BASE, context);

export const clear = () => ({type: ACTION.CLEAR});
