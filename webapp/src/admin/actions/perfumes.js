import {
    apiDeleteRequest,
    apiGetListRequest,
    apiGetRequest,
    apiPatchRequest,
    apiPostRequest,
} from '../../app/utils/api/requests';
import {DETAILS_INITIAL_STATE, ENDPOINT as ENDPOINT_PERFUMES, LIST_INITIAL_STATE} from '../../perfumes/constants';
import {ACTION, ENDPOINT} from '../constants/perfumes';

export const create = (requestBody, context) => apiPostRequest(ACTION.CREATE, {}, ENDPOINT.BASE, requestBody, context);

export const update = (productId, requestBody, context) =>
    apiPatchRequest(ACTION.UPDATE, {}, ENDPOINT_PERFUMES.DETAILS(productId), requestBody, context);

const _updateQuantity = (action, index, productId, newQuantity, mode, context = {}) =>
    apiPatchRequest(
        action,
        {},
        ENDPOINT_PERFUMES.DETAILS(productId),
        {quantity: newQuantity},
        {...context, index, newQuantity, mode}
    );

export const subQuantity = (index, productId, currentQuantity, mode, context) => {
    const newQuantity = currentQuantity - 1;
    return _updateQuantity(ACTION.SUB_QUANTITY, index, productId, newQuantity, mode, context);
};

export const addQuantity = (index, productId, currentQuantity, mode, context) => {
    const newQuantity = currentQuantity + 1;
    return _updateQuantity(ACTION.ADD_QUANTITY, index, productId, newQuantity, mode, context);
};

export const retrieveAllList = page =>
    apiGetListRequest(ACTION.RETRIEVE_ALL_LIST, LIST_INITIAL_STATE, ENDPOINT_PERFUMES.LIST_ALL(page));

export const clearAllList = () => ({type: ACTION.CLEAR_ALL_LIST});

export const retrieveNotAvailableList = page =>
    apiGetListRequest(ACTION.RETRIEVE_NOT_AVAILABLE_LIST, LIST_INITIAL_STATE, ENDPOINT.LIST_NOT_AVAILABLE(page));

export const clearNotAvailableList = () => ({type: ACTION.CLEAR_NOT_AVAILABLE_LIST});

export const retrieveAvailableList = page =>
    apiGetListRequest(ACTION.RETRIEVE_AVAILABLE_LIST, LIST_INITIAL_STATE, ENDPOINT.LIST_AVAILABLE(page));

export const clearAvailableList = () => ({type: ACTION.CLEAR_AVAILABLE_LIST});

export const retrieveDetails = productId =>
    apiGetRequest(ACTION.RETRIEVE_DETAILS, DETAILS_INITIAL_STATE, ENDPOINT_PERFUMES.DETAILS(productId));

export const clearDetails = () => ({type: ACTION.CLEAR_DETAILS});

export const destroy = (index, productId, mode, context = {}) =>
    apiDeleteRequest(ACTION.DESTROY, {}, ENDPOINT_PERFUMES.DETAILS(productId), {...context, index, mode});
