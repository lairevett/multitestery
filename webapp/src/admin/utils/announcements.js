import IsomorphicFormData from 'form-data';
import {MODE} from '../constants/announcements';
import {makeDispatchEvents} from '../../app/utils/dispatch_events';
import {clearDetails, clearList, retrieveAllList, retrieveDetails} from '../actions/announcements';
import {isomorphicFileAppend} from '../../app/utils/form';

export const callbackGetDispatchEvents = urlProperties => urlParameters => {
    switch (urlProperties.mode) {
        case MODE.CREATE:
            break;
        case MODE.UPDATE: {
            const announcementId = +urlParameters.announcementId;

            return makeDispatchEvents([() => retrieveDetails(announcementId)], [clearDetails]);
        }
        case MODE.LIST_ALL:
            return makeDispatchEvents([retrieveAllList], [clearList]);
        default:
            throw new Error(
                `Unexpected mode ${urlProperties.mode} provided in admin announcements callbackGetDispatchEvents.`
            );
    }
};

export const makeRequestBody = values => {
    const data = {
        priority: values.priority,
        title: values.title,
        description: values.description,
        is_active: values.isActive,
    };

    const formData = new IsomorphicFormData();

    if (values.image) isomorphicFileAppend(formData, 'image', values.image);

    // Streams can only handle appending strings and buffers, so cast any integers to string before doing so.
    Object.entries(data).forEach(([key, value]) => formData.append(key, `${value}`));

    return formData;
};
