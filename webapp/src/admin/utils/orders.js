import {
    clear,
    retrieveAllList,
    retrieveCanceledList,
    retrieveDetails,
    retrieveNotPaidList,
    retrievePaidList,
    retrieveSentList,
} from '../actions/orders';

import {MODE} from '../constants/orders';
import {makeDispatchEvents} from '../../app/utils/dispatch_events';
import {makeTitle} from '../../perfumes/utils';
import {getPolishPluralForPositiveNumber} from '../../app/utils/app';

export const mapValueForMode = (
    mode,
    valueIfModeAll,
    valueIfModeNotPaid,
    valueIfModePaid,
    valueIfModeCanceled,
    valueIfModeSent,
    valueIfModeDetails
) => {
    switch (mode) {
        case MODE.LIST_ALL:
            return valueIfModeAll;
        case MODE.LIST_NOT_PAID:
            return valueIfModeNotPaid;
        case MODE.LIST_PAID:
            return valueIfModePaid;
        case MODE.LIST_CANCELED:
            return valueIfModeCanceled;
        case MODE.LIST_SENT:
            return valueIfModeSent;
        case MODE.DETAILS:
            return valueIfModeDetails;
        default:
            throw new Error(`Unexpected mode "${mode}" provided in admin orders mapValueForMode.`);
    }
};

export const callbackGetDispatchEvents = urlProperties => (urlParameters, queryParameters, state, context) => {
    const orderId = +urlParameters.orderId;
    const page = +queryParameters.page || 1;

    const dispatchEvents = makeDispatchEvents([], [() => clear(urlProperties.mode)], [page]);

    switch (urlProperties.mode) {
        case MODE.LIST_ALL:
            dispatchEvents.onInit.push(() => retrieveAllList(page, context));
            break;
        case MODE.LIST_NOT_PAID:
            dispatchEvents.onInit.push(() => retrieveNotPaidList(page, context));
            break;
        case MODE.LIST_PAID:
            dispatchEvents.onInit.push(() => retrievePaidList(page, context));
            break;
        case MODE.LIST_CANCELED:
            dispatchEvents.onInit.push(() => retrieveCanceledList(page, context));
            break;
        case MODE.LIST_SENT:
            dispatchEvents.onInit.push(() => retrieveSentList(page, context));
            break;
        case MODE.DETAILS:
            dispatchEvents.onInit.push(() => retrieveDetails(orderId, context));
            dispatchEvents.dependencies.length = 0; // Remove page from dependencies array, details aren't paginated.
            break;
        default:
            throw new Error(
                `Unexpected mode "${urlProperties.mode}" provided in admin orders callbackGetDispatchEvents.`
            );
    }

    return dispatchEvents;
};

export const getMarkAsSentModalBodyTextAndButtonState = entries => {
    let bodyText = '';

    const notEnoughQuantityEntries = entries.filter(
        entry => !entry.product.is_deleted && entry.product.quantity - entry.quantity < 0
    );
    const isAnyEntryQuantityInsufficient = notEnoughQuantityEntries.length > 0;

    if (isAnyEntryQuantityInsufficient) {
        bodyText += `
            Następujące produkty mają niewystarczająca ilość na magazynie, aby wysłać zamówienie:
            <ul class="list-group">
                ${notEnoughQuantityEntries
                    .map(entry => {
                        const missingCount = Math.abs(entry.product.quantity - entry.quantity);

                        return `<li class="list-group-item border-0 p-1">
                            <em>${makeTitle(entry.product.brand, entry.product.model, entry.product.amount)}</em> 
                            (o ${missingCount} ${getPolishPluralForPositiveNumber(missingCount, 'sztuk')} za mało)
                            </li>`;
                    })
                    .join('')}
            </ul>
        `;
    } else {
        bodyText += `
            Ta akcja spowoduje zmniejszenie ilości następujących produktów na magazynie:
            <ul class="list-group">
                ${entries
                    .map(
                        entry =>
                            `<li class="list-group-item border-0 p-1">
                            <em>${makeTitle(entry.product.brand, entry.product.model, entry.product.amount)}</em> 
                            (${entry.product.quantity} -> ${entry.product.quantity - entry.quantity})
                         </li>`
                    )
                    .join('')}
            </ul>
        `;
    }

    const deletedEntries = entries.filter(entry => entry.product.is_deleted);
    if (deletedEntries.length > 0) {
        bodyText += `<br><br>
            Następujące produkty zostały wcześniej usunięte z bazy, <strong>sprawdź ich ilość przed potwierdzeniem wysłania zamówienia</strong>:
            <ul class="list-group">
                ${deletedEntries
                    .map(
                        entry =>
                            `<li class="list-group-item border-0 p-1">
                            <em>${makeTitle(entry.product.brand, entry.product.model, entry.product.amount)}</em>
                         </li>`
                    )
                    .join('')}
            </ul>
            <br>
        `;
    }

    return [bodyText, isAnyEntryQuantityInsufficient];
};
