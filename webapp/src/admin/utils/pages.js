import {MODE} from '../constants/pages';
import {makeDispatchEvents} from '../../app/utils/dispatch_events';
import {clearDetails, clearList, retrieveAllList, retrieveDetails} from '../actions/pages';

export const callbackGetDispatchEvents = urlProperties => urlParameters => {
    switch (urlProperties.mode) {
        case MODE.CREATE:
            break;
        case MODE.LIST_ALL:
            return makeDispatchEvents([retrieveAllList], [clearList]);
        case MODE.UPDATE:
        case MODE.DETAILS:
            return makeDispatchEvents([() => retrieveDetails(urlParameters.pageSlug)], [clearDetails]);
        default:
            throw new Error(
                `Unexpected mode "${urlProperties.mode} provided to admin pages callbackGetDispatchEvents."`
            );
    }
};
