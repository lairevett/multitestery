import {makeDispatchEvents} from '../../app/utils/dispatch_events';
import {MODE} from '../constants/users';
import {clearDetails, clearList, retrieveAllList, retrieveDetails} from '../actions/users';

export const callbackGetDispatchEvents = urlProperties => (urlParameters, queryParameters, state, context) => {
    switch (urlProperties.mode) {
        case MODE.UPDATE:
            return makeDispatchEvents([() => retrieveDetails(+urlParameters.userId, context)], [clearDetails]);
        case MODE.LIST_ALL: {
            const page = +queryParameters.page || 1;

            return makeDispatchEvents([() => retrieveAllList(page, context)], [clearList], [page]);
        }
        default:
            throw new Error(
                `Unexpected mode "${urlProperties.mode}" provided in admin users callbackGetDispatchEvents.`
            );
    }
};
