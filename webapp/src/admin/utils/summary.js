import {makeDispatchEvents} from '../../app/utils/dispatch_events';
import {clear, retrieve} from '../actions/summary';

export const callbackGetDispatchEvents = () => (urlParameters, queryParameters, state, context) =>
    makeDispatchEvents([() => retrieve(context)], [clear]);
