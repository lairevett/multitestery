import IsomorphicFormData from 'form-data';
import {
    clearAllList,
    clearAvailableList,
    clearDetails,
    clearNotAvailableList,
    retrieveAllList,
    retrieveAvailableList,
    retrieveDetails,
    retrieveNotAvailableList,
} from '../actions/perfumes';

import {MODE} from '../constants/perfumes';
import {makeDispatchEvents} from '../../app/utils/dispatch_events';
import {isomorphicFileAppend} from '../../app/utils/form';

export const mapValueForMode = (mode, valueIfModeAll, valueIfModeNotAvailable, valueIfModeAvailable) => {
    switch (mode) {
        case MODE.LIST_ALL:
            return valueIfModeAll;
        case MODE.LIST_NOT_AVAILABLE:
            return valueIfModeNotAvailable;
        case MODE.LIST_AVAILABLE:
            return valueIfModeAvailable;
        default:
            throw new Error(`Unexpected mode "${mode}" provided in admin perfumes mapValueForMode.`);
    }
};

export const callbackGetDispatchEvents = urlProperties => (urlParameters, queryParameters) => {
    const page = +queryParameters.page || 1;

    switch (urlProperties.mode) {
        case MODE.UPDATE:
            return makeDispatchEvents([() => retrieveDetails(+urlParameters.productId)], [clearDetails]);
        case MODE.LIST_ALL:
            return makeDispatchEvents([() => retrieveAllList(page)], [clearAllList], [page]);
        case MODE.LIST_NOT_AVAILABLE:
            return makeDispatchEvents([() => retrieveNotAvailableList(page)], [clearNotAvailableList], [page]);
        case MODE.LIST_AVAILABLE:
            return makeDispatchEvents([() => retrieveAvailableList(page)], [clearAvailableList], [page]);
        default:
            throw new Error(
                `Unexpected mode "${urlProperties.mode}" provided in admin perfumes callbackGetDispatchEvents.`
            );
    }
};

export const makeRequestBody = values => {
    const data = {
        brand: values.brand,
        model: values.model,
        category: values.category,
        unit_price: values.unitPrice,
        amount: values.amount,
        quantity: values.quantity,
        description: values.description ?? '',
        top_notes: values.topNotes ?? '',
        heart_notes: values.heartNotes ?? '',
        base_notes: values.baseNotes ?? '',
        percent_off: values.percentOff,
        is_featured: values.isFeatured,
    };

    const formData = new IsomorphicFormData();

    if (values.photo) isomorphicFileAppend(formData, 'photo_big', values.photo);

    // Streams can only handle appending strings and buffers, so cast any integers to string before doing so.
    Object.entries(data).forEach(([key, value]) => formData.append(key, `${value}`));

    return formData;
};
