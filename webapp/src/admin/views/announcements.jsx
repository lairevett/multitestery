/* eslint-disable react/prop-types */
import React from 'react';
import Content from '../../app/templates/__partials__/content';
import AdminAnnouncementsList from '../templates/announcements/list';
import Suspense from '../../app/utils/api/suspense';
import Loading from '../../app/templates/__partials__/loading';
import {MODE} from '../constants/announcements';
import AdminAnnouncementsCreateUpdate from '../templates/announcements/create_update';
import Menu from '../templates/__partials__/menu';
import {submitServer} from '../forms/process_announcements_create_update';
import {STATUS} from '../../app/utils/api/constants';

/*
 * GET /admin/announcements/create
 * POST /admin/announcements/create
 * GET /admin/announcements/:announcementId(\d+)/update
 * POST /admin/announcements/:announcementId(\d+)/update
 */
// noinspection DuplicatedCode
export const AdminAnnouncementsCreateUpdateView = ({title, mode, formFileFields, queryParameters, state}) => {
    const isError = 'error' in queryParameters;

    const {
        __meta__: {status: detailsStatus},
        ...details
    } = state.admin.announcements.details;

    const status = mode === MODE.CREATE ? STATUS.OK : detailsStatus;

    return (
        <Content status={status} title={title} menu={<Menu />} ignoreNoResults>
            <Suspense status={status} fallback={<Loading />}>
                <AdminAnnouncementsCreateUpdate
                    details={details}
                    mode={mode}
                    formFileFields={formFileFields}
                    isError={isError}
                />
            </Suspense>
        </Content>
    );
};

AdminAnnouncementsCreateUpdateView.willDispatchEvents = (request, response, urlProperties, state, dispatch) => {
    if (request.method === 'POST') {
        return submitServer(request, response, dispatch);
    }
};

/*
 * GET /admin/announcements
 */
export const AdminAnnouncementsListView = ({title, state}) => {
    const {list} = state.admin.announcements;

    const {
        results,
        __meta__: {status},
    } = list;

    return (
        <Content status={status} title={title} menu={<Menu />} ignoreNoResults>
            <Suspense status={status} fallback={<Loading />}>
                <AdminAnnouncementsList list={results} />
            </Suspense>
        </Content>
    );
};
