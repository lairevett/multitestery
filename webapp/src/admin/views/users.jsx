/* eslint-disable react/prop-types */
import React from 'react';
import Content from '../../app/templates/__partials__/content';
import AdminUsersList from '../templates/users/list';
import AdminUsersUpdate from '../templates/users/update';
import Loading from '../../app/templates/__partials__/loading';
import Suspense from '../../app/utils/api/suspense';
import Menu from '../templates/__partials__/menu';
import {submitServer} from '../forms/process_user_update';

/*
 * GET /admin/users/:userId(\d+)/update
 * POST /admin/users/:userId(\d+)/update
 */
export const UsersUpdateView = ({title, queryParameters, state}) => {
    const isSuccess = 'success' in queryParameters;
    const isError = 'error' in queryParameters;

    const {
        __meta__: {status},
        ...details
    } = state.admin.users.details;

    return (
        <Content status={status} title={title} menu={<Menu />}>
            <Suspense status={status} fallback={<Loading />}>
                <AdminUsersUpdate details={details} isSuccess={isSuccess} isError={isError} />
            </Suspense>
        </Content>
    );
};

UsersUpdateView.willDispatchEvents = (request, response, urlProperties, state, dispatch) => {
    if (request.method === 'POST') {
        return submitServer(request, response, dispatch);
    }
};

/*
 * GET /admin/users[?page={page}]
 */
export const UsersListView = ({title, match: {path}, queryParameters, state}) => {
    const userId = state.auth.user.id;

    const {
        previous,
        next,
        results,
        __meta__: {status},
    } = state.admin.users.list;

    return (
        <Content status={status} title={title} menu={<Menu />} ignoreNoResults>
            <Suspense status={status} fallback={<Loading />}>
                <AdminUsersList
                    list={results}
                    userId={userId}
                    paginationData={{
                        previous,
                        next,
                        path,
                        queryParameters,
                    }}
                />
            </Suspense>
        </Content>
    );
};
