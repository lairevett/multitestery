/* eslint-disable react/prop-types */
import React from 'react';
import Content from '../../app/templates/__partials__/content';
import Suspense from '../../app/utils/api/suspense';
import Loading from '../../app/templates/__partials__/loading';
import {MODE} from '../constants/pages';
import Menu from '../templates/__partials__/menu';
import {submitServer} from '../forms/process_pages_create_update';
import AdminPagesCreateUpdate from '../templates/pages/create_update';
import AdminPagesList from '../templates/pages/list';
import {STATUS} from '../../app/utils/api/constants';

/*
 * GET /admin/pages/create
 * POST /admin/pages/create
 * GET /admin/pages/:pageSlug/update
 * POST /admin/pages/:pageSlug/update
 */
export const AdminPagesCreateUpdateView = ({title, mode, queryParameters, state}) => {
    const isError = 'error' in queryParameters;

    const {
        __meta__: {status: detailsStatus},
        ...details
    } = state.admin.pages.details;

    const status = mode === MODE.CREATE ? STATUS.OK : detailsStatus;

    return (
        <Content status={status} title={title} menu={<Menu />} ignoreNoResults>
            <Suspense status={status} fallback={<Loading />}>
                <AdminPagesCreateUpdate details={details} mode={mode} isError={isError} />
            </Suspense>
        </Content>
    );
};

AdminPagesCreateUpdateView.willDispatchEvents = (request, response, urlProperties, state, dispatch) => {
    if (request.method === 'POST') {
        return submitServer(request, response, dispatch);
    }
};

/*
 * GET /admin/announcements
 */
export const AdminPagesListView = ({title, state}) => {
    const {list} = state.admin.pages;

    const {
        results,
        __meta__: {status},
    } = list;

    return (
        <Content status={status} title={title} menu={<Menu />} ignoreNoResults>
            <Suspense status={status} fallback={<Loading />}>
                <AdminPagesList list={results} />
            </Suspense>
        </Content>
    );
};
