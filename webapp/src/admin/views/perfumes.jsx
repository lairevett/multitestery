/* eslint-disable react/prop-types */
import React from 'react';

import AdminPerfumesCreateUpdate from '../templates/perfumes/create_update';
import AdminPerfumesList from '../templates/perfumes/list';
import Content from '../../app/templates/__partials__/content';
import {mapValueForMode} from '../utils/perfumes';
import Loading from '../../app/templates/__partials__/loading';
import {MODE} from '../constants/perfumes';
import Menu from '../templates/__partials__/menu';
import Suspense from '../../app/utils/api/suspense';
import {submitServer} from '../forms/process_perfumes_create_update';
import {STATUS} from '../../app/utils/api/constants';
import {useRedirectWhenLastOnPageDestroyed} from '../../perfumes/utils';

/*
 * GET /admin/perfumes/all/create
 * POST /admin/perfumes/all/create
 * GET /admin/perfumes/all/:productId(\d+)/update
 * POST /admin/perfumes/all/:productId(\d+)/update
 */
export const AdminPerfumesCreateUpdateView = ({title, mode, formFileFields, queryParameters, state}) => {
    const isError = 'error' in queryParameters;

    const {
        __meta__: {status: detailsStatus},
        ...details
    } = state.admin.perfumes.details;

    const status = mode === MODE.CREATE ? STATUS.OK : detailsStatus;

    return (
        <Content status={status} title={title} menu={<Menu />}>
            <Suspense status={status} fallback={<Loading />}>
                <AdminPerfumesCreateUpdate
                    details={details}
                    mode={mode}
                    formFileFields={formFileFields}
                    isError={isError}
                />
            </Suspense>
        </Content>
    );
};

AdminPerfumesCreateUpdateView.willDispatchEvents = (request, response, urlProperties, state, dispatch) => {
    if (request.method === 'POST') {
        return submitServer(request, response, dispatch);
    }
};

/*
 * GET /admin/perfumes/all[?page={page}]
 * GET /admin/perfumes/not-available[?page={page}]
 * GET /admin/perfumes/available[?page={page}]
 */
export const AdminPerfumesListView = ({title, mode, match: {path}, queryParameters, state}) => {
    const {
        admin: {
            perfumes: {allList, notAvailableList, availableList},
        },
    } = state;

    const {
        previous,
        next,
        results,
        __meta__: {status},
    } = mapValueForMode(mode, allList, notAvailableList, availableList);

    useRedirectWhenLastOnPageDestroyed(queryParameters, status);

    return (
        <Content status={status} title={title} menu={<Menu />} ignoreNoResults>
            <Suspense status={status} fallback={<Loading />}>
                <AdminPerfumesList
                    list={results}
                    mode={mode}
                    paginationData={{previous, next, path, queryParameters}}
                />
            </Suspense>
        </Content>
    );
};
