/* eslint-disable react/prop-types */
import React from 'react';
import AdminSummary from '../templates/summary';
import Content from '../../app/templates/__partials__/content';
import Loading from '../../app/templates/__partials__/loading';
import Suspense from '../../app/utils/api/suspense';
import Menu from '../templates/__partials__/menu';

/*
 * GET /admin/summary
 */
export const AdminSummaryView = ({title, state}) => {
    const {
        __meta__: {status},
        ...result
    } = state.admin.summary;

    return (
        <Content status={status} title={title} menu={<Menu />} ignoreNoResults>
            <Suspense status={status} fallback={<Loading />}>
                <AdminSummary summary={result} />
            </Suspense>
        </Content>
    );
};
