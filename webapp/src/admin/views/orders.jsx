/* eslint-disable react/prop-types */
import React from 'react';
import {mapValueForMode} from '../utils/orders';
import Content from '../../app/templates/__partials__/content';
import AdminOrdersList from '../templates/orders/list';
import AdminOrdersDetails from '../templates/orders/details';
import Menu from '../templates/__partials__/menu';
import Suspense from '../../app/utils/api/suspense';
import Loading from '../../app/templates/__partials__/loading';

/*
 * GET /admin/orders/all[?page={page}]
 * GET /admin/orders/not-paid[?page={page}]
 * GET /admin/orders/paid[?page={page}]
 * GET /admin/orders/canceled[?page={page}]
 * GET /admin/orders/sent[?page={page}]
 */
export const AdminOrdersListView = ({title, mode, match: {path}, queryParameters, state}) => {
    const adminOrders = state.admin.orders;

    const {
        previous,
        next,
        results,
        __meta__: {status},
    } = mapValueForMode(
        mode,
        adminOrders.allList,
        adminOrders.notPaidList,
        adminOrders.paidList,
        adminOrders.canceledList,
        adminOrders.sentList,
        undefined
    );

    return (
        <Content status={status} title={title} menu={<Menu />} ignoreNoResults>
            <Suspense status={status} fallback={<Loading />}>
                <AdminOrdersList list={results} paginationData={{previous, next, path, queryParameters}} />
            </Suspense>
        </Content>
    );
};

/*
 * GET /admin/orders/all/:orderId(\d+)
 */
export const AdminOrdersDetailsView = ({state}) => {
    const {details} = state.admin.orders;

    const {
        name,
        __meta__: {status},
    } = details;

    return (
        <Content status={status} title={name} menu={<Menu />}>
            <Suspense status={status} fallback={<Loading />}>
                <AdminOrdersDetails details={details} />
            </Suspense>
        </Content>
    );
};
