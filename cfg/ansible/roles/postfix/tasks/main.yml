---
- name: install the package
  package:
    name: postfix-sasl
    state: present
  when: postfix_executed is not defined

- name: ensure cyrus sasl library is installed
  package:
    name: cyrus-sasl
    state: present
  when: postfix_executed is not defined

- name: create /usr/local/etc/mail directory
  file:
    path: /usr/local/etc/mail
    state: directory
    owner: root
    group: wheel
  when: postfix_executed is not defined

- name: activate postfix in mailer
  command: install -m 0644 /usr/local/share/postfix/mailer.conf.postfix /usr/local/etc/mail/mailer.conf
  when: postfix_executed is not defined

- name: enable the service
  command: sysrc postfix_enable="YES"
  when: postfix_executed is not defined

- name: stop the service
  service:
    name: postfix
    state: stopped
  when: postfix_executed is not defined

- name: disable all sendmail(8) services
  command: sysrc sendmail_enable="NONE"
  when: postfix_executed is not defined

- name: disable sendmail specific periodic tasks
  lineinfile:
    path: '{{ periodic_conf_file_path }}'
    regexp: ^{{ item }}
    line: '{{ item }}="NO"'
  with_items:
    - daily_clean_hoststat_enable
    - daily_status_mail_rejects_enable
    - daily_status_include_submit_mailq
    - daily_submit_queuerun
  when: postfix_executed is not defined

- name: copy master conf file
  copy:
    src: master.cf
    dest: '{{ postfix_dir }}/master.cf'
  notify: restart postfix
  when: postfix_executed is not defined

- name: copy main conf file template
  template:
    src: main.cf.j2
    dest: '{{ postfix_dir }}/main.cf'
  notify: restart postfix
  when: postfix_executed is not defined

- name: change root mail alias to {{ user_admin_name }}
  lineinfile:
    path: /etc/aliases
    regexp: ^# root
    line: 'root: {{ user_admin_name }}@{{ project_site }}'
  when: postfix_executed is not defined

- name: point aliases to root
  lineinfile:
    path: /etc/aliases
    regexp: ^# {{ item }}
    line: '{{ item }}: root'
  with_items:
    - manager
    - dumper
    - hostmaster
    - webmaster
    - www
  when: postfix_executed is not defined

- name: make new aliases
  command: newaliases
  notify: restart postfix
  when: postfix_executed is not defined

- name: make sasl directory
  file:
    path: '{{ postfix_sasl_passwd_dir }}'
    state: directory
  when: postfix_executed is not defined

- name: copy sasl password file template
  template:
    src: sasl_passwd_gmail.j2
    dest: '{{ postfix_sasl_passwd_dir }}/{{ postfix_sasl_passwd_file_name }}'
  when: postfix_executed is not defined

- name: postmap sasl password file
  shell: /bin/sh -c 'cd {{ postfix_sasl_passwd_dir }} && postmap {{ postfix_sasl_passwd_file_name }}'
  notify: restart postfix
  when: postfix_executed is not defined

- name: start the service
  service:
    name: postfix
    state: started
  when: postfix_executed is not defined

- set_fact:
    postfix_executed: true