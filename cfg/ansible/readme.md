## Provisioning the server
```bash
ansible-playbook -i hosts provision.yml -l application -e 'secret_key=value admin_email_pass=value noreply_email_pass=value email_app_pass=value api_dbpass=value webmail_dbpass=value'
```

## Updating the server
```bash
ansible-playbook -i hosts rolling_update.yml -l application -e 'secret_key=value admin_email_pass=value noreply_email_pass=value email_app_pass=value api_dbpass=value webmail_dbpass=value'
```

## Variable meaning
* `secret_key`: Django application secret key
* `admin_email_pass`: System administrator email password
* `noreply_email_pass`: Email password for noreply user
* `email_app_pass`: Email *app* password (ordinary password won't work because of two-factor-auth)
* `api_dbpass`: PostgreSQL user database password for application
* `webmail_dbpass`: PostgreSQL user database password for rainloop
* `is_vagrant`: If defined, deploys/updates the application in vagrant mode

## Notes
* Keep the `secret_key` in rolling update same as in provision
* Don't define `is_vagrant` (even as falsy value) in environments other than vagrant