#!/usr/bin/env bash
pushd . > /dev/null
SCRIPTS_PATH="${BASH_SOURCE[0]}"
if [[ -h "${SCRIPTS_PATH}" ]]; then
    while [[ -h "${SCRIPTS_PATH}" ]]; do cd "$(dirname "${SCRIPTS_PATH}")" || return;
    SCRIPTS_PATH="$(readlink "${SCRIPTS_PATH}")"; done
fi
cd "$(dirname "${SCRIPTS_PATH}")" > /dev/null || return
SCRIPTS_PATH="$(pwd)"
popd > /dev/null || return

ROOT_PATH="$SCRIPTS_PATH/.."

API_PATH="$ROOT_PATH/../multitestery_api"
API_CONFIG_PATH="$ROOT_PATH/api"

CDN_CONFIG_PATH="$ROOT_PATH/cdn"

WEB_PATH="$ROOT_PATH/../multitestery_web"
WEB_CONFIG_PATH="$ROOT_PATH/web"

MAIL_CONFIG_PATH="$ROOT_PATH/mail"

OTHER_CONFIG_PATH="$ROOT_PATH/other"

echo "INFO: updating system."
apt update -y
apt upgrade -y

echo "INFO: setting up firewall."
apt install -y ufw
ufw default deny incoming
ufw default allow outgoing
ufw allow ssh
ufw allow http
ufw allow https
ufw allow imap
ufw allow smtp
ufw allow 993/tcp
ufw allow 465/tcp
ufw allow 587/tcp
ufw enable

echo "INFO: installing some stuff."
apt install -y vim software-properties-common net-tools curl wget htop

if [ -e $API_PATH ]; then
    echo "INFO: installing nginx."
    apt install -y nginx

    echo "INFO: installing python 3 and requirements."
    apt install -y python3 python3-setuptools python3-pip libpq-dev python3-dev
    pip3 install -r $API_CONFIG_PATH/requirements.pip

    echo "INFO: configuring nginx and ssl."
    systemctl enable nginx
    systemctl start nginx

    apt install -y certbot python-certbot-nginx

    cp $API_CONFIG_PATH/nginx/api.multitestery.pl.conf /etc/nginx/sites-available
    ln -sf /etc/nginx/sites-available/api.multitestery.pl.conf /etc/nginx/sites-enabled/api.multitestery.pl.conf

    cp $CDN_CONFIG_PATH/nginx/cdn.multitestery.pl.conf /etc/nginx/sites-available
    ln -sf /etc/nginx/sites-available/cdn.multitestery.pl.conf /etc/nginx/sites-enabled/cdn.multitestery.pl.conf

    python3 $API_PATH/src/manage.py collectstatic

    mkdir /var/www/cdn.multitestery.pl
    chown www-data:www-data /var/www/cdn.multitestery.pl
    if [ ! -d "/var/www/cdn.multitestery.pl/media" ]; then
        mkdir /var/www/cdn.multitestery.pl/media
        chown $PROJECT_USER:www-data -R /var/www/cdn.multitestery.pl/media
    fi

    mkdir -p /var/log/nginx/multitestery.pl
    mkdir /var/log/nginx/api.multitestery.pl
    mkdir /var/log/nginx/mail.multitestery.pl
    mkdir /var/log/nginx/cdn.multitestery.pl
    chown $PROJECT_USER:www-data /var/log/nginx/multitestery.pl
    chown $PROJECT_USER:www-data /var/log/nginx/api.multitestery.pl
    chown $PROJECT_USER:www-data /var/log/nginx/mail.multitestery.pl
    chown $PROJECT_USER:www-data /var/log/nginx/cdn.multitestery.pl

    echo "INFO: installing postgresql."
    wget -q https://www.postgresql.org/media/keys/ACCC4CF8.asc -O - | sudo apt-key add -
    sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ buster-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
    sudo apt update -y
    sudo apt install -y postgresql-11 postgresql-contrib-11

    echo "INFO: configuring postgresql."
    systemctl enable postgresql
    systemctl start postgresql

    sudo -u postgres psql -c "ALTER ROLE postgres PASSWORD '$POSTGRES_PASS';"
    sudo -u postgres createuser $PROJECT_USER
    sudo -u postgres createdb $PROJECT_USER -O $PROJECT_USER
    sudo -u postgres psql -c "ALTER ROLE \"$PROJECT_USER\" PASSWORD '$PROJECT_USER_PASS';"
    sudo -u postgres psql -c "ALTER ROLE \"$PROJECT_USER\" CREATEDB;"
    sudo -u postgres createuser rainloop
    sudo -u postgres createdb rainloop -O rainloop
    sudo -u postgres psql -c "ALTER ROLE \"rainloop\" PASSWORD 't8iHx2ErwBLL4mTw8DPa5b3JV0xNTgOz';"
    sudo -u postgres psql -c "ALTER ROLE \"$PROJECT_USER\" LOGIN;"

    echo "INFO: configuring gunicorn."
    cp $API_CONFIG_PATH/gunicorn/multitestery_api.service /etc/systemd/system
    cp $API_CONFIG_PATH/gunicorn/multitestery_api.socket /etc/systemd/system
    cp $API_CONFIG_PATH/gunicorn/multitestery_api.conf /etc/tmpfiles.d

    mkdir -p /var/log/gunicorn/api.multitestery.pl
    chown root:adm /var/log/gunicorn
    touch /var/log/gunicorn/api.multitestery.pl/access.log
    touch /var/log/gunicorn/api.multitestery.pl/error.log
    chown $PROJECT_USER:www-data -R /var/log/gunicorn/api.multitestery.pl

    systemctl enable multitestery_api.service
    systemctl enable multitestery_api.socket

    echo "INFO: applying migrations."
    DATABASE_URL="postgres://$PROJECT_USER:$PROJECT_USER_PASS@127.0.0.1/$PROJECT_USER"
    DJANGO_SETTINGS_MODULE=app.settings.prod SECRET_KEY="123" DATABASE_URL=$DATABASE_URL EMAIL_HOST="" EMAIL_USER="" EMAIL_PASS="" python3 $API_PATH/src/manage.py migrate

    echo "INFO: copying api source code and static files."
    mkdir /var/www/api.multitestery.pl
    cp -r $API_PATH/src/* /var/www/api.multitestery.pl
    chown $PROJECT_USER:www-data -R /var/www/api.multitestery.pl

    cp -r $API_PATH/static /var/www/cdn.multitestery.pl
    chown -R www-data:www-data /var/www/cdn.multitestery.pl/static

    if [ -e "/var/www/api.multitestery.pl/.coveragerc" ]; then
        rm /var/www/api.multitestery.pl/.coveragerc
    fi

    echo "INFO: installing redis."
    apt install -y redis-server

    sed -i 's/supervised no/supervised systemd/g' /etc/redis/redis.conf
    sed -i 's/bind 127.0.0.1 ::1/bind 127.0.0.1/g' /etc/redis/redis.conf

    systemctl enable redis-server
    
    echo "INFO: api config completed."
else
    echo "INFO: $API_PATH doesn't exist, skipping."
fi

if [ -e $WEB_PATH ]; then
    echo "INFO: installing nodejs."
    apt install -y npm

    echo "INFO: configuring nginx."
    cp $WEB_CONFIG_PATH/nginx/multitestery.pl.conf /etc/nginx/sites-available
    ln -sf /etc/nginx/sites-available/multitestery.pl.conf /etc/nginx/sites-enabled/multitestery.pl.conf

    echo "INFO: configuring node daemon."
    cp $WEB_CONFIG_PATH/node/multitestery_web.service /etc/systemd/system
    cp $WEB_CONFIG_PATH/node/multitestery_web.socket /etc/systemd/system
    cp $WEB_CONFIG_PATH/node/multitestery_web.conf /etc/tmpfiles.d

    mkdir -p /var/log/node/multitestery.pl
    chown root:adm /var/log/node
    touch /var/log/node/multitestery.pl/info.log
    touch /var/log/node/multitestery.pl/error.log
    chown $PROJECT_USER:www-data -R /var/log/node/multitestery.pl

    systemctl enable multitestery_web.service
    systemctl enable multitestery_web.socket

    echo "INFO: building front source code."
    cd $WEB_PATH
    npm install
    npm run build:all

    echo "INFO: copying front source code and static files."
    mkdir /var/www/multitestery.pl
    cp -r $WEB_PATH/* /var/www/multitestery.pl
    chown -R $PROJECT_USER:www-data /var/www/multitestery.pl
    chown -R www-data:www-data /var/www/multitestery.pl/public

    echo "INFO: web config completed."
else
    echo "INFO: $WEB_PATH doesn't exist, skipping."
fi

echo "INFO: installing mail server."
apt install -y postfix dovecot-imapd php7.3 php7.3-fpm php7.3-curl php7.3-xml php7.3-pgsql
systemctl enable postfix
systemctl enable dovecot
systemctl enable php7.3-fpm

echo "INFO: configuring mail server."
groupadd -g 5000 vmail
useradd -u 5000 -g vmail -s /bin/bash vmail

cp -r $MAIL_CONFIG_PATH/postfix/* /etc/postfix
cp -r $MAIL_CONFIG_PATH/dovecot/* /etc/dovecot

cd /etc/postfix/sasl
postmap sasl_passwd_gmail

cp $MAIL_CONFIG_PATH/other/aliases /etc
chown root:root /etc/aliases
newaliases

cp $MAIL_CONFIG_PATH/other/users /etc/dovecot
chown root:dovecot /etc/dovecot/users
chmod 640 /etc/dovecot/users

echo "INFO: configuring nginx."
cp $MAIL_CONFIG_PATH/nginx/mail.multitestery.pl.conf /etc/nginx/sites-available
ln -sf /etc/nginx/sites-available/mail.multitestery.pl.conf /etc/nginx/sites-enabled/mail.multitestery.pl.conf

echo "INFO: copying mail source code."
mkdir /var/www/mail.multitestery.pl
cp -r $MAIL_CONFIG_PATH/nginx/rainloop_webmail/* /var/www/mail.multitestery.pl

echo "INFO: updating mail source code file permissions."
cd /var/www/mail.multitestery.pl
find . -type d -exec chmod 755 {} \;
find . -type f -exec chmod 644 {} \;
chown www-data:www-data -R /var/www/mail.multitestery.pl
echo "INFO: mail server config completed."

echo "INFO: configuring default nginx server."
cp $OTHER_CONFIG_PATH/nginx/nginx.conf /etc/nginx
cp $OTHER_CONFIG_PATH/nginx/mime.types /etc/nginx
cp $OTHER_CONFIG_PATH/nginx/default.conf /etc/nginx/sites-available
ln -s /etc/nginx/sites-available/default.conf /etc/nginx/sites-enabled/default.conf

if [ -e "/etc/nginx/sites-available/default" ]; then
    rm /etc/nginx/sites-available/default
    rm /etc/nginx/sites-enabled/default
    rm -r /var/www/html
fi
echo "INFO: default nginx server config completed."
