#!/usr/bin/env bash
pushd . > /dev/null
SCRIPTS_PATH="${BASH_SOURCE[0]}"
if [[ -h "${SCRIPTS_PATH}" ]]; then
  while [[ -h "${SCRIPTS_PATH}" ]]; do cd "$(dirname "${SCRIPTS_PATH}")" || return;
  SCRIPTS_PATH="$(readlink "${SCRIPTS_PATH}")"; done
fi
cd "$(dirname "${SCRIPTS_PATH}")" > /dev/null || return
SCRIPTS_PATH="$(pwd)"
popd > /dev/null || return

ROOT_PATH="$SCRIPTS_PATH/.."

API_PATH="$ROOT_PATH/../multitestery_api"
API_CONFIG_PATH="$ROOT_PATH/api"

CDN_CONFIG_PATH="$ROOT_PATH/cdn"

WEB_PATH="$ROOT_PATH/../multitestery_web"
WEB_CONFIG_PATH="$ROOT_PATH/web"

MAIL_CONFIG_PATH="$ROOT_PATH/mail"

OTHER_CONFIG_PATH="$ROOT_PATH/other"

echo "INFO: updating system."
apt update -y
apt upgrade -y

if [ -e $API_PATH ]; then
    echo "INFO: updating nginx configuration."
    cp $API_CONFIG_PATH/nginx/api.multitestery.pl.conf /etc/nginx/sites-available
    ln -sf /etc/nginx/sites-available/api.multitestery.pl.conf /etc/nginx/sites-enabled/api.multitestery.pl.conf

    cp $CDN_CONFIG_PATH/nginx/cdn.multitestery.pl.conf /etc/nginx/sites-available
    ln -sf /etc/nginx/sites-available/cdn.multitestery.pl.conf /etc/nginx/sites-enabled/cdn.multitestery.pl.conf

    python3 $API_PATH/src/manage.py collectstatic

    echo "INFO: updating gunicorn configuration."
    cp $API_CONFIG_PATH/gunicorn/multitestery_api.service /etc/systemd/system
    cp $API_CONFIG_PATH/gunicorn/multitestery_api.socket /etc/systemd/system
    cp $API_CONFIG_PATH/gunicorn/multitestery_api.conf /etc/tmpfiles.d

    echo "INFO: applying migrations."
    DATABASE_URL="postgres://$PROJECT_USER:$PROJECT_USER_PASS@127.0.0.1/$PROJECT_USER"
    DJANGO_SETTINGS_MODULE=app.settings.prod SECRET_KEY="123" DATABASE_URL=$DATABASE_URL EMAIL_HOST="" EMAIL_USER="" EMAIL_PASS="" python3 $API_PATH/src/manage.py migrate

    echo "INFO: updating api source code and static files."
    rm -rf /var/www/api.multitestery.pl
    mkdir /var/www/api.multitestery.pl
    cp -r $API_PATH/src/* /var/www/api.multitestery.pl
    chown $PROJECT_USER:www-data -R /var/www/api.multitestery.pl

    cp -r $API_PATH/static /var/www/cdn.multitestery.pl
    chown -R www-data:www-data /var/www/cdn.multitestery.pl/static
    
    if [ -e "/var/www/api.multitestery.pl/.coveragerc" ]; then
        rm /var/www/api.multitestery.pl/.coveragerc
    fi

    echo "INFO: api update completed."
else
  echo "INFO: $API_PATH doesn't exist, skipping."
fi

if [ -e $WEB_PATH ]; then
    echo "INFO: updating npm."
    npm install -g npm

    echo "INFO: updating nginx configuration."
    cp $WEB_CONFIG_PATH/nginx/multitestery.pl.conf /etc/nginx/sites-available
    ln -sf /etc/nginx/sites-available/multitestery.pl.conf /etc/nginx/sites-enabled/multitestery.pl.conf

    echo "INFO: updating node daemon."
    cp $WEB_CONFIG_PATH/node/multitestery_web.service /etc/systemd/system
    cp $WEB_CONFIG_PATH/node/multitestery_web.socket /etc/systemd/system
    cp $WEB_CONFIG_PATH/node/multitestery_web.conf /etc/tmpfiles.d

    echo "INFO: building front source code."
    cd $WEB_PATH
    npm install
    npm run build:all

    echo "INFO: updating front source code and static files."
    rm -rf /var/www/multitestery.pl
    mkdir /var/www/multitestery.pl
    cp -r $WEB_PATH/* /var/www/multitestery.pl
    chown -R $PROJECT_USER:www-data /var/www/multitestery.pl
    chown -R www-data:www-data /var/www/multitestery.pl/public

  echo "INFO: web config completed."
else
  echo "INFO: $WEB_PATH doesn't exist, skipping."
fi

echo "INFO: updating mail server config."
cp -r $MAIL_CONFIG_PATH/postfix/* /etc/postfix
cp -r $MAIL_CONFIG_PATH/dovecot/* /etc/dovecot

cd /etc/postfix/sasl
postmap sasl_passwd_gmail

cp $MAIL_CONFIG_PATH/other/aliases /etc
chown root:root /etc/aliases
newaliases

cp $MAIL_CONFIG_PATH/other/users /etc/dovecot
chown root:dovecot /etc/dovecot/users
chmod 640 /etc/dovecot/users

echo "INFO: updating mail nginx configuration."
cp $MAIL_CONFIG_PATH/nginx/mail.multitestery.pl.conf /etc/nginx/sites-available
ln -sf /etc/nginx/sites-available/mail.multitestery.pl.conf /etc/nginx/sites-enabled/mail.multitestery.pl.conf

echo "INFO: updating mail source code."
rm -rf /var/www/mail.multitestery.pl
mkdir /var/www/mail.multitestery.pl
cp -r $MAIL_CONFIG_PATH/nginx/rainloop_webmail/* /var/www/mail.multitestery.pl

echo "INFO: updating mail source code file permissions."
cd /var/www/mail.multitestery.pl
find . -type d -exec chmod 755 {} \;
find . -type f -exec chmod 644 {} \;
chown www-data:www-data -R /var/www/mail.multitestery.pl
echo "INFO: mail server config completed."

echo "INFO: updating default nginx server configuration."
cp $OTHER_CONFIG_PATH/nginx/nginx.conf /etc/nginx
cp $OTHER_CONFIG_PATH/nginx/mime.types /etc/nginx
cp $OTHER_CONFIG_PATH/nginx/default.conf /etc/nginx/sites-available
echo "INFO: default nginx server config completed."
