from unittest.mock import patch

from PIL import Image
from django.test import TestCase
from rest_framework import status

from app.utils.cache import get_cache_key_for_instance
from app.utils.tests import get_image_path
from .factories import ProductFactory
from .models import Product
from .serializers import ProductSerializerV1
from .utils.tests import ProductAPITestCase
from .views import ProductViewSetV1


class ProductModelTestCase(TestCase):

    def setUp(self):
        self.product = ProductFactory(brand='Brand', model='Model', amount=100)

    def test_to_str_returns_id_brand_model_and_amount(self):
        self.assertEqual(str(self.product), f'#{self.product.id} Brand Model 100ml')


class ProductSerializerAPITestCase(ProductAPITestCase):

    def test_fields_layout_is_correct(self):
        serialized_product = ProductSerializerV1(self.not_featured_product).data
        fields = ['id', 'photo_small', 'photo_medium', 'photo_big', 'brand', 'model', 'category', 'amount', 'quantity',
                  'description', 'top_notes', 'heart_notes', 'base_notes', 'percent_off', 'is_featured', 'is_deleted']

        for field in fields:
            self.assertIn(field, serialized_product)

    def test_read_only_fields_are_correct(self):
        read_only_fields = ['id', 'photo_small', 'photo_medium', 'is_deleted']
        self.assertEqual(read_only_fields, ProductSerializerV1.Meta.read_only_fields)

    def test_create_photo_big_size_is_1024_by_1024(self):
        self.client.force_login(self.super_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='multipart')
        path = get_image_path(response.data['photo_big'])

        with Image.open(path) as image:
            width, height = image.size

        self.assertEqual(width, 1024)
        self.assertEqual(height, 1024)

    def test_create_photo_big_name_has_postfix__big(self):
        self.client.force_login(self.super_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='multipart')
        path = get_image_path(response.data['photo_big'])
        self.assertTrue(path.split('.')[0].endswith('_big'))

    def test_create_photo_big_format_is_webp(self):
        self.client.force_login(self.super_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='multipart')

        path = get_image_path(response.data['photo_big'])
        with Image.open(path) as pillow_image:
            form = pillow_image.format

        self.assertTrue(path.endswith('.webp'))
        self.assertEqual(form, 'WEBP')

    def test_create_photo_medium_size_is_768_by_768(self):
        self.client.force_login(self.super_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='multipart')
        path = get_image_path(response.data['photo_medium'])

        with Image.open(path) as image:
            width, height = image.size

        self.assertEqual(width, 768)
        self.assertEqual(height, 768)

    def test_create_photo_medium_name_has_postfix__medium(self):
        self.client.force_login(self.super_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='multipart')
        path = get_image_path(response.data['photo_medium'])
        self.assertTrue(path.split('.')[0].endswith('_medium'))

    def test_create_photo_medium_format_is_webp(self):
        self.client.force_login(self.super_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='multipart')

        path = get_image_path(response.data['photo_medium'])
        with Image.open(path) as pillow_image:
            form = pillow_image.format

        self.assertTrue(path.endswith('.webp'))
        self.assertEqual(form, 'WEBP')

    def test_create_photo_small_size_is_512_by_512(self):
        self.client.force_login(self.super_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='multipart')
        path = get_image_path(response.data['photo_small'])

        with Image.open(path) as image:
            width, height = image.size

        self.assertEqual(width, 512)
        self.assertEqual(height, 512)

    def test_create_photo_small_name_has_postfix__small(self):
        self.client.force_login(self.super_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='multipart')
        path = get_image_path(response.data['photo_small'])
        self.assertTrue(path.split('.')[0].endswith('_small'))

    def test_create_photo_small_format_is_webp(self):
        self.client.force_login(self.super_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='multipart')

        path = get_image_path(response.data['photo_small'])
        with Image.open(path) as pillow_image:
            form = pillow_image.format

        self.assertTrue(path.endswith('.webp'))
        self.assertEqual(form, 'WEBP')

    def test_update_photo_big_size_is_1024_by_1024(self):
        self.client.force_login(self.super_user)

        response = self.client.put(self.details_endpoint, self.dummy_payload, format='multipart')
        path = get_image_path(response.data['photo_big'])

        with Image.open(path) as image:
            width, height = image.size

        self.assertEqual(width, 1024)
        self.assertEqual(height, 1024)

    def test_update_photo_big_name_has_postfix__big(self):
        self.client.force_login(self.super_user)

        response = self.client.put(self.details_endpoint, self.dummy_payload, format='multipart')
        path = get_image_path(response.data['photo_big'])
        self.assertTrue(path.split('.')[0].endswith('_big'))

    def test_update_photo_big_format_is_webp(self):
        self.client.force_login(self.super_user)

        response = self.client.put(self.details_endpoint, self.dummy_payload, format='multipart')

        path = get_image_path(response.data['photo_big'])
        with Image.open(path) as pillow_image:
            form = pillow_image.format

        self.assertTrue(path.endswith('.webp'))
        self.assertEqual(form, 'WEBP')

    def test_update_photo_medium_size_is_768_by_768(self):
        self.client.force_login(self.super_user)

        response = self.client.put(self.details_endpoint, self.dummy_payload, format='multipart')
        path = get_image_path(response.data['photo_medium'])

        with Image.open(path) as image:
            width, height = image.size

        self.assertEqual(width, 768)
        self.assertEqual(height, 768)

    def test_update_photo_medium_name_has_postfix__medium(self):
        self.client.force_login(self.super_user)

        response = self.client.put(self.details_endpoint, self.dummy_payload, format='multipart')
        path = get_image_path(response.data['photo_medium'])
        self.assertTrue(path.split('.')[0].endswith('_medium'))

    def test_update_photo_medium_format_is_webp(self):
        self.client.force_login(self.super_user)

        response = self.client.put(self.details_endpoint, self.dummy_payload, format='multipart')

        path = get_image_path(response.data['photo_medium'])
        with Image.open(path) as pillow_image:
            form = pillow_image.format

        self.assertTrue(path.endswith('.webp'))
        self.assertEqual(form, 'WEBP')

    def test_update_photo_small_size_is_512_by_512(self):
        self.client.force_login(self.super_user)

        response = self.client.put(self.details_endpoint, self.dummy_payload, format='multipart')
        path = get_image_path(response.data['photo_small'])

        with Image.open(path) as image:
            width, height = image.size

        self.assertEqual(width, 512)
        self.assertEqual(height, 512)

    def test_update_photo_small_name_has_postfix__small(self):
        self.client.force_login(self.super_user)

        response = self.client.put(self.details_endpoint, self.dummy_payload, format='multipart')
        path = get_image_path(response.data['photo_small'])
        self.assertTrue(path.split('.')[0].endswith('_small'))

    def test_update_photo_small_format_is_webp(self):
        self.client.force_login(self.super_user)

        response = self.client.put(self.details_endpoint, self.dummy_payload, format='multipart')

        path = get_image_path(response.data['photo_small'])
        with Image.open(path) as pillow_image:
            form = pillow_image.format

        self.assertTrue(path.endswith('.webp'))
        self.assertEqual(form, 'WEBP')


class ProductViewAPITestCase(ProductAPITestCase):

    def test_create_as_anonymous_user_is_prohibited(self):
        response = self.client.post(self.base_endpoint, self.dummy_payload, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_as_ordinary_user_is_prohibited(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    @patch('django.core.cache.cache.delete')
    def test_create_products_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.post(self.base_endpoint, self.dummy_payload, format='multipart')
        mocked_method.assert_called_with(ProductViewSetV1.queryset_cache_key)

    # noinspection DuplicatedCode
    def test_updates_as_anonymous_user_are_prohibited(self):
        response = self.client.put(self.details_endpoint, self.dummy_payload, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.patch(self.details_endpoint, {'brand': 'Updated brand'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_updates_as_ordinary_user_are_prohibited(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.put(self.details_endpoint, self.dummy_payload, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        response = self.client.patch(self.details_endpoint, {'brand': 'Updated brand'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.put(self.details_endpoint, self.dummy_payload, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.put(self.details_endpoint, self.dummy_payload, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @patch('django.core.cache.cache.delete')
    def test_update_products_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.put(self.details_endpoint, self.dummy_payload, format='multipart')
        mocked_method.assert_called_with(ProductViewSetV1.queryset_cache_key)

    @patch('django.core.cache.cache.set')
    def test_update_product_instance_is_updated(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.put(self.details_endpoint, self.dummy_payload, format='multipart')
        mocked_method.assert_called_with(
            get_cache_key_for_instance(ProductViewSetV1.queryset_cache_key, self.details_pk),
            Product.objects.get(pk=self.details_pk))

    def test_partial_update_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.patch(self.details_endpoint, {'brand': 'Updated brand'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_partial_update_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.patch(self.details_endpoint, {'brand': 'Updated brand'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @patch('django.core.cache.cache.delete')
    def test_partial_update_products_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.patch(self.details_endpoint, {'brand': 'Updated brand'}, format='json')
        mocked_method.assert_called_with(ProductViewSetV1.queryset_cache_key)

    @patch('django.core.cache.cache.set')
    def test_partial_update_product_instance_is_updated(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.put(self.details_endpoint, {'brand': 'Updated brand'}, format='json')
        mocked_method.assert_called_with(
            get_cache_key_for_instance(ProductViewSetV1.queryset_cache_key, self.details_pk),
            Product.objects.get(pk=self.details_pk))

    def test_list_all_as_anonymous_user_is_permitted(self):
        response = self.client.get(self.base_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_all_as_ordinary_user_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.get(self.base_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_all_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.base_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_all_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.get(self.base_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    # These are assuming, that response is not being paginated, as there are not enough of products in setUp() method.
    def test_list_all_responds_with_all_not_deleted_products(self):
        response = self.client.get(self.base_endpoint)
        self.assertEqual(len(response.data['results']), Product.objects.filter(is_deleted=False).count())

    def test_list_not_featured_responds_only_with_not_featured_products(self):
        response = self.client.get(self.list_not_featured_endpoint)
        self.assertEqual(len(response.data['results']),
                         Product.objects.filter(is_deleted=False, is_featured=False).count())

        for result in response.data['results']:
            self.assertFalse(result['is_featured'])

    def test_list_featured_responds_only_with_featured_products(self):
        response = self.client.get(self.list_featured_endpoint)
        self.assertEqual(len(response.data['results']),
                         Product.objects.filter(is_deleted=False, is_featured=True).count())

        for result in response.data['results']:
            self.assertTrue(result['is_featured'])

    # noinspection DuplicatedCode
    def test_list_category_male_responds_only_with_male_products(self):
        response = self.client.get(self.list_category_male_endpoint)
        self.assertEqual(len(response.data['results']),
                         Product.objects.filter(is_deleted=False, category=Product.MALE).count())

        for result in response.data['results']:
            self.assertEqual(result['category'], Product.MALE)

    # noinspection DuplicatedCode
    def test_list_category_female_responds_only_with_female_products(self):
        response = self.client.get(self.list_category_female_endpoint)
        self.assertEqual(len(response.data['results']),
                         Product.objects.filter(is_deleted=False, category=Product.FEMALE).count())

        for result in response.data['results']:
            self.assertEqual(result['category'], Product.FEMALE)

    # noinspection DuplicatedCode
    def test_list_category_unisex_responds_only_with_unisex_products(self):
        response = self.client.get(self.list_category_unisex_endpoint)
        self.assertEqual(len(response.data['results']),
                         Product.objects.filter(is_deleted=False, category=Product.UNISEX).count())

        for result in response.data['results']:
            self.assertEqual(result['category'], Product.UNISEX)

    def test_list_searched_responds_only_with_searched_products(self):
        brand = 'Brand to test search'

        self.client.force_login(self.super_user)
        self.client.patch(self.details_endpoint, {'brand': brand})
        self.client.logout()

        response = self.client.get(self._get_list_searched_endpoint(brand))
        self.assertEqual(len(response.data['results']), 1)
        self.assertEqual(response.data['results'][0]['brand'], brand)

    def test_list_not_on_sale_responds_only_with_products_not_on_sale(self):
        response = self.client.get(self.list_not_on_sale_endpoint)
        self.assertEqual(len(response.data['results']), Product.objects.filter(is_deleted=False, percent_off=0).count())

        for result in response.data['results']:
            self.assertEqual(result['percent_off'], 0)

    def test_list_on_sale_responds_only_with_products_on_sale(self):
        response = self.client.get(self.list_on_sale_endpoint)
        self.assertEqual(len(response.data['results']),
                         Product.objects.filter(is_deleted=False, percent_off__gt=0).count())

        for result in response.data['results']:
            self.assertGreater(result['percent_off'], 0)

    def test_list_not_available_responds_only_with_not_available_products(self):
        response = self.client.get(self.list_not_available_endpoint)
        self.assertEqual(len(response.data['results']), Product.objects.filter(is_deleted=False, quantity=0).count())

        for result in response.data['results']:
            self.assertEqual(result['quantity'], 0)

    def test_list_available_responds_only_with_available_products(self):
        response = self.client.get(self.list_available_endpoint)
        self.assertEqual(len(response.data['results']),
                         Product.objects.filter(is_deleted=False, quantity__gt=0).count())

        for result in response.data['results']:
            self.assertGreater(result['quantity'], 0)

    def test_action_all_as_anonymous_user_is_permitted(self):
        response = self.client.get(self.action_all_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_action_all_as_ordinary_user_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.get(self.action_all_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_action_all_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.action_all_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_action_all_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.get(self.action_all_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_action_all_response_is_not_paginated(self):
        response = self.client.get(self.action_all_endpoint)
        self.assertNotIn('count', response.data)
        self.assertNotIn('next', response.data)
        self.assertNotIn('previous', response.data)
        self.assertEqual(len(response.data['results']), Product.objects.filter(is_deleted=False).count())

    def test_details_as_anonymous_user_is_permitted(self):
        response = self.client.get(self.details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_details_as_ordinary_user_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.get(self.details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_details_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_details_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.get(self.details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_details_responds_with_404_on_deleted_product(self):
        response = self.client.get(f'{self.base_endpoint}{self.deleted_product.pk}/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_as_anonymous_user_is_not_permitted(self):
        response = self.client.delete(self.details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_as_ordinary_user_is_not_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.delete(self.details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.delete(self.details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.delete(self.details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_makes_object_property_is_featured_false(self):
        self.client.force_login(self.staff_user)
        self.client.delete(self.details_endpoint)

        product = Product.objects.get(pk=self.details_pk)
        self.assertFalse(product.is_featured)

    def test_delete_makes_object_property_is_deleted_true(self):
        self.client.force_login(self.staff_user)
        self.client.delete(self.details_endpoint)

        product = Product.objects.get(pk=self.details_pk)
        self.assertTrue(product.is_deleted)

    @patch('django.core.cache.cache.delete')
    def test_delete_products_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.delete(self.details_endpoint)
        mocked_method.assert_any_call(ProductViewSetV1.queryset_cache_key)

    @patch('django.core.cache.cache.delete')
    def test_details_product_instance_is_deleted(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.delete(self.details_endpoint)
        mocked_method.assert_any_call(
            get_cache_key_for_instance(ProductViewSetV1.queryset_cache_key, self.details_pk))
