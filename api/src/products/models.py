from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models


class Product(models.Model):
    MALE = 0
    FEMALE = 1
    UNISEX = 2
    CATEGORY_CHOICES = [
        (MALE, 'Dla mężczyzn'),
        (FEMALE, 'Dla kobiet'),
        (UNISEX, 'Unisex'),
    ]
    photo_small = models.ImageField(upload_to='products')
    photo_medium = models.ImageField(upload_to='products')
    photo_big = models.ImageField(upload_to='products')
    brand = models.CharField(max_length=25)
    model = models.CharField(max_length=50)
    category = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(MALE), MaxValueValidator(UNISEX)], choices=CATEGORY_CHOICES, default=UNISEX)
    unit_price = models.PositiveIntegerField()
    amount = models.PositiveSmallIntegerField()
    quantity = models.PositiveSmallIntegerField()
    description = models.TextField(blank=True)
    top_notes = models.CharField(max_length=150, blank=True)
    heart_notes = models.CharField(max_length=150, blank=True)
    base_notes = models.CharField(max_length=150, blank=True)
    percent_off = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(0), MaxValueValidator(100)], default=0)
    is_featured = models.BooleanField(default=False)
    is_deleted = models.BooleanField(default=False)

    def __str__(self):
        return f'#{self.id} {self.brand} {self.model} {self.amount}ml'
