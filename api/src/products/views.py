from django.core.cache import cache
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.filters import SearchFilter

from app.utils.cache import get_cache_key_for_instance
from app.utils.viewsets import CachingModelViewSet
from .apps import ProductsConfig
from .models import Product
from .permissions import CreateUpdateDeleteIfStaff
from .serializers import ProductSerializerV1


class ProductViewSetV1(CachingModelViewSet):
    queryset = Product.objects.filter(is_deleted=False).order_by('id')
    queryset_cache_key = ProductsConfig.name
    serializer_class = ProductSerializerV1
    permission_classes = [CreateUpdateDeleteIfStaff]
    filter_backends = [SearchFilter]
    search_fields = ['brand', 'model', 'amount', 'description']

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        featured = request.GET.get('featured')
        if featured == '0':
            queryset = self.filter_queryset(queryset.filter(is_deleted=False, is_featured=False)).order_by('id')
        elif featured == '1':
            queryset = self.filter_queryset(queryset.filter(is_deleted=False, is_featured=True)).order_by('id')

        category = request.GET.get('category')
        if category:
            queryset = self.filter_queryset(queryset.filter(is_deleted=False, category=category).order_by('id'))

        on_sale = request.GET.get('on_sale')
        if on_sale == '0':
            queryset = self.filter_queryset(queryset.filter(is_deleted=False, percent_off=0).order_by('id'))
        elif on_sale == '1':
            queryset = self.filter_queryset(queryset.filter(is_deleted=False, percent_off__gt=0).order_by('id'))

        available = request.GET.get('available')
        if available == '0':
            queryset = self.filter_queryset(queryset.filter(is_deleted=False, quantity=0)).order_by('id')
        elif available == '1':
            queryset = self.filter_queryset(queryset.filter(is_deleted=False, quantity__gt=0)).order_by('id')

        page = self.paginate_queryset(queryset)
        serializer = self.get_serializer(page, many=True)
        return self.get_paginated_response(serializer.data)

    @action(detail=False, methods=['GET'])
    def all(self, request):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        return Response({'results': serializer.data})

    def perform_destroy(self, instance):
        instance.is_featured = False
        instance.is_deleted = True
        instance.save()

        cache.delete(get_cache_key_for_instance(self.queryset_cache_key, instance.pk))
        cache.delete(self.queryset_cache_key)
