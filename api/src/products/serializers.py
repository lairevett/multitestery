from rest_framework import serializers

from .models import Product
from .utils.serializers import get_adjusted_images


class ProductSerializerV1(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'
        read_only_fields = ['id', 'photo_small', 'photo_medium', 'is_deleted']

    def create(self, validated_data):
        adjusted_images = get_adjusted_images(validated_data['photo_big'])
        validated_data.update(adjusted_images)

        return super().create(validated_data)

    def update(self, instance, validated_data):
        photo_big = validated_data.get('photo_big')
        if photo_big:
            adjusted_images = get_adjusted_images(photo_big)
            validated_data.update(adjusted_images)

        return super().update(instance, validated_data)
