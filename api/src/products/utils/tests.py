from tempfile import TemporaryDirectory

from django.test import override_settings

from app.utils.tests import get_uploaded_dummy_image
from products.factories import ProductFactory
from products.models import Product
from users.utils.tests import APITestCaseWithUsers


@override_settings(MEDIA_ROOT=TemporaryDirectory(prefix='multitestery_test_media').name)
class ProductAPITestCase(APITestCaseWithUsers):
    base_endpoint = '/v1/products/'
    action_all_endpoint = f'{base_endpoint}all/'
    list_not_featured_endpoint = f'{base_endpoint}?featured=0'
    list_featured_endpoint = f'{base_endpoint}?featured=1'
    list_category_male_endpoint = f'{base_endpoint}?category={Product.MALE}'
    list_category_female_endpoint = f'{base_endpoint}?category={Product.FEMALE}'
    list_category_unisex_endpoint = f'{base_endpoint}?category={Product.UNISEX}'
    list_not_on_sale_endpoint = f'{base_endpoint}?on_sale=0'
    list_on_sale_endpoint = f'{base_endpoint}?on_sale=1'
    list_not_available_endpoint = f'{base_endpoint}?available=0'
    list_available_endpoint = f'{base_endpoint}?available=1'

    def _get_list_searched_endpoint(self, keyword):
        return f'{self.base_endpoint}?search={keyword}'

    def setUp(self):
        super().setUp()

        self.not_featured_product = ProductFactory(is_featured=False)
        self.featured_product = ProductFactory(is_featured=True)
        self.category_male_product = ProductFactory(category=Product.MALE)
        self.category_female_product = ProductFactory(category=Product.FEMALE)
        self.category_unisex_product = ProductFactory(category=Product.UNISEX)
        self.not_on_sale_product = ProductFactory(percent_off=0)
        self.on_sale_product = ProductFactory(percent_off=10)
        self.not_available_product = ProductFactory(quantity=0)
        self.available_product = ProductFactory(quantity=1)
        self.deleted_product = ProductFactory(is_deleted=True)

        self.details_pk = self.featured_product.pk
        self.details_endpoint = f'{self.base_endpoint}{self.details_pk}/'

        self.dummy_payload = {
            'photo_big': get_uploaded_dummy_image(),
            'brand': 'Dummy brand',
            'model': 'Dummy model',
            'category': Product.UNISEX,
            'unit_price': 10000,
            'amount': 100,
            'quantity': 1,
            'description': '',
            'top_notes': '',
            'heart_notes': '',
            'base_notes': '',
            'percent_off': 0,
            'is_featured': False
        }
