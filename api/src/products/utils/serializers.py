import sys
from io import BytesIO
from uuid import uuid4

from PIL import Image
from django.core.files.uploadedfile import InMemoryUploadedFile


def get_adjusted_images(image):
    """
    Changes the size of uploaded image to 1024x1024, 768x768 and 512x512, formats images to .webp and saves
    them accordingly to 'photo_big', 'photo_medium' and 'photo_small' dict fields, then returns the dict.
    """
    width_and_height = 1024
    files = [BytesIO(), BytesIO(), BytesIO()]
    name_postfixes = ['big', 'medium', 'small']
    field_names = ['photo_big', 'photo_medium', 'photo_small']
    adjusted_images = {}

    with Image.open(image) as pillow_image:
        for file, name_postfix, field_name in zip(files, name_postfixes, field_names):
            pillow_image = pillow_image.resize((width_and_height, width_and_height), Image.BILINEAR)
            pillow_image.save(file, format='WebP', quality=80)

            name = f'{uuid4()}_{name_postfix}.webp'
            adjusted_image = InMemoryUploadedFile(file, 'ImageField', name, 'image/webp', sys.getsizeof(file), None)
            adjusted_images.update({field_name: adjusted_image})

            width_and_height -= 256

    return adjusted_images
