import factory

from app.utils.tests import get_dummy_image
from .models import Product


class ProductFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Product

    brand = 'Default brand'
    model = 'Default model'
    category = Product.UNISEX
    unit_price = 10000
    amount = 100
    quantity = 1
    description = ''
    top_notes = ''
    heart_notes = ''
    base_notes = ''
    percent_off = 0
    is_featured = False
    is_deleted = False

    @factory.lazy_attribute
    def photo_big(self):
        dummy_image = get_dummy_image()
        return dummy_image.name
