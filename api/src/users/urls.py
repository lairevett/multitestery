from django.urls import path, include

from .views import ForwardingAddressViewSetV1, OrderViewSetV1

urlpatterns = [
    path('v1/', include([
        path('users/', include([
            path('forwarding-addresses/', ForwardingAddressViewSetV1.as_view({'post': 'create'})),
            path('<int:user_pk>/forwarding-addresses/', ForwardingAddressViewSetV1.as_view({'get': 'list'})),
            path('<int:user_pk>/forwarding-addresses/<int:pk>/', ForwardingAddressViewSetV1.as_view(
                {'get': 'retrieve', 'delete': 'destroy'}
            )),
            path('orders/', OrderViewSetV1.as_view({'post': 'create', 'get': 'list'})),
            path('orders/<int:pk>/', OrderViewSetV1.as_view({'get': 'retrieve'})),
            path('orders/<int:pk>/update-status/', OrderViewSetV1.as_view({'post': 'update_status'})),
            path('<int:user_pk>/orders/', OrderViewSetV1.as_view({'get': 'list'})),
            path('<int:user_pk>/orders/<int:pk>/', OrderViewSetV1.as_view({'get': 'retrieve'})),
            path('<int:user_pk>/orders/<int:pk>/cancel/', OrderViewSetV1.as_view({'post': 'cancel'})),
        ]))
    ]))
]
