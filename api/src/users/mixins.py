from users.models import Order
from users.utils.order.pagination import OrdersPagination
from users.utils.order.views import OrdersCachingAPIView


class CanceledPaidSentOrdersListMixin(OrdersCachingAPIView):
    queryset = Order.objects.all()
    pagination_class = OrdersPagination

    def get_queryset(self):
        queryset = super().get_queryset()

        if not self.kwargs.get('pk'):
            exclude_indices = []

            canceled = self.request.GET.get('canceled')
            if canceled == '0':
                exclude_indices = [entry.id for entry in queryset if entry.status == Order.CANCELED]
            elif canceled == '1':
                exclude_indices = [entry.id for entry in queryset if entry.status != Order.CANCELED]

            paid = self.request.GET.get('paid')
            if paid == '0':
                # Not paid doesn't include canceled orders, it responds with orders with status Order.PLACED.
                exclude_indices = [entry.id for entry in queryset if entry.status != Order.PLACED]
            elif paid == '1':
                exclude_indices = [entry.id for entry in queryset if entry.status != Order.PAID]

            sent = self.request.GET.get('sent')
            if sent == '0':
                exclude_indices = [entry.id for entry in queryset if entry.status == Order.SENT]
            if sent == '1':
                exclude_indices = [entry.id for entry in queryset if entry.status != Order.SENT]

            queryset = queryset.exclude(id__in=exclude_indices).order_by(
                '-id' if self.kwargs.get('user_id') else 'id')  # Sort orders by oldest in admin panel.

        return queryset
