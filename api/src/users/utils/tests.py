from rest_framework.test import APITestCase

from users.factories.forwarding_address import ForwardingAddressWithAptNumberFactory
from users.factories.user import OrdinaryUserFactory, StaffUserFactory, SuperUserFactory


class APITestCaseWithUsers(APITestCase):

    def setUp(self):
        self.ordinary_user = OrdinaryUserFactory()
        self.ordinary_user_forwarding_address = ForwardingAddressWithAptNumberFactory(user=self.ordinary_user)

        self.staff_user = StaffUserFactory()
        self.staff_user_forwarding_address = ForwardingAddressWithAptNumberFactory(user=self.staff_user)

        self.super_user = SuperUserFactory()
        self.super_user_forwarding_address = ForwardingAddressWithAptNumberFactory(user=self.super_user)
