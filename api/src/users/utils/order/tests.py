from products.factories import ProductFactory
from users.factories.order import OrderFactory, OrderEntryFactory
from users.models import Order
from users.utils.tests import APITestCaseWithUsers


class OrderAPITestCase(APITestCaseWithUsers):
    base_endpoint = '/v1/users/orders/'
    list_not_canceled_endpoint = f'{base_endpoint}?canceled=0'
    list_canceled_endpoint = f'{base_endpoint}?canceled=1'
    list_not_paid_endpoint = f'{base_endpoint}?paid=0'
    list_paid_endpoint = f'{base_endpoint}?paid=1'
    list_not_sent_endpoint = f'{base_endpoint}?sent=0'
    list_sent_endpoint = f'{base_endpoint}?sent=1'

    def __init__(self, methodName):
        super().__init__(methodName)

        self.get_details_endpoint = lambda order_pk: f'{self.base_endpoint}{order_pk}/'
        self.get_list_user_endpoint = lambda user_pk: f'/v1/users/{user_pk}/orders/'
        self.get_details_user_endpoint = lambda user_pk, order_pk: f'{self.get_list_user_endpoint(user_pk)}{order_pk}/'
        self.get_update_status_endpoint = lambda order_pk: f'/v1/users/orders/{order_pk}/update-status/'
        self.get_cancel_endpoint = lambda user_pk, order_pk: f'/v1/users/{user_pk}/orders/{order_pk}/cancel/'

    def setUp(self):
        super().setUp()

        self.get_dummy_payload = lambda forwarding_address_pk: {
            'entries': [
                {'product': ProductFactory().pk, 'quantity': 1}
            ],
            'forwarding_address': forwarding_address_pk,
            'shipping': Order.CASH_UP_FRONT
        }

        # Create some dummy orders to test count.
        for order_status in [Order.CANCELED, Order.PLACED, Order.PAID, Order.SENT]:
            OrderFactory(status=order_status)

        self.sample_order = OrderFactory(user=self.ordinary_user,
                                         forwarding_address=self.ordinary_user_forwarding_address)
        OrderEntryFactory(order=self.sample_order)
