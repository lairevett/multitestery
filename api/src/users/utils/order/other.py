import math


def get_subtotal(order_entry):
    product = order_entry.get('product')
    unit_price = product.unit_price if product.percent_off == 0 else math.ceil(
        product.unit_price - product.unit_price * (product.percent_off / 100))

    return unit_price * order_entry.get('quantity', 1)
