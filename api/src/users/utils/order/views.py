from django.core.cache import cache
from rest_framework.viewsets import GenericViewSet

from app.utils.cache import get_cache_key_for_user, get_cache_key_for_instance
from users.apps import UsersConfig


class OrdersCachingAPIView(GenericViewSet):
    queryset_cache_key = f'{UsersConfig.name}_orders'

    def get_queryset(self):
        # If user_id is present, then it's the user browsing his private list of orders,
        # otherwise it's the admin browsing all orders.
        user_pk = self.kwargs.get('user_pk')
        cache_key = get_cache_key_for_user(self.queryset_cache_key, user_pk) if user_pk else self.queryset_cache_key

        queryset = cache.get(cache_key)
        if not queryset:
            queryset = super().get_queryset().filter(user__pk=user_pk) if user_pk else super().get_queryset()
            cache.set(cache_key, queryset)

        return queryset

    def get_object(self):
        cache_key = get_cache_key_for_instance(self.queryset_cache_key, self.kwargs['pk'])

        instance = cache.get(cache_key)
        if not instance:
            instance = super().get_object()
            cache.set(cache_key, instance)

        return instance
