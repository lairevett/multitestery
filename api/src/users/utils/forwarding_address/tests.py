from users.factories.forwarding_address import ForwardingAddressWithAptNumberFactory
from users.utils.tests import APITestCaseWithUsers


class ForwardingAddressAPITestCase(APITestCaseWithUsers):
    base_endpoint = '/v1/users/forwarding-addresses/'

    def __init__(self, methodName):
        super().__init__(methodName)

        self.get_list_endpoint = lambda user_pk: f'/v1/users/{user_pk}/forwarding-addresses/'

        self.dummy_payload = {
            'first_name': 'Dummy',
            'last_name': 'Name',
            'phone': '+48123456789',
            'street': 'Dummy Square',
            'home_number': '1B',
            'apt_number': 1,
            'city': 'Dummy City',
            'postal_code': '00-001'
        }

    def setUp(self):
        super().setUp()

        self.ordinary_user_forwarding_address = ForwardingAddressWithAptNumberFactory(user=self.ordinary_user)
        self.ordinary_user_deleted_forwarding_address = ForwardingAddressWithAptNumberFactory(user=self.ordinary_user,
                                                                                              is_deleted=True)
        self.staff_user_forwarding_address = ForwardingAddressWithAptNumberFactory(user=self.staff_user)
        self.super_user_forwarding_address = ForwardingAddressWithAptNumberFactory(user=self.super_user)

        self.ordinary_user_details_endpoint = \
            f'{self.get_list_endpoint(self.ordinary_user.pk)}{self.ordinary_user_forwarding_address.pk}/'

        self.staff_user_details_endpoint = \
            f'{self.get_list_endpoint(self.staff_user.pk)}{self.staff_user_forwarding_address.pk}/'

        self.super_user_details_endpoint = \
            f'{self.get_list_endpoint(self.super_user.pk)}{self.super_user_forwarding_address.pk}/'
