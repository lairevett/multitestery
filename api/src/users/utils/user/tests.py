from users.utils.tests import APITestCaseWithUsers


class UserAPITestCase(APITestCaseWithUsers):
    base_endpoint = '/v1/users/'

    def setUp(self):
        super().setUp()

        self.get_details_endpoint = lambda user_pk: f'{self.base_endpoint}{user_pk}/'

        self.dummy_payload = {
            'username': 'johndoe',
            'email': 'johndoe@example.com',
            'wants_newsletters': True,
            'password': 'p@ssword123',
            'is_staff': False,
            'is_superuser': False
        }
