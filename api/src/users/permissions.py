from rest_framework import permissions


class IsOwnerOrStaff(permissions.BasePermission):

    def has_permission(self, request, view):
        return view.kwargs.get('user_pk') == request.user.pk or request.user.is_staff

    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view)


class RejectUpdateDeleteIfOwner(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method in ('PUT', 'PATCH', 'DELETE'):
            return request.user != obj

        return True


class RejectUpdateDeleteIfInstanceSuperuser(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method in ('PUT', 'PATCH', 'DELETE'):
            return not obj.is_superuser

        return True
