from django.contrib import admin
from .models import *


admin.site.register([User, ForwardingAddress, Order, OrderEntry])
