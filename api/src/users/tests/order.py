from unittest.mock import patch

from django.test import TestCase
from rest_framework import status

from app.utils.cache import get_cache_key_for_user, get_cache_key_for_instance
from products.factories import ProductFactory
from users.factories.forwarding_address import ForwardingAddressWithAptNumberFactory
from users.factories.order import OrderFactory, OrderEntryFactory
from users.factories.user import OrdinaryUserFactory
from users.models import Order, Product
from users.utils.order.tests import OrderAPITestCase
from users.views import OrderViewSetV1


class OrderModelTestCase(TestCase):
    product_unit_price = 10000
    order_entry_quantity = 3

    def setUp(self):
        self.order = OrderFactory()
        self.product = ProductFactory(unit_price=self.product_unit_price)
        OrderEntryFactory(order=self.order, product=self.product, quantity=self.order_entry_quantity)

    def test_str(self):
        self.assertEqual(str(self.order), f'Zamówienie #{self.order.id}')

    def test__get_total(self):
        total = self.product_unit_price * self.order_entry_quantity
        self.assertEqual(self.order.total, total)


class OrderEntryModelTestCase(TestCase):
    product_brand = 'Brand'
    product_model = 'Model'
    product_amount = 100

    def setUp(self):
        self.product = ProductFactory(brand=self.product_brand, model=self.product_model, amount=self.product_amount)
        self.order_entry = OrderEntryFactory(product=self.product)

    def test_str(self):
        product_to_str = f'#{self.product.id} {self.product_brand} {self.product_model} {self.product_amount}ml'
        self.assertEqual(str(self.order_entry), product_to_str)


class OrderSerializerAPITestCase(OrderAPITestCase):

    def test_fields_layout_is_correct(self):
        fields = ['id', 'name', 'entries', 'forwarding_address', 'total', 'shipping', 'status', 'created_at',
                  'updated_at']
        self.client.force_login(self.staff_user)

        response = self.client.get(self.get_details_endpoint(self.sample_order.pk))
        self.assertEqual(list(response.data.keys()), fields)

    def test_if_user_can_create_order_only_using_one_of_his_forwarding_addresses(self):
        # API responds with 'forwarding_address': '{some error string}' on error,
        # so this unit test only checks if there is 'forwarding_address' key present in response.
        user = OrdinaryUserFactory()
        user_forwarding_address = ForwardingAddressWithAptNumberFactory(user=user)

        self.client.force_login(user)
        response = self.client.post(self.base_endpoint, {
            'forwarding_address': user_forwarding_address.pk,
            'shipping': 1
        }, format='json')
        self.assertNotIn('forwarding_address', response.data)

        response = self.client.post(self.base_endpoint, {
            'forwarding_address': self.ordinary_user_forwarding_address.pk,
            'shipping': 1
        }, format='json')
        self.assertIn('forwarding_address', response.data)


class OrderEntrySerializerAPITestCase(OrderAPITestCase):

    def test_fields_layout_is_correct(self):
        fields = ['id', 'product', 'quantity', 'subtotal']
        self.client.force_login(self.staff_user)

        response = self.client.get(self.get_details_endpoint(self.sample_order.pk))
        self.assertEqual(list(response.data['entries'][0].keys()), fields)


class OrderViewAPITestCase(OrderAPITestCase):

    def test_create_as_anonymous_user_is_prohibited(self):
        response = self.client.post(self.base_endpoint,
                                    self.get_dummy_payload(self.ordinary_user_forwarding_address.pk), format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_as_ordinary_user_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.post(self.base_endpoint,
                                    self.get_dummy_payload(self.ordinary_user_forwarding_address.pk), format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.post(self.base_endpoint, self.get_dummy_payload(self.staff_user_forwarding_address.pk),
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.post(self.base_endpoint, self.get_dummy_payload(self.super_user_forwarding_address.pk),
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    @patch('django.core.cache.cache.delete')
    def test_create_orders_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.post(self.base_endpoint, self.get_dummy_payload(self.ordinary_user_forwarding_address.pk),
                         format='json')
        mocked_method.assert_any_call(OrderViewSetV1.queryset_cache_key)

    @patch('django.core.cache.cache.delete')
    def test_create_user_orders_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.post(self.base_endpoint, self.get_dummy_payload(self.ordinary_user_forwarding_address.pk),
                         format='json')
        mocked_method.assert_any_call(get_cache_key_for_user(OrderViewSetV1.queryset_cache_key, self.ordinary_user.pk))

    def test_update_status_as_anonymous_user_is_prohibited(self):
        response = self.client.post(self.get_update_status_endpoint(self.sample_order.pk), {'new_status': Order.PAID},
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_status_as_ordinary_user_is_prohibited(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.post(self.get_update_status_endpoint(self.sample_order.pk), {'new_status': Order.PAID},
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_status_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.post(self.get_update_status_endpoint(self.sample_order.pk), {'new_status': Order.PAID},
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_status_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.post(self.get_update_status_endpoint(self.sample_order.pk), {'new_status': Order.PAID},
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_status_to_the_same_state_responds_with_bad_request(self):
        self.client.force_login(self.staff_user)

        response = self.client.post(self.get_update_status_endpoint(self.sample_order.pk),
                                    {'new_status': self.sample_order.status}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    # noinspection DuplicatedCode
    def test_update_status_to_sent_with_deleted_product_to_sent_does_not_change_quantity_of_the_product(self):
        self.client.force_login(self.staff_user)

        qty = 1
        deleted_product = ProductFactory(is_deleted=True, quantity=qty)
        OrderEntryFactory(order=self.sample_order, product=deleted_product, quantity=1)

        self.client.post(self.get_update_status_endpoint(self.sample_order.pk), {'new_status': Order.SENT},
                         format='json')
        self.assertEqual(Product.objects.get(pk=deleted_product.pk).quantity, qty)

    def test_update_status_to_sent_with_insufficient_product_quantity_responds_with_bad_request(self):
        self.client.force_login(self.staff_user)

        product = ProductFactory(quantity=10)
        OrderEntryFactory(order=self.sample_order, product=product, quantity=product.quantity + 1)

        response = self.client.post(self.get_update_status_endpoint(self.sample_order.pk), {'new_status': Order.SENT},
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Product.objects.get(pk=product.pk).quantity, product.quantity)

    # noinspection DuplicatedCode
    def test_update_status_to_sent_decreases_quantity_of_products(self):
        self.client.force_login(self.staff_user)

        qty_of_product = 10
        product = ProductFactory(quantity=qty_of_product)

        ordered_qty = 4
        OrderEntryFactory(order=self.sample_order, product=product, quantity=ordered_qty)

        self.client.post(self.get_update_status_endpoint(self.sample_order.pk), {'new_status': Order.SENT},
                         format='json')
        self.assertEqual(Product.objects.get(pk=product.pk).quantity, qty_of_product - ordered_qty)

    # noinspection DuplicatedCode
    def test_update_status_from_sent_with_deleted_product_to_sent_does_not_change_quantity_of_the_product(self):
        self.sample_order.status = Order.SENT
        self.sample_order.save()
        self.client.force_login(self.staff_user)

        qty = 1
        deleted_product = ProductFactory(is_deleted=True, quantity=qty)
        OrderEntryFactory(order=self.sample_order, product=deleted_product, quantity=1)

        self.client.post(self.get_update_status_endpoint(self.sample_order.pk), {'new_status': Order.CANCELED},
                         format='json')
        self.assertEqual(Product.objects.get(pk=deleted_product.pk).quantity, qty)

    # noinspection DuplicatedCode
    def test_update_status_from_sent_increases_quantity_of_products(self):
        self.sample_order.status = Order.SENT
        self.sample_order.save()
        self.client.force_login(self.staff_user)

        qty_of_product = 6
        product = ProductFactory(quantity=qty_of_product)

        ordered_qty = 4
        OrderEntryFactory(order=self.sample_order, product=product, quantity=ordered_qty)

        self.client.post(self.get_update_status_endpoint(self.sample_order.pk), {'new_status': Order.CANCELED},
                         format='json')
        self.assertEqual(Product.objects.get(pk=product.pk).quantity, qty_of_product + ordered_qty)

    def test_update_status_changes_order_status_accordingly(self):
        self.client.force_login(self.staff_user)

        order_statuses = [Order.CANCELED, Order.PLACED, Order.PAID, Order.SENT]
        for order_status in order_statuses:
            response = self.client.post(self.get_update_status_endpoint(self.sample_order.pk),
                                        {'new_status': order_status}, format='json')
            self.assertDictEqual(response.data, {'status': order_status})
            self.assertEqual(Order.objects.get(pk=self.sample_order.pk).status, order_status)

    @patch('django.core.cache.cache.delete')
    def test_update_status_orders_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.staff_user)

        self.client.post(self.get_update_status_endpoint(self.sample_order.pk), {'new_status': Order.PAID},
                         format='json')
        mocked_method.assert_any_call(OrderViewSetV1.queryset_cache_key)

    @patch('django.core.cache.cache.delete')
    def test_update_status_user_orders_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.staff_user)

        self.client.post(self.get_update_status_endpoint(self.sample_order.pk), {'new_status': Order.PAID},
                         format='json')
        mocked_method.assert_any_call(get_cache_key_for_user(OrderViewSetV1.queryset_cache_key, self.ordinary_user.pk))

    @patch('django.core.cache.cache.set')
    def test_update_status_user_order_instance_is_updated(self, mocked_method):
        self.client.force_login(self.staff_user)

        self.client.post(self.get_update_status_endpoint(self.sample_order.pk), {'new_status': Order.PAID},
                         format='json')
        mocked_method.assert_called_with(
            get_cache_key_for_instance(OrderViewSetV1.queryset_cache_key, self.sample_order.pk),
            Order.objects.get(pk=self.sample_order.pk))

    def test_cancel_as_anonymous_user_is_prohibited(self):
        response = self.client.post(self.get_cancel_endpoint(self.ordinary_user.pk, self.sample_order.pk))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_cancel_as_ordinary_user_not_owner_is_prohibited(self):
        user = OrdinaryUserFactory()
        self.client.force_login(user)

        response = self.client.post(self.get_cancel_endpoint(self.ordinary_user.pk, self.sample_order.pk))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_cancel_as_ordinary_user_owner_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.post(self.get_cancel_endpoint(self.ordinary_user.pk, self.sample_order.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_cancel_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.post(self.get_cancel_endpoint(self.ordinary_user.pk, self.sample_order.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_cancel_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.post(self.get_cancel_endpoint(self.ordinary_user.pk, self.sample_order.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_cancel_on_already_canceled_or_paid_or_sent_order_responds_with_bad_request(self):
        self.client.force_login(self.ordinary_user)

        order_statuses = [Order.CANCELED, Order.PAID, Order.SENT]
        for order_status in order_statuses:
            self.sample_order.status = order_status
            self.sample_order.save()

            response = self.client.post(self.get_cancel_endpoint(self.ordinary_user.pk, self.sample_order.pk))
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_cancel_updates_status_of_order_accordingly(self):
        self.client.force_login(self.ordinary_user)

        self.client.post(self.get_cancel_endpoint(self.ordinary_user.pk, self.sample_order.pk))
        self.assertEqual(Order.objects.get(pk=self.sample_order.pk).status, Order.CANCELED)

    @patch('django.core.cache.cache.delete')
    def test_cancel_orders_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.post(self.get_cancel_endpoint(self.ordinary_user.pk, self.sample_order.pk))
        mocked_method.assert_any_call(OrderViewSetV1.queryset_cache_key)

    @patch('django.core.cache.cache.delete')
    def test_cancel_user_orders_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.post(self.get_cancel_endpoint(self.ordinary_user.pk, self.sample_order.pk))
        mocked_method.assert_any_call(get_cache_key_for_user(OrderViewSetV1.queryset_cache_key, self.ordinary_user.pk))

    @patch('django.core.cache.cache.set')
    def test_cancel_user_order_instance_cache_is_updated(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.post(self.get_cancel_endpoint(self.ordinary_user.pk, self.sample_order.pk))
        mocked_method.assert_any_call(
            get_cache_key_for_instance(OrderViewSetV1.queryset_cache_key, self.sample_order.pk),
            Order.objects.get(pk=self.sample_order.pk))

    def test_list_as_anonymous_user_is_prohibited(self):
        response = self.client.get(self.base_endpoint)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_list_as_ordinary_user_is_prohibited(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.get(self.base_endpoint)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.base_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.get(self.base_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_responds_with_all_orders(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.base_endpoint)
        self.assertEqual(response.data['count'], Order.objects.count())

    def test_list_not_canceled_responds_with_all_not_canceled_orders(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.list_not_canceled_endpoint)
        self.assertEqual(len(response.data['results']), Order.objects.exclude(status=Order.CANCELED).count())

    def test_list_canceled_responds_with_all_canceled_orders(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.list_canceled_endpoint)
        self.assertEqual(len(response.data['results']), Order.objects.filter(status=Order.CANCELED).count())

    def test_list_not_paid_responds_with_all_placed_orders(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.list_not_paid_endpoint)
        self.assertEqual(len(response.data['results']), Order.objects.filter(status=Order.PLACED).count())

    def test_list_paid_responds_with_all_paid_orders(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.list_paid_endpoint)
        self.assertEqual(len(response.data['results']), Order.objects.filter(status=Order.PAID).count())

    def test_list_not_sent_responds_with_all_not_sent_orders(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.list_not_sent_endpoint)
        self.assertEqual(len(response.data['results']), Order.objects.exclude(status=Order.SENT).count())

    def test_list_sent_responds_with_all_sent_orders(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.list_sent_endpoint)
        self.assertEqual(len(response.data['results']), Order.objects.filter(status=Order.SENT).count())

    def test_retrieve_as_anonymous_user_is_prohibited(self):
        response = self.client.get(self.get_details_endpoint(self.sample_order.pk))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_retrieve_as_ordinary_user_is_prohibited(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.get(self.get_details_endpoint(self.sample_order.pk))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_retrieve_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.get_details_endpoint(self.sample_order.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.get(self.get_details_endpoint(self.sample_order.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_user_as_anonymous_user_is_prohibited(self):
        response = self.client.get(self.get_list_user_endpoint(self.ordinary_user.pk))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_list_user_as_ordinary_user_not_owner_is_prohibited(self):
        user = OrdinaryUserFactory()
        self.client.force_login(user)

        response = self.client.get(self.get_list_user_endpoint(self.ordinary_user.pk))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list_user_as_ordinary_user_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.get(self.get_list_user_endpoint(self.ordinary_user.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_user_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.get_list_user_endpoint(self.ordinary_user.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_user_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.get(self.get_list_user_endpoint(self.ordinary_user.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_user_responds_only_with_user_orders(self):
        self.client.force_login(self.staff_user)

        OrderFactory(user=self.ordinary_user)
        response = self.client.get(self.get_list_user_endpoint(self.ordinary_user.pk))
        self.assertEqual(response.data['count'], Order.objects.filter(user=self.ordinary_user).count())

    def test_retrieve_user_as_anonymous_user_is_prohibited(self):
        response = self.client.get(self.get_details_user_endpoint(self.ordinary_user.pk, self.sample_order.pk))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_retrieve_user_as_ordinary_user_not_owner_is_prohibited(self):
        user = OrdinaryUserFactory()
        self.client.force_login(user)

        response = self.client.get(self.get_details_user_endpoint(self.ordinary_user.pk, self.sample_order.pk))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_retrieve_user_as_ordinary_user_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.get(self.get_details_user_endpoint(self.ordinary_user.pk, self.sample_order.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_user_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.get_details_user_endpoint(self.ordinary_user.pk, self.sample_order.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_user_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.get(self.get_details_user_endpoint(self.ordinary_user.pk, self.sample_order.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
