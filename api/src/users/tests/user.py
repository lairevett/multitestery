from unittest.mock import patch

from django.test import TestCase
from rest_framework import status

from app.utils.cache import get_cache_key_for_instance
from users.factories.user import OrdinaryUserFactory
from users.models import User
from users.utils.user.tests import UserAPITestCase
from users.views import UserViewSetV1


class UserModelTestCase(TestCase):

    def setUp(self):
        self.user = OrdinaryUserFactory()

    def test_to_str_returns_username(self):
        self.assertEqual(str(self.user), self.user.username)


class UserSerializerAPITestCase(UserAPITestCase):

    def test_fields_layout_is_correct(self):
        fields = ['id', 'username', 'email', 'wants_newsletters', 'is_staff', 'is_superuser', 'is_active',
                  'date_joined']
        self.client.force_login(self.staff_user)

        response = self.client.get(self.get_details_endpoint(self.staff_user.pk))
        self.assertEqual(list(response.data.keys()), fields)


class UserViewAPITestCase(UserAPITestCase):

    def test_create_as_anonymous_user_is_prohibited(self):
        response = self.client.post(self.base_endpoint, self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_as_ordinary_user_is_prohibited(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_hashes_password(self):
        self.client.force_login(self.staff_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='json')
        self.assertNotEqual(User.objects.get(pk=response.data['id']).password, self.dummy_payload['password'])

    @patch('django.core.cache.cache.delete')
    def test_create_users_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.post(self.base_endpoint, self.dummy_payload, format='json')
        mocked_method.assert_called_with(UserViewSetV1.queryset_cache_key)

    # noinspection DuplicatedCode
    def test_updates_as_anonymous_user_are_prohibited(self):
        response = self.client.put(self.get_details_endpoint(self.ordinary_user.pk), self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.patch(self.get_details_endpoint(self.ordinary_user.pk), {'username': 'new_username'},
                                     format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_updates_as_ordinary_user_are_prohibited(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.put(self.get_details_endpoint(self.ordinary_user.pk), self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        response = self.client.patch(self.get_details_endpoint(self.ordinary_user.pk), {'username': 'new_username'},
                                     format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    # noinspection DuplicatedCode
    def test_updates_of_super_user_as_staff_user_are_prohibited(self):
        self.client.force_login(self.staff_user)

        response = self.client.put(self.get_details_endpoint(self.super_user.pk), self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        response = self.client.put(self.get_details_endpoint(self.super_user.pk), {'username': 'new_username'},
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    # noinspection DuplicatedCode
    def test_updates_of_self_are_prohibited(self):
        self.client.force_login(self.staff_user)

        response = self.client.put(self.get_details_endpoint(self.staff_user.pk), self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        response = self.client.put(self.get_details_endpoint(self.staff_user.pk), {'username': 'new_username'},
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_updates_hash_password(self):
        self.client.force_login(self.super_user)

        # No need to check put() because they both are using perform_update(), which is overriden.
        self.client.patch(self.get_details_endpoint(self.staff_user.pk), self.dummy_payload, format='json')
        self.assertNotEqual(User.objects.get(pk=self.staff_user.pk).password, self.dummy_payload['password'])

    def test_update_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.put(self.get_details_endpoint(self.ordinary_user.pk), self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.put(self.get_details_endpoint(self.ordinary_user.pk), self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @patch('django.core.cache.cache.delete')
    def test_update_users_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.put(self.get_details_endpoint(self.ordinary_user.pk), self.dummy_payload, format='json')
        mocked_method.assert_called_with(UserViewSetV1.queryset_cache_key)

    @patch('django.core.cache.cache.set')
    def test_update_user_instance_cache_is_updated(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.put(self.get_details_endpoint(self.ordinary_user.pk), self.dummy_payload, format='json')
        mocked_method.assert_called_with(
            get_cache_key_for_instance(UserViewSetV1.queryset_cache_key, self.ordinary_user.pk),
            User.objects.get(pk=self.ordinary_user.pk))

    def test_partial_update_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.patch(self.get_details_endpoint(self.ordinary_user.pk), {'username': 'new_username'},
                                     format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_partial_update_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.patch(self.get_details_endpoint(self.ordinary_user.pk), {'username': 'new_username'},
                                     format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @patch('django.core.cache.cache.delete')
    def test_partial_update_users_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.patch(self.get_details_endpoint(self.ordinary_user.pk), {'username': 'new_username'}, format='json')
        mocked_method.assert_called_with(UserViewSetV1.queryset_cache_key)

    @patch('django.core.cache.cache.set')
    def test_partial_update_user_instance_cache_is_updated(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.put(self.get_details_endpoint(self.ordinary_user.pk), {'username': 'new_username'}, format='json')
        mocked_method.assert_called_with(
            get_cache_key_for_instance(UserViewSetV1.queryset_cache_key, self.ordinary_user.pk),
            User.objects.get(pk=self.ordinary_user.pk))

    def test_list_as_anonymous_user_is_prohibited(self):
        response = self.client.get(self.base_endpoint)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_list_as_ordinary_user_is_prohibited(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.get(self.base_endpoint)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.base_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.get(self.base_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_responds_with_all_not_deleted_users(self):
        self.client.force_login(self.staff_user)

        OrdinaryUserFactory(is_deleted=True)
        response = self.client.get(self.base_endpoint)
        self.assertEqual(response.data['count'], User.objects.filter(is_deleted=False).count())

    def test_retrieve_as_anonymous_user_is_prohibited(self):
        response = self.client.get(self.get_details_endpoint(self.ordinary_user.pk))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_retrieve_as_ordinary_user_is_prohibited(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.get(self.get_details_endpoint(self.ordinary_user.pk))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_retrieve_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.get_details_endpoint(self.ordinary_user.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.get(self.get_details_endpoint(self.ordinary_user.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_as_anonymous_user_is_prohibited(self):
        response = self.client.delete(self.get_details_endpoint(self.staff_user.pk))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_as_ordinary_user_is_prohibited(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.delete(self.get_details_endpoint(self.ordinary_user.pk))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        response = self.client.delete(self.get_details_endpoint(self.staff_user.pk))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.delete(self.get_details_endpoint(self.ordinary_user.pk))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.delete(self.get_details_endpoint(self.staff_user.pk))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_user_makes_property_is_deleted_true(self):
        self.client.force_login(self.staff_user)

        self.client.delete(self.get_details_endpoint(self.ordinary_user.pk))
        self.assertTrue(User.objects.get(pk=self.ordinary_user.pk).is_deleted)

    def test_delete_cannot_delete_self(self):
        self.client.force_login(self.staff_user)

        response = self.client.delete(self.get_details_endpoint(self.staff_user.pk))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_staff_user_cannot_delete_super_user(self):
        self.client.force_login(self.staff_user)

        response = self.client.delete(self.get_details_endpoint(self.super_user.pk))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    @patch('django.core.cache.cache.delete')
    def test_delete_users_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.delete(self.get_details_endpoint(self.ordinary_user.pk))
        mocked_method.assert_any_call(UserViewSetV1.queryset_cache_key)

    @patch('django.core.cache.cache.delete')
    def test_delete_user_instance_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.delete(self.get_details_endpoint(self.ordinary_user.pk))
        mocked_method.assert_any_call(
            get_cache_key_for_instance(UserViewSetV1.queryset_cache_key, self.ordinary_user.pk))
