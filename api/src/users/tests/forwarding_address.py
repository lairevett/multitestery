from unittest.mock import patch

from django.test import TestCase, override_settings
from rest_framework import status

from app.utils.cache import get_cache_key_for_user, get_cache_key_for_instance
from users.factories.forwarding_address import ForwardingAddressWithAptNumberFactory, \
    ForwardingAddressWithoutAptNumberFactory
from users.models import ForwardingAddress
from users.utils.forwarding_address.tests import ForwardingAddressAPITestCase
from users.views import ForwardingAddressViewSetV1


class ForwardingAddressModelTestCase(TestCase):

    def setUp(self):
        self.forwarding_address_with_apt = ForwardingAddressWithAptNumberFactory()
        self.forwarding_address_without_apt = ForwardingAddressWithoutAptNumberFactory()

    def test__get_full_name_returns_first_name_and_last_name(self):
        full_name = f'{self.forwarding_address_with_apt.first_name} {self.forwarding_address_with_apt.last_name}'
        self.assertEqual(self.forwarding_address_with_apt._get_full_name(), full_name)

    def test__get_forwarding_address_returns_full_address(self):
        forwarding_address_with_apt = \
            f'{self.forwarding_address_with_apt.street} ' \
            f'{self.forwarding_address_with_apt.home_number}/{self.forwarding_address_with_apt.apt_number}, ' \
            f'{self.forwarding_address_with_apt.postal_code} {self.forwarding_address_with_apt.city}'

        self.assertEqual(self.forwarding_address_with_apt._get_forwarding_address(), forwarding_address_with_apt)

        forwarding_address_without_apt = \
            f'{self.forwarding_address_without_apt.street} {self.forwarding_address_without_apt.home_number}, ' \
            f'{self.forwarding_address_without_apt.postal_code} {self.forwarding_address_without_apt.city}'

        self.assertEqual(self.forwarding_address_without_apt._get_forwarding_address(), forwarding_address_without_apt)

    def test__get_full_forwarding_address_returns_first_and_last_name_with_phone_and_full_address(self):
        full_forwarding_address = \
            f'{self.forwarding_address_without_apt.first_name} {self.forwarding_address_without_apt.last_name}, ' \
            f'{self.forwarding_address_without_apt.phone}, ' \
            f'{self.forwarding_address_without_apt.street} {self.forwarding_address_without_apt.home_number}, ' \
            f'{self.forwarding_address_without_apt.postal_code} {self.forwarding_address_without_apt.city}'

        self.assertEqual(self.forwarding_address_without_apt._get_full_forwarding_address(), full_forwarding_address)

    def test_to_str_returns_full_forwarding_address(self):
        self.assertEqual(str(self.forwarding_address_without_apt),
                         self.forwarding_address_without_apt._get_full_forwarding_address())


# Users already have 2 forwarding addresses assigned in setUp(), so cap at 3.
@override_settings(MAX_USER_FORWARDING_ADDRESSES=3)
class ForwardingAddressSerializerAPITestCase(ForwardingAddressAPITestCase):

    # noinspection DuplicatedCode
    def test_forwarding_address_cap_for_one_user(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.client.force_login(self.staff_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class ForwardingAddressViewAPITestCase(ForwardingAddressAPITestCase):

    def test_create_as_anonymous_user_is_prohibited(self):
        response = self.client.post(self.base_endpoint, self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_as_ordinary_user_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    @patch('django.core.cache.cache.delete')
    def test_create_user_forwarding_addresses_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.post(self.base_endpoint, self.dummy_payload, format='json')
        mocked_method.assert_called_with(
            get_cache_key_for_user(ForwardingAddressViewSetV1.queryset_cache_key, self.ordinary_user.pk))

    def test_update_is_not_allowed_method(self):
        self.client.force_login(self.super_user)

        response = self.client.put(self.super_user_details_endpoint, self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_partial_update_is_not_allowed_method(self):
        self.client.force_login(self.super_user)

        response = self.client.patch(self.super_user_details_endpoint, self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_list_all_as_anonymous_user_is_prohibited(self):
        response = self.client.get(self.get_list_endpoint(self.ordinary_user.pk))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_list_all_as_ordinary_user_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.get(self.get_list_endpoint(self.ordinary_user.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_all_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.get_list_endpoint(self.staff_user.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_all_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.get(self.get_list_endpoint(self.super_user.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_all_as_ordinary_user_cant_lookup_other_users_forwarding_addresses(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.get(self.get_list_endpoint(self.staff_user.pk))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list_all_as_staff_user_can_lookup_other_users_forwarding_addresses(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.get_list_endpoint(self.ordinary_user.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_all_as_super_user_can_lookup_other_users_forwarding_addresses(self):
        self.client.force_login(self.super_user)

        response = self.client.get(self.get_list_endpoint(self.ordinary_user.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_all_responds_with_all_not_deleted_forwarding_addresses(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.get(self.get_list_endpoint(self.ordinary_user.pk))
        self.assertEqual(len(response.data['results']),
                         ForwardingAddress.objects.filter(is_deleted=False, user=self.ordinary_user).count())

    def test_details_as_anonymous_user_is_prohibited(self):
        response = self.client.get(self.ordinary_user_details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_details_as_ordinary_user_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.get(self.ordinary_user_details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_details_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.staff_user_details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_details_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.get(self.super_user_details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_details_as_ordinary_user_cant_lookup_other_users_forwarding_address(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.get(self.staff_user_details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_details_as_staff_user_can_lookup_other_users_forwarding_address(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.ordinary_user_details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_details_as_super_user_can_lookup_other_users_forwarding_address(self):
        self.client.force_login(self.super_user)

        response = self.client.get(self.ordinary_user_details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_as_anonymous_user_is_prohibited(self):
        response = self.client.delete(self.ordinary_user_details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_as_ordinary_user_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.delete(self.ordinary_user_details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.delete(self.staff_user_details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.delete(self.super_user_details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_as_ordinary_user_cant_delete_other_users_forwarding_addresses(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.delete(self.staff_user_details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_as_staff_user_can_delete_other_users_forwarding_addresses(self):
        self.client.force_login(self.staff_user)

        response = self.client.delete(self.ordinary_user_details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_as_super_user_can_delete_other_users_forwarding_addresses(self):
        self.client.force_login(self.super_user)

        response = self.client.delete(self.ordinary_user_details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_makes_object_property_is_deleted_true(self):
        self.client.force_login(self.ordinary_user)

        self.client.delete(self.ordinary_user_details_endpoint)
        self.assertTrue(
            ForwardingAddress.objects.get(pk=self.ordinary_user_forwarding_address.pk).is_deleted)

    @patch('django.core.cache.cache.delete')
    def test_delete_user_forwarding_addresses_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.delete(self.ordinary_user_details_endpoint)
        mocked_method.assert_any_call(
            get_cache_key_for_user(ForwardingAddressViewSetV1.queryset_cache_key, self.ordinary_user.pk))

    @patch('django.core.cache.cache.delete')
    def test_delete_user_forwarding_address_instance_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.delete(self.ordinary_user_details_endpoint)
        mocked_method.assert_any_call(get_cache_key_for_instance(ForwardingAddressViewSetV1.queryset_cache_key,
                                                                 self.ordinary_user_forwarding_address.pk))
