from rest_framework import serializers

from products.serializers import ProductSerializerV1
from users.models import ForwardingAddress
from users.models.order import Order, OrderEntry
from .forwarding_address import ForwardingAddressSerializerV1


class OrderSerializerV1(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['forwarding_address'].queryset = ForwardingAddress.objects.filter(user=self.context['request'].user,
                                                                                      is_deleted=False)

    class Meta:
        model = Order
        fields = ['id', 'name', 'entries', 'forwarding_address', 'total', 'shipping', 'status', 'created_at',
                  'updated_at']
        read_only_fields = ['id', 'name', 'entries', 'total', 'status', 'created_at', 'updated_at']

    def to_representation(self, instance):
        representation = super().to_representation(instance)

        representation['forwarding_address'] = ForwardingAddressSerializerV1(instance.forwarding_address, context={
            'request': self.context['request']}).data

        representation['entries'] = OrderEntrySerializerV1(instance.entries, many=True,
                                                           context={'request': self.context['request']}).data

        return representation


class OrderEntrySerializerV1(serializers.ModelSerializer):
    class Meta:
        model = OrderEntry
        fields = ['id', 'product', 'quantity', 'subtotal']
        read_only_fields = ['id', 'subtotal']

    # noinspection PyMethodMayBeStatic
    def validate_product(self, value):
        if value.is_deleted:
            raise serializers.ValidationError(f'Produkt o id "{value.id}" nie istnieje.')

        return value

    def to_representation(self, instance):
        representation = super().to_representation(instance)

        representation['product'] = ProductSerializerV1(instance.product,
                                                        context={'request': self.context['request']}).data

        return representation
