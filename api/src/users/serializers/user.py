from rest_framework import serializers
from users.models import User


class UserSerializerV1(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True, style={'input_type': 'password'})

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'wants_newsletters', 'password', 'is_staff', 'is_superuser',
                  'is_active', 'date_joined']
        read_only_fields = ['id', 'is_active', 'date_joined']
