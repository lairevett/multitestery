from django.conf import settings
from rest_framework import serializers

from users.models import ForwardingAddress


class ForwardingAddressSerializerV1(serializers.ModelSerializer):
    class Meta:
        model = ForwardingAddress
        fields = ['id', 'first_name', 'last_name', 'phone', 'street', 'home_number', 'apt_number', 'city',
                  'postal_code']
        read_only_fields = ['id']

    def validate(self, attrs):
        if self.context['request'].user \
                .forwarding_addresses.filter(is_deleted=False).count() >= settings.MAX_USER_FORWARDING_ADDRESSES:
            raise serializers.ValidationError(f'Przekroczono maksymalną ilość adresów'
                                              f' ({settings.MAX_USER_FORWARDING_ADDRESSES}).', 'max_value_exceeded')
        return attrs
