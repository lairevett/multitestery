from django.core.cache import cache
from rest_framework import permissions, status, mixins
from rest_framework.decorators import action
from rest_framework.response import Response

from app.utils.cache import get_cache_key_for_user, get_cache_key_for_instance
from products.views import ProductViewSetV1
from users.mixins import CanceledPaidSentOrdersListMixin
from users.models.order import Order
from users.permissions import IsOwnerOrStaff
from users.serializers.order import OrderSerializerV1, OrderEntrySerializerV1
from users.utils.order.other import get_subtotal


class OrderViewSetV1(CanceledPaidSentOrdersListMixin, mixins.CreateModelMixin, mixins.ListModelMixin,
                     mixins.RetrieveModelMixin):
    queryset = Order.objects.all()
    serializer_class = OrderSerializerV1

    def get_permissions(self):
        if self.action == 'create':
            return [permissions.IsAuthenticated()]

        if 'user_pk' in self.kwargs:
            return [permissions.IsAuthenticated(), IsOwnerOrStaff()]

        return [permissions.IsAdminUser()]

    def create(self, request, *args, **kwargs):
        errors = {}

        order_entry_serializer = OrderEntrySerializerV1(data=request.data.get('entries'), many=True,
                                                        context={'request': request})
        if not order_entry_serializer.is_valid():
            # raise_exception=True doesn't add "entries" field to response, so server responds with
            # 500 internal server error instead of validation errors
            # solution: need to add "entries" to errors dict manually
            errors['entries'] = order_entry_serializer.errors

        order_serializer = OrderSerializerV1(data=request.data, context={'request': request})
        if not order_serializer.is_valid():
            errors.update(order_serializer.errors)

        if errors:
            return Response(data=errors, status=status.HTTP_400_BAD_REQUEST)

        self._perform_create(order_serializer, order_entry_serializer)
        headers = self.get_success_headers(order_serializer.data)
        return Response(order_serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def _perform_create(self, order_serializer, order_entry_serializer):
        """
        Creates new order with assigned currently logged in user as owner,
        loops all products in order and sets subtotal to product unit price * order product quantity
        if product isn't on sale, if product is on sale, then it subtracts product.percent_off from unit price before.
        """
        user = self.request.user
        order = order_serializer.save(user=user)
        for order_entry in order_entry_serializer.validated_data:
            order_entry['subtotal'] = get_subtotal(order_entry)
        order_entry_serializer.save(order=order)

        # Delete user's private order list from cache after placing new order and all orders list cache.
        cache.delete(get_cache_key_for_user(CanceledPaidSentOrdersListMixin.queryset_cache_key, user.pk))
        cache.delete(self.queryset_cache_key)

    @action(detail=True, methods=['post'])
    def update_status(self, request, pk):
        instance = self.get_object()

        new_status = request.data.get('new_status')
        if instance.status == new_status:
            return Response({'errors': ['Aktualny status jest taki sam, jak ten, który próbujesz ustawić.']},
                            status.HTTP_400_BAD_REQUEST)

        if new_status == Order.SENT:
            for entry in instance.entries.all():
                if not entry.product.is_deleted:
                    new_quantity = entry.product.quantity - entry.quantity
                    if new_quantity >= 0:
                        entry.product.quantity = new_quantity
                        cache.set(get_cache_key_for_instance(ProductViewSetV1.queryset_cache_key, entry.product.pk),
                                  entry.product.save())
                    else:
                        return Response({'errors': [f'Niedostateczna ilość produktu "{entry.product}" na magazynie.']},
                                        status.HTTP_400_BAD_REQUEST)
        elif instance.status == Order.SENT and new_status != Order.SENT:
            for entry in instance.entries.all():
                if not entry.product.is_deleted:
                    entry.product.quantity += entry.quantity
                    cache.set(get_cache_key_for_instance(ProductViewSetV1.queryset_cache_key, entry.product.pk),
                              entry.product.save())

        # Update order details.
        instance.status = new_status
        instance.save()
        cache.set(get_cache_key_for_instance(self.queryset_cache_key, pk), instance)

        # Delete private user's order list and all orders list cache.
        cache.delete(get_cache_key_for_user(self.queryset_cache_key, instance.user.pk))
        cache.delete(self.queryset_cache_key)

        # Delete products list cache after quantity change.
        cache.delete(ProductViewSetV1.queryset_cache_key)

        return Response({'status': new_status})

    @action(detail=True, methods=['post'])
    def cancel(self, request, pk, *args, **kwargs):
        instance = self.get_object()

        if instance.status == Order.PLACED:
            # Update order details.
            instance.status = Order.CANCELED
            instance.save()
            cache.set(get_cache_key_for_instance(self.queryset_cache_key, pk), instance)

            # Delete private user's order list cache and all orders list cache.
            cache.delete(get_cache_key_for_user(self.queryset_cache_key, instance.user.pk))
            cache.delete(self.queryset_cache_key)

            serializer = self.get_serializer(instance)
            return Response(serializer.data)

        return Response({'errors': ['Operacja niedozwolona w tym stanie zamówienia.']},
                        status=status.HTTP_400_BAD_REQUEST)
