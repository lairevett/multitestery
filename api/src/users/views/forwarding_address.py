from django.core.cache import cache
from rest_framework import permissions
from rest_framework.response import Response

from app.utils.cache import get_cache_key_for_instance, get_cache_key_for_user
from app.utils.viewsets import CachingModelViewSet
from users.apps import UsersConfig
from users.models import ForwardingAddress
from users.permissions import IsOwnerOrStaff
from users.serializers import ForwardingAddressSerializerV1


class ForwardingAddressViewSetV1(CachingModelViewSet):
    queryset_cache_key = f'{UsersConfig.name}_forwarding_addresses'
    serializer_class = ForwardingAddressSerializerV1
    pagination_class = None

    def get_permissions(self):
        if self.action == 'create':
            return [permissions.IsAuthenticated()]

        return [permissions.IsAuthenticated(), IsOwnerOrStaff()]

    def get_queryset(self):
        user_pk = self.kwargs['user_pk']
        cache_key = get_cache_key_for_user(self.queryset_cache_key, user_pk)

        queryset = cache.get(cache_key)
        if not queryset:
            queryset = ForwardingAddress.objects.filter(user__pk=user_pk, is_deleted=False).order_by('id')
            cache.set(cache_key, queryset)

        return queryset

    def perform_create(self, serializer):
        user = self.request.user
        serializer.save(user=user)

        # Delete user's private forwarding addresses list cache.
        cache.delete(get_cache_key_for_user(self.queryset_cache_key, user.pk))

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)

        return Response({'results': serializer.data})

    def perform_destroy(self, instance):
        instance.is_deleted = True
        instance.save()

        # Delete the forwarding address instance cache.
        cache.delete(get_cache_key_for_instance(self.queryset_cache_key, instance.pk))

        # Delete user's private forwarding addresses list cache.
        cache.delete(get_cache_key_for_user(self.queryset_cache_key, instance.user.pk))
