from django.contrib.auth.hashers import make_password
from django.core.cache import cache
from rest_framework import permissions

from app.utils.cache import get_cache_key_for_instance
from app.utils.viewsets import CachingModelViewSet
from custom_auth.utils import delete_authentication_token
from users.apps import UsersConfig
from users.models import User
from users.permissions import RejectUpdateDeleteIfOwner, RejectUpdateDeleteIfInstanceSuperuser
from users.serializers import UserSerializerV1


class UserViewSetV1(CachingModelViewSet):
    queryset_cache_key = UsersConfig.name
    queryset = User.objects.filter(is_deleted=False).order_by('-id')
    serializer_class = UserSerializerV1
    permission_classes = [permissions.IsAdminUser, RejectUpdateDeleteIfOwner, RejectUpdateDeleteIfInstanceSuperuser]

    def perform_create(self, serializer):
        serializer.save(password=make_password(serializer.validated_data['password']))
        cache.delete(self.queryset_cache_key)

    def perform_update(self, serializer):
        saved_instance = serializer.save(password=make_password(
            serializer.validated_data['password'])) if 'password' in serializer.validated_data else serializer.save()

        cache.set(get_cache_key_for_instance(self.queryset_cache_key, self.kwargs['pk']), saved_instance)
        cache.delete(self.queryset_cache_key)

    def perform_destroy(self, instance):
        instance.is_deleted = True
        instance.save()
        delete_authentication_token(instance)

        cache.delete(get_cache_key_for_instance(self.queryset_cache_key, instance.pk))
        cache.delete(self.queryset_cache_key)
