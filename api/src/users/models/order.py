from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models

from products.models import Product
from users.models import User, ForwardingAddress


class Order(models.Model):
    CASH_ON_DELIVERY = 0
    CASH_UP_FRONT = 1
    SHIPPING_CHOICES = [
        (CASH_ON_DELIVERY, 'Płatność przy odbiorze (przesyłka za pobraniem)'),
        (CASH_UP_FRONT, 'Płatność z góry (przesyłka kurierska)')
    ]
    CANCELED = -1
    PLACED = 0
    PAID = 1
    SENT = 2
    STATUS_CHOICES = [
        (CANCELED, 'Anulowane'),
        (PLACED, 'Złożone'),
        (PAID, 'Zapłacone'),
        (SENT, 'Wysłane')
    ]
    user = models.ForeignKey(to=User, on_delete=models.PROTECT, related_name='orders')
    forwarding_address = models.ForeignKey(to=ForwardingAddress, on_delete=models.PROTECT, related_name='orders')
    shipping = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(CASH_ON_DELIVERY), MaxValueValidator(CASH_UP_FRONT)], choices=SHIPPING_CHOICES)
    status = models.SmallIntegerField(validators=[MinValueValidator(CANCELED), MaxValueValidator(SENT)],
                                      choices=STATUS_CHOICES, default=PLACED)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'Zamówienie #{self.id}'

    name = property(__str__)

    def _get_total(self):
        return sum([entry.subtotal for entry in self.entries.all()])

    total = property(_get_total)


class OrderEntry(models.Model):
    order = models.ForeignKey(to=Order, on_delete=models.CASCADE, related_name='entries')
    product = models.ForeignKey(to=Product, on_delete=models.PROTECT)
    quantity = models.PositiveSmallIntegerField()
    subtotal = models.PositiveIntegerField()

    class Meta:
        verbose_name_plural = 'Order entries'

    def __str__(self):
        return self.product.__str__()
