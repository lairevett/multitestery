from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

from . import User


class ForwardingAddress(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.PROTECT, related_name='forwarding_addresses')
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    phone = PhoneNumberField()
    street = models.CharField(max_length=200)
    home_number = models.CharField(max_length=10)
    apt_number = models.PositiveSmallIntegerField(blank=True, null=True)
    city = models.CharField(max_length=35)
    postal_code = models.CharField(max_length=25)
    is_deleted = models.BooleanField(default=False)

    def __str__(self):
        return self._get_full_forwarding_address()

    def _get_full_name(self):
        return f'{self.first_name} {self.last_name}'

    def _get_forwarding_address(self):
        apt_number_to_str = f'/{self.apt_number}' if self.apt_number else ''
        return f'{self.street} {self.home_number}{apt_number_to_str}, {self.postal_code} {self.city}'

    def _get_full_forwarding_address(self):
        return f'{self._get_full_name()}, {self.phone}, {self._get_forwarding_address()}'
