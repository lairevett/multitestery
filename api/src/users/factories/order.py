import factory

from products.factories import ProductFactory
from users.models import Order, OrderEntry
from .user import OrdinaryUserFactory
from .forwarding_address import ForwardingAddressWithAptNumberFactory


class OrderFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Order

    user = factory.SubFactory(OrdinaryUserFactory)
    forwarding_address = factory.SubFactory(ForwardingAddressWithAptNumberFactory)
    shipping = Order.CASH_ON_DELIVERY


class OrderEntryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = OrderEntry

    order = factory.SubFactory(OrderFactory)
    product = factory.SubFactory(ProductFactory)
    quantity = 1
    subtotal = factory.LazyAttribute(lambda obj: obj.product.unit_price * obj.quantity)
