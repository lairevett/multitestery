import factory

from users.models import ForwardingAddress
from .user import OrdinaryUserFactory


class ForwardingAddressWithAptNumberFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ForwardingAddress

    user = factory.SubFactory(OrdinaryUserFactory)
    first_name = 'Dummy'
    last_name = 'Name'
    phone = '+48123456789'
    street = 'Dummy St.'
    home_number = '1A'
    apt_number = 1
    city = 'Dummy City'
    postal_code = '00-001'


class ForwardingAddressWithoutAptNumberFactory(ForwardingAddressWithAptNumberFactory):
    apt_number = None
