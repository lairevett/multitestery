import factory
from django.contrib.auth.hashers import make_password

from users.models import User


class OrdinaryUserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User
        django_get_or_create = ['username']

    username = factory.Sequence(lambda n: f'ordinary_user{n}')
    email = factory.LazyAttribute(lambda obj: f'{obj.username}@example.com')
    password = make_password('password')
    is_active = True


class StaffUserFactory(OrdinaryUserFactory):
    username = factory.Sequence(lambda n: f'staff_user{n}')
    is_staff = True


class SuperUserFactory(StaffUserFactory):
    username = factory.Sequence(lambda n: f'super_user{n}')
    is_superuser = True
