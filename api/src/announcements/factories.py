import factory

from app.utils.tests import get_dummy_image
from .models import Announcement


class ActiveAnnouncementFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Announcement

    priority = factory.Sequence(lambda n: n)
    title = 'Default title'
    description = 'Default description'
    is_active = True

    @factory.lazy_attribute
    def image(self):
        dummy_image = get_dummy_image()
        return dummy_image.name


class InactiveAnnouncementFactory(ActiveAnnouncementFactory):
    is_active = False
