from tempfile import TemporaryDirectory

from django.test import override_settings

from announcements.factories import ActiveAnnouncementFactory, InactiveAnnouncementFactory
from announcements.models import Announcement
from app.utils.tests import get_uploaded_dummy_image
from users.utils.tests import APITestCaseWithUsers


@override_settings(MEDIA_ROOT=TemporaryDirectory(prefix='multitestery_test_media').name)
class AnnouncementAPITestCase(APITestCaseWithUsers):
    base_endpoint = '/v1/announcements/'
    list_inactive_endpoint = base_endpoint + '?active=0'
    list_active_endpoint = base_endpoint + '?active=1'

    def setUp(self):
        super().setUp()

        self.active_announcement = ActiveAnnouncementFactory()
        self.inactive_announcement = InactiveAnnouncementFactory()

        self.details_pk = self.active_announcement.pk
        self.details_endpoint = f'{self.base_endpoint}{self.details_pk}/'

        self.dummy_payload = {
            'priority': Announcement.objects.last().priority + 1,  # Priority needs to be unique.
            'image': get_uploaded_dummy_image(),
            'title': 'Dummy title',
            'description': 'Dummy description.',
            'is_active': True
        }
