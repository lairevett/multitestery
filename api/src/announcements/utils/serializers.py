import sys
from io import BytesIO
from uuid import uuid4

from PIL import Image
from django.core.files.uploadedfile import InMemoryUploadedFile


def get_adjusted_image(image):
    """
    Changes size of uploaded image to 1100 x 500 and its format to .webp + decreases quality to 80%
    in attempt to save some disk space.
    """
    file = BytesIO()
    name = f'{uuid4()}.webp'

    with Image.open(image) as pillow_image:
        pillow_image = pillow_image.resize((1100, 500), Image.BILINEAR)
        pillow_image.save(file, format='WEBP', quality=80)

    return InMemoryUploadedFile(file, 'ImageField', name, 'image/webp', sys.getsizeof(file), charset=None)
