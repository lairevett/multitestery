from unittest.mock import patch

from PIL import Image
from django.test import TestCase
from rest_framework import status

from app.utils.cache import get_cache_key_for_instance
from app.utils.tests import get_uploaded_dummy_image, get_image_path
from .models import Announcement
from .serializers import AnnouncementSerializerV1
from .utils.tests import AnnouncementAPITestCase
from .views import AnnouncementViewSetV1


class AnnouncementModelTestCase(TestCase):

    def test_to_str_returns_announcement_title(self):
        announcement_title = 'Sample announcement'

        announcement = Announcement()
        announcement.title = announcement_title
        self.assertEqual(str(announcement), announcement_title)


class AnnouncementSerializerAPITestCase(AnnouncementAPITestCase):

    def test_fields_layout_is_correct(self):
        serialized_announcement = AnnouncementSerializerV1(self.active_announcement).data

        fields = ['id', 'priority', 'image', 'title', 'description', 'is_active']
        for field in fields:
            self.assertIn(field, serialized_announcement)

    def test_create_image_size_is_1100_by_500(self):
        self.client.force_login(self.staff_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='multipart')

        path = get_image_path(response.data['image'])
        with Image.open(path) as image:
            width, height = image.size

        self.assertEqual(width, 1100)
        self.assertEqual(height, 500)

    def test_create_image_format_is_webp(self):
        self.client.force_login(self.staff_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='multipart')
        path = get_image_path(response.data['image'])

        with Image.open(path) as image:
            form = image.format

        self.assertTrue(path.endswith('.webp'))
        self.assertEqual(form, 'WEBP')

    def test_update_image_size_is_1100_by_500(self):
        self.client.force_login(self.staff_user)

        response = self.client.patch(self.details_endpoint, {'image': get_uploaded_dummy_image()}, format='multipart')
        path = get_image_path(response.data['image'])

        with Image.open(path) as image:
            width, height = image.size

        self.assertEqual(width, 1100)
        self.assertEqual(height, 500)

    def test_update_image_format_is_webp(self):
        self.client.force_login(self.staff_user)

        response = self.client.patch(self.details_endpoint, {'image': get_uploaded_dummy_image()}, format='multipart')
        path = get_image_path(response.data['image'])

        with Image.open(path) as image:
            form = image.format

        self.assertTrue(path.endswith('.webp'))
        self.assertEqual(form, 'WEBP')


class AnnouncementViewAPITestCase(AnnouncementAPITestCase):

    def test_create_as_anonymous_user_is_prohibited(self):
        response = self.client.post(self.base_endpoint, self.dummy_payload, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_as_ordinary_user_is_prohibited(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    @patch('django.core.cache.cache.delete')
    def test_create_announcements_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.post(self.base_endpoint, self.dummy_payload, format='multipart')
        mocked_method.assert_called_with(AnnouncementViewSetV1.queryset_cache_key)

    # noinspection DuplicatedCode
    def test_updates_as_anonymous_user_are_prohibited(self):
        response = self.client.put(self.details_endpoint, self.dummy_payload, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.patch(self.details_endpoint, {'title': 'Updated title'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_updates_as_ordinary_user_are_prohibited(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.put(self.details_endpoint, self.dummy_payload, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        response = self.client.patch(self.details_endpoint, {'title': 'Updated title'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.put(self.details_endpoint, self.dummy_payload, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.put(self.details_endpoint, self.dummy_payload, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @patch('django.core.cache.cache.delete')
    def test_update_announcements_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.put(self.details_endpoint, self.dummy_payload, format='multipart')
        mocked_method.assert_called_with(AnnouncementViewSetV1.queryset_cache_key)

    @patch('django.core.cache.cache.set')
    def test_update_announcement_instance_cache_is_updated(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.put(self.details_endpoint, self.dummy_payload, format='multipart')
        mocked_method.assert_called_with(
            get_cache_key_for_instance(AnnouncementViewSetV1.queryset_cache_key, self.details_pk),
            Announcement.objects.get(pk=self.details_pk))

    def test_partial_update_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.patch(self.details_endpoint, {'title': 'Updated title'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_partial_update_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.patch(self.details_endpoint, {'title': 'Updated title'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @patch('django.core.cache.cache.delete')
    def test_partial_update_announcements_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.patch(self.details_endpoint, self.dummy_payload, format='multipart')
        mocked_method.assert_called_with(AnnouncementViewSetV1.queryset_cache_key)

    @patch('django.core.cache.cache.set')
    def test_partial_update_announcement_instance_cache_is_updated(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.patch(self.details_endpoint, self.dummy_payload, format='multipart')
        mocked_method.assert_called_with(
            get_cache_key_for_instance(AnnouncementViewSetV1.queryset_cache_key, self.details_pk),
            Announcement.objects.get(pk=self.details_pk))

    def test_list_all_as_anonymous_user_is_permitted(self):
        response = self.client.get(self.base_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_all_as_ordinary_user_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.get(self.base_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_all_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.base_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_all_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.get(self.base_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_all_responds_with_all_announcements(self):
        response = self.client.get(self.base_endpoint)
        self.assertEqual(len(response.data['results']), Announcement.objects.count())

    def test_list_active_responds_only_with_active_announcements(self):
        response = self.client.get(self.list_active_endpoint)
        self.assertEqual(len(response.data['results']), Announcement.objects.filter(is_active=True).count())

    def test_list_inactive_responds_only_with_inactive_announcements(self):
        response = self.client.get(self.list_inactive_endpoint)
        self.assertEqual(len(response.data['results']), Announcement.objects.filter(is_active=False).count())

    def test_delete_as_anonymous_user_is_prohibited(self):
        response = self.client.delete(self.details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_as_ordinary_user_is_prohibited(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.delete(self.details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.delete(self.details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.delete(self.details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    @patch('django.core.cache.cache.delete')
    def test_delete_announcements_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.delete(self.details_endpoint)
        mocked_method.assert_any_call(AnnouncementViewSetV1.queryset_cache_key)

    @patch('django.core.cache.cache.delete')
    def test_delete_announcement_instance_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.delete(self.details_endpoint)
        mocked_method.assert_any_call(
            get_cache_key_for_instance(AnnouncementViewSetV1.queryset_cache_key, self.details_pk))
