from rest_framework.response import Response

from app.utils.viewsets import CachingModelViewSet
from .apps import AnnouncementConfig
from .models import Announcement
from .permissions import CreateUpdateDeleteIfStaff
from .serializers import AnnouncementSerializerV1


class AnnouncementViewSetV1(CachingModelViewSet):
    queryset = Announcement.objects.all().order_by('priority')
    queryset_cache_key = AnnouncementConfig.name
    serializer_class = AnnouncementSerializerV1
    permission_classes = [CreateUpdateDeleteIfStaff]
    pagination_class = None  # No pagination class here since announcements are displayed in a bootstrap carousel.

    def get_queryset(self):
        queryset = super().get_queryset()

        if not self.kwargs.get('pk'):
            active = self.request.GET.get('active')
            if active == '0':
                queryset = queryset.filter(is_active=False)
            elif active == '1':
                queryset = queryset.filter(is_active=True)

        return queryset

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        return Response({'results': serializer.data})
