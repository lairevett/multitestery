from rest_framework import serializers

from .models import Announcement
from announcements.utils.serializers import get_adjusted_image


class AnnouncementSerializerV1(serializers.ModelSerializer):
    class Meta:
        model = Announcement
        fields = '__all__'

    def create(self, validated_data):
        validated_data.update({'image': get_adjusted_image(validated_data['image'])})

        return super().create(validated_data)

    def update(self, instance, validated_data):
        image = validated_data.get('image')
        if image:
            validated_data.update({'image': get_adjusted_image(image)})

        return super().update(instance, validated_data)
