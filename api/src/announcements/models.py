from django.db import models


class Announcement(models.Model):
    priority = models.PositiveIntegerField(unique=True)
    image = models.ImageField(upload_to='announcements')
    title = models.CharField(max_length=50)
    description = models.TextField()
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.title
