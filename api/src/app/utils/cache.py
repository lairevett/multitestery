def get_cache_key_for_instance(queryset_cache_key, lookup):
    return f'{queryset_cache_key}_{lookup}'


def get_cache_key_for_user(queryset_cache_key, user_id):
    return f'{queryset_cache_key}_u{user_id}'
