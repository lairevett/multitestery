from mimetypes import MimeTypes
from tempfile import NamedTemporaryFile

from PIL import Image
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile


def get_image_path(field):
    relative_image_path = field.split('/')[-2:]
    image_path = settings.MEDIA_ROOT + '/' + '/'.join(relative_image_path)

    return image_path


def get_dummy_image(mode='RGBA', ext='png', size=(1, 1), color=(0, 0, 0)):
    tmp_file = NamedTemporaryFile(suffix='.' + ext)
    image = Image.new(mode, size, color)
    image.save(tmp_file, ext)

    return tmp_file


def get_uploaded_dummy_image(mode='RGBA', ext='png', size=(1, 1), color=(0, 0, 0)):
    dummy_image = get_dummy_image(mode, ext, size, color)
    dummy_image_path = dummy_image.name

    with open(dummy_image_path, 'rb') as file:
        image = SimpleUploadedFile(file.name, file.read(), MimeTypes().guess_type(dummy_image_path)[0])

    return image
