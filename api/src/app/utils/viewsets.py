from django.core.cache import cache
from rest_framework.viewsets import ModelViewSet

from app.utils.cache import get_cache_key_for_instance


class CachingModelViewSet(ModelViewSet):
    queryset_cache_key = None

    def dispatch(self, request, *args, **kwargs):
        if not self.queryset_cache_key:
            raise AttributeError('You forgot to supply queryset_cache_key to class attributes.')  # pragma: no cover

        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = cache.get(self.queryset_cache_key)
        if not queryset:
            queryset = super().get_queryset()
            cache.set(self.queryset_cache_key, queryset)

        return queryset

    def get_object(self):
        lookup = self.kwargs[self.lookup_field]
        instance_cache_key = get_cache_key_for_instance(self.queryset_cache_key, lookup)

        instance = cache.get(instance_cache_key)
        if not instance:
            instance = super().get_object()
            cache.set(instance_cache_key, instance)

        return instance

    def perform_create(self, serializer):
        super().perform_create(serializer)

        cache.delete(self.queryset_cache_key)

    def perform_update(self, serializer):
        lookup = self.kwargs[self.lookup_field]
        cache.set(get_cache_key_for_instance(self.queryset_cache_key, lookup), serializer.save())
        cache.delete(self.queryset_cache_key)

    def perform_destroy(self, instance):
        super().perform_destroy(instance)

        lookup = self.kwargs[self.lookup_field]
        cache.delete(get_cache_key_for_instance(self.queryset_cache_key, lookup))
        cache.delete(self.queryset_cache_key)
