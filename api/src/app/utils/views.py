from rest_framework.views import exception_handler


def custom_exception_handler(exc, context):
    response = exception_handler(exc, context)
    if response:
        custom_data = {'errors': response.data}
        response.data = custom_data

    return response
