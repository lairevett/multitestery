from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from announcements.views import AnnouncementViewSetV1
from pages.views import PageViewSetV1
from products.views import ProductViewSetV1
from users.views import UserViewSetV1
from .views import ping

router_v1 = routers.DefaultRouter()
router_v1.register(r'users', UserViewSetV1, basename='user')
router_v1.register(r'announcements', AnnouncementViewSetV1, basename='announcement')
router_v1.register(r'pages', PageViewSetV1, basename='page')
router_v1.register(r'products', ProductViewSetV1, basename='product')

urlpatterns = [
    path('ping/', ping),
    path('admin/', admin.site.urls),
    path('auth/', include('rest_framework.urls')),
    path('', include('custom_auth.urls')),
    path('', include('users.urls')),
    path('', include('summary.urls')),
    path('v1/', include(router_v1.urls))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
