import dj_database_url
import sentry_sdk
from django.utils.log import DEFAULT_LOGGING
from sentry_sdk.integrations.django import DjangoIntegration
from sentry_sdk.integrations.redis import RedisIntegration

from .common import *

DEBUG = False
PREPEND_WWW = True

SECURE_SSL_REDIRECT = True
SESSION_COOKIE_SECURE = True
AUTH_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True
COOKIES_DOMAIN = '.multitestery.pl'
CORS_ORIGIN_WHITELIST = ['https://www.multitestery.pl']
CSRF_TRUSTED_ORIGINS = ['www.multitestery.pl']
ALLOWED_HOSTS = ['www.api.multitestery.pl']

SECRET_KEY = os.environ['SECRET_KEY']

# noinspection PyUnresolvedReferences
STATIC_ROOT = '/usr/local/www/static.multitestery.pl/static'
# noinspection PyUnresolvedReferences
MEDIA_ROOT = '/usr/local/www/static.multitestery.pl/media'
STATIC_URL = 'https://www.static.multitestery.pl/static/'
MEDIA_URL = 'https://www.static.multitestery.pl/media/'

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = os.environ['EMAIL_HOST']
EMAIL_USER = os.environ['EMAIL_USER']
EMAIL_PASSWORD = os.environ['EMAIL_PASS']
DEFAULT_FROM_EMAIL = EMAIL_USER if '@' in EMAIL_USER else f'{EMAIL_USER}@multitestery.pl'

DATABASES = {
    'default': dj_database_url.config()
}

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': 'redis://127.0.0.1:6379/1',
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
            'IGNORE_EXCEPTIONS': True
        },
        'KEY_PREFIX': 'multitestery',
        'TIMEOUT': 60 * 60 * 24
    }
}

REST_FRAMEWORK.update({
    'DEFAULT_RENDERER_CLASSES': [
        'rest_framework.renderers.JSONRenderer'
    ]
})

sentry_sdk.init(
    dsn="https://feb6c22b845245fbaf1905a5eb85d399@sentry.io/1540776",
    integrations=[DjangoIntegration(), RedisIntegration()]
)

# noinspection SpellCheckingInspection
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'console': {
            'format': '%(asctime)s %(name)-12s %(levelname)-8s %(message)s'
        },
        'django.server': DEFAULT_LOGGING['formatters']['django.server']
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'console'
        },
        'sentry': {
            'level': 'WARNING',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler'
        },
        'django.server': DEFAULT_LOGGING['handlers']['django.server']
    },
    'loggers': {
        '': {
            'level': 'WARNING',
            'handlers': ['console', 'sentry']
        },
        'src': {
            'level': 'WARNING',
            'handlers': ['console', 'sentry'],
            'propagate': False
        },
        'django.server': DEFAULT_LOGGING['loggers']['django.server']
    }
}
