import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECURE_HSTS_SECONDS = 60
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_HSTS_PRELOAD = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_BROWSER_XSS_FILTER = True
X_FRAME_OPTIONS = 'DENY'
AUTH_COOKIE_NAME = '_at'
AUTH_COOKIE_HTTPONLY = True
CSRF_HEADER_NAME = 'HTTP_X_CSRF_TOKEN'
CSRF_COOKIE_NAME = '_ct'
CSRF_COOKIE_HTTPONLY = False
CORS_ALLOW_HEADERS = [
    'accept',
    'accept-encoding',
    'authorization',
    'content-type',
    'dnt',
    'origin',
    'user-agent',
    'x-csrf-token',
    'x-requested-with',
]
CORS_ALLOW_CREDENTIALS = True

ROOT_URLCONF = 'app.urls'
WSGI_APPLICATION = 'app.wsgi.application'
AUTH_USER_MODEL = 'users.User'
STATICFILES_STORAGE = 'whitenoise.storage.CompressedStaticFilesStorage'

FRONT_URL = 'https://www.multitestery.pl'
ACTIVATION_LINK_URL = FRONT_URL + '/activate'
ORDERS_NOT_PAID_URL = FRONT_URL + '/orders/not-paid'
ORDERS_PAID_URL = FRONT_URL + '/orders/paid'

MAX_USER_FORWARDING_ADDRESSES = 5

EMAIL_PORT = 587
EMAIL_USE_TLS = True

LANGUAGE_CODE = 'pl'
TIME_ZONE = 'Europe/Warsaw'
PHONENUMBER_DEFAULT_REGION = 'PL'
USE_I18N = True
USE_L10N = True
USE_TZ = True

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'corsheaders',
    'rest_framework',
    'rest_framework.authtoken',
    'app',
    'custom_auth',
    'announcements',
    'pages',
    'users',
    'products',
    'summary',
    'django_cleanup.apps.CleanupConfig'
]

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware'
]

# noinspection PyUnresolvedReferences
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages'
            ]
        }
    }
]

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    }
]

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'custom_auth.utils.HttpOnlyCookieTokenAuthentication',
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication'
    ],
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 25,
    'EXCEPTION_HANDLER': 'app.utils.views.custom_exception_handler'
}
