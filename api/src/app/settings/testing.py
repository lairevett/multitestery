from .prod import *


COOKIES_DOMAIN = '.multitestery.lairevett.mywire.org'
CORS_ORIGIN_WHITELIST = ['https://www.multitestery.lairevett.mywire.org']
CSRF_TRUSTED_ORIGINS = ['www.multitestery.lairevett.mywire.org']
ALLOWED_HOSTS = ['www.api.multitestery.lairevett.mywire.org']

# noinspection PyUnresolvedReferences
STATIC_ROOT = '/usr/local/www/static.multitestery.lairevett.mywire.org/static'
# noinspection PyUnresolvedReferences
MEDIA_ROOT = '/usr/local/www/static.multitestery.lairevett.mywire.org/media'
STATIC_URL = 'https://www.static.multitestery.lairevett.mywire.org/static/'
MEDIA_URL = 'https://www.static.multitestery.lairevett.mywire.org/media/'

DEFAULT_FROM_EMAIL = EMAIL_USER if '@' in EMAIL_USER else f'{EMAIL_USER}@multitestery.lairevett.mywire.org'
