from django.views.decorators.csrf import ensure_csrf_cookie
from rest_framework.decorators import api_view
from rest_framework.response import Response


@api_view(['GET'])
@ensure_csrf_cookie
def ping(request):
    return Response('pong')  # pragma: no cover
