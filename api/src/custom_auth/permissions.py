from rest_framework.permissions import BasePermission


class IsLoggedInUserOrdinary(BasePermission):

    def has_permission(self, request, view):
        return request.user.is_authenticated and not request.user.is_staff

    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view)  # pragma: no cover
