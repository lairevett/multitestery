from unittest.mock import patch

from django.conf import settings
from django.test import TestCase
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from rest_framework import status

from app.utils.cache import get_cache_key_for_instance
from users.models import User
from users.serializers import UserSerializerV1
from users.utils.tests import APITestCaseWithUsers
from users.views import UserViewSetV1
from .utils import get_decoded_uid, get_encoded_uid, get_activation_token


class AuthUtilsTestCase(TestCase):
    test_pk = 123

    def test_get_encoded_uid_returns_base64_encoded_value(self):
        encoded_uid = urlsafe_base64_encode(str(self.test_pk).encode('utf-8'))
        self.assertEqual(get_encoded_uid(self.test_pk), encoded_uid)

    def test_get_decoded_uid_returns_base64_decoded_value(self):
        encoded_uid = urlsafe_base64_encode(str(self.test_pk).encode('utf-8'))
        decoded_uid = urlsafe_base64_decode(encoded_uid).decode()
        self.assertEqual(get_decoded_uid(encoded_uid), decoded_uid)
        self.assertEqual(int(decoded_uid), self.test_pk)


class CustomAuthViewsAPITestCase(APITestCaseWithUsers):
    base_endpoint = '/v1/auth/'
    retrieve_user_endpoint = f'{base_endpoint}user/'
    sign_up_endpoint = f'{base_endpoint}sign-up/'
    sign_in_endpoint = f'{base_endpoint}sign-in/'
    sign_out_endpoint = f'{base_endpoint}sign-out/'
    resend_activation_link_endpoint = f'{base_endpoint}resend-activation-link/'
    activate_endpoint = f'{base_endpoint}activate/'
    change_email_endpoint = f'{base_endpoint}change-email/'
    change_password_endpoint = f'{base_endpoint}change-password/'
    change_wants_newsletters_endpoint = f'{base_endpoint}change-wants-newsletters/'
    delete_user_endpoint = f'{base_endpoint}delete-user/'

    dummy_sign_up_payload = {
        'username': 'non_existent_user',
        'email': 'non_existent_user@localhost.localdomain',
        'password': 'password'
    }

    dummy_change_email_payload = {
        'password': 'password',
        'new_email': 'john_doe@localhost.localdomain'
    }

    dummy_change_password_payload = {
        'old_password': 'password',
        'new_password': 'password123'
    }

    dummy_change_wants_newsletters_payload = {'wants_newsletters': False}

    def setUp(self):
        super().setUp()
        self.dummy_sign_in_payload = {
            'username': self.ordinary_user.username,
            'password': 'password'
        }

    def test_retrieve_user_as_anonymous_user_is_prohibited(self):
        response = self.client.get(self.retrieve_user_endpoint)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_retrieve_user_as_ordinary_user_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.get(self.retrieve_user_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_user_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.retrieve_user_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_user_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.get(self.retrieve_user_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_user_responds_with_currently_logged_in_serialized_user_data(self):
        user = self.ordinary_user
        self.client.force_login(user)

        serialized_user_from_response = self.client.get(self.retrieve_user_endpoint).data
        serialized_user = UserSerializerV1(user).data
        self.assertDictEqual(serialized_user_from_response, serialized_user)

    def test_sign_up_as_anonymous_user_is_permitted(self):
        response = self.client.post(self.sign_up_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_sign_up_as_ordinary_user_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.post(self.sign_up_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_sign_up_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.post(self.sign_up_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_sign_up_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.post(self.sign_up_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    @patch('users.serializers.UserSerializerV1.is_valid')
    def test_sign_up_serializer_validation_is_called_with_raise_exception(self, mocked_method):
        # Mocked .is_valid() raises exception when trying to call .save().
        with self.assertRaisesMessage(Exception, 'You must call `.is_valid()` before calling `.save()`.'):
            self.client.post(self.sign_up_endpoint, self.dummy_sign_up_payload, format='json')
            mocked_method.assert_called_with(raise_exception=True)

    @patch('users.serializers.UserSerializerV1.save')
    def test_sign_up_serializer_save_is_called(self, mocked_method):
        self.client.post(self.sign_up_endpoint, self.dummy_sign_up_payload, format='json')
        self.assertTrue(mocked_method.called)

    def test_sign_up_password_is_hashed(self):
        self.client.post(self.sign_up_endpoint, self.dummy_sign_up_payload, format='json')
        self.assertTrue(User.objects.get(username=self.dummy_sign_up_payload['username']).check_password(
            self.dummy_sign_up_payload['password']))

    @patch('django.core.cache.cache.delete')
    def test_sign_up_users_list_cache_is_deleted(self, mocked_method):
        self.client.post(self.sign_up_endpoint, self.dummy_sign_up_payload, format='json')
        mocked_method.assert_called_with(UserViewSetV1.queryset_cache_key)

    def test_sign_in_as_anonymous_user_is_permitted(self):
        response = self.client.post(self.sign_in_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_sign_in_as_ordinary_user_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.post(self.sign_in_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_sign_in_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.post(self.sign_in_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_sign_in_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.post(self.sign_in_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_sign_in_without_username_and_password_responds_with_bad_request(self):
        response = self.client.post(self.sign_in_endpoint, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_sign_in_without_username_responds_with_bad_request(self):
        response = self.client.post(self.sign_in_endpoint, {'password': '0'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_sign_in_without_password_responds_with_bad_request(self):
        response = self.client.post(self.sign_in_endpoint, {'username': '0'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_sign_in_with_wrong_credentials_responds_with_bad_request(self):
        response = self.client.post(self.sign_in_endpoint, {'username': '0', 'password': '0'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_sign_in_with_right_credentials_responds_with_is_authenticated_true(self):
        response = self.client.post(self.sign_in_endpoint, self.dummy_sign_in_payload, format='json')
        self.assertDictEqual(response.data, {'is_authenticated': True})

    def test_sign_in_with_valid_credentials_responds_with_set_cookie_csrf_token_header(self):
        response = self.client.post(self.sign_in_endpoint, self.dummy_sign_in_payload, format='json')
        self.assertIn(settings.CSRF_COOKIE_NAME, response.cookies)
        self.assertIn('SameSite=Lax', str(response.cookies[settings.CSRF_COOKIE_NAME]))

    def test_sign_in_with_valid_credentials_responds_with_set_cookie_auth_token_header(self):
        response = self.client.post(self.sign_in_endpoint, self.dummy_sign_in_payload, format='json')
        self.assertIn(settings.AUTH_COOKIE_NAME, response.cookies)
        self.assertIn('HttpOnly;', str(response.cookies[settings.AUTH_COOKIE_NAME]))
        self.assertIn('SameSite=Lax', str(response.cookies[settings.AUTH_COOKIE_NAME]))

    def test_sign_out_as_anonymous_user_is_prohibited(self):
        response = self.client.delete(self.sign_out_endpoint)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_sign_out_as_ordinary_user_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.delete(self.sign_out_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_sign_out_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.delete(self.sign_out_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_sign_out_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.delete(self.sign_out_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_sign_out_responds_with_is_authenticated_false(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.delete(self.sign_out_endpoint)
        self.assertDictEqual(response.data, {'is_authenticated': False})

    @patch('rest_framework.response.Response.delete_cookie')
    def test_sign_out_calls_response_delete_cookie_with_auth_token(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.delete(self.sign_out_endpoint)
        mocked_method.assert_called_with(settings.AUTH_COOKIE_NAME, domain=settings.COOKIES_DOMAIN)

    def test_resend_activation_link_as_anonymous_user_is_permitted(self):
        response = self.client.post(self.resend_activation_link_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_resend_activation_link_as_ordinary_user_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.post(self.resend_activation_link_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_resend_activation_link_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.post(self.resend_activation_link_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_resend_activation_link_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.post(self.resend_activation_link_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_resend_activation_link_without_email_responds_with_bad_request(self):
        response = self.client.post(self.resend_activation_link_endpoint, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_resend_activation_link_with_already_activated_user_responds_with_bad_request(self):
        response = self.client.post(self.resend_activation_link_endpoint, {'email': self.ordinary_user.email},
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_resend_activation_link_with_non_existent_user_responds_with_bad_request(self):
        response = self.client.post(self.resend_activation_link_endpoint, {'email': '0'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_resend_activation_link_with_existing_not_active_user_responds_with_ok(self):
        self.ordinary_user.is_active = False
        self.ordinary_user.save()

        response = self.client.post(self.resend_activation_link_endpoint, {'email': self.ordinary_user.email},
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_activate_as_anonymous_user_is_permitted(self):
        response = self.client.post(self.activate_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_activate_as_ordinary_user_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.post(self.activate_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_activate_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.post(self.activate_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_activate_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.post(self.activate_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_activate_without_uid_responds_with_bad_request(self):
        response = self.client.post(self.activate_endpoint, {'token': '0'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_activate_without_token_responds_with_bad_request(self):
        response = self.client.post(self.activate_endpoint, {'uid': '0'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_activate_on_non_existent_user_id_responds_with_bad_request(self):
        response = self.client.post(self.activate_endpoint, {'uid': get_encoded_uid(0), 'token': '0'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_activate_with_non_base64_encoded_uid_responds_with_bad_request(self):
        response = self.client.post(self.activate_endpoint, {'uid': '`!@#$%^&*()_+-"', 'token': '0'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_activate_on_activated_user_responds_with_bad_request(self):
        response = self.client.post(self.activate_endpoint, {'uid': get_encoded_uid(self.ordinary_user.pk),
                                                             'token': get_activation_token(self.ordinary_user)},
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_activate_user_is_updated(self):
        self.ordinary_user.is_active = False
        self.ordinary_user.save()

        self.client.post(self.activate_endpoint, {'uid': get_encoded_uid(self.ordinary_user.pk),
                                                  'token': get_activation_token(self.ordinary_user)}, format='json')
        self.assertTrue(User.objects.get(pk=self.ordinary_user.pk).is_active)

    def test_change_email_as_anonymous_user_is_prohibited(self):
        response = self.client.patch(self.change_email_endpoint)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_change_email_as_ordinary_user_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.patch(self.change_email_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_change_email_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.patch(self.change_email_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_change_email_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.patch(self.change_email_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_change_email_with_empty_request_responds_with_bad_request(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.patch(self.change_email_endpoint, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_change_email_without_password_responds_with_bad_request(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.patch(self.change_email_endpoint, {'new_email': '0'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_change_email_without_new_email_responds_with_bad_request(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.patch(self.change_email_endpoint, {'password': 'password'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_change_email_with_wrong_password_responds_with_bad_request(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.patch(self.change_email_endpoint,
                                     {'password': '0', 'new_email': 'john_doe@localhost.localdomain'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @patch('users.serializers.UserSerializerV1.is_valid')
    def test_change_email_serializer_validation_is_called_with_raise_exception(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        # Mocked .is_valid() raises exception when trying to call .save().
        with self.assertRaisesMessage(Exception, 'You must call `.is_valid()` before calling `.save()`.'):
            self.client.patch(self.change_email_endpoint, self.dummy_change_email_payload, format='json')
            mocked_method.assert_called_with(raise_exception=True)

    @patch('users.serializers.UserSerializerV1.save')
    def test_change_email_serializer_save_is_called(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.patch(self.change_email_endpoint, self.dummy_change_email_payload, format='json')
        self.assertTrue(mocked_method.called)

    @patch('django.core.cache.cache.set')
    def test_change_email_user_instance_cache_is_updated(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.patch(self.change_email_endpoint, self.dummy_change_email_payload, format='json')
        mocked_method.assert_called_with(
            get_cache_key_for_instance(UserViewSetV1.queryset_cache_key, self.ordinary_user.pk),
            User.objects.get(pk=self.ordinary_user.pk))

    @patch('django.core.cache.cache.delete')
    def test_change_email_user_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.patch(self.change_email_endpoint, self.dummy_change_email_payload, format='json')
        mocked_method.assert_called_with(UserViewSetV1.queryset_cache_key)

    def test_change_email_user_is_updated(self):
        self.client.force_login(self.ordinary_user)

        self.client.patch(self.change_email_endpoint, self.dummy_change_email_payload, format='json')
        self.assertEqual(User.objects.get(pk=self.ordinary_user.pk).email, self.dummy_change_email_payload['new_email'])

    def test_change_password_as_anonymous_user_is_prohibited(self):
        response = self.client.patch(self.change_password_endpoint)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_change_password_as_ordinary_user_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.patch(self.change_password_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_change_password_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.patch(self.change_password_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_change_password_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.patch(self.change_password_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_change_password_with_empty_request_responds_with_bad_request(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.patch(self.change_password_endpoint, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_change_password_without_old_password_responds_with_bad_request(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.patch(self.change_password_endpoint, {'old_password': '0'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_change_password_without_new_password_responds_with_bad_request(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.patch(self.change_password_endpoint, {'new_password': '0'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_change_password_with_wrong_old_password_responds_with_bad_request(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.patch(self.change_password_endpoint, {'old_password': '0', 'new_password': '0'},
                                     format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @patch('custom_auth.views.make_password')
    def test_change_password_new_password_is_hashed_before_save(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.patch(self.change_password_endpoint, self.dummy_change_password_payload, format='json')
        mocked_method.assert_called_with(self.dummy_change_password_payload['new_password'])

    @patch('users.serializers.UserSerializerV1.is_valid')
    def test_change_password_serializer_validation_with_raise_exception_is_called(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        # Mocked .is_valid() raises exception when trying to call .save().
        with self.assertRaisesMessage(Exception, 'You must call `.is_valid()` before calling `.save()`.'):
            self.client.patch(self.change_password_endpoint, self.dummy_change_password_payload, format='json')
            mocked_method.assert_called_with(raise_exception=True)

    @patch('users.serializers.UserSerializerV1.save')
    def test_change_password_serializer_save_is_called(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.patch(self.change_password_endpoint, self.dummy_change_password_payload, format='json')
        self.assertTrue(mocked_method.called)

    @patch('custom_auth.views.delete_authentication_token')
    def test_change_password_delete_auth_token_is_called_with_user(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.patch(self.change_password_endpoint, self.dummy_change_password_payload, format='json')
        mocked_method.assert_called_with(self.ordinary_user)

    @patch('django.core.cache.cache.set')
    def test_change_password_user_instance_cache_is_updated(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.patch(self.change_password_endpoint, self.dummy_change_password_payload, format='json')
        mocked_method.assert_called_with(
            get_cache_key_for_instance(UserViewSetV1.queryset_cache_key, self.ordinary_user.pk),
            User.objects.get(pk=self.ordinary_user.pk))

    @patch('django.core.cache.cache.delete')
    def test_change_password_user_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.patch(self.change_password_endpoint, self.dummy_change_password_payload, format='json')
        mocked_method.assert_called_with(UserViewSetV1.queryset_cache_key)

    @patch('rest_framework.response.Response.delete_cookie')
    def test_change_password_responds_with_set_cookie_auth_token_to_empty_header(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.patch(self.change_password_endpoint, self.dummy_change_password_payload, format='json')
        mocked_method.assert_called_with(settings.AUTH_COOKIE_NAME, domain=settings.COOKIES_DOMAIN)

    def test_change_password_user_is_updated(self):
        self.client.force_login(self.ordinary_user)

        self.client.patch(self.change_password_endpoint, self.dummy_change_password_payload, format='json')
        self.assertTrue(User.objects.get(pk=self.ordinary_user.pk).check_password(
            self.dummy_change_password_payload['new_password']))

    def test_change_wants_newsletters_as_anonymous_user_is_prohibited(self):
        response = self.client.patch(self.change_wants_newsletters_endpoint)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_change_wants_newsletters_as_ordinary_user_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.patch(self.change_wants_newsletters_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_change_wants_newsletters_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.patch(self.change_wants_newsletters_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_change_wants_newsletters_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.patch(self.change_wants_newsletters_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_change_wants_newsletters_with_empty_request_responds_with_bad_request(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.patch(self.change_wants_newsletters_endpoint, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @patch('users.serializers.UserSerializerV1.is_valid')
    def test_change_wants_newsletters_serializer_validation_with_raise_exception_is_called(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        with self.assertRaisesMessage(Exception, 'You must call `.is_valid()` before calling `.save()`.'):
            self.client.patch(self.change_wants_newsletters_endpoint, self.dummy_change_wants_newsletters_payload,
                              format='json')
            mocked_method.assert_called_with(raise_exception=True)

    @patch('users.serializers.UserSerializerV1.save')
    def test_change_wants_newsletters_serializer_save_is_called(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.patch(self.change_wants_newsletters_endpoint, self.dummy_change_wants_newsletters_payload,
                          format='json')
        self.assertTrue(mocked_method.called)

    @patch('django.core.cache.cache.set')
    def test_change_wants_newsletters_user_instance_cache_is_updated(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.patch(self.change_wants_newsletters_endpoint, self.dummy_change_wants_newsletters_payload,
                          format='json')
        mocked_method.assert_called_with(
            get_cache_key_for_instance(UserViewSetV1.queryset_cache_key, self.ordinary_user.pk),
            User.objects.get(pk=self.ordinary_user.pk))

    @patch('django.core.cache.cache.delete')
    def test_change_wants_newsletters_user_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.patch(self.change_wants_newsletters_endpoint, self.dummy_change_wants_newsletters_payload,
                          format='json')
        mocked_method.assert_called_with(UserViewSetV1.queryset_cache_key)

    def test_change_wants_newsletters_updates_user(self):
        self.client.force_login(self.ordinary_user)

        self.client.patch(self.change_wants_newsletters_endpoint, {'wants_newsletters': False}, format='json')
        self.assertFalse(User.objects.get(pk=self.ordinary_user.pk).wants_newsletters)

        self.client.patch(self.change_wants_newsletters_endpoint, {'wants_newsletters': True}, format='json')
        self.assertTrue(User.objects.get(pk=self.ordinary_user.pk).wants_newsletters)

    def test_delete_user_as_anonymous_user_is_prohibited(self):
        response = self.client.patch(self.delete_user_endpoint)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_user_as_ordinary_user_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.patch(self.delete_user_endpoint)
        self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_user_as_staff_user_is_prohibited(self):
        self.client.force_login(self.staff_user)

        response = self.client.patch(self.delete_user_endpoint)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_user_as_super_user_is_prohibited(self):
        self.client.force_login(self.super_user)

        response = self.client.patch(self.delete_user_endpoint)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_user_with_empty_request_responds_with_bad_request(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.patch(self.delete_user_endpoint, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete_user_with_wrong_password_responds_with_bad_request(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.patch(self.delete_user_endpoint, {'password': '0'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete_user_wants_newsletters_is_set_to_false_and_is_deleted_is_set_to_true(self):
        self.client.force_login(self.ordinary_user)

        self.client.patch(self.delete_user_endpoint, {'password': 'password'}, format='json')
        user_after_update = User.objects.get(pk=self.ordinary_user.pk)
        self.assertFalse(user_after_update.wants_newsletters)
        self.assertTrue(user_after_update.is_deleted)

    @patch('custom_auth.views.delete_authentication_token')
    def test_delete_user_delete_authentication_token_is_called_with_user(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.patch(self.delete_user_endpoint, {'password': 'password'}, format='json')
        mocked_method.assert_called_with(self.ordinary_user)

    @patch('django.core.cache.cache.delete')
    def test_delete_user_user_instance_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.patch(self.delete_user_endpoint, {'password': 'password'}, fromat='json')
        mocked_method.assert_any_call(
            get_cache_key_for_instance(UserViewSetV1.queryset_cache_key, self.ordinary_user.pk))

    @patch('django.core.cache.cache.delete')
    def test_delete_user_user_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.patch(self.delete_user_endpoint, {'password': 'password'}, format='json')
        mocked_method.assert_any_call(UserViewSetV1.queryset_cache_key)

    @patch('rest_framework.response.Response.delete_cookie')
    def test_delete_user_responds_with_set_cookie_auth_token_to_empty_header(self, mocked_method):
        self.client.force_login(self.ordinary_user)

        self.client.patch(self.delete_user_endpoint, {'password': 'password'}, fromat='json')
        mocked_method.assert_called_with(settings.AUTH_COOKIE_NAME, domain=settings.COOKIES_DOMAIN)
