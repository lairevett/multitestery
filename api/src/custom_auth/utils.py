import logging
import smtplib

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.response import Response

from users.tokens import UserActivationTokenGenerator

logger = logging.getLogger(__name__)


class HttpOnlyCookieTokenAuthentication(TokenAuthentication):

    def authenticate(self, request):
        token_name = settings.AUTH_COOKIE_NAME
        if token_name in request.COOKIES and 'HTTP_AUTHORIZATION' not in request.META:
            return self.authenticate_credentials(request.COOKIES[token_name])

        return super().authenticate(request)


def get_encoded_uid(user_id):
    return urlsafe_base64_encode(str(user_id).encode('utf-8'))


def get_decoded_uid(uid):
    return urlsafe_base64_decode(uid).decode()


def get_activation_token(user):
    return UserActivationTokenGenerator().make_token(user)


def get_authentication_token(user):
    token, _ = Token.objects.get_or_create(user=user)
    return token.key


def delete_authentication_token(user):
    try:
        user.auth_token.delete()
    except ObjectDoesNotExist:
        pass


def send_activation_link(user):
    encoded_uid = get_encoded_uid(user.pk)
    token = get_activation_token(user)
    activation_link = f'{settings.ACTIVATION_LINK_URL}/{encoded_uid}/{token}'

    try:
        user.email_user(
            f'Link aktywacyjny dla konta {user.username}',
            render_to_string('custom_auth/activation_email.html', {'username': user.username,
                                                                   'activation_link': activation_link})
        )
    except (smtplib.SMTPConnectError, smtplib.SMTPServerDisconnected, smtplib.SMTPAuthenticationError):
        logger.critical('Check your django settings for SMTP connection credentials.')
        return Response({'errors': ['Coś poszło nie tak podczas próby wysłania linku aktywacyjnego.']},
                        status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    except smtplib.SMTPException as e:
        logger.critical(e)
        return Response({'errors': ['Coś poszło nie tak podczas próby wysłania linku aktywacyjnego.']},
                        status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    except Exception as e:
        logger.critical(e)
        return Response({'errors': ['Coś poszło nie tak.']}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    return Response({'message': ['Link aktywacyjny został wysłany pomyślnie.']})
