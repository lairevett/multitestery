from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth.hashers import make_password
from django.core.cache import cache
from django.middleware.csrf import rotate_token as rotate_csrf_token
from django.views.decorators.csrf import csrf_protect
from rest_framework import permissions, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from app.utils.cache import get_cache_key_for_instance
from custom_auth.permissions import IsLoggedInUserOrdinary
from custom_auth.utils import get_decoded_uid, send_activation_link, get_authentication_token, \
    delete_authentication_token
from users.models import User
from users.serializers import UserSerializerV1
from users.tokens import UserActivationTokenGenerator
from users.views import UserViewSetV1


@api_view(['GET'])
@permission_classes([permissions.IsAuthenticated])
def retrieve_user_v1(request):
    serializer = UserSerializerV1(request.user)
    return Response(serializer.data)


@api_view(['POST'])
@permission_classes([permissions.AllowAny])
@csrf_protect
def sign_up_v1(request):
    data = {
        'username': request.data.get('username'),
        'email': request.data.get('email'),
        'password': request.data.get('password')
    }

    serializer = UserSerializerV1(data=data)
    serializer.is_valid(raise_exception=True)
    user = serializer.save(password=make_password(data['password']))

    cache.delete(UserViewSetV1.queryset_cache_key)

    return send_activation_link(user)


@api_view(['POST'])
@permission_classes([permissions.AllowAny])
@csrf_protect
def sign_in_v1(request):
    username = request.data.get('username')
    password = request.data.get('password')
    if not username or not password:
        raise ValidationError('Musisz podać nazwę użytkownika, jak i hasło.')

    user = authenticate(username=username, password=password)
    if not user or user.is_deleted:
        raise ValidationError('Wprowadzone dane są niepoprawne', 'invalid')

    # TODO: implement token expiration
    response = Response({'is_authenticated': True})

    response.set_cookie(settings.CSRF_COOKIE_NAME, rotate_csrf_token(request), domain=settings.COOKIES_DOMAIN,
                        secure=settings.CSRF_COOKIE_SECURE, samesite='Lax')

    response.set_cookie(settings.AUTH_COOKIE_NAME, get_authentication_token(user), domain=settings.COOKIES_DOMAIN,
                        httponly=True, secure=settings.AUTH_COOKIE_SECURE, samesite='Lax')
    return response


@api_view(['DELETE'])
@permission_classes([permissions.IsAuthenticated])
@csrf_protect
def sign_out_v1(request):
    response = Response({'is_authenticated': False})
    response.delete_cookie(settings.AUTH_COOKIE_NAME, domain=settings.COOKIES_DOMAIN)
    return response


@api_view(['POST'])
@permission_classes([permissions.AllowAny])
@csrf_protect
def resend_activation_link_v1(request):
    email = request.data.get('email')
    if not email:
        raise ValidationError('Musisz podać adres email.')

    try:
        user = User.objects.get(email=email, is_deleted=False)
        if user.is_active:
            raise ValidationError('Twoje konto jest już aktywne.')

        # Returns 200 when email is sent successfully, otherwise 500 with error string.
        return send_activation_link(user)
    except User.DoesNotExist:
        raise ValidationError('Konto z podanym adresem email nie istnieje.')


@api_view(['POST'])
@permission_classes([permissions.AllowAny])
@csrf_protect
def activate_v1(request):
    encoded_uid = request.data.get('uid')
    token = request.data.get('token')
    if not encoded_uid or not token:
        raise ValidationError('Musisz podać uid, jak i token.')

    try:
        pk = int(get_decoded_uid(encoded_uid))
        try:
            # Assuming that user.pk == user.id.
            cache_key = get_cache_key_for_instance(UserViewSetV1.queryset_cache_key, pk)
            user = cache.get(cache_key)
            if not user:
                user = User.objects.get(pk=pk)  # Raises User.DoesNotExist exception, which is then caught below.
                cache.set(cache_key, user)

            if not user.is_active and UserActivationTokenGenerator().check_token(user, token):
                user.is_active = True
                cache.set(cache_key, user.save())
                cache.delete(UserViewSetV1.queryset_cache_key)

                return Response({'message': 'Aktywacja konta przebiegła pomyślnie.'})
        except User.DoesNotExist:
            raise ValidationError(
                'Próba aktywacji konta nie powiodła się, sprawdź, czy link aktywacyjny jest prawidłowy.')
    except ValueError:
        raise ValidationError('Próba aktywacji konta nie powiodła się, sprawdź, czy link aktywacyjny jest prawidłowy.')

    raise ValidationError('Próba aktywacji konta nie powiodła się, '
                          'twoje konto jest już aktywne lub link jest niepoprawny.')


@api_view(['PATCH'])
@permission_classes([permissions.IsAuthenticated])
@csrf_protect
def change_email_v1(request):
    password = request.data.get('password')
    new_email = request.data.get('new_email')
    if not password or not new_email:
        raise ValidationError('Musisz podać hasło, jak i nowy email.')

    if not request.user.check_password(password):
        raise ValidationError({'password': 'Wprowadzone hasło jest niepoprawne.'})

    serializer = UserSerializerV1(request.user, data={'email': new_email}, partial=True)
    serializer.is_valid(raise_exception=True)

    cache.set(get_cache_key_for_instance(UserViewSetV1.queryset_cache_key, request.user.pk), serializer.save())
    cache.delete(UserViewSetV1.queryset_cache_key)

    return Response(serializer.data)


@api_view(['PATCH'])
@permission_classes([permissions.IsAuthenticated])
@csrf_protect
def change_password_v1(request):
    old_password = request.data.get('old_password')
    new_password = request.data.get('new_password')
    if not old_password or not new_password:
        raise ValidationError('Musisz podać stare, jak i nowe hasło.')

    if not request.user.check_password(old_password):
        raise ValidationError({'password': 'Wprowadzone hasło jest niepoprawne.'})

    serializer = UserSerializerV1(request.user, data={'password': make_password(new_password)}, partial=True)
    serializer.is_valid(raise_exception=True)

    delete_authentication_token(request.user)

    cache.set(get_cache_key_for_instance(UserViewSetV1.queryset_cache_key, request.user.pk), serializer.save())
    cache.delete(UserViewSetV1.queryset_cache_key)

    response = Response(serializer.data)
    response.delete_cookie(settings.AUTH_COOKIE_NAME, domain=settings.COOKIES_DOMAIN)
    return response


@api_view(['PATCH'])
@permission_classes([permissions.IsAuthenticated])
@csrf_protect
def change_wants_newsletters_v1(request):
    wants_newsletters = request.data.get('wants_newsletters')
    if wants_newsletters is None:
        raise ValidationError('Musisz podać wartość tego pola.')

    serializer = UserSerializerV1(request.user, data={'wants_newsletters': wants_newsletters}, partial=True)
    serializer.is_valid(raise_exception=True)

    cache.set(get_cache_key_for_instance(UserViewSetV1.queryset_cache_key, request.user.pk), serializer.save())
    cache.delete(UserViewSetV1.queryset_cache_key)

    return Response(serializer.data)


@api_view(['PATCH'])
@permission_classes([IsLoggedInUserOrdinary])
@csrf_protect
def delete_user_v1(request):
    password = request.data.get('password')

    if not password:
        raise ValidationError('Musisz podać hasło.')

    if not request.user.check_password(password):
        raise ValidationError('Wprowadzone hasło jest niepoprawne.')

    request.user.wants_newsletters = False
    request.user.is_deleted = True
    request.user.save()

    delete_authentication_token(request.user)

    cache.delete(get_cache_key_for_instance(UserViewSetV1.queryset_cache_key, request.user.pk))
    cache.delete(UserViewSetV1.queryset_cache_key)

    response = Response(status=status.HTTP_204_NO_CONTENT)
    response.delete_cookie(settings.AUTH_COOKIE_NAME, domain=settings.COOKIES_DOMAIN)
    return response
