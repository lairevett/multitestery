from django.urls import path
from .views import (
    sign_up_v1, sign_in_v1, sign_out_v1, resend_activation_link_v1, retrieve_user_v1, activate_v1, change_email_v1,
    change_password_v1, change_wants_newsletters_v1, delete_user_v1
)


urlpatterns = [
    path('v1/auth/sign-up/', sign_up_v1),
    path('v1/auth/sign-in/', sign_in_v1),
    path('v1/auth/sign-out/', sign_out_v1),
    path('v1/auth/resend-activation-link/', resend_activation_link_v1),
    path('v1/auth/user/', retrieve_user_v1),
    path('v1/auth/activate/', activate_v1),
    path('v1/auth/change-email/', change_email_v1),
    path('v1/auth/change-password/', change_password_v1),
    path('v1/auth/change-wants-newsletters/', change_wants_newsletters_v1),
    path('v1/auth/delete-user/', delete_user_v1)
]
