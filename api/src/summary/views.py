from rest_framework.decorators import api_view
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response

from announcements.models import Announcement
from pages.models import Page
from products.models import Product
from users.models import User, Order


@api_view(['GET'])
@permission_classes([IsAdminUser])
def summary(request):
    all_users = User.objects.filter(is_deleted=False).count()
    all_products = Product.objects.filter(is_deleted=False).count()
    not_available_products = Product.objects.filter(is_deleted=False, quantity=0).count()
    available_products = all_products - not_available_products
    all_orders = Order.objects.count()
    paid_orders = Order.objects.filter(status=Order.PAID).count()
    canceled_orders = Order.objects.filter(status=Order.CANCELED).count()
    sent_orders = Order.objects.filter(status=Order.SENT).count()
    not_paid_orders = all_orders - paid_orders - canceled_orders - sent_orders
    all_announcements = Announcement.objects.count()
    all_pages = Page.objects.count()

    return Response(
        {'all_users': all_users, 'all_products': all_products, 'not_available_products': not_available_products,
         'available_products': available_products, 'all_orders': all_orders, 'not_paid_orders': not_paid_orders,
         'paid_orders': paid_orders, 'canceled_orders': canceled_orders, 'sent_orders': sent_orders,
         'all_announcements': all_announcements, 'all_pages': all_pages})
