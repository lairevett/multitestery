from django.db import models


class Page(models.Model):
    slug = models.SlugField(unique=True)
    priority = models.PositiveIntegerField(unique=True)
    title = models.CharField(max_length=50)
    content = models.TextField()
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.title
