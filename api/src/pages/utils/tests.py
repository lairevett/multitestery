from pages.factories import ActivePageFactory, InactivePageFactory
from pages.models import Page
from users.utils.tests import APITestCaseWithUsers


class PageAPITestCase(APITestCaseWithUsers):
    base_endpoint = '/v1/pages/'
    list_inactive_endpoint = f'{base_endpoint}?active=0'
    list_active_endpoint = f'{base_endpoint}?active=1'

    def setUp(self):
        super().setUp()

        self.inactive_page = InactivePageFactory()
        self.active_page = ActivePageFactory()

        self.details_slug = self.active_page.slug
        self.details_endpoint = f'{self.base_endpoint}{self.active_page.slug}/'

        last = Page.objects.last()
        self.dummy_payload = {
            'slug': f'dummy-slug-{last.id + 1}',  # Slug has to be unique.
            'priority': last.priority + 1,  # Priority has to be unique.
            'title': 'Dummy title',
            'content': 'Dummy content.',
            'is_active': True
        }
