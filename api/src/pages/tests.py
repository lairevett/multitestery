from unittest.mock import patch

from django.test import TestCase
from rest_framework import status

from app.utils.cache import get_cache_key_for_instance
from .models import Page
from .utils.tests import PageAPITestCase
from .views import PageViewSetV1


class PageModelTestCase(TestCase):

    def test_to_str_returns_page_title(self):
        page_title = 'Sample page title'

        page = Page()
        page.title = page_title
        self.assertEqual(str(page), page_title)


class PageSerializerAPITestCase(PageAPITestCase):

    def test_create_fields_layout_contains_content_field(self):
        self.client.force_login(self.super_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='json')
        self.assertIn('content', response.data)

    def test_update_fields_layout_contains_content_field(self):
        self.client.force_login(self.super_user)

        response = self.client.put(self.details_endpoint, self.dummy_payload, format='json')
        self.assertIn('content', response.data)

    def test_list_fields_layout_does_not_contain_content_field(self):
        response = self.client.get(self.base_endpoint)
        self.assertNotIn('content', response.data)

    def test_details_fields_layout_contains_content_field(self):
        response = self.client.get(self.details_endpoint)
        self.assertIn('content', response.data)


class PageViewAPITestCase(PageAPITestCase):

    def test_create_as_anonymous_user_is_prohibited(self):
        response = self.client.post(self.base_endpoint, self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_as_ordinary_user_is_prohibited(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.post(self.base_endpoint, self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    @patch('django.core.cache.cache.delete')
    def test_create_pages_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.post(self.base_endpoint, self.dummy_payload, format='json')
        mocked_method.assert_called_with(PageViewSetV1.queryset_cache_key)

    # noinspection DuplicatedCode
    def test_updates_as_anonymous_user_are_prohibited(self):
        response = self.client.put(self.base_endpoint, self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.patch(self.base_endpoint, {'title': 'Updated title'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_updates_as_ordinary_user_are_prohibited(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.put(self.details_endpoint, self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        response = self.client.patch(self.details_endpoint, {'title': 'Updated title'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.put(self.details_endpoint, self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.put(self.details_endpoint, self.dummy_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @patch('django.core.cache.cache.delete')
    def test_update_pages_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.put(self.details_endpoint, self.dummy_payload, format='json')
        mocked_method.assert_any_call(PageViewSetV1.queryset_cache_key)

    @patch('pages.views.cache')
    def test_update_page_instance_cache_is_replaced(self, mocked_cache):
        self.client.force_login(self.super_user)

        self.client.put(self.details_endpoint, self.dummy_payload, format='json')
        mocked_cache.delete.assert_any_call(
            get_cache_key_for_instance(PageViewSetV1.queryset_cache_key, self.details_slug))

        mocked_cache.set.assert_called_with(
            get_cache_key_for_instance(PageViewSetV1.queryset_cache_key, self.dummy_payload['slug']),
            Page.objects.get(slug=self.dummy_payload['slug']))

    def test_partial_update_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.patch(self.details_endpoint, {'title': self.dummy_payload['title']}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_partial_update_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.patch(self.details_endpoint, {'title': self.dummy_payload['title']}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @patch('django.core.cache.cache.delete')
    def test_partial_update_pages_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.super_user)

        self.client.patch(self.details_endpoint, self.dummy_payload, format='json')
        mocked_method.assert_called_with(PageViewSetV1.queryset_cache_key)

    @patch('pages.views.cache')
    def test_partial_update_with_slug_in_request_page_instance_cache_is_replaced(self, mocked_cache):
        self.client.force_login(self.super_user)

        self.client.patch(self.details_endpoint, self.dummy_payload, format='json')
        mocked_cache.delete.assert_any_call(
            get_cache_key_for_instance(PageViewSetV1.queryset_cache_key, self.details_slug))

        mocked_cache.set.assert_called_with(
            get_cache_key_for_instance(PageViewSetV1.queryset_cache_key, self.dummy_payload['slug']),
            Page.objects.get(slug=self.dummy_payload['slug']))

    @patch('django.core.cache.cache.set')
    def test_partial_update_without_slug_in_request_page_instance_cache_is_updated(self, mocked_method):
        self.client.force_login(self.super_user)

        request_body = self.dummy_payload.copy()
        del request_body['slug']

        self.client.patch(self.details_endpoint, request_body, format='json')
        mocked_method.assert_called_with(
            get_cache_key_for_instance(PageViewSetV1.queryset_cache_key, self.details_slug),
            Page.objects.get(slug=self.details_slug))

    def test_list_all_as_anonymous_user_is_permitted(self):
        response = self.client.get(self.base_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_all_as_ordinary_user_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.get(self.base_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_all_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.base_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_all_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.get(self.base_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_all_responds_with_all_pages(self):
        response = self.client.get(self.base_endpoint)
        self.assertEqual(len(response.data['results']), Page.objects.count())

    def test_list_inactive_responds_only_with_inactive_pages(self):
        response = self.client.get(self.list_inactive_endpoint)
        self.assertEqual(len(response.data['results']), Page.objects.filter(is_active=False).count())

    def test_list_active_responds_only_with_active_pages(self):
        response = self.client.get(self.list_active_endpoint)
        self.assertEqual(len(response.data['results']), Page.objects.filter(is_active=True).count())

    def test_details_as_anonymous_user_is_permitted(self):
        response = self.client.get(self.details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_details_as_ordinary_user_is_permitted(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.get(self.details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_details_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.get(self.details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_details_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.get(self.details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_as_anonymous_user_is_prohibited(self):
        response = self.client.delete(self.details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_as_ordinary_user_is_prohibited(self):
        self.client.force_login(self.ordinary_user)

        response = self.client.delete(self.details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_as_staff_user_is_permitted(self):
        self.client.force_login(self.staff_user)

        response = self.client.delete(self.details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_as_super_user_is_permitted(self):
        self.client.force_login(self.super_user)

        response = self.client.delete(self.details_endpoint)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    @patch('django.core.cache.cache.delete')
    def test_delete_pages_list_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.staff_user)

        self.client.delete(self.details_endpoint)
        mocked_method.assert_any_call(PageViewSetV1.queryset_cache_key)

    @patch('django.core.cache.cache.delete')
    def test_delete_page_instance_cache_is_deleted(self, mocked_method):
        self.client.force_login(self.staff_user)

        self.client.delete(self.details_endpoint)
        mocked_method.assert_any_call(get_cache_key_for_instance(PageViewSetV1.queryset_cache_key, self.details_slug))
