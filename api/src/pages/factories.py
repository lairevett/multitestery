import factory
from .models import Page


class ActivePageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Page

    slug = factory.Sequence(lambda n: f'default-slug-{n}')
    priority = factory.Sequence(lambda n: n)
    title = 'Default title'
    content = 'Default content'
    is_active = True


class InactivePageFactory(ActivePageFactory):
    is_active = False
