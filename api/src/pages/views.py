from django.core.cache import cache
from rest_framework.response import Response

from app.utils.cache import get_cache_key_for_instance
from app.utils.viewsets import CachingModelViewSet
from .apps import PagesConfig
from .models import Page
from .permissions import CreateUpdateDeleteIfStaff
from .serializers import PageSerializerV1


class PageViewSetV1(CachingModelViewSet):
    queryset = Page.objects.all().order_by('priority')
    lookup_field = 'slug'
    queryset_cache_key = PagesConfig.name
    serializer_class = PageSerializerV1
    permission_classes = [CreateUpdateDeleteIfStaff]
    pagination_class = None

    def get_queryset(self):
        queryset = super().get_queryset()

        active = self.request.GET.get('active')
        if active == '0':
            queryset = queryset.filter(is_active=False)
        elif active == '1':
            queryset = queryset.filter(is_active=True)

        return queryset

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({'request': self.request})
        return context

    def perform_update(self, serializer):
        """
        This viewset's lookup kwarg is slug, which is a modifiable field.
        If slug is present in patch request, then the instance's cache with the old key needs to be replaced
        with a new one from request.
        """
        current_lookup = self.kwargs[self.lookup_field]
        current_lookup_cache_key = get_cache_key_for_instance(self.queryset_cache_key, current_lookup)

        new_lookup = serializer.validated_data.get(self.lookup_field)
        if new_lookup:
            cache.delete(current_lookup_cache_key)
            cache.set(get_cache_key_for_instance(self.queryset_cache_key, new_lookup), serializer.save())
        else:
            cache.set(current_lookup_cache_key, serializer.save())

        cache.delete(self.queryset_cache_key)

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        return Response({'results': serializer.data})
