from rest_framework import serializers

from .models import Page


class PageSerializerV1(serializers.ModelSerializer):
    class Meta:
        model = Page
        fields = '__all__'

    def get_fields(self, *args, **kwargs):
        fields = super().get_fields()

        view = self.context['request'].parser_context['view']
        if view.action == 'list':
            fields.pop('content')

        return fields
