# MultiTestery API

## Introduction

-   **Name:** MultiTestery
-   **Website:** ~~[www.api.multitestery.pl](https://www.api.multitestery.pl/)~~
-   **Description:** Perfume store API for [the MultiTestery app](https://www.bitbucket.org/lairevett/multitestery/src/master/webapp/) written in Django.
-   **Additional info:**

    -   [Repository with config files](https://www.bitbucket.org/lairevett/multitestery/src/master/cfg/).

    -   ~~User can pay for his order within an app with a debit/credit card, the payments are realized by [Stripe](https://www.stripe.com/).~~

## Requirements

-   Vagrant box w/ FreeBSD 12.1
-   NGINX + Gunicorn
-   Python 3.7 + Django + Django REST Framework
-   PostgreSQL 11
-   [...](https://bitbucket.org/lairevett/multitestery/src/master/cfg/ansible/roles/api_python/files/requirements.pip)

## API v1 routes

### POST /v1/auth/sign-up/

Request for any user, creates new account and sends email activation link.

```json
{
    "username": "johndoe",
    "email": "johndoe@example.com",
    "password": "p@ssword123"
}
```

### POST /v1/auth/sign-in/

Request for unauthenticated users only, responds with authentication token on correct credentials.

```json
{
    "username": "johndoe",
    "password": "p@ssword123"
}
```

### POST /v1/auth/resend-activation-link/

Request for any user, sends activation link to provided email address.

```json
{
    "email": "johndoe@example.com"
}
```

### GET /v1/auth/user/

Request for authenticated users only, responds with user data.

```json
{
    "id": 1,
    "username": "johndoe",
    "email": "johndoe@example.com",
    "is_staff": false,
    "date_joined": "2019-08-24T13:28:26.097808+02:00"
}
```

### POST /v1/auth/activate/

Request for any user, given the user uid and token in request, activates user account.

```json
{
    "uid": "OQ",
    "token": "authentication token"
}
```

### PATCH /v1/auth/change-email/

Request for authenticated users only, given the password and new email updates user's email address.

```json
{
    "password": "somepassword",
    "new_email": "johndoe@example.com"
}
```

### PATCH /v1/auth/change-password/

Request for authenticated users only, given the old password and new password updates user's password.

```json
{
    "old_password": "someoldpassword",
    "new_password": "somenewpassword"
}
```

### PATCH /v1/auth/change-wants-newsletters/

Request for authenticated users only, given the wants_newsletters attribute opts in or out of email newsletters.

```json
{
    "wants_newsletters": false
}
```

### PATCH /v1/auth/delete-user/

Request for authenticated users only, given the password set `is_deleted=True` on user.

```json
{
    "password": "somepassword"
}
```

### GET /v1/users/[?page={page_number}]/

Request for staff only, lists basic data of all users.

```json
{
    "count": 2,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 2,
            "username": "user",
            "email": "user@example.com",
            "is_staff": false,
            "is_superuser": false,
            "is_active": true,
            "date_joined": "2019-08-23T23:58:12.850956+02:00"
        },
        {
            "id": 1,
            "username": "admin",
            "email": "admin@example.com",
            "is_staff": true,
            "is_superuser": true,
            "is_active": true,
            "date_joined": "2019-08-23T22:14:35.074880+02:00"
        }
    ]
}
```

### POST /v1/users/

Request for staff only, creates new user account.

```json
{
    "username": "johndoe",
    "email": "johndoe@example.com",
    "password": "p@ssword123",
    "is_staff": false,
    "is_superuser": false
}
```

### GET /v1/users/{user_id}/

Request for staff only, lists information about user.

```json
{
    "id": 2,
    "username": "user",
    "email": "user@example.com",
    "is_staff": false,
    "is_superuser": false,
    "is_active": true,
    "date_joined": "2019-08-23T23:58:12.850956+02:00"
}
```

### PUT/PATCH /v1/users/{user_id}

Request for staff only, edits information about user.

```json
{
    "username": "user",
    "email": "user@example.com",
    "is_staff": false,
    "is_superuser": false,
    "is_active": true
}
```

### DELETE /v1/users/{user_id}

Request for staff only, sets `is_deleted=True` on user.

### GET /v1/users/{user_id}/forwarding-addresses/

Request for staff and owner, lists all forwarding addresses assigned to user.

```json
{
    "count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 3,
            "first_name": "Jan",
            "last_name": "Kowalski",
            "phone": "+48555123456",
            "street": "Poznańska 4 m. 49",
            "city": "Poznań",
            "postal_code": "12-345"
        }
    ]
}
```

### GET /v1/users/{user_id}/forwarding-addresses/{forwarding_address_id}/

Request for staff and owner, retrieves information about specific forwarding address only.

```json
{
    "id": 3,
    "first_name": "Jan",
    "last_name": "Kowalski",
    "phone": "+48555123456",
    "street": "Poznańska 4 m. 49",
    "city": "Poznań",
    "postal_code": "12-345"
}
```

### PUT/PATCH /v1/users/{user_id}/forwarding-addresses/{forwarding_address_id}/

Request for staff and owner, edits user's forwarding address.

```json
{
    "first_name": "Jan",
    "last_name": "Kowalski",
    "phone": "+48555123456",
    "street": "Poznańska 4 m. 49",
    "city": "Poznań",
    "postal_code": "12-345"
}
```

### DELETE /v1/users/{user_id}/forwarding-addresses/{forwarding_address_id}/

Request for staff and owner, sets `is_deleted = True` on forwarding address object.

### GET /v1/users/{user_id}/orders/[?page={page_number}, ?paid={0 | 1}, ?sent={0 | 1}, ?canceled={0 | 1}]/

Request for staff and owner, lists of user's all orders.

```json
{
    "count": 7,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 11,
            "stripe_payment_id": "pi_1FB6zIGB1C4EoBOZm8HCkGh9",
            "entries": [
                {
                    "id": 6,
                    "product": {
                        "id": 1,
                        "photo": "http://127.0.0.1:3000/media/products/hb_bb_20th_edt_100ml_W3vP5CS.jpg",
                        "brand": "HUGO BOSS",
                        "model": "Bottled 20th Anniversary Edition",
                        "category": 0,
                        "unit_price": 12990,
                        "amount": 100,
                        "quantity": 16,
                        "description": "Bottled 20th Anniversary to woda toaletowa od marki Hugo Boss.\r\nDrzewno-przyprawowa kompozycja zapachowa wydana w 2018 roku. Zapach dedykowany szczęśliwemu mężczyźnie, zadowolonemu z życia.",
                        "percent_off": 0,
                        "is_available": true
                    },
                    "quantity": 2,
                    "subtotal": 25980
                }
            ],
            "forwarding_address": {
                "id": 3,
                "first_name": "Jan",
                "last_name": "Kowalski",
                "phone": "+48555123456",
                "street": "Poznańska 4 m. 49",
                "city": "Poznań",
                "postal_code": "12-345"
            },
            "total": 25980,
            "created_at": "2019-08-24T23:40:00.950281+02:00"
        },
        {
            "id": 10,
            "stripe_payment_id": "pi_1FB3zBGB1C4EoBOZZ4sIeEkY",
            "entries": [
                {
                    "id": 5,
                    "product": {
                        "id": 1,
                        "photo": "http://127.0.0.1:3000/media/products/hb_bb_20th_edt_100ml_W3vP5CS.jpg",
                        "brand": "HUGO BOSS",
                        "model": "Bottled 20th Anniversary Edition",
                        "category": 0,
                        "unit_price": 12990,
                        "amount": 100,
                        "quantity": 16,
                        "description": "Bottled 20th Anniversary to woda toaletowa od marki Hugo Boss.\r\nDrzewno-przyprawowa kompozycja zapachowa wydana w 2018 roku. Zapach dedykowany szczęśliwemu mężczyźnie, zadowolonemu z życia.",
                        "percent_off": 0,
                        "is_available": true
                    },
                    "quantity": 4,
                    "subtotal": 51960
                }
            ],
            "forwarding_address": {
                "id": 3,
                "first_name": "Jan",
                "last_name": "Kowalski",
                "phone": "+48555123456",
                "street": "Poznańska 4 m. 49",
                "city": "Poznań",
                "postal_code": "12-345"
            },
            "total": 51960,
            "created_at": "2019-08-24T20:10:06.592690+02:00"
        }
    ]
}
```

### GET /v1/users/{user_id}/orders/{order_id}/

Request for staff and owner, retrieves information about specific order id.

```json
{
    "id": 11,
    "stripe_payment_id": "pi_1FB6zIGB1C4EoBOZm8HCkGh9",
    "entries": [
        {
            "id": 6,
            "product": {
                "id": 1,
                "photo": "http://127.0.0.1:3000/media/products/hb_bb_20th_edt_100ml_W3vP5CS.jpg",
                "brand": "HUGO BOSS",
                "model": "Bottled 20th Anniversary Edition",
                "category": 0,
                "unit_price": 12990,
                "amount": 100,
                "quantity": 16,
                "description": "Bottled 20th Anniversary to woda toaletowa od marki Hugo Boss.\r\nDrzewno-przyprawowa kompozycja zapachowa wydana w 2018 roku. Zapach dedykowany szczęśliwemu mężczyźnie, zadowolonemu z życia.",
                "percent_off": 0,
                "is_available": true
            },
            "quantity": 2,
            "subtotal": 25980
        }
    ],
    "forwarding_address": {
        "id": 3,
        "first_name": "Jan",
        "last_name": "Kowalski",
        "phone": "+48555123456",
        "street": "Poznańska 4 m. 49",
        "city": "Poznań",
        "postal_code": "12-345"
    },
    "total": 25980,
    "created_at": "2019-08-24T23:40:00.950281+02:00"
}
```

### POST /v1/users/{user_id}/orders/{order_id}/cancel/

Request for staff and owner, sets the order status to cancel, it can be invoked only when order `status == 0` (placed).

### POST /v1/users/forwarding-addresses/

Request for authenticated users only, adds new forwarding address to account.

```json
{
    "first_name": "Jan",
    "last_name": "Kowalski",
    "phone": "+48555123456",
    "street": "Poznańska 4 m. 49",
    "city": "Poznań",
    "postal_code": "12-345"
}
```

### GET /v1/users/orders/[?page={page_number}, ?paid={0 | 1}, ?sent={0 | 1}, ?canceled={0 | 1}]/

Request for staff only, lists all of the orders in database.

```json
{
    "count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 11,
            "stripe_payment_id": "pi_1FB6zIGB1C4EoBOZm8HCkGh9",
            "entries": [
                {
                    "id": 6,
                    "product": {
                        "id": 1,
                        "photo": "http://127.0.0.1:3000/media/products/hb_bb_20th_edt_100ml_W3vP5CS.jpg",
                        "brand": "HUGO BOSS",
                        "model": "Bottled 20th Anniversary Edition",
                        "category": 0,
                        "unit_price": 12990,
                        "amount": 100,
                        "quantity": 16,
                        "description": "Bottled 20th Anniversary to woda toaletowa od marki Hugo Boss.\r\nDrzewno-przyprawowa kompozycja zapachowa wydana w 2018 roku. Zapach dedykowany szczęśliwemu mężczyźnie, zadowolonemu z życia.",
                        "is_sale": false,
                        "percent_off": 0
                    },
                    "quantity": 2,
                    "subtotal": 25980
                }
            ],
            "forwarding_address": {
                "id": 3,
                "first_name": "Jan",
                "last_name": "Kowalski",
                "phone": "+48555123456",
                "street": "Poznańska 4 m. 49",
                "city": "Poznań",
                "postal_code": "12-345"
            },
            "total": 25980,
            "created_at": "2019-08-24T23:40:00.950281+02:00"
        }
    ]
}
```

### GET /v1/users/orders/{order_id}/

Request for staff only, retrieves user's order.

```json
{
    "id": 11,
    "stripe_payment_id": "pi_1FB6zIGB1C4EoBOZm8HCkGh9",
    "entries": [
        {
            "id": 6,
            "product": {
                "id": 1,
                "photo": "http://127.0.0.1:3000/media/products/hb_bb_20th_edt_100ml_W3vP5CS.jpg",
                "brand": "HUGO BOSS",
                "model": "Bottled 20th Anniversary Edition",
                "category": 0,
                "unit_price": 12990,
                "amount": 100,
                "quantity": 16,
                "description": "Bottled 20th Anniversary to woda toaletowa od marki Hugo Boss.\r\nDrzewno-przyprawowa kompozycja zapachowa wydana w 2018 roku. Zapach dedykowany szczęśliwemu mężczyźnie, zadowolonemu z życia.",
                "is_sale": false,
                "percent_off": 0
            },
            "quantity": 2,
            "subtotal": 25980
        }
    ],
    "forwarding_address": {
        "id": 3,
        "first_name": "Jan",
        "last_name": "Kowalski",
        "phone": "+48555123456",
        "street": "Poznańska 4 m. 49",
        "city": "Poznań",
        "postal_code": "12-345"
    },
    "total": 25980,
    "created_at": "2019-08-24T23:40:00.950281+02:00"
}
```

### POST /v1/users/orders/{order_id}/update-status/

Request for staff users only, when invoked with:

-   `status == -1` - sets order status to canceled;
-   `status == 0` - sets order status to placed;
-   `status == 1` - sets order status to paid;
-   `status == 2` - checks the quantity of all order entries, if they are sufficient sets the order status to sent and substracts proper amount of quantity from product entry.

When invoked on order with `status == 2`, sets order status accordingly and adds back the subtracted product' quantities to product' entries.

```json
{
    "status": 3
}
```

### POST /v1/users/orders/

Request for authenticated users only, places new order.

```json
{
    "entries": [
        {
            "product": 1,
            "quantity": 3
        },
        {
            "product": 2,
            "quantity": 1
        }
    ],
    "forwarding_address": 1
}
```

### GET /v1/products/[?page={page_number}, ?category={0 | 1 | 2}, ?featured={0 | 1}, ?search={string}, ?available={0 | 1}, ?on_sale={0 | 1}]/

Request for any user, lists all available products, only staff can see `percent_off` field if `is_sale == False`.

```json
{
    "count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 1,
            "photo_small": "http://127.0.0.1:3000/media/products/hb_bb_20th_edt_100ml_snall_W3vP5CS.jpg",
            "photo_medium": "http://127.0.0.1:3000/media/products/hb_bb_20th_edt_100ml_medium_W3vP5CS.jpg",
            "photo_big": "http://127.0.0.1:3000/media/products/hb_bb_20th_edt_100ml_big_W3vP5CS.jpg",
            "brand": "HUGO BOSS",
            "model": "Bottled 20th Anniversary Edition",
            "category": 0,
            "unit_price": 12990,
            "amount": 100,
            "quantity": 16,
            "description": "Bottled 20th Anniversary to woda toaletowa od marki Hugo Boss.\r\nDrzewno-przyprawowa kompozycja zapachowa wydana w 2018 roku. Zapach dedykowany szczęśliwemu mężczyźnie, zadowolonemu z życia.",
            "top_notes": "Nuty zapachowe głowy.",
            "heart_notes": "Nuty zapachowe serca.",
            "base_notes": "Nuty zapachowe podstawy.",
            "is_sale": false,
            "percent_off": 0,
            "is_featured": false,
            "is_available": true
        }
    ]
}
```

### GET /v1/products/all/

```json
{
    "results": [
        {
            "id": 1,
            "photo_small": "http://127.0.0.1:3000/media/products/hb_bb_20th_edt_100ml_snall_W3vP5CS.jpg",
            "photo_medium": "http://127.0.0.1:3000/media/products/hb_bb_20th_edt_100ml_medium_W3vP5CS.jpg",
            "photo_big": "http://127.0.0.1:3000/media/products/hb_bb_20th_edt_100ml_big_W3vP5CS.jpg",
            "brand": "HUGO BOSS",
            "model": "Bottled 20th Anniversary Edition",
            "category": 0,
            "unit_price": 12990,
            "amount": 100,
            "quantity": 16,
            "description": "Bottled 20th Anniversary to woda toaletowa od marki Hugo Boss.\r\nDrzewno-przyprawowa kompozycja zapachowa wydana w 2018 roku. Zapach dedykowany szczęśliwemu mężczyźnie, zadowolonemu z życia.",
            "top_notess": "Nuty zapachowe głowy.",
            "heart_notes": "Nuty zapachowe serca.",
            "base_notes": "Nuty zapachowe podstawy.",
            "is_sale": false,
            "percent_off": 0,
            "is_featured": false,
            "is_available": true
        }
    ]
}
```

### POST /v1/products/

Request for staff only, adds new product to database.

```json
{
    "photo_big": /* image file object */,
    "brand": "Chanel",
    "model": "Bleu de Chanel",
    "category": 0,
    "unit_price": 9999,
    "amount": 100,
    "quantity": 16,
    "description": "Zapach obdarzony urzekającą aurą składa hołd męskiej wolności. Ponadczasowy, nonkonformistyczny zapach skryty w enigmatycznym ciemnoniebieskim flakonie. Spotkanie siły i elegancji. Ten aromatyczno-świeży zapach łączy orzeźwiającą świeżość cytrusów i siłę akordu aromatycznego z drzewnym tchnieniem wytrawnych nut cedrowych i ambrowych. Drewno sandałowe z Nowej Kaledonii dodaje mu ciepła i zmysłowości.",
    "top_notes": "Nuty zapachowe głowy.",
    "heart_notes": "Nuty zapachowe serca.",
    "base_notes": "Nuty zapachowe podstawy.",
    "percent_off": 0
}
```

### PUT/PATCH /v1/products/{product_id}/

Request for staff only, updates added product.

```json
{
    "photo_big": /* image file object */,
    "brand": "Chanel",
    "model": "Bleu de Chanel",
    "category": 0,
    "unit_price": 9999,
    "amount": 100,
    "quantity": 16,
    "description": "Zapach obdarzony urzekającą aurą składa hołd męskiej wolności. Ponadczasowy, nonkonformistyczny zapach skryty w enigmatycznym ciemnoniebieskim flakonie. Spotkanie siły i elegancji. Ten aromatyczno-świeży zapach łączy orzeźwiającą świeżość cytrusów i siłę akordu aromatycznego z drzewnym tchnieniem wytrawnych nut cedrowych i ambrowych. Drewno sandałowe z Nowej Kaledonii dodaje mu ciepła i zmysłowości.",
    "top_notes": "Nuty zapachowe głowy.",
    "heart_notes": "Nuty zapachowe serca.",
    "base_notes": "Nuty zapachowe podstawy.",
    "percent_off": 0
}
```

### DELETE /v1/products/{product_id}/

Request for staff only, sets `is_deleted == True` on product object.

### GET /v1/announcements/

Request for all users, lists announcements.

```json
{
    "results": [
        {
            "id": 1,
            "priority": 0,
            "image": "https://www.api.multitestery.pl/media/announcements/Tytu%C5%82.jpg",
            "title": "Tytuł",
            "description": "Opis pod tytułem ogłoszenia.",
            "is_active": true
        }
    ]
}
```

### GET /v1/announcements/{announcement_id}/

Request for all users, retrieves single announcement object.

```json
{
    "id": 1,
    "priority": 0,
    "image": "https://www.api.multitestery.pl/media/announcements/Tytu%C5%82.jpg",
    "title": "Tytuł",
    "description": "Opis pod tytułem ogłoszenia.",
    "is_active": true
}
```

### POST /v1/announcements/

Request for staff only, creates new announcement.

```json
{
    "priority": 0,
    "image": /* image file object */,
    "title": "Tytuł",
    "description": "Opis pod tytułem ogłoszenia.",
    "is_active": true
}
```

### PUT/PATCH /v1/announcements/{announcement_id}/

Request for staff only, updates created announcement.

```json
{
    "priority": 0,
    "image": /* image file object */,
    "title": "Tytuł",
    "description": "Opis pod tytułem ogłoszenia.",
    "is_active": true
}
```

### DELETE /v1/announcements/{announcement_id}/

Request for staff only, deletes announcement and its image.

### GET /v1/pages/

Request for all users, lists all pages.

```json
{
    "results": [
        {
            "id": 1,
            "slug": "about",
            "priority": 0,
            "title": "O nas",
            "content": "#### Zawartość strony.",
            "is_active": true
        }
    ]
}
```

### GET /v1/pages/{page_id}/

Request for all users, retrieves single page object.

```json
{
    "id": 1,
    "slug": "about",
    "priority": 0,
    "title": "O nas",
    "content": "#### Zawartość strony.",
    "is_active": true
}
```

### POST /v1/pages/

Request for staff only, creates new page object.

```json
{
    "slug": "about",
    "priority": 0,
    "title": "O nas",
    "content": "#### Zawartość strony.",
    "is_active": true
}
```

### PUT/PATCH /v1/pages/{page_id}/

Request for staff only, updates created page object.

```json
{
    "slug": "about",
    "priority": 0,
    "title": "O nas",
    "content": "#### Zawartość strony.",
    "is_active": true
}
```

### DELETE /v1/pages/{page_id}/

Request for staff only, deletes page instance from database.
